/**
 * ShadeX Engine Demo
 * (c) 2014 by Tristan Bauer
 */


/**
 * scene specific data
 */
(scene) {
	backgroundmusic = "data/audio/HeartOfCourage.wav";
	terrainheightmap = "data/texture/Terrain.png";
	maxlogotime = 15;

	(renderstages) {
		(fluidlightshaft) {
			time = 0;
			backgroundsound = 1;
			emitsmoke = "true";
			lightshaftintensity = 1;
			sunrotationspeed = 0;
			(smokeposition) {"3.5 -112.5 0"}
			(fireflyattractor) {"3.5 -112.5 25"}
		}
		(fluidlightshaft) {
			time = 37;
			emitsmoke = "false";
			(fireflyattractor) {"64.9582 -59.3703 25"}
		}
		(fluidlightshaft) {
			time = 44;
			emitsmoke = "true";
			(smokeposition) {"65.2727 -59.695 -3"}
		}
		(fluidlightshaft) {
			time = 57;
			emitsmoke = "false";
			(fireflyattractor) {"50 50 25"}
		}
		(lightshaft) {
			time = 64;
		}
		(lightshaft) {
			time = 90;
			sunrotationspeed = 0.13285;
		}
		(fluidlightshaft) {
			time = 101;
			emitsmoke = "true";
			(smokeposition) {"103.545 100.513 20.5308"}
		}
		(fluid) {
			time = 109;
			backgroundsound = 1;
		}
		(fluid) {
			time = 114;
			backgroundsound = 1;
			sunrotationspeed = 0;
		}
		(fluid) {
			time = 116.5;
			backgroundsound = 1;
		}
	}

	(camerapath) {
		(keypoint) {
			time = -1.58819e-023;
			(position) {"9.84995 -165.746 8.2887"}
			(view) {"-0.225507 0.133289 -0.965081"}
		}
		(keypoint) {
			time = 3.532;
			(position) {"4.8058 -156.17 6.60542"}
			(view) {"-0.406703 0.913732 -0.0496914"}
		}
		(keypoint) {
			time = 7.57699;
			(position) {"-19.1089 -148.743 6.00785"}
			(view) {"0.510482 0.852914 -0.117089"}
		}
		(keypoint) {
			time = 11;
			(position) {"-45.7804 -127.001 7.66911"}
			(view) {"0.923322 0.365114 -0.11903"}
		}
		(keypoint) {
			time = 24;
			(position) {"-26.107 -95.0335 7.87891"}
			(view) {"0.964078 -0.265499 -0.00796956"}
		}
		(keypoint) {
			time = 28.5;
			(position) {"9.13447 -88.5142 8.1545"}
			(view) {"-0.201078 -0.966765 0.157901"}
		}
		(keypoint) {
			time = 31.416;
			(position) {"27.2116 -98.5143 8.28651"}
			(view) {"-0.652211 -0.749099 0.116066"}
		}
		(keypoint) {
			time = 32.777;
			(position) {"31.7341 -109.906 8.28651"}
			(view) {"-0.966481 -0.167997 0.194145"}
		}
		(keypoint) {
			time = 35.228;
			(position) {"24.7898 -131.069 8.54142"}
			(view) {"-0.629631 0.776923 -0.00369426"}
		}
		(keypoint) {
			time = 38.114;
			(position) {"3.11261 -140.114 8.5429"}
			(view) {"-0.294146 0.955754 -0.00360082"}
		}
		(keypoint) {
			time = 41.0791;
			(position) {"1.70893 -133.851 8.51976"}
			(view) {"-0.131191 0.991351 -0.00359729"}
		}
		(keypoint) {
			time = 45.6189;
			(position) {"2.81635 -122.092 8.42883"}
			(view) {"-0.0107663 0.999718 -0.021191"}
		}
		(keypoint) {
			time = 49.3491;
			(position) {"28.1358 -101.816 6.209"}
			(view) {"0.843226 0.537532 -0.00541166"}
		}
		(keypoint) {
			time = 52;
			(position) {"31.8475 -103.723 16.1096"}
			(view) {"0.715932 0.668585 0.201087"}
		}
		(keypoint) {
			time = 54;
			(position) {"55.6293 -102.508 16.9782"}
			(view) {"0.143189 0.97522 -0.168649"}
		}
		(keypoint) {
			time = 56;
			(position) {"67.2955 -81.7584 14.9699"}
			(view) {"-0.114888 0.975739 -0.186371"}
		}
		(keypoint) {
			time = 60;
			(position) {"80.0148 -65.5757 10.865"}
			(view) {"-0.900564 0.318188 -0.296212"}
		}
		(keypoint) {
			time = 65.3105;
			(position) {"45.7801 -40.8739 14.7024"}
			(view) {"-0.804856 0.580343 0.128611"}
		}
		(keypoint) {
			time = 66.3165;
			(position) {"32.987 -40.799 16.1"}
			(view) {"-0.250389 0.966838 0.0460899"}
		}
		(keypoint) {
			time = 67.8235;
			(position) {"25.1945 -25.6088 15.1674"}
			(view) {"0.739039 0.665648 -0.103606"}
		}
		(keypoint) {
			time = 70.8213;
			(position) {"52.7643 -13.2016 13.7252"}
			(view) {"0.817022 0.575094 -0.0417357"}
		}
		(keypoint) {
			time = 73.2582;
			(position) {"70.1349 2.77737 12.1243"}
			(view) {"0.147456 0.98825 0.0573676"}
		}
		(keypoint) {
			time = 75.0;
			(position) {"68.8656 26.2954 16.5488"}
			(view) {"-0.0371598 0.973082 0.227445"}
		}
		(keypoint) {
			time = 76.4624;
			(position) {"68.3371 40.1341 17.8699"}
			(view) {"-0.0376863 0.98687 -0.157058"}
		}
		(keypoint) {
			time = 77.3045;
			(position) {"68.421 47.5823 13.8709"}
			(view) {"0.149673 0.533019 -0.866484"}
		}
		(keypoint) {
			time = 78.3668;
			(position) {"70.3082 54.0702 1.44014"}
			(view) {"-0.108653 0.569267 -0.778258"}
		}
		(keypoint) {
			time = 79.89;
			(position) {"66.7846 55.9949 1.25969"}
			(view) {"-0.4453 0.3552 -0.3356"}
		}
		(keypoint) {
			time = 81.1452;
			(position) {"67.3239 56.3718 0"}
			(view) {"-0.781982 0.141034 0.107136"}
		}
		(keypoint) {
			time = 82.8025;
			(position) {"73.418 54.9438 10.1209"}
			(view) {"-0.580364 0.139 -0.802407"}
		}
		(keypoint) {
			time = 83.8196;
			(position) {"75.7215 48.9623 16.3773"}
			(view) {"-0.535469 0.211947 -0.795539"}
		}
		(keypoint) {
			time = 85.3228;
			(position) {"71.969 39.2938 23.0828"}
			(view) {"-0.215721 0.622271 -0.752491"}
		}
		(keypoint) {
			time = 86.9499;
			(position) {"56.0003 39.2066 31.5533"}
			(view) {"0.458805 0.196188 -0.866607"}
		}
		(keypoint) {
			time = 89.7373;
			(position) {"54.7149 67.3661 44.8641"}
			(view) {"0.524184 -0.03529 -0.850874"}
		}
		(keypoint) {
			time = 92.0973;
			(position) {"53.0571 91.4173 49.9456"}
			(view) {"0.458499 -0.82146 -0.331967"}
		}
		(keypoint) {
			time = 95.1966;
			(position) {"61.2329 137.213 27.9168"}
			(view) {"0.217494 -0.910415 0.364943"}
		}
		(keypoint) {
			time = 98.2135;
			(position) {"78.5077 156.217 18.5522"}
			(view) {"-0.0580627 -0.963018 0.263125"}
		}
		(keypoint) {
			time = 102.93;
			(position) {"78.6916 166.685 14.9371"}
			(view) {"-0.02328 -0.991145 0.130731"}
		}
		(keypoint) {
			time = 107.5779;
			(position) {"73.4777 176.378 13.4739"}
			(view) {"-0.0232141 -0.988336 0.150507"}
		}
		(keypoint) {
			time = 117;
			(position) {"73.8998 194.355 10.9443"}
			(view) {"-0.0232672 -0.990596 0.134829"}
		}
	}

}
