(grass) {

	//generate grass: multiply geometry of grass mesh
	(shader) {
		id = "gen.grass.shader";
		(transformfeedback) {
			"overtices"
			"otexcoords"
			"oterrainNormal"
			"omidposition"
		}
		(vertex) {"
			#version 400 core
			
			in vec3 vertices;
			in vec2 texcoords;
			in vec3 terrainNormal;
			in vec4 midposition;
			out vec3 gvertices;
			out vec2 gtexcoords;
			out vec3 gterrainNormal;
			out vec4 gmidposition;

			void main() {
				gvertices = vertices;
				gtexcoords = texcoords;
				gterrainNormal = terrainNormal;
				gmidposition = midposition;
			}
			"
		}
		(geometry) {"
			#version 400 core

			layout(triangles) in;
			layout(triangle_strip,max_vertices = 27) out;

			in vec3 gvertices[];
			in vec2 gtexcoords[];
			in vec3 gterrainNormal[];
			in vec4 gmidposition[];
			out vec3 overtices;
			out vec2 otexcoords;
			out vec3 oterrainNormal;
			out vec4 omidposition;

			void main() {

				for(float gx = -1.0; gx <= 1.1 ; gx = gx + 1.0) {
					for(float gy = -1.0 ; gy <=1.1 ; gy = gy + 1.0) {
						for(int i=0 ; i<3 ; i++) {
							overtices = gvertices[i] + vec3(10.0,0.0,0.0) * gx + vec3(0.0,10.0,0.0) * gy;
							otexcoords = gtexcoords[i];
							oterrainNormal = gterrainNormal[i];
							omidposition = gmidposition[i];
							EmitVertex();
						}
						EndPrimitive();
					}
				}
			}
			"
		}
	}

	//generate geometry: create even more geometry, and adapt grass to terrain surface
	(shader) {
		id = "gen.grass2.shader";
		(transformfeedback) {
			"overtices"
			"otexcoords"
			"oterrainNormal"
			"omidposition"
		}
		(vertex) {"
			#version 400 core
			
			in vec3 vertices;
			in vec2 texcoords;
			out vec3 gvertices;
			out vec2 gtexcoords;

			uniform mat4 model;

			void main() {
				gvertices = (model * vec4(vertices,1)).xyz;
				gtexcoords = texcoords;
			}
			"
		}
		(geometry) {"
			#version 400 core

			layout(triangles) in;
			layout(triangle_strip,max_vertices = 27) out;

			in vec3 gvertices[];
			in vec2 gtexcoords[];
			out vec3 overtices;
			out vec2 otexcoords;
			out vec3 oterrainNormal;
			out vec4 omidposition;

			uniform sampler2D heightmap;
			uniform sampler2D heightmapNormals;
			uniform sampler2D terroverlay;
			uniform mat4 projheighmap;
			uniform float waterheight;

			void main() {

				for(float gx = -1.0; gx <= 1.1 ; gx = gx + 1.0) {
					for(float gy = -1.0 ; gy <=1.1 ; gy = gy + 1.0) {						
						vec4 p[3];
						vec2 heightmapCoord[3];
						bool usePrimitive = true;
						for(int i=0 ; i<3 ; i++) {
							p[i] = vec4(gvertices[i] + vec3(30.0,0.0,0.0) * gx + vec3(0.0,30.0,0.0) * gy,1.0);
							heightmapCoord[i] = (projheighmap * p[i]).xy;
							float isGrass = texture2D(terroverlay,clamp(heightmapCoord[i],0,1)).g;
							p[i].z += texture2D(heightmap,clamp(heightmapCoord[i],0,1)).r * 30.0;
							if(p[i].z < waterheight || isGrass == 0) {
								usePrimitive = false;
							}
						}
						if(usePrimitive) {
							vec4 hvec = 0.5*(p[1]-p[0]);
							vec4 mp = vec4((p[0]+hvec).xy,hvec.xy);
							for(int i=0 ; i<3 ; i++) {
								overtices = p[i].xyz;
								otexcoords = gtexcoords[i];
								oterrainNormal = normalize(texture2D(heightmapNormals,heightmapCoord[i]).xyz - vec3(0.5));
								omidposition = mp;
								EmitVertex();
							}
							EndPrimitive();
						}
					}
				}
			}
			"
		}
	}

	//render grass	
	(shader) {
		id = "mainobj.grass.shader";

		(vertex) {"
			#version 400 core

			in vec3 vertices;
			in vec2 texcoords;
			in vec3 terrainNormal;
			in vec4 midposition;
			out vec2 vtexcoords;
			out float vintensity;
			out vec3 vterrainNormal;
			out vec2 vformparameter;
			out float vatmosphere;
			out float vdistance;
			out vec4 vmidposition;
			out float vwindbending;

			uniform sampler2D waves;

			uniform mat4 viewprojection;
			uniform mat4 world2tex;
			uniform vec4 wave1;
			uniform vec4 wave2;
			uniform vec4 campos;
			uniform vec4 lightdir;
			uniform float ambient;

			void main() {
				vtexcoords = texcoords;
				float d = distance(vec3(campos.xy,0) , vec3(vertices.xy,0));
				vec4 wpoint = vec4(vertices - vec3(0,0,d / 40.0),1);
				vec4 ppoint = viewprojection * wpoint;
				gl_Position = wpoint;
				vformparameter = vec2(texcoords.x * 2.0 - 1.0,texcoords.y);
				vatmosphere = clamp((exp(ppoint.z / 80.0) - 1)/2.71828182,0,1);
				vdistance = ppoint.z;
				vmidposition = midposition;

				//terrain lighting calculation
				vec3 antilightdir = normalize(-lightdir.xyz);
				float diffuse = clamp( dot(antilightdir,terrainNormal) , 0 , 1);
				float intensity = max(diffuse,ambient);
				float microchanges = ambient + terrainNormal.z * 0.1;
				vintensity = max(intensity,microchanges);
				vterrainNormal = terrainNormal;

				//wind bending
				vec4 wavecoords = world2tex * vec4(vertices,1);
				vec3 w1 = texture2D(waves,wavecoords.xy + wave1.xy).xyz * 2.0;
				vec3 w2 = texture2D(waves,wavecoords.xy + wave2.xy).xyz * 2.0;
				w1 = 1.5 * (w1 - vec3(1));
				w2 = 1.5 * (w2 - vec3(1));
				vwindbending = dot( vec3(1,1,0) , w1+w2 );
			}
			"
		}

		(tessellation_control) {"
			#version 400 core
			#define ID gl_InvocationID

			in vec2 vtexcoords[];
			in float vintensity[];
			in vec3 vterrainNormal[];
			in float vatmosphere[];
			in float vdistance[];
			in vec2 vformparameter[];
			in vec4 vmidposition[];
			in float vwindbending[];
			out vec2 tctexcoords[];
			out float tcintensity[];
			out vec3 tcterrainNormal[];
			out vec3 tcgroundNormal[];
			out vec3 tctopNormal[];
			out float tcatmosphere[];
			out vec2 tcformparameter[];
			out vec4 tcmidposition[];
			out float tcwindbending[];

			layout(vertices=3) out;

			void main() {
				gl_out[ID].gl_Position = gl_in[ID].gl_Position;
				tctexcoords[ID] = vtexcoords[ID];
				tcintensity[ID] = vintensity[ID];
				tcterrainNormal[ID] = vterrainNormal[ID];
				tcatmosphere[ID] = vatmosphere[ID];
				tcformparameter[ID] = vformparameter[ID];
				tcmidposition[ID] = vmidposition[ID];
				tcwindbending[ID] = vwindbending[ID];

				//calculate normal at the top and at the ground of the grass blade
				vec3 p0 = gl_in[0].gl_Position.xyz;
				vec3 p1 = gl_in[1].gl_Position.xyz;
				vec3 p2 = gl_in[2].gl_Position.xyz;
				vec3 v01 = p1 - p0;
				vec3 v02 = p2 - p1;
				vec3 n = normalize(cross(v01,v02));
				tcgroundNormal[ID] = n;
				tctopNormal[ID] = n + vec3(0,0,-tcwindbending[0]);

				gl_TessLevelOuter[0] = clamp(10.0 * (-vdistance[ID] + 3.0)*0.3333 , 1.0 , 10.0 );
				gl_TessLevelOuter[1] = clamp(10.0 * (-vdistance[ID] + 3.0)*0.3333 , 1.0 , 10.0 );
				gl_TessLevelOuter[2] = clamp(10.0 * (-vdistance[ID] + 3.0)*0.3333 , 1.0 , 10.0 );
				gl_TessLevelInner[0] = clamp(2.0 * (-vdistance[ID] + 3.0)*0.3333 , 1.0 , 2.0 );
			}
			"
		}

		(tessellation_evaluation) {"
			#version 400 core

			in vec2 tctexcoords[];
			in float tcintensity[];
			in vec3 tcterrainNormal[];
			in vec3 tcgroundNormal[];
			in vec3 tctopNormal[];
			in float tcatmosphere[];
			in vec2 tcformparameter[];
			in vec4 tcmidposition[];
			in float tcwindbending[];
			out vec2 ttexcoords;
			out float tintensity;
			out vec3 tterrainNormal;
			out vec3 tgroundNormal;
			out vec3 ttopNormal;
			out vec3 tview;
			out float tatmosphere;
			out float theight;
			out float worldz;
			out vec4 worldpos;

			uniform sampler2D grassform;
			uniform mat4 viewprojection;
			uniform vec4 position;

			layout(triangles,equal_spacing) in;

			void main() {
				float w[3] = float[](gl_TessCoord.x,gl_TessCoord.y,gl_TessCoord.z);
				vec2 s_texcoords = vec2(0.0);
				vec3 s_terrainNormal = vec3(0.0);
				float s_atmosphere = 0.0;
				vec4 s_position = vec4(0.0);
				vec2 s_formparameter = vec2(0.0);
				for(int i=0 ; i<3 ; i++) {
					s_texcoords += tctexcoords[i] * w[i];
					s_terrainNormal += tcterrainNormal[i] * w[i];
					s_atmosphere += tcatmosphere[i] * w[i];
					s_position += gl_in[i].gl_Position * w[i];
					s_formparameter += tcformparameter[i] * w[i];
				}
				ttexcoords = s_texcoords;
				tgroundNormal = tcgroundNormal[0];
				ttopNormal = tctopNormal[0];
				tintensity = tcintensity[0];
				tterrainNormal = s_terrainNormal;
				tatmosphere = s_atmosphere;
				theight = s_formparameter.y;

				//calculate grassdisplacement giving grassblade its form
				float grassdisplacement = texture2D(grassform,vec2(s_formparameter.y*0.888888 + 0.05555,0.0)).r * 0.35; //displace grass vertices to make it look like a blade

				//calculate grass blade's geometry
				worldpos = vec4( tcmidposition[0].xy + tcmidposition[0].zw * s_formparameter.x * (1 / clamp(1.0 - s_formparameter.y , 0.001 , 1.0)) * grassdisplacement , s_position.zw);
				tview = position.xyz - worldpos.xyz;
				worldpos += vec4(tcwindbending[0] * s_texcoords.y * s_texcoords.y * tgroundNormal,0);
				vec4 ppoint = viewprojection * worldpos;
				worldz = ppoint.z;
				gl_Position = ppoint;
			}
			"
		}

		(fragment) {"
			#version 400 core

			in vec2 ttexcoords;
			in float tintensity;
			in vec3 tterrainNormal;
			in vec3 tgroundNormal;
			in vec3 ttopNormal;
			in vec3 tview;
			in float tatmosphere;
			in float theight;
			in float worldz;
			in vec4 worldpos;
			out vec4 frag1;
			out vec4 dmap;

			uniform vec4 lightdir;
			uniform vec4 skygroundcolor;
			uniform  float maxbrightness;

			void main() {
				//calculate intensity caused by local structure of grass blade
				vec3 normal = tgroundNormal * (1.0 - ttexcoords.y) + ttopNormal * ttexcoords.y;
				float diffuse = abs(dot(normal,-lightdir.xyz)) * 0.5;
				vec3 nview = normalize(tview);
				vec3 reflected = normal * 2.0 * dot(normal,-lightdir.xyz) - (-lightdir.xyz);
				float spec = clamp(dot(reflected,nview),0,1);
				spec = pow(spec,128.0 * 0.2);

				float intensity = tintensity + max(spec,diffuse);

				frag1 = maxbrightness * intensity * vec4(0.3569,0.5843,0.0118,1) * 1.5 * (log(theight+1.0)*1.4427 * 0.4 + 0.6) * (1.0 - tatmosphere) + /*vec4(0.3451,0.4196,0.4666,1)*/skygroundcolor * tatmosphere;
				dmap = vec4(worldz,worldpos.xyz);
			}
			"
		}
	
	}

}