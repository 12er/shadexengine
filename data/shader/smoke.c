(smoke) {

	//initialization shader for smoke volumes
	(shader) {
		id = "fluiddynamic.init.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec2 texcoords;
			out vec2 vtc;

			void main() {
				gl_Position = vec4(vertices,1);
				vtc = texcoords;
			}
		"
		}

		(fragment) {"
			#version 400 core
			in vec2 vtc;
			out vec4 destination;
			
			uniform float depthcoordinate;

			void main() {
				destination = vec4(0.0);
			}
		"
		}
	}

	//copy shader for volumes
	(shader) {
		id = "fluiddynamic.copy.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec2 texcoords;
			out vec2 vtc;

			void main() {
				gl_Position = vec4(vertices,1);
				vtc = texcoords;
			}
		"
		}

		(fragment) {"
			#version 400 core
			in vec2 vtc;
			out vec4 destination;
			
			uniform float depthcoordinate;
			uniform sampler3D source;

			void main() {
				destination = texture(source,vec3(vtc,depthcoordinate));
			}
		"
		}
	}

	//advection shader solving
	//\frac{\partial}{\partial t} u = -(u \cdot \nabla)u
	//of Navier-Stokes equations
	//introduces body forces and advects the visible medium
	(shader) {
		id = "fluiddynamic.advect.shader";

		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec2 texcoords;
			out vec2 vtc;

			void main() {
				gl_Position = vec4(vertices,1);
				vtc = texcoords;
			}
		"
		}

		(fragment) {"
			#version 400 core
			in vec2 vtc;
			out vec4 velpress2;
			out vec4 medium2;

			uniform float dx;
			uniform float deltatime;
			uniform float emitsmoke;
			uniform float decay;
			uniform float depthcoordinate;
			uniform vec4 randacceleration;
			uniform vec4 p1;
			uniform vec4 p2;
			uniform vec4 p3;
			uniform vec4 p4;
			uniform vec4 p5;
			uniform vec4 v1;
			uniform vec4 v2;
			uniform vec4 v3;
			uniform vec4 v4;
			uniform vec4 v5;
			uniform sampler3D velpress;
			uniform sampler3D medium;

			void main() {
				vec3 vp = vec3(vtc,depthcoordinate);
				vec4 current = texture(velpress,vp);

				//use this pass too to inject density and change the speed
				float source = 0.0;
				vec3 acceleration = vec3(0.0);
				if(emitsmoke > 0.5 && vp.x < 0.55 && vp.x > 0.45 && vp.y < 0.55 && vp.y > 0.45 && vp.z < 0.1 && vp.z > 0.01) {
					source = 1.0;
					acceleration = randacceleration.xyz;
				}
				
				//predictor step
				vec3 vel = current.xyz * 2.0 * dx * deltatime;
				vec3 predictorp = vp - vel;
				vec4 predictor = texture(velpress,predictorp);
				vec3 velpredictor = predictor.xyz * 2.0 * dx * deltatime;
				
				//corrector step
				vec3 correctorp = predictorp + velpredictor;
				vec4 corrector = texture(velpress,correctorp);

				//calculate ranges
				vec4 vp_min = min( current , min( predictor , corrector ) );
				vec4 vp_max = max( current , max( predictor , corrector ) );

				//maccormack advection scheme not unconditionally stable -> clamp to avoid explosion
				vec4 advected = clamp(
					predictor + 0.5 * (current - corrector)
					, vp_min , vp_max);

				//computer body force caused by smoke interactors
				float i1 = 1.0 - clamp(length(p1.xyz-vp)*10.0,0,1);
				float i2 = 1.0 - clamp(length(p2.xyz-vp)*10.0,0,1);
				float i3 = 1.0 - clamp(length(p3.xyz-vp)*10.0,0,1);
				float i4 = 1.0 - clamp(length(p4.xyz-vp)*10.0,0,1);
				float i5 = 1.0 - clamp(length(p5.xyz-vp)*10.0,0,1);
				//apply body force caused by smoke emitter
				advected.xyz += (acceleration + i1*v1.xyz + i2*v2.xyz + i3*v3.xyz + i4*v4.xyz + i5*v5.xyz) * deltatime;
				velpress2 = advected;

				//for advection of other physical properties than pressure and velocity
				//use simple euler forward
				//the first three components are used as normal vectors for lighting calculations
				//the forth component represents density
				vec3 delx = vec3(dx,0,0);
				vec3 dely = vec3(0,dx,0);
				vec3 delz = vec3(0,0,dx);
				vec4 currm = texture(medium,predictorp);
				
				//calculate gradient, which will be used as
				//a normal for lighting calculations
				vec3 gradient = vec3(
					texture(medium,predictorp + delx).a - currm.a ,
					texture(medium,predictorp + dely).a - currm.a ,
					texture(medium,predictorp + delz).a - currm.a
					);
				if(length(gradient) != 0) {
					gradient = normalize(gradient);
				}

				//the smoke dynamics look a lot more interesting in the demo
				//without the free slip condition on the boundaries of the box
				//but smoke going through boundaries sucks a little bit
				//hence some additional decay on the smoke box's boundaries is introduced
				float walldecay = 100.0 * (1.0 -
					0.5 * (1.0 + sign(vp.x - 0.01)) *
					0.5 * (1.0 + sign(vp.y - 0.01)) *
					0.5 * (1.0 + sign(vp.z - 0.01)) *
					0.5 * (1.0 + sign(0.99 - vp.x)) *
					0.5 * (1.0 + sign(0.99 - vp.y)) *
					0.5 * (1.0 + sign(0.99 - vp.z))
					);

				medium2 = vec4(-gradient,max(currm.a - (decay + walldecay) * deltatime,source));
			}
		
		"
		}
	}

	//solving the discretized Poisson equation
	//\nabla \cdot u^{t} = \nabla^2 q
	//q := \frac{\Delta t \, p}{\rho}
	//using Jacobi fixed point iterations
	(shader) {
		id = "fluiddynamic.poisson.shader";

		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec2 texcoords;
			out vec2 vtc;

			void main() {
				gl_Position = vec4(vertices,1);
				vtc = texcoords;
			}
		"
		}

		(fragment) {"
			#version 400 core
			in vec2 vtc;
			out vec4 velpress2;

			uniform float dx;
			uniform float depthcoordinate;
			uniform sampler3D velpress;

			void main() {
				vec3 vp = vec3(vtc,depthcoordinate);
				vec3 vel = texture(velpress,vp).rgb;
				vec3 delx = vec3(dx,0,0);
				vec3 dely = vec3(0,dx,0);
				vec3 delz = vec3(0,0,dx);

				float divergence =
					texture(velpress,vp + delx).x - texture(velpress,vp - delx).x +
					texture(velpress,vp + dely).y - texture(velpress,vp - dely).y +
					texture(velpress,vp + delz).z - texture(velpress,vp - delz).z
					;

				float oldpressure =
					texture(velpress,vp + delx).a + texture(velpress,vp - delx).a +
					texture(velpress,vp + dely).a + texture(velpress,vp - dely).a +
					texture(velpress,vp + delz).a + texture(velpress,vp - delz).a
					;

				velpress2 = vec4(vel,(oldpressure - divergence)*0.16666666);
			}
		
		"
		}
	}

	//projection shader solving
	//\frac{\partial}{\partial t} u = -\frac{1}{\rho} \nabla p
	//of Navier-Stokes equations
	//this step projects the velocity field onto its divergence free component (obeying constraint \nabla \cdot u = 0)
	//by making use of the solution q, q:=\frac{\Delta t \, p}{\rho} of the poisson equation
	//\nabla \cdot u^{t} = \nabla^2 q
	(shader) {
		id = "fluiddynamic.project.shader";

		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec2 texcoords;
			out vec2 vtc;

			void main() {
				gl_Position = vec4(vertices,1);
				vtc = texcoords;
			}
		"
		}

		(fragment) {"
			#version 400 core
			in vec2 vtc;
			out vec4 velpress2;

			uniform float dx;
			uniform float depthcoordinate;
			uniform sampler3D velpress;

			void main() {
				vec3 vp = vec3(vtc,depthcoordinate);
				vec4 velp = texture(velpress,vp);
				vec3 delx = vec3(dx,0,0);
				vec3 dely = vec3(0,dx,0);
				vec3 delz = vec3(0,0,dx);

				vec3 gradient = vec3(
					texture(velpress,vp + delx).a - texture(velpress,vp - delx).a ,
					texture(velpress,vp + dely).a - texture(velpress,vp - dely).a ,
					texture(velpress,vp + delz).a - texture(velpress,vp - delz).a
					);

				velpress2 = vec4(velp.xyz - gradient,velp.a);
			}
		
		"
		}
	}

	//identify smoke region, and compute start positions in texture space for raycasting
	(shader) {
		id = "identifycastbox.shader";
		(vertex) {"
			#version 400 core
			
			in vec3 vertices;
			out float raycaststart;
			out vec3 texeye;
			out vec4 worldpos;

			uniform vec4 eye;
			uniform mat4 viewprojection;
			uniform mat4 model;
			uniform mat4 box2tex;
			uniform mat4 world2tex;

			void main() {
				worldpos = model * vec4(vertices,1);
				gl_Position = viewprojection * worldpos;
				texeye = (world2tex * eye).xyz;
			}
			"
		}
		(fragment) {"
			#version 400 core
			
			in float raycaststart;
			in vec3 texeye;
			in vec4 worldpos;
			out vec4 frag1;

			uniform mat4 world2tex;

			void main() {
				vec4 texpos = world2tex * worldpos;
				texpos = texpos / texpos.w;
				frag1 = vec4(length(texpos.xyz - texeye),0,0,0);
			}
			"
		}

	}

	//raycast smoke volume, render integration into scene into frag1,
	//and render smoke mask into dmap
	(shader) {
		id = "raycast.shader";
		(vertex) {"
			#version 400 core
			
			in vec3 vertices;
			in vec2 texcoords;
			out vec2 vtexcoords;
			out vec3 veye;
			out vec3 texviewvector;
			out float unityworldlength;
			out vec3 projpoint;
			out float vsmokeatmosphere;

			uniform mat4 world2tex;
			uniform mat4 viewprojection;
			uniform vec4 eye;
			uniform vec4 viewvector;
			uniform vec4 smokeboxpos;

			void main() {
				gl_Position = vec4(vertices,1);
				vtexcoords = texcoords;
				veye = (world2tex * eye).xyz;
				projpoint = vec3(vertices.xy,-1);

				//estimate depth step in world space
				vec3 nviewvector = normalize(viewvector.xyz);
				vec3 ntexviewvector = (world2tex * vec4(nviewvector,0)).xyz;
				unityworldlength = 1.0 / length(ntexviewvector);
				texviewvector = normalize(ntexviewvector);

				//estimate smoke z-buffer distance
				vec4 proj = viewprojection * smokeboxpos;
				vsmokeatmosphere = clamp((exp(proj.z / 80.0) - 1)/2.71828182,0,1);;
			}
			"
		}
		(fragment) {"
			#version 400 core
			
			in vec2 vtexcoords;
			in vec3 veye;
			in vec3 texviewvector;
			in float unityworldlength;
			in vec3 projpoint;
			in float vsmokeatmosphere;
			out vec4 frag1;
			out vec4 dmapout;

			uniform sampler2D raycaststencil;
			uniform sampler2D dmap;
			uniform sampler2D backgroundscene;
			uniform sampler3D medium;
			uniform mat4 proj2world;
			uniform mat4 world2tex;
			uniform float insidecastbox;
			uniform float maxbrightness;
			uniform vec4 lightdir;
			uniform vec4 skygroundcolor;

			void main() {
				vec4 rcs = texture(raycaststencil,vtexcoords);
				vec4 backgroundcolor = texture2D(backgroundscene,vtexcoords);
				
				if(rcs.a > 0) {
					frag1 = backgroundcolor;
					dmapout = vec4(1);
					return;
				}
				vec4 dvalue = texture2D(dmap,vtexcoords);
				vec4 scenedepthpoint = (world2tex * vec4(dvalue.gba,1)) - vec4(veye,0);
				float scenedepth = length(scenedepthpoint.xyz);
				vec4 planeposw = world2tex * proj2world * vec4(projpoint,1);
				vec3 planepos = planeposw.xyz / planeposw.w;
				vec3 startpos = planepos;
				vec3 rcvec = normalize(planepos - veye);
				if(insidecastbox < 0.5) {
					startpos = rcvec * rcs.x + veye;
				}
				rcvec = rcvec * 0.006666666;

				//depth estimation parameters
				float depthstep = length(rcvec);
				float depthstart = length(startpos - veye);

				vec3 castpos = startpos;
				float currentdepth = depthstart;
				float density = 0.0;
				float absorbtion = 0.0;

				for(int i=0 ; i<150 ; i++) {
					if(currentdepth > scenedepth) {
						//raycaster entered part of the scene hiding the smoke
						//hence no further steps are taken
						break;
					}

					vec4 currentparticle = texture(medium,castpos);
					float currentdensity = currentparticle.a;
					float currentabsorbtion = currentdensity * 0.5;
					vec3 currentnormal = currentparticle.xyz;

					//lighting
					float diffuse = clamp( ( dot(currentnormal , -lightdir.xyz) + 1.0) * 0.5 , 0.3 , 1.0);
					currentdensity = currentdensity * diffuse;

					density += (1-absorbtion) * currentdensity;
					absorbtion += clamp( (1-absorbtion) * currentabsorbtion , 0 , 1);

					if(absorbtion >= 1.0) {
						//everything gets absorbed, continuing this loop makes no sense
						absorbtion = 1.0;
						break;
					}
					castpos = castpos + rcvec;
					currentdepth = currentdepth + depthstep;
					if(castpos.x < 0.0 || castpos.x > 1.0 || castpos.y < 0.0 || castpos.y > 1.0 || castpos.z < 0.0 || castpos.z > 1.0) {
						//ray must not leave cube
						break;
					}
				}

				frag1 = vec4((vec3(0.3412, 0.4118, 0.9765) * density * maxbrightness * (1-vsmokeatmosphere) + skygroundcolor.rgb * vsmokeatmosphere) * absorbtion + backgroundcolor.rgb * (1-absorbtion) ,1);
				dmapout = vec4((1-absorbtion));
			}
			"
		}

	}

}