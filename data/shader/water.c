(water) {

	(shader) {
		id = "mainobj.water.shader";

		(vertex) {"
			#version 400 core

			in vec3 vertices;
			out vec4 projected_refraction;
			out vec4 projected_reflection;
			out vec4 world;
			out vec2 wavecoords;
			out float atmosphere;
			out float projz;

			uniform mat4 model;
			uniform mat4 viewprojection;
			uniform mat4 water_vp;
			uniform mat4 world2tex;

			void main() {
				world = model * vec4(vertices,1);
				wavecoords =  (world2tex * world).xy;
				projected_refraction = viewprojection * world;
				projected_reflection = water_vp * world;
				atmosphere = clamp((exp(projected_reflection.z / 80.0) - 1)/2.71828182,0,1);
				projz = projected_reflection.z;
				gl_Position = projected_refraction;
			}
			"
		}

		(fragment) {"
			#version 400 core

			in vec4 projected_refraction;
			in vec4 projected_reflection;
			in vec4 world;
			in vec2 wavecoords;
			in float atmosphere;
			in float projz;
			out vec4 frag1;
			out vec4 dmap;

			uniform sampler2D refractionmap;
			uniform sampler2D reflectionmap;
			uniform sampler2D waves;

			uniform vec4 skygroundcolor;
			uniform vec4 wave1;
			uniform vec4 wave2;
			uniform vec4 position;

			void main() {
				vec3 w1 = texture2D(waves,wavecoords + wave1.xy).xyz;
				vec3 w2 = texture2D(waves,wavecoords + wave2.xy).xyz;
				w1 = (w1 - vec3(0.5)) * 2.0;
				w2 = (w2 - vec3(0.5)) * 2.0;
				vec3 normal = (w1 + w2) / 2.0;
				normal = normalize(normal);

				//clipping correction factor
				vec2 coords_refract = projected_refraction.xy / projected_refraction.w;
				coords_refract = (coords_refract + vec2(1,1) ) * 0.5;
				float waterDepth = texture2D(refractionmap,coords_refract).a - projz;
				float clippingFactor = 
					1.0 - max(
						clamp(1 - waterDepth , 0 , 1), max(
						clamp(1.0 - coords_refract.x*60.0 , 0 , 1), max(
						clamp(1.0 - coords_refract.y*60.0 , 0 , 1), max(
						clamp(coords_refract.x * 60.0 - 59.0 , 0 , 1),
						clamp(coords_refract.y * 60.0 - 59.0 , 0 , 1)
					))));

				//calculate refracted fragment by projecting it onto the water plane
				vec4 frag_refract = texture2D(refractionmap,coords_refract + normal.xy * 0.1 * clippingFactor);

				//calculate reflection fragment by projecting it onto the water plane
				vec2 coords_reflect = projected_reflection.xy / projected_reflection.w;
				coords_reflect = (coords_reflect + vec2(1,1)) * 0.5;
				vec4 frag_reflect = texture2D(reflectionmap,coords_reflect + normal.xy * 0.1 * clippingFactor);

				//interpolate refraction and reflection fragment depending on the angle between the outgoing
				//light ray and the water normal
				vec3 viewdir = normalize(world.xyz - position.xyz);
				float doRefraction = abs(dot(normal,viewdir));
				vec4 purefrag = frag_reflect * (1 - doRefraction) + vec4(frag_refract.xyz,1) * doRefraction;
				
				//calculate final fragment
				frag1 = purefrag * (1 - atmosphere) + skygroundcolor * atmosphere;
				dmap = vec4(projz,world.xyz);
			}
			"
		}

	}

}