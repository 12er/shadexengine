(terrain) {

	//precompute terrain normals
	(shader) {
		id = "terrnormalscene.shader";

		(vertex) {"
			#version 400 core

			in vec3 vertices;
			in vec2 texcoords;
			out vec2 coords;

			void main() {
				coords = texcoords;
				gl_Position = vec4(vertices,1);
			}
			"
		}

		(fragment) {"
			#version 400 core

			in vec2 coords;
			out vec4 frag1;

			uniform sampler2D heightmap;

			float getDCoord(float maxheight, float terrainwidth, float delta, vec2 direction) {
				float h_w_ratio = maxheight / terrainwidth;
				float diff = texture2D(heightmap,coords + direction * delta).x - texture2D(heightmap,coords).x;
				return h_w_ratio * diff / delta;
			}

			void main() {
				//terrain's max height is supposed
				//to have height 1/10 of terrainwidth
				
				float dx = getDCoord(30.0, 300.0, 0.008, vec2(1,0));
				float dy = getDCoord(30.0, 300.0, 0.008, vec2(0,1));
				
				vec3 normal = cross( vec3(1,0,dx) , vec3(0,1,dy) );
				normal = normalize(normal);

				vec3 packing = normal * 0.5 + vec3(0.5);
				frag1 = vec4(packing,1);
			}
			"
		}

	}

	//calculate terrain overlay
	(shader) {
		id = "calcterroverlay.shader";

		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec2 texcoords;
			out vec2 coords;
			
			void main() {
				gl_Position = vec4(vertices,1);
				coords = texcoords;
			}
			"
		}

		(fragment) {"
			#version 400 core
			in vec2 coords;
			out vec4 terroverlay;

			uniform sampler2D heightmap;
			uniform sampler2D normalmap;
			uniform float waterheight;
			
			void main() {
				float grassThreshold = 0.7;
				float grassDelta = 0.05;
				float sandDelta = 0.5;

				float height = texture2D(heightmap,coords).r * 30.0;
				vec3 normal = normalize( texture2D(normalmap,coords).xyz - vec3(0.5) );
				float g = dot(normal,vec3(0,0,1));

				float isGrass = 0.0;
				if(g < grassThreshold) {
					isGrass = 0;
				} else if(g < grassThreshold + grassDelta) {
					isGrass = (g - grassThreshold) / grassDelta;
				} else {
					isGrass = 1;
				}
				float isSand = 0.0;
				if(height < waterheight) {
					isSand = 1;
				} else if(height < waterheight + sandDelta) {
					isSand = (waterheight + sandDelta - height) / sandDelta;
				} else {
					isSand = 0;
				}
				isGrass = (1-isSand) * isGrass;
				terroverlay = vec4(isSand,isGrass,0,0);
			}
			"
		}

	}

	//generate terrain geometry
	(shader) {
		id = "terrain.generategeometry.shader";

		(transformfeedback) {
			"overtices"
			"otexcoords"
			"onormals"
		}
		(vertex) {"
			#version 400 core
			
			in vec3 vertices;
			in vec2 texcoords;
			in vec3 normals;
			out vec3 overtices;
			out vec2 otexcoords;
			out vec3 onormals;

			uniform sampler2D heightmap;
			uniform sampler2D normalmap;
			uniform mat4 model;
			uniform mat4 projheightmap;

			void main() {
				vec4 vworldpoint = model * vec4(vertices,1);
				otexcoords = clamp( (projheightmap * vworldpoint).xy , 0 , 1) 
					+ clamp(texcoords , -2 , -1); //without texcoords in a term assigned (for instance: multiplication of texcoords with 0 -> optimization by constant folding in compiler eliminates term texcoords -> app crash) to otexcoords OpenGL crashes Oo ... I don't understand why
				float height = texture2D(heightmap,otexcoords).x;
				vworldpoint.z = height * 30.0;
				overtices = vworldpoint.xyz;

				onormals = normalize(texture2D(normalmap,otexcoords).xyz - vec3(0.5))
					+ clamp(normals,0.0,0.000001); //without normals in a term assigned (for instance: multiplication of normals with 0 -> optimization by constant folding in compiler eliminates term texcoords -> app crash) to otexcoords OpenGL crashes Oo ... I don't understand why
			}
			"
		}

	}

	(shader) {
		id = "mainobj.terrain.shader";

		(vertex) {"
			#version 400 core

			in vec3 vertices;
			in vec2 texcoords;
			in vec3 normals;
			out vec2 vterrtex;
			out float vworldz;
			out vec4 vworldpoint;
			out vec3 vapproxNormal;
			out vec3 vapproxTangentX;
			out vec3 vapproxTangentY;

			uniform sampler2D heightmap;
			uniform sampler2D normalmap;
			uniform mat4 model;
			uniform mat4 projheightmap;

			void main() {
				vworldpoint = vec4(vertices,1);
				vterrtex = texcoords;
				vworldz = vertices.z;

				//calculate tangent space coordinate axes of rock areas in world space coordinates
				vapproxNormal = normals;
				vapproxTangentX = normalize( cross(vec3(0,0,1) , vapproxNormal) );
				vapproxTangentY = cross(vapproxNormal , vapproxTangentX);
			}
			"
		}

		(tessellation_control) {"
			#version 400 core
			#define ID gl_InvocationID

			in vec2 vterrtex[];
			in float vworldz[];
			in vec4 vworldpoint[];
			in vec3 vapproxNormal[];
			in vec3 vapproxTangentX[];
			in vec3 vapproxTangentY[];
			out vec2 tcterrtex[];
			out float tcworldz[];
			out vec4 tcworldpoint[];
			out float tcisRock[];
			out vec3 tcapproxNormal[];
			out vec3 tcapproxTangentX[];
			out vec3 tcapproxTangentY[];

			uniform sampler2D normalmap;
			uniform sampler2D terroverlay;
			uniform mat4 maincam_vp;
			uniform float isRefractionscene;
			uniform float isReflectioscene;
			uniform float waterheight;

			layout(vertices=3) out;

			void main() {
				tcterrtex[ID] = vterrtex[ID];
				tcworldz[ID] = vworldz[ID];
				tcworldpoint[ID] = vworldpoint[ID];
				tcapproxNormal[ID] = vapproxNormal[ID];
				tcapproxTangentX[ID] = vapproxTangentX[ID];
				tcapproxTangentY[ID] = vapproxTangentY[ID];

				//calculate level of detail for each vertex of the face
				//to obtain level of detail for each edge
				float outer_detail[3];
				float visRock[3];
				float avg_detail = 0.0;
				for(int i=0 ; i<3 ; i++) {
					//get distance
					vec4 transformed = maincam_vp * vworldpoint[i];
					float vdistance = transformed.z;

					//calculate ground type
					//only rock requires tessellation
					vec3 normal = normalize(texture2D(normalmap,vterrtex[i]).xyz - vec3(0.5));
					vec4 floortype = texture2D(terroverlay,vterrtex[i]);
					float isGrass = floortype.g;
					float isSand = floortype.r;
					float isRock = (1 - isGrass) * (1 - isSand);
					visRock[i] = isRock;

					//compute lod
					float secondaryPassDetail = clamp( (vworldz[i] - waterheight - 2.9) , 0 , 1);
					outer_detail[i] = clamp((1-isRefractionscene*secondaryPassDetail) * (1-isReflectioscene*secondaryPassDetail) * isRock * (5.0 - abs(vdistance) * 0.1) , 1.0 , 5.0);
					avg_detail += outer_detail[i] * 0.33333;
				}
				tcisRock[ID] = visRock[ID];

				gl_TessLevelOuter[0] = 0.5 * (outer_detail[1] + outer_detail[2]);
				gl_TessLevelOuter[1] = 0.5 * (outer_detail[0] + outer_detail[2]);
				gl_TessLevelOuter[2] = 0.5 * (outer_detail[0] + outer_detail[1]);
				gl_TessLevelInner[0] = clamp(avg_detail , 1.0 , 5.0 );
			}
			"
		}

		(tessellation_evaluation) {"
			#version 400 core
			
			in vec2 tcterrtex[];
			in float tcworldz[];
			in vec4 tcworldpoint[];
			in float tcisRock[];
			in vec3 tcapproxNormal[];
			in vec3 tcapproxTangentX[];
			in vec3 tcapproxTangentY[];
			out vec2 terrtex;
			out float worldz;
			out float projz;
			out float atmosphere;
			out vec3 approxTangentX;
			out vec3 approxTangentY;

			uniform sampler2D rock_heightmap;
			uniform mat4 viewprojection;

			layout(triangles,equal_spacing) in;

			void main() {
				float w[3] = float[](gl_TessCoord.x,gl_TessCoord.y,gl_TessCoord.z);

				vec2 s_terrtex = vec2(0.0);
				float s_worldz = 0.0;
				vec4 s_worldpoint = vec4(0.0);
				float s_isRock = 0.0;
				vec3 s_approxNormal = vec3(0.0);
				vec3 s_approxTangentX = vec3(0.0);
				vec3 s_approxTangentY = vec3(0.0);
				for(int i=0 ; i<3 ; i++) {
					s_terrtex += tcterrtex[i] * w[i];
					s_worldz += tcworldz[i] * w[i];
					s_worldpoint += tcworldpoint[i] * w[i];
					s_isRock += tcisRock[i] * w[i];
					s_approxNormal += tcapproxNormal[i] * w[i];
					s_approxTangentX += tcapproxTangentX[i] * w[i];
					s_approxTangentY += tcapproxTangentY[i] * w[i];
				}
				terrtex = s_terrtex;
				approxTangentX = s_approxTangentX;
				approxTangentY = s_approxTangentY;

				//tessellate rocks
				float rockDisplacement = 0.0;
				if(s_isRock > 0) {
					vec3 texnormal = abs(normalize(vec3(s_approxNormal.xy,0)));
					float rz = s_worldz*0.1;
					float rnx = texture2D(rock_heightmap,vec2(s_terrtex.x*25.0,rz)).r;
					float rny = texture2D(rock_heightmap,vec2(s_terrtex.y*25.0,rz)).r;
					rockDisplacement = (rnx * texnormal.y + rny * texnormal.x) * s_isRock;
				}

				s_worldpoint += vec4(s_approxNormal,0) * rockDisplacement;
				s_worldpoint.w = 1;
				vec4 ppoint = viewprojection * s_worldpoint;
				projz = ppoint.z;
				worldz = s_worldpoint.z;
				atmosphere = clamp((exp(projz / 80.0) - 1)/2.71828182,0,1);
				gl_Position = ppoint;
			}
			"
		}

		(fragment) {"
			#version 400 core

			in vec2 terrtex;
			in float worldz;
			in float projz;
			in float atmosphere;
			in vec3 approxTangentX;
			in vec3 approxTangentY;
			out vec4 frag1;

			uniform sampler2D heightmap;
			uniform sampler2D normalmap;
			uniform sampler2D finemap;
			uniform sampler2D terroverlay;
			uniform sampler2D grass;
			uniform sampler2D rock;
			uniform sampler2D rock_normalmap;

			uniform vec4 lightdir;
			uniform vec4 view;
			uniform vec4 skygroundcolor;
			uniform float ambient;
			uniform float maxbrightness;
			uniform float isRefractionscene;
			uniform float isReflectioscene;
			uniform float waterheight;
			uniform float abovewater;

			void main() {
				if(isRefractionscene > 0.5 || isReflectioscene > 0.5) {
					bool cancelFrag = (worldz > waterheight);
					if(abovewater < 0.5) {
						cancelFrag = !cancelFrag;
					}
					if(isReflectioscene > 0.5) {
						cancelFrag = !cancelFrag;
					}
					if(cancelFrag) {
						discard;
					}
				}

				vec3 normal = normalize(texture2D(normalmap,terrtex).xyz - vec3(0.5));
				vec3 antilightdir = normalize(-lightdir.xyz);

				//get floortype and color
				vec4 floortype = texture2D(terroverlay,terrtex);
				float isGrass = floortype.g;
				float isSand = floortype.r;
				vec4 grassColor = vec4(1.0);
				vec4 rockColor = vec4(1.0);
				vec4 sandColor = vec4(1.0);
				if(isGrass > 0) {
					//it's grass
					grassColor = texture2D(grass,terrtex);
				}
				if((1-isGrass) * (1-isSand) > 0) {
					//it's at least partially rock
					
					//get rock texture color
					vec3 texnormal = abs(normalize(vec3(normal.xy,0)));
					float rz = worldz*0.1;
					vec2 rtx = vec2(terrtex.x*25.0,rz);
					vec2 rty = vec2(terrtex.y*25.0,rz);
					vec4 rcolx = texture2D(rock,rtx);
					vec4 rcoly = texture2D(rock,rty);
					rockColor = rcolx * texnormal.y + rcoly * texnormal.x;

					if(projz < 15.0 && isReflectioscene < 0.5 && isRefractionscene < 0.5) {
						//get rock normal from tangent space
						vec3 rnx = texture2D(rock_normalmap,rtx).xyz;
						vec3 rny = texture2D(rock_normalmap,rty).xyz;
						vec3 tangentNormal = normalize( (rnx+rny)*0.5 - vec3(0.5) );

						//transform rock tangent normal to world space
						mat3 tangent2world = mat3(approxTangentX,approxTangentY,normal);
						vec3 rockNormal = tangent2world * tangentNormal;

						//diffuse lighting of rock
						float rockDiffuse = clamp(abs(dot(rockNormal,antilightdir)),0.7,1.0);

						float lf = projz / 15.0;
						rockColor *= lf + rockDiffuse*(1-lf);
					}
				}
				vec4 groundColor = (grassColor * isGrass + rockColor * (1 - isGrass)) * (1 - isSand) + sandColor * isSand;

				float diffuse = clamp(dot(normal,antilightdir) , 0 , 1);
				float intensity = max(diffuse,ambient);

				//microchanges bring structure into dark areas
				float microchanges = ambient + normal.z * 0.1;
				intensity = max(intensity,microchanges);

				frag1 = groundColor * vec4(vec3(intensity * maxbrightness),1) * (1 - atmosphere) + skygroundcolor * atmosphere;
				frag1.a = projz;
			}
			"
		}

	}

	(shader) {
		id = "mainobj.main.terrain.shader";

		(vertex) {"
			#version 400 core

			in vec3 vertices;
			in vec2 texcoords;
			in vec3 normals;
			out vec2 vterrtex;
			out float vworldz;
			out vec4 vworldpoint;
			out vec3 vapproxNormal;
			out vec3 vapproxTangentX;
			out vec3 vapproxTangentY;

			uniform sampler2D heightmap;
			uniform sampler2D normalmap;
			uniform mat4 model;
			uniform mat4 projheightmap;

			void main() {
				vworldpoint = vec4(vertices,1);
				vterrtex = texcoords;
				vworldz = vertices.z;

				//calculate tangent space coordinate axes of rock areas in world space coordinates
				vapproxNormal = normals;
				vapproxTangentX = normalize( cross(vec3(0,0,1) , vapproxNormal) );
				vapproxTangentY = cross(vapproxNormal , vapproxTangentX);
			}
			"
		}

		(tessellation_control) {"
			#version 400 core
			#define ID gl_InvocationID

			in vec2 vterrtex[];
			in float vworldz[];
			in vec4 vworldpoint[];
			in vec3 vapproxNormal[];
			in vec3 vapproxTangentX[];
			in vec3 vapproxTangentY[];
			out vec2 tcterrtex[];
			out float tcworldz[];
			out vec4 tcworldpoint[];
			out float tcisRock[];
			out vec3 tcapproxNormal[];
			out vec3 tcapproxTangentX[];
			out vec3 tcapproxTangentY[];

			uniform sampler2D normalmap;
			uniform sampler2D terroverlay;
			uniform mat4 maincam_vp;

			layout(vertices=3) out;

			void main() {
				tcterrtex[ID] = vterrtex[ID];
				tcworldz[ID] = vworldz[ID];
				tcworldpoint[ID] = vworldpoint[ID];
				tcapproxNormal[ID] = vapproxNormal[ID];
				tcapproxTangentX[ID] = vapproxTangentX[ID];
				tcapproxTangentY[ID] = vapproxTangentY[ID];

				//calculate level of detail for each vertex of the face
				//to obtain level of detail for each edge
				float outer_detail[3];
				float visRock[3];
				float avg_detail = 0.0;
				for(int i=0 ; i<3 ; i++) {
					//get distance
					vec4 transformed = maincam_vp * vworldpoint[i];
					float vdistance = transformed.z;

					//calculate ground type
					//only rock requires tessellation
					vec3 normal = normalize(texture2D(normalmap,vterrtex[i]).xyz - vec3(0.5));
					vec4 floortype = texture2D(terroverlay,vterrtex[i]);
					float isGrass = floortype.g;
					float isSand = floortype.r;
					float isRock = (1 - isGrass) * (1 - isSand);
					visRock[i] = isRock;

					//compute lod
					outer_detail[i] = clamp(isRock * (5.0 - abs(vdistance) * 0.1) , 1.0 , 5.0);
					avg_detail += outer_detail[i] * 0.33333;
				}
				tcisRock[ID] = visRock[ID];

				gl_TessLevelOuter[0] = 0.5 * (outer_detail[1] + outer_detail[2]);
				gl_TessLevelOuter[1] = 0.5 * (outer_detail[0] + outer_detail[2]);
				gl_TessLevelOuter[2] = 0.5 * (outer_detail[0] + outer_detail[1]);
				gl_TessLevelInner[0] = clamp(avg_detail , 1.0 , 5.0 );
			}
			"
		}

		(tessellation_evaluation) {"
			#version 400 core
			
			in vec2 tcterrtex[];
			in float tcworldz[];
			in vec4 tcworldpoint[];
			in float tcisRock[];
			in vec3 tcapproxNormal[];
			in vec3 tcapproxTangentX[];
			in vec3 tcapproxTangentY[];
			out vec2 terrtex;
			out float worldz;
			out float projz;
			out float atmosphere;
			out vec3 approxTangentX;
			out vec3 approxTangentY;
			out vec4 worldpoint;

			uniform sampler2D rock_heightmap;
			uniform mat4 viewprojection;

			layout(triangles,equal_spacing) in;

			void main() {
				float w[3] = float[](gl_TessCoord.x,gl_TessCoord.y,gl_TessCoord.z);

				vec2 s_terrtex = vec2(0.0);
				float s_worldz = 0.0;
				vec4 s_worldpoint = vec4(0.0);
				float s_isRock = 0.0;
				vec3 s_approxNormal = vec3(0.0);
				vec3 s_approxTangentX = vec3(0.0);
				vec3 s_approxTangentY = vec3(0.0);
				for(int i=0 ; i<3 ; i++) {
					s_terrtex += tcterrtex[i] * w[i];
					s_worldz += tcworldz[i] * w[i];
					s_worldpoint += tcworldpoint[i] * w[i];
					s_isRock += tcisRock[i] * w[i];
					s_approxNormal += tcapproxNormal[i] * w[i];
					s_approxTangentX += tcapproxTangentX[i] * w[i];
					s_approxTangentY += tcapproxTangentY[i] * w[i];
				}
				terrtex = s_terrtex;
				approxTangentX = s_approxTangentX;
				approxTangentY = s_approxTangentY;

				//tessellate rocks
				float rockDisplacement = 0.0;
				if(s_isRock > 0) {
					vec3 texnormal = abs(normalize(vec3(s_approxNormal.xy,0)));
					float rz = s_worldz*0.1;
					float rnx = texture2D(rock_heightmap,vec2(s_terrtex.x*25.0,rz)).r;
					float rny = texture2D(rock_heightmap,vec2(s_terrtex.y*25.0,rz)).r;
					rockDisplacement = (rnx * texnormal.y + rny * texnormal.x) * s_isRock;
				}

				s_worldpoint += vec4(s_approxNormal,0) * rockDisplacement;
				s_worldpoint.w = 1;
				vec4 ppoint = viewprojection * s_worldpoint;
				worldpoint = s_worldpoint;
				projz = ppoint.z;
				worldz = s_worldpoint.z;
				atmosphere = clamp((exp(projz / 80.0) - 1)/2.71828182,0,1);
				gl_Position = ppoint;
			}
			"
		}

		(fragment) {"
			#version 400 core

			in vec2 terrtex;
			in float worldz;
			in float projz;
			in float atmosphere;
			in vec3 approxTangentX;
			in vec3 approxTangentY;
			in vec4 worldpoint;
			out vec4 frag1;
			out vec4 dmap;

			uniform sampler2D heightmap;
			uniform sampler2D normalmap;
			uniform sampler2D finemap;
			uniform sampler2D terroverlay;
			uniform sampler2D grass;
			uniform sampler2D rock;
			uniform sampler2D rock_normalmap;

			uniform vec4 lightdir;
			uniform vec4 view;
			uniform vec4 skygroundcolor;
			uniform float ambient;
			uniform float maxbrightness;
			uniform float isRefractionscene;
			uniform float isReflectioscene;
			uniform float waterheight;
			uniform float abovewater;

			void main() {
				if(isRefractionscene > 0.5 || isReflectioscene > 0.5) {
					bool cancelFrag = (worldz > waterheight);
					if(abovewater < 0.5) {
						cancelFrag = !cancelFrag;
					}
					if(isReflectioscene > 0.5) {
						cancelFrag = !cancelFrag;
					}
					if(cancelFrag) {
						discard;
					}
				}

				vec3 normal = normalize(texture2D(normalmap,terrtex).xyz - vec3(0.5));
				vec3 antilightdir = normalize(-lightdir.xyz);

				//get floortype and color
				vec4 floortype = texture2D(terroverlay,terrtex);
				float isGrass = floortype.g;
				float isSand = floortype.r;
				vec4 grassColor = vec4(1.0);
				vec4 rockColor = vec4(1.0);
				vec4 sandColor = vec4(1.0);
				if(isGrass > 0) {
					//it's grass
					grassColor = texture2D(grass,terrtex);
				}
				if((1-isGrass) * (1-isSand) > 0) {
					//it's at least partially rock
					
					//get rock texture color
					vec3 texnormal = abs(normalize(vec3(normal.xy,0)));
					float rz = worldz*0.1;
					vec2 rtx = vec2(terrtex.x*25.0,rz);
					vec2 rty = vec2(terrtex.y*25.0,rz);
					vec4 rcolx = texture2D(rock,rtx);
					vec4 rcoly = texture2D(rock,rty);
					rockColor = rcolx * texnormal.y + rcoly * texnormal.x;

					if(projz < 15.0 && isReflectioscene < 0.5 && isRefractionscene < 0.5) {
						//get rock normal from tangent space
						vec3 rnx = texture2D(rock_normalmap,rtx).xyz;
						vec3 rny = texture2D(rock_normalmap,rty).xyz;
						vec3 tangentNormal = normalize( (rnx+rny)*0.5 - vec3(0.5) );

						//transform rock tangent normal to world space
						mat3 tangent2world = mat3(approxTangentX,approxTangentY,normal);
						vec3 rockNormal = tangent2world * tangentNormal;

						//diffuse lighting of rock
						float rockDiffuse = clamp(abs(dot(rockNormal,antilightdir)),0.7,1.0);

						float lf = projz / 15.0;
						rockColor *= lf + rockDiffuse*(1-lf);
					}
				}
				vec4 groundColor = (grassColor * isGrass + rockColor * (1 - isGrass)) * (1 - isSand) + sandColor * isSand;

				float diffuse = clamp(dot(normal,antilightdir) , 0 , 1);
				float intensity = max(diffuse,ambient);

				//microchanges bring structure into dark areas
				float microchanges = ambient + normal.z * 0.1;
				intensity = max(intensity,microchanges);

				frag1 = groundColor * vec4(vec3(intensity * maxbrightness),1) * (1 - atmosphere) + skygroundcolor * atmosphere;
				dmap = vec4(projz,worldpoint.xyz);
			}
			"
		}

	}

	//reading terrain height

	(shader) {
		id = "terrain.getheight.shader";
		(transformfeedback) {
			"opositions"
		}
		(vertex) {"
			#version 400 core
			in vec3 positions;
			out vec3 opositions;

			uniform sampler2D heightmap;
			uniform mat4 projheightmap;
			uniform vec4 worldpos;

			void main() {
				vec2 texcoord = (projheightmap * worldpos).xy;
				opositions.xy = worldpos.xy;
				opositions.z = texture(heightmap,clamp(texcoord,0,1)).r * 30.0;
			}
			"
		}
	}

	//reading terrain heights
	(shader) {
		id = "terrain.getheights.shader";
		(transformfeedback) {
			"opositions"
		}
		(vertex) {"
			#version 400 core
			in vec3 positions;
			out vec3 opositions;

			uniform sampler2D heightmap;
			uniform mat4 projheightmap;

			void main() {
				vec2 texcoord = (projheightmap * vec4(positions,1)).xy;
				opositions.xy = positions.xy;
				opositions.z = texture(heightmap,clamp(texcoord,0,1)).r * 30.0;
			}
			"
		}
	}

}