(postscene) {

	(shader) {
		id = "postscene.shader";

		(vertex) {"
			#version 400 core

			in vec3 vertices;
			in vec2 texcoords;
			out vec2 coords;

			void main() {
				coords = texcoords;
				gl_Position = vec4(vertices,1);
			}
			"
		}

		(fragment) {"
			#version 400 core

			in vec2 coords;
			out vec4 frag1;

			uniform sampler2D maininput;
			uniform sampler2D waves;
			uniform sampler2D shadexlogo;
			uniform float abovewater;
			uniform vec4 wave1;
			uniform vec4 wave2;
			uniform float showlogo;
			uniform float glow;

			void main() {
				vec4 scenefrag = vec4(0);
				vec4 logofrag = vec4(0);
				if(abovewater > 0.5) {
					scenefrag = texture2D(maininput,coords);
				} else {
					float clippingFactor = max(
						clamp( 1.0-coords.x*60.0 , 0 , 1) , max(
						clamp( 1.0-coords.y*60.0 , 0 , 1) , max(
						clamp( coords.x*60.0 - 59.0 , 0 , 1) ,
						clamp( coords.y*60.0 - 59.0 , 0 , 1)
						)));
					clippingFactor = 1.0 - clippingFactor;
					vec3 w1 = texture2D(waves,coords*0.2 + wave1.xy).xyz;
					vec3 w2 = texture2D(waves,coords*0.2 + wave2.xy).xyz;
					w1 = (w1 - vec3(0.5)) * 2.0;
					w2 = (w2 - vec3(0.5)) * 2.0;
					vec3 normal = (w1 + w2) / 2.0;
					normal = normalize(normal);
					
					scenefrag = texture2D(maininput,coords + normal.xy*0.1*clippingFactor);
				}
				if(showlogo > 0) {
					logofrag = texture2D(shadexlogo,coords);
				}
				frag1 = (scenefrag * (1 - showlogo) + logofrag * showlogo) * glow;
			}
			"
		}

	}

	(shader) {
		id = "postscene3.shader";

		(vertex) {"
			#version 400 core

			in vec3 vertices;
			in vec2 texcoords;
			out vec2 coords;

			void main() {
				coords = texcoords;
				gl_Position = vec4(vertices,1);
			}
			"
		}

		(fragment) {"
			#version 400 core

			in vec2 coords;
			out vec4 frag1;

			uniform sampler2D colormap;
			uniform sampler2D shafts;
			uniform sampler2D shadexlogo;
			uniform sampler2D waves;
			uniform float showlogo;
			uniform float glow;
			uniform float blendlightshafts;
			uniform float daynightlightshafts;
			uniform vec4 sunposition;
			uniform float abovewater;
			uniform vec4 wave1;
			uniform vec4 wave2;

			void main() {
				vec4 scenefrag = vec4(0);
				vec4 logofrag = vec4(0);
				
				if(abovewater > 0.5) {
					scenefrag = texture2D(colormap,coords);
					vec4 lightshaftfrag = texture2D(shafts,coords);

					scenefrag = max(scenefrag,lightshaftfrag * 0.9 * blendlightshafts * daynightlightshafts * 0.7);
				} else {
					float clippingFactor = max(
						clamp( 1.0-coords.x*60.0 , 0 , 1) , max(
						clamp( 1.0-coords.y*60.0 , 0 , 1) , max(
						clamp( coords.x*60.0 - 59.0 , 0 , 1) ,
						clamp( coords.y*60.0 - 59.0 , 0 , 1)
						)));
					clippingFactor = 1.0 - clippingFactor;
					vec3 w1 = texture2D(waves,coords*0.2 + wave1.xy).xyz;
					vec3 w2 = texture2D(waves,coords*0.2 + wave2.xy).xyz;
					w1 = (w1 - vec3(0.5)) * 2.0;
					w2 = (w2 - vec3(0.5)) * 2.0;
					vec3 normal = (w1 + w2) / 2.0;
					normal = normalize(normal);
					
					scenefrag = texture2D(colormap,coords + normal.xy*0.1*clippingFactor);
				}
				if(showlogo > 0) {
					logofrag = texture2D(shadexlogo,coords);
				}
				frag1 = (scenefrag * (1 - showlogo) + logofrag * showlogo) * glow;
			}
			"
		}

	}

}