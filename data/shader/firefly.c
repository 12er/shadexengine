(firefly) {

	(shader) {
		id = "mainobj.firefly.shader";

		(vertex) {"
			#version 400 core

			in vec3 vertices;
			in vec3 normals;
			in vec2 texcoords;
			out vec4 world;
			out vec3 tlight;
			out vec3 tview;
			out vec2 coords;
			out float atmosphere;
			out float projz;

			uniform mat4 model;
			uniform mat4 viewprojection;
			uniform mat4 normalv;
			uniform mat4 normalm;
			uniform vec4 lightdir;
			uniform vec4 position;
			uniform vec4 view;
			uniform mat4 left;
			uniform mat4 right;
			uniform mat4 behind;
			uniform mat4 middle;

			void main() {
				float e = exp(1) - 1;
				float color_left = (exp( clamp(-vertices.x/15.0,0,1) ) - 1)/e;
				float color_right = (exp( clamp(vertices.x/15.0,0,1) ) - 1)/e;
				float color_behind = (exp( clamp(-vertices.y/16.0,0,1) ) - 1)/e;
				float color_middle = 1 - color_left - color_right - color_behind;

				mat4 move = left * color_left + right * color_right + behind * color_behind + middle * color_middle;

				vec3 tnormal = normalize( (normalm * vec4(normals,1)).xyz );
				vec3 ttangent = normalize(cross(tnormal,vec3(0,0.1,1)));
				vec3 tbitangent = cross(tnormal,ttangent);

				mat3 toTangent = mat3(normalize(ttangent),normalize(tbitangent),normalize(tnormal));
				tlight = (-lightdir.xyz) * toTangent;
				tview = (-view.xyz) * toTangent;

				coords = texcoords;
				world = model * move * vec4(vertices,1);

				vec4 vproj = viewprojection * world;
				projz = vproj.z;
				atmosphere = clamp((exp(vproj.z / 80.0) - 1)/2.71828182,0,1);
				gl_Position = vproj;
			}
			"
		}

		(fragment) {"
			#version 400 core

			in vec2 coords;
			in vec4 world;
			in vec3 tlight;
			in vec3 tview;
			in float atmosphere;
			in float projz;
			out vec4 frag1;

			uniform float isRefractionscene;
			uniform float isReflectioscene;
			uniform float waterheight;
			uniform float abovewater;
			uniform float daytime;
			uniform float ambient;
			uniform float maxbrightness;
			uniform vec4 skygroundcolor;

			uniform sampler2D tex;
			uniform sampler2D normalmap;

			void main() {
				if(isRefractionscene > 0.5 || isReflectioscene > 0.5) {
					bool cancelFrag = (world.z > waterheight);
					if(abovewater < 0.5) {
						cancelFrag = !cancelFrag;
					}
					if(isReflectioscene > 0.5) {
						cancelFrag = !cancelFrag;
					}
					if(cancelFrag) {
						discard;
					}
				}
				

				vec3 texnormal = texture2D(normalmap,coords).xyz;
				texnormal = normalize((texnormal - vec3(0.5)) * 2.0);

				vec3 nlight = normalize(tlight);
				float diffuse = clamp(dot(nlight,texnormal),0,1);

				vec3 nview = normalize(tview);
				vec3 reflected = texnormal * 2.0 * dot(texnormal,nlight) - nlight;
				float spec = clamp(dot(reflected,nlight),0,1);
				spec = pow(spec,128.0);

				float intensity = max(diffuse,ambient) + spec;

				//microchanges bring structure into dark areas
				float microchanges = ambient + texnormal.z * 0.1;
				intensity = max(intensity,microchanges);

				frag1 = texture2D(tex,coords) * intensity * maxbrightness * (1-atmosphere) + atmosphere * skygroundcolor;
				frag1.a = projz;
			}
			"
		}

	}

	(shader) {
		id = "mainobj.main.firefly.shader";

		(vertex) {"
			#version 400 core

			in vec3 vertices;
			in vec3 normals;
			in vec2 texcoords;
			out vec4 world;
			out vec3 tlight;
			out vec3 tview;
			out vec2 coords;
			out float atmosphere;
			out float projz;

			uniform mat4 model;
			uniform mat4 viewprojection;
			uniform mat4 normalv;
			uniform mat4 normalm;
			uniform vec4 lightdir;
			uniform vec4 position;
			uniform vec4 view;
			uniform mat4 left;
			uniform mat4 right;
			uniform mat4 behind;
			uniform mat4 middle;

			void main() {
				float e = exp(1) - 1;
				float color_left = (exp( clamp(-vertices.x/15.0,0,1) ) - 1)/e;
				float color_right = (exp( clamp(vertices.x/15.0,0,1) ) - 1)/e;
				float color_behind = (exp( clamp(-vertices.y/16.0,0,1) ) - 1)/e;
				float color_middle = 1 - color_left - color_right - color_behind;

				mat4 move = left * color_left + right * color_right + behind * color_behind + middle * color_middle;

				vec3 tnormal = normalize( (normalm * vec4(normals,1)).xyz );
				vec3 ttangent = normalize(cross(tnormal,vec3(0,0.1,1)));
				vec3 tbitangent = cross(tnormal,ttangent);

				mat3 toTangent = mat3(normalize(ttangent),normalize(tbitangent),normalize(tnormal));
				tlight = (-lightdir.xyz) * toTangent;
				tview = (-view.xyz) * toTangent;

				coords = texcoords;
				world = model * move * vec4(vertices,1);

				vec4 vproj = viewprojection * world;
				atmosphere = clamp((exp(vproj.z / 80.0) - 1)/2.71828182,0,1);
				projz = vproj.z;
				gl_Position = vproj;
			}
			"
		}

		(fragment) {"
			#version 400 core

			in vec2 coords;
			in vec4 world;
			in vec3 tlight;
			in vec3 tview;
			in float atmosphere;
			in float projz;
			out vec4 frag1;
			out vec4 dmap;

			uniform float isRefractionscene;
			uniform float isReflectioscene;
			uniform float waterheight;
			uniform float abovewater;
			uniform float daytime;
			uniform float ambient;
			uniform  float maxbrightness;
			uniform vec4 skygroundcolor;

			uniform sampler2D tex;
			uniform sampler2D normalmap;

			void main() {
				if(isRefractionscene > 0.5 || isReflectioscene > 0.5) {
					bool cancelFrag = (world.z > waterheight);
					if(abovewater < 0.5) {
						cancelFrag = !cancelFrag;
					}
					if(isReflectioscene > 0.5) {
						cancelFrag = !cancelFrag;
					}
					if(cancelFrag) {
						discard;
					}
				}
				

				vec3 texnormal = texture2D(normalmap,coords).xyz;
				texnormal = normalize((texnormal - vec3(0.5)) * 2.0);

				vec3 nlight = normalize(tlight);
				float diffuse = clamp(dot(nlight,texnormal),0,1);

				vec3 nview = normalize(tview);
				vec3 reflected = texnormal * 2.0 * dot(texnormal,nlight) - nlight;
				float spec = clamp(dot(reflected,nlight),0,1);
				spec = pow(spec,128.0);

				float intensity = max(diffuse,ambient) + spec;

				//microchanges bring structure into dark areas
				float microchanges = ambient + texnormal.z * 0.1;
				intensity = max(intensity,microchanges);

				frag1 = texture2D(tex,coords) * intensity * maxbrightness * (1-atmosphere) + atmosphere * skygroundcolor;
				dmap = vec4(projz,world.xyz);
			}
			"
		}
	}
		

}