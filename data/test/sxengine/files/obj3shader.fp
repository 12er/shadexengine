#version 400 core

in vec2 vtc;
out vec4 vol4;
out vec4 vol3;

uniform float lcoord;
uniform sampler2D tex1;
uniform sampler2D tex2;
uniform sampler2D tex3;
uniform sampler2D tex4;
uniform sampler2D tex5;
uniform sampler2D tex6;

void main() {
	vol3 = vec4(1,0,0,1);
	vol4 = vec4(0,1,0,1);

	//test rendering multiple renderobjects into a
	//3D texture
	//layer m out of n layers should be addressed by
	//(1/(2*n)) + (m/n)
	if(lcoord > 0.083332 && lcoord < 0.083334) {
		//render into layer 0 of 3 layers
		vol3 = texture2D(tex1,vtc);
		vol4 = texture2D(tex2,vtc);
	} else if(lcoord > 0.083332 + 0.16666666 && lcoord < 0.083334 + 0.16666666) {
		//render into layer 1 of 3 layers
		vol3 = texture2D(tex2,vtc);
		vol4 = texture2D(tex3,vtc);
	} else if(lcoord > 0.083332 + 0.33333333 && lcoord < 0.083334 + 0.33333333) {
		//render into layer 2 of 3 layers
		vol3 = texture2D(tex3,vtc);
		vol4 = texture2D(tex4,vtc);
	} else if(lcoord > 0.083332 + 0.5 && lcoord < 0.083334 + 0.5) {
		//render into layer 2 of 3 layers
		vol3 = texture2D(tex4,vtc);
		vol4 = texture2D(tex5,vtc);
	} else if(lcoord > 0.083332 + 0.66666666 && lcoord < 0.083334 + 0.66666666) {
		//render into layer 2 of 3 layers
		vol3 = texture2D(tex5,vtc);
		vol4 = texture2D(tex6,vtc);
	} else if(lcoord > 0.083332 + 0.83333333 && lcoord < 0.083334 + 0.83333333) {
		//render into layer 2 of 3 layers
		vol3 = texture2D(tex6,vtc);
		vol4 = texture2D(tex1,vtc);
	}
}
