/**
 * resources to be loaded
 */
(resources) {

	(effect) {
		id = "effect.e1";

		(passes) {
			"pass2"
			"pass2"
			"pass1"
			"pass2"
			"pass1"
		}
	}

	(effect) {
		id = "effect.e2";
		visible = "false";
	}

	(object) {
		id = "object.o1";
		shader = "effect1.shader1";
		mesh = "some.mesh";
		(textures) {
			otex1 = "tex.tex1";
			"unloaded"
		}
		(floats) {
			ofloat = "float.float1";
		}
		(vectors) {
			ovec1 = "vector.v2";
			ovec2 = "vector.v3";
		}
		(matrices) {
			omat1 = "matrix.m3";
			omat2 = "matrix.m4";
		}
		(uniformset) {
			id = 1337;
			(matrices) {
				omat3 = "matrix.m2";
				omat4 = "matrix.m1";
			}
		}
		(uniformset) {
			id = 7331;
			(vectors) {
				omat3 = "vector.v4";
			}
		}
		(passes) {
			"pass1"
			"pass2"
		}
	}

	(object) {
		id = "object.o2";
		visible = "false";
	}

	(pass) {
		id = "pass1";
		shader = "effect1.shader2";
		(textures) {
			pfloat = "tex.tex1";
			"unloaded"
		}
		(floats) {
			ptex1 = "float.float1";
		}
		(vectors) {
			pvec2 = "vector.v2";
			pvec1 = "vector.v3";
		}
		(matrices) {
			pmat2 = "matrix.m3";
			pmat1 = "matrix.m4";
		}
		(output) {
			rendertarget = "target.t1";
			out1 = "tex.tex2";
			out2 = "tex.tex4";
			"outputtex"
		}
	}

	
	(pass) {
		id = "pass2";
		visible = "false";
		(output) {
			rendertarget = "target.t1";
			clearDepthBuffer = "false";
			clearColorBuffer = "false";
			out1 = "tex.tex2";
			out2 = "tex.tex4";
			"outputtex"
		}
	}

	(shader) {
		id = "effect1.shader1";
		vertex = "data/test/sxengine/files/shader1.vp";
	}

	(shader) {
		id = "effect1.shader3";
	}

	(shader) {
		id = "effect1.shader2";
	}

	(shader) {
		id = "effect1.shader1";
		fragment = "data/test/sxengine/files/shader1.fp";
	}

	(shader) {
		id = "effect1.shader2";
		vertex = "data/test/sxengine/files/shader2.vp";
		tessellation_control = "data/test/sxengine/files/shader2.tcp";
		tessellation_evaluation = "data/test/sxengine/files/shader2.tep";
		geometry = "data/test/sxengine/files/shader2.gp";
		fragment = "data/test/sxengine/files/shader2.fp";
	}

	(float) {
		id = "float.float1";
		value = 3.14159;
	}

	(vector) {
		id = "vector.v0";
	}

	(vector) {
		id = "vector.v1";
		(value) {
			"1"
		}
	}

	(vector) {
		id = "vector.v2";
		(value) {
			"1" "3"
		}
	}

	(vector) {
		id = "vector.v3";
		(value) {
			"1" "3" "3"
		}
	}

	(vector) {
		id = "vector.v4";
		(value) {
			"1" "3" "3" "7"
		}
	}

	(matrix) {
		id = "matrix.m0";
	}

	(matrix) {
		id = "matrix.m1";
		(value) {
			"5"
		}
	}

	(matrix) {
		id = "matrix.m2";
		(value) {
			"1" "2"
			"3" "4"
		}
	}

	(matrix) {
		id = "matrix.m3";
		(value) {
			"1" "2" "3"
			"4" "5" "6"
			"7" "8" "9"
		}
	}

	(matrix) {
		id = "matrix.m4";
		(value) {
			"1" "2" "3" "4"
			"5" "6" "7" "8"
			"9" "10" "11" "12"
			"13" "14" "15" "16"
		}
	}

	(mesh) {
		id = "some.mesh";
		path = "data/test/sxengine/files/mesh.ply";
		(path) {
			"data/test/sxengine/files/compatible.ply"
			discardStandardBuffers = "true";
		}
	}

	(texture) {
		id = "tex.tex1";
		width = 600;
		height = 400;
		format = "float";
	}

	(texture) {
		id = "tex.tex2";
		width = 50;
		height = 20;
		format = "byte";
	}

	(texture) {
		id = "tex.tex3";
		path = "data/test/sxengine/files/background.jpg";
	}

	(texture) {
		id = "tex.tex4";
		width = 30;
		height = 15;
		format = "float";
	}

	(rendertarget) {
		id = "target.t1";
		width = 320;
		height = 210;
	}

	(rendertarget) {
		id = "target.t2";
		width = 500;
		height = 400;
		renderToDisplay = "true";
	}

}
