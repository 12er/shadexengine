#version 400 core

layout(triangles) in;
layout(triangle_strip, max_vertices=3) out;
in vec2 tep_attr1[];
out vec2 gp_attr1;

void main() {
	for(int i=0 ; i<gl_in.length() ; i++) {
		gp_attr1 = tep_attr1[i];
		gl_Position = gl_in[i].gl_Position;
		EmitVertex();
	}
	EndPrimitive();
}
