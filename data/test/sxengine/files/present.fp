#version 400 core

in vec2 vtc;
out vec4 frag1;
uniform sampler3D volume1;
uniform sampler3D volume2;
uniform sampler2D v3;
uniform sampler2D v1;
uniform sampler2D v2;
uniform float presentweight;

void main() {
	float coord = 0;
	float s = 0.071428571;
	float p = 0.142857143;
	if(vtc.x < 0.143) {
		coord = s+0;
	} else if(vtc.x < 0.143*2) {
		coord = s+p;
	} else if(vtc.x < 0.143*3) {
		coord = s+p*2;
	} else if(vtc.x < 0.143*4) {
		coord = s+p*3;
	} else if(vtc.x < 0.143*5) {
		coord = s+p*4;
	} else if(vtc.x < 0.143*6) {
		coord = s+p*5;
	} else {
		coord = s+p*6;
	}
	if(presentweight < 1.1) {
		//show both volume textures
		frag1 = texture(volume1,vec3(vtc.x,vtc.y,coord))*(1.0 - presentweight) + texture(volume2,vec3(vtc.x,vtc.y,coord))*presentweight;
	} else if(presentweight < 2.1) {
		//show first 2D texture
		frag1 = texture(v1,vtc);
	} else if(presentweight < 3.1) {
		//show first 2D texture
		frag1 = texture(v2,vtc);
	} else {
		//show first 2D texture
		frag1 = texture(v3,vtc);
	}
}
