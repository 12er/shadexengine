#version 400 core

in vec2 vp_texcoords;
out vec4 frag;

uniform sampler2D tex2;
uniform sampler2D tex1;
uniform sampler2D tex3;

void main() {
	frag = texture2D(tex1,vp_texcoords) + texture2D(tex2,vp_texcoords) + texture2D(tex3,vp_texcoords);
}
