#version 400 core

in vec2 vtc;
out vec4 frag1;

uniform float layercoordinate;
uniform sampler3D vol4;
uniform sampler3D vol3;
uniform sampler3D v2;
uniform sampler3D vol1;

void main() {
	//present vol1 and v2
	if(vtc.y < 0.25 && vtc.x < 0.16666) {
		vec2 tc = vec2(vtc.x*6.0,vtc.y*4.0);
		frag1 = texture(vol1,vec3(tc,0.16666666));
	} else if(vtc.y < 0.25 && vtc.x < 0.33333) {
		vec2 tc = vec2((vtc.x - 0.16666)*6.0,vtc.y*4.0);
		frag1 = texture(vol1,vec3(tc,0.16666666 + 0.33333333));
	} else if(vtc.y < 0.25 && vtc.x < 0.5) {
		vec2 tc = vec2((vtc.x - 0.33333)*6.0,vtc.y*4.0);
		frag1 = texture(vol1,vec3(tc,0.16666666 + 0.66666666));
	} else if(vtc.y < 0.25 && vtc.x < 0.66666) {
		vec2 tc = vec2((vtc.x - 0.5)*6.0,vtc.y*4.0);
		frag1 = texture(v2,vec3(tc,0.16666666));
	} else if(vtc.y < 0.25 && vtc.x < 0.83333) {
		vec2 tc = vec2((vtc.x - 0.66666)*6.0,vtc.y*4.0);
		frag1 = texture(v2,vec3(tc,0.16666666 + 0.33333333));
	} else if(vtc.y < 0.25) {
		vec2 tc = vec2((vtc.x - 0.83333)*6.0,vtc.y*4.0);
		frag1 = texture(v2,vec3(tc,0.16666666 + 0.66666666));
	}

	//present vol3
	if(vtc.y >= 0.25 && vtc.y < 0.5 && vtc.x < 0.16666) {
		vec2 tc = vec2(vtc.x*6.0,(vtc.y-0.25)*4.0);
		frag1 = texture(vol3,vec3(tc,0.08333333));
	} else if(vtc.y >= 0.25 && vtc.y < 0.5 && vtc.x < 0.33333) {
		vec2 tc = vec2((vtc.x - 0.16666)*6.0,(vtc.y-0.25)*4.0);
		frag1 = texture(vol3,vec3(tc,0.08333333 + 0.16666666));
	} else if(vtc.y >= 0.25 && vtc.y < 0.5 && vtc.x < 0.5) {
		vec2 tc = vec2((vtc.x - 0.33333)*6.0,(vtc.y-0.25)*4.0);
		frag1 = texture(vol3,vec3(tc,0.08333333 + 0.33333333));
	} else if(vtc.y >= 0.25 && vtc.y < 0.5 && vtc.x < 0.66666) {
		vec2 tc = vec2((vtc.x - 0.5)*6.0,(vtc.y-0.25)*4.0);
		frag1 = texture(vol3,vec3(tc,0.08333333 + 0.5));
	} else if(vtc.y >= 0.25 && vtc.y < 0.5 && vtc.x < 0.83333) {
		vec2 tc = vec2((vtc.x - 0.66666)*6.0,(vtc.y-0.25)*4.0);
		frag1 = texture(vol3,vec3(tc,0.08333333 + 0.66666666));
	} else if(vtc.y >= 0.25 && vtc.y < 0.5) {
		vec2 tc = vec2((vtc.x - 0.83333)*6.0,(vtc.y-0.25)*4.0);
		frag1 = texture(vol3,vec3(tc,0.08333333 + 0.83333333));
	}

	//present vol4
	if(vtc.y >= 0.5 && vtc.y < 0.75 && vtc.x < 0.16666) {
		vec2 tc = vec2(vtc.x*6.0,(vtc.y-0.25)*4.0);
		frag1 = texture(vol4,vec3(tc,0.08333333));
	} else if(vtc.y >= 0.5 && vtc.y < 0.75 && vtc.x < 0.33333) {
		vec2 tc = vec2((vtc.x - 0.16666)*6.0,(vtc.y-0.25)*4.0);
		frag1 = texture(vol4,vec3(tc,0.08333333 + 0.16666666));
	} else if(vtc.y >= 0.5 && vtc.y < 0.75 && vtc.x < 0.5) {
		vec2 tc = vec2((vtc.x - 0.33333)*6.0,(vtc.y-0.25)*4.0);
		frag1 = texture(vol4,vec3(tc,0.08333333 + 0.33333333));
	} else if(vtc.y >= 0.5 && vtc.y < 0.75 && vtc.x < 0.66666) {
		vec2 tc = vec2((vtc.x - 0.5)*6.0,(vtc.y-0.25)*4.0);
		frag1 = texture(vol4,vec3(tc,0.08333333 + 0.5));
	} else if(vtc.y >= 0.5 && vtc.y < 0.75 && vtc.x < 0.83333) {
		vec2 tc = vec2((vtc.x - 0.66666)*6.0,(vtc.y-0.25)*4.0);
		frag1 = texture(vol4,vec3(tc,0.08333333 + 0.66666666));
	} else if(vtc.y >= 0.5 && vtc.y < 0.75) {
		vec2 tc = vec2((vtc.x - 0.83333)*6.0,(vtc.y-0.25)*4.0);
		frag1 = texture(vol4,vec3(tc,0.08333333 + 0.83333333));
	}
}
