#version 400 core

in vec2 vp_texcoords;
out vec4 frag1;
out vec4 frag2;

uniform sampler2D tex2;
uniform sampler2D tex1;
uniform sampler2D tex3;

void main() {
	vec2 coord = vp_texcoords;
	if(vp_texcoords.x < 0.333333) {
		coord.x = coord.x * 3.0;
		frag1 = texture2D(tex1,coord);
	} else if(vp_texcoords.x < 0.666666) {
		coord.x = (coord.x - 0.333333) * 3.0;
		frag1 = texture2D(tex2,coord);
	} else {
		coord.x = (coord.x - 0.666666) * 3.0;
		frag1 = texture2D(tex3,coord);
	}
	frag2 = texture2D(tex1,vp_texcoords);
}
