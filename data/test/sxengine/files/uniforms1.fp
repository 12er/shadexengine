#version 400 core

in vec2 vp_texcoords;
out vec4 frag;

uniform float interpolate;
uniform sampler2D sx;
uniform sampler2D simple;

void main() {
	frag = texture2D(simple,vp_texcoords) * (1 - interpolate) + texture2D(sx,vp_texcoords) * interpolate;
}
