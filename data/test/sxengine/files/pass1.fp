#version 400 core

in vec2 vp_texcoords;
out vec4 frag1;
out vec4 frag2;

uniform sampler2D tex2;
uniform sampler2D tex1;

void main() {
	vec2 coord = vp_texcoords;
	if(vp_texcoords.x < 0.5) {
		coord.x = coord.x * 2.0;
		frag1 = texture2D(tex1,coord);
	} else {
		coord.x = (coord.x - 0.5) * 2.0;
		frag1 = texture2D(tex2,coord);
	}
	frag2 = texture2D(tex1,vp_texcoords);
}
