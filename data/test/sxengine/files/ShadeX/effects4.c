(effects) {

	(mesh) {
		id = "process.mesh";
		path = "data/test/sxengine/files/ShadeX/target.ply";
	}

	(object) {
		id = "process.object";
		mesh = "process.mesh";
		(passes) {
			"display.pass"
		}
	}

	(effect) {
		id = "blend.effect";
		(passes) {
			"blend.pass1"
			"blend.pass2"
			"blend.pass3"
			"depth.pass1"
			"depth.pass2"
			"depth.pass3"
			"depth.pass4"
			"depth.pass5"
			"depth.pass6"
			"depth.pass7"
			"display.pass"
		}
	}

	//blend

	(matrix) {
		id = "blend.object1.transform";
		(value) {
			"0.2"	"0"		"0"		"-0.5"
			"0"		"0.2"	"0"		"0.5"
			"0"		"0"		"1"		"0"
			"0"		"0"		"0"		"1"
		}
	}

	(texture) {
		id = "blend.object1.texture";
		path = "data/test/sxengine/files/background.jpg";
	}

	(object) {
		id = "object1";
		mesh = "process.mesh";
		(matrices) {
			transform = "blend.object1.transform";
		}
		(textures) {
			tex = "blend.object1.texture";
		}
		(passes) {
			"blend.pass1"
		}
	}

	(matrix) {
		id = "blend.object2.transform";
		(value) {
			"0.6"	"0"		"0"		"-0.4"
			"0"		"0.4"	"0"		"0.4"
			"0"		"0"		"1"		"-0.1"
			"0"		"0"		"0"		"1"
		}
	}

	(texture) {
		id = "blend.object2.texture";
		path = "data/test/sxengine/files/ShadeXEngineAlpha.png";
	}

	(object) {
		id = "object2";
		mesh = "process.mesh";
		(matrices) {
			transform = "blend.object2.transform";
		}
		(textures) {
			tex = "blend.object2.texture";
		}
		(passes) {
			"blend.pass2"
		}
	}

	(matrix) {
		id = "blend.object3.transform";
		(value) {
			"0.4"	"0"		"0"		"0"
			"0"		"0.2"	"0"		"0.3"
			"0"		"0"		"1"		"-0.2"
			"0"		"0"		"0"		"1"
		}
	}

	(object) {
		id = "blend.object3";
		mesh = "process.mesh";
		(matrices) {
			transform = "blend.object3.transform";
		}
		(textures) {
			tex = "blend.object2.texture";
		}
		(passes) {
			"blend.pass3"
		}
	}

	(texture) {
		id = "blend.output1";
		width = 1200;
		height = 800;
	}

	(texture) {
		id = "blend.output2";
		width = 1200;
		height = 800;
		format = "float16";
	}

	(rendertarget) {
		id = "blend.target";
		width = 1200;
		height = 800;
	}

	(shader) {
		id = "blend.shader";
	
		(vertex) {"
			#version 400 core
			in vec4 vertices;
			in vec2 texcoords;
			out vec2 vtc;

			uniform mat4 transform;

			void main() {
				gl_Position = transform * vertices;
				vtc = texcoords;
			}
		"}

		(fragment) {"
			#version 400 core
			in vec2 vtc;
			out vec4 frag1;
			out vec4 frag2;

			uniform sampler2D tex;

			void main() {
				vec4 f = texture2D(tex,vtc);
				frag1 = f;
				frag2 = vec4(vtc.x,vtc.y,vtc.x+vtc.y,f.a);
			}
		"}
	
	}

	(pass) {
		id = "blend.pass1";
		shader = "blend.shader";
		(output) {
			rendertarget = "blend.target";
			frag1 = "blend.output1";
			frag2 = "blend.output2";
		}
	}

	(pass) {
		id = "blend.pass2";
		shader = "blend.shader";
		(output) {
			rendertarget = "blend.target";
			clearDepthBuffer = "false";
			clearColorBuffer = "false";
			fragmentBlendFactor = "fragmentalpha";
			targetBlendFactor = "one_minus_fragmentalpha";
			frag1 = "blend.output1";
			frag2 = "blend.output2";
		}
	}

	(pass) {
		id = "blend.pass3";
		shader = "blend.shader";
		(output) {
			rendertarget = "blend.target";
			clearDepthBuffer = "false";
			clearColorBuffer = "false";
			frag1 = "blend.output1";
			frag2 = "blend.output2";
		}
	}

	//test disabling / enabling depth buffer write

	(vector) {
		id = "depth.object1.color";
		(value) {
			"1"	"0"	"0" "1"
		}
	}

	(matrix) {
		id = "depth.object1.transform";
		(value) {
			"0.1"	"0"		"0"		"-0.5"
			"0"		"0.1"	"0"		"-0.5"
			"0"		"0"		"1"		"0"
			"0"		"0"		"0"		"1"
		}
	}

	(object) {
		id = "depth.object1";
		mesh = "process.mesh";
		(vectors) {
			color = "depth.object1.color";
		}
		(matrices) {
			transform = "depth.object1.transform";
		}
		(passes) {
			"depth.pass1"
		}
	}

	(vector) {
		id = "depth.object2.color";
		(value) {
			"0"	"1"	"0" "1"
		}
	}

	(matrix) {
		id = "depth.object2.transform";
		(value) {
			"0.2"	"0"		"0"		"-0.5"
			"0"		"0.2"	"0"		"-0.5"
			"0"		"0"		"1"		"0.1"
			"0"		"0"		"0"		"1"
		}
	}

	(object) {
		id = "depth.object2";
		mesh = "process.mesh";
		(vectors) {
			color = "depth.object2.color";
		}
		(matrices) {
			transform = "depth.object2.transform";
		}
		(passes) {
			"depth.pass2"
		}
	}

	(vector) {
		id = "depth.object3.color";
		(value) {
			"0"	"0"	"1" "1"
		}
	}

	(matrix) {
		id = "depth.object3.transform";
		(value) {
			"0.3"	"0"		"0"		"-0.5"
			"0"		"0.15"	"0"		"-0.65"
			"0"		"0"		"1"		"0.2"
			"0"		"0"		"0"		"1"
		}
	}

	(object) {
		id = "depth.object3";
		mesh = "process.mesh";
		(vectors) {
			color = "depth.object3.color";
		}
		(matrices) {
			transform = "depth.object3.transform";
		}
		(passes) {
			"depth.pass3"
		}
	}

	(vector) {
		id = "depth.object4.color";
		(value) {
			"1"	"0"	"1" "1"
		}
	}

	(matrix) {
		id = "depth.object4.transform";
		(value) {
			"0.4"	"0"		"0"		"-0.5"
			"0"		"0.2"	"0"		"-0.7"
			"0"		"0"		"1"		"0.3"
			"0"		"0"		"0"		"1"
		}
	}

	(object) {
		id = "depth.object4";
		mesh = "process.mesh";
		(vectors) {
			color = "depth.object4.color";
		}
		(matrices) {
			transform = "depth.object4.transform";
		}
		(passes) {
			"depth.pass3"
		}
	}

	(shader) {
		id = "depth.shader";
		
		(vertex) {"
			#version 400 core
			in vec4 vertices;
			in vec2 texcoords;
			out vec2 vtc;

			uniform mat4 transform;

			void main() {
				gl_Position = transform * vertices;
				vtc = texcoords;
			}
		"}

		(fragment) {"
			#version 400 core
			in vec2 vtc;
			out vec4 frag1;
			out vec4 frag2;

			uniform vec4 color;

			void main() {
				frag1 = color;
				frag2 = vec4(vtc.x,vtc.y,vtc.x+vtc.y,1);
			}
		"}
	}

	(pass) {
		id = "depth.pass1";
		shader = "depth.shader";
		(output) {
			rendertarget = "blend.target";
			clearDepthBuffer = "false";
			clearColorBuffer = "false";
			frag1 = "blend.output1";
			frag2 = "blend.output2";
		}
	}

	(pass) {
		id = "depth.pass2";
		shader = "depth.shader";
		(output) {
			rendertarget = "blend.target";
			clearDepthBuffer = "false";
			clearColorBuffer = "false";
			writeDepthBuffer = "false";
			frag1 = "blend.output1";
			frag2 = "blend.output2";
		}
	}

	(pass) {
		id = "depth.pass3";
		shader = "depth.shader";
		(output) {
			rendertarget = "blend.target";
			clearDepthBuffer = "false";
			clearColorBuffer = "false";
			frag1 = "blend.output1";
			frag2 = "blend.output2";
		}
	}

	//test depth buffer function

	(vector) {
		id = "depth.object5.color";
		(value) {
			"1"	"0"	"0" "1"
		}
	}

	(matrix) {
		id = "depth.object5.transform";
		(value) {
			"0.1"	"0"		"0"		"0.5"
			"0"		"0.1"	"0"		"0.1"
			"0"		"0"		"1"		"0"
			"0"		"0"		"0"		"1"
		}
	}

	(object) {
		id = "depth.object5";
		mesh = "process.mesh";
		(vectors) {
			color = "depth.object5.color";
		}
		(matrices) {
			transform = "depth.object5.transform";
		}
		(passes) {
			"depth.pass4"
		}
	}

	(vector) {
		id = "depth.object6.color";
		(value) {
			"0"	"1"	"0" "1"
		}
	}

	(matrix) {
		id = "depth.object6.transform";
		(value) {
			"0.4"	"0"		"0"		"0.5"
			"0"		"0.5"	"0"		"-0.4"
			"0"		"0"		"1"		"0.1"
			"0"		"0"		"0"		"1"
		}
	}

	(object) {
		id = "depth.object6";
		mesh = "process.mesh";
		(vectors) {
			color = "depth.object6.color";
		}
		(matrices) {
			transform = "depth.object6.transform";
		}
		(passes) {
			"depth.pass5"
		}
	}

	(vector) {
		id = "depth.object7.color";
		(value) {
			"0"	"0"	"1" "1"
		}
	}

	(matrix) {
		id = "depth.object7.transform";
		(value) {
			"0.5"	"0"		"0"		"0.5"
			"0"		"0.5"	"0"		"-0.5"
			"0"		"0"		"1"		"0.2"
			"0"		"0"		"0"		"1"
		}
	}

	(object) {
		id = "depth.object7";
		mesh = "process.mesh";
		(vectors) {
			color = "depth.object7.color";
		}
		(matrices) {
			transform = "depth.object7.transform";
		}
		(passes) {
			"depth.pass6"
		}
	}

	(vector) {
		id = "depth.object8.color";
		(value) {
			"1"	"0"	"1" "1"
		}
	}

	(matrix) {
		id = "depth.object8.transform";
		(value) {
			"0.5"	"0"		"0"		"0.5"
			"0"		"0.25"	"0"		"-0.75"
			"0"		"0"		"1"		"0.3"
			"0"		"0"		"0"		"1"
		}
	}

	(object) {
		id = "depth.object8";
		mesh = "process.mesh";
		(vectors) {
			color = "depth.object8.color";
		}
		(matrices) {
			transform = "depth.object8.transform";
		}
		(passes) {
			"depth.pass7"
		}
	}

	(pass) {
		id = "depth.pass4";
		shader = "depth.shader";
		(output) {
			rendertarget = "blend.target";
			clearDepthBuffer = "false";
			clearColorBuffer = "false";
			frag1 = "blend.output1";
			frag2 = "blend.output2";
		}
	}

	(pass) {
		id = "depth.pass5";
		shader = "depth.shader";
		(output) {
			rendertarget = "blend.target";
			clearDepthBuffer = "false";
			clearColorBuffer = "false";
			depthTest = "accept_always";
			frag1 = "blend.output1";
			frag2 = "blend.output2";
		}
	}

	(pass) {
		id = "depth.pass6";
		shader = "depth.shader";
		(output) {
			rendertarget = "blend.target";
			clearDepthBuffer = "false";
			clearColorBuffer = "false";
			depthTest = "accept_greater";
			frag1 = "blend.output1";
			frag2 = "blend.output2";
		}
	}

	(pass) {
		id = "depth.pass7";
		shader = "depth.shader";
		(output) {
			rendertarget = "blend.target";
			clearDepthBuffer = "false";
			clearColorBuffer = "false";
			frag1 = "blend.output1";
			frag2 = "blend.output2";
		}
	}

	//display

	(shader) {
		id = "display.shader";
		
		(vertex) {"
			#version 400 core
			in vec4 vertices;
			in vec2 texcoords;
			out vec2 vtc;

			void main() {
				gl_Position = vertices;
				vtc = texcoords;
			}
		"}
		
		(fragment) {"
			#version 400 core
			in vec2 vtc;
			out vec4 frag1;

			uniform sampler2D tex1;
			uniform sampler2D tex2;

			void main() {
				frag1 = vec4(vtc.x,vtc.y,vtc.x+vtc.y,1);
				if(vtc.x > 0.1 && vtc.x < 0.45 && vtc.y > 0.1 && vtc.y < 0.9) {
					//render tex1
					vec2 tc = vec2((vtc.x-0.1)*2.8571,(vtc.y-0.1)*1.25);
					frag1 = texture2D(tex1,tc);
				}
				if(vtc.x > 0.55 && vtc.x < 0.9 && vtc.y > 0.1 && vtc.y < 0.9) {
					//render tex2
					vec2 tc = vec2((vtc.x-0.55)*2.8571,(vtc.y-0.1)*1.25);
					frag1 = texture2D(tex2,tc);
				}
			}
		"}

	}

	(rendertarget) {
		id = "display.target";
		width = 800;
		height = 600;
		renderToDisplay = "true";
	}

	(pass) {
		id = "display.pass";
		shader = "display.shader";
		(textures) {
			tex1 = "blend.output1";
			tex2 = "blend.output2";
		}
		(output) {
			rendertarget = "display.target";
		}
	}

}
