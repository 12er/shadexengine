#version 400 core

in vec2 vp_tex;
out vec4 frag1;

uniform sampler2D waves;
uniform sampler2D colored;
uniform sampler2D blurred;
uniform vec4 trans;
uniform float wavefactor;

void main() {
	//read wave normal generators
	vec3 color3 = texture2D(waves,vp_tex + vec2(trans.x,trans.y)).xyz;
	vec3 color4 = texture2D(waves,vp_tex + vec2(trans.z,trans.w)).xyz;

	//transform normal generators to one normal
	color3 = color3 - vec3(0.5);
	color4 = color4 - vec3(0.5);
	vec3 normal = normalize(color3 + color4);

	//use normal to disturb textures
	vec4 color1 = texture2D(colored,vp_tex + vec2(normal.x,normal.y) * wavefactor);
	vec4 color2 = texture2D(blurred,vp_tex + vec2(normal.x,normal.y) * wavefactor);

	frag1 = max(color1,color2);
}
