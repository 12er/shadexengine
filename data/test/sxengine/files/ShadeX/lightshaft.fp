#version 400 core

in vec2 vp_tex;
out vec4 frag1;

uniform sampler2D tex;

void main() {
	
	float count = 50.0;

	vec2 dir = (vec2(0.5,0.5)-vp_tex)/count;
	vec2 start = vp_tex;
	float factor = 1;
	float decay = 0.98;
	float sum = 0;
	vec4 sumcolor = vec4(0);

	for(float index = 0 ; index < count ; index++) {
		sumcolor += texture2D(tex,start) * factor;
		sum += factor;
		
		factor *= decay;
		start += dir;
	}

	frag1 = sumcolor/sum;
}
