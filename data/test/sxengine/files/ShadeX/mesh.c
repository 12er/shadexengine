(meshes) {

	/**
	 * present1
	 */

	(mesh) {
		id = "meshes.m1";
		path = "data/test/sxengine/files/ShadeX/cmesh1.dae";
	}

	(object) {
		id = "objects.m1";
		mesh = "meshes.m1";
		(matrices) {
			model = "objects.m1.transform";
		}
	}

	(matrix) {
		id = "objects.m1.transform";
	}

	(rendertarget) {
		id = "present1.target";
		width = 800;
		height = 600;
		renderToDisplay = "true";
	}

	(matrix) {
		id = "present1.view";
		(value) {
			(view) {
				(position) {"5 0 0"}
				(view) {"-1 0 0"}
				(up) {"0 0 1"}
			}
		}
	}

	(matrix) {
		id = "present1.projection";
		(value) {
			(perspective) {
				angle = 1.5;
				znear = 1; zfar = 100;
				width = 800 ; height = 600;
			}
		}
	}

	(shader) {
		id = "present1.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec3 normals;
			out vec3 vnormals;
			out vec3 lightdir;

			uniform mat4 model;
			uniform mat4 view;
			uniform mat4 projection;

			void main() {
				mat4 mvp = projection * view * model;
				mat3 nmatrix = mat3(mvp);
				gl_Position = mvp * vec4(vertices,1);
				vnormals = nmatrix * normals;
				lightdir = mat3(view) * vec3(0,-1,0);
			}
		"}
		(fragment) {"
			#version 400 core
			in vec3 vnormals;
			in vec3 lightdir;
			out vec4 color1;

			void main() {
				vec3 normal = normalize(vnormals);
				float diffuse = dot(normal,lightdir);
				float maxambient = 0.3;
				float minambient = 0.1;
				float ambient = minambient + (maxambient-minambient)*(1+diffuse)/(1+maxambient);
				float fac = max(diffuse,ambient);
				color1 = vec4(0,1,0,1)*fac;
			}
		"}
	}

	(pass) {
		id = "present1.pass";
		shader = "present1.shader";
		(matrices) {
			view = "present1.view";
			projection = "present1.projection";
		}
		(objects) {
			"objects.m1"
		}
		(output) {
			rendertarget = "present1.target";
		}
	}

	(effect) {
		id = "present1.effect";
		(passes) {
			"present1.pass"
		}
	}

	/**
	 * present2
	 */

	(mesh) {
		id = "meshes.m2";
		path = "data/test/sxengine/files/ShadeX/cmesh2.dae";
	}

	(texture) {
		id = "textures.m2";
		path = "data/test/sxengine/files/ShadeX/cmesh2.jpg";
	}

	(object) {
		id = "objects.m2";
		mesh = "meshes.m2";
		(matrices) {
			model = "objects.m2.transform";
		}
		(textures) {
			colortex = "textures.m2";
		}
	}

	(pass) {
		id = "present2.pass";
		(objects) {
			"objects.m2"
		}
	}

	(matrix) {
		id = "objects.m2.transform";
	}

	(shader) {
		id = "present2.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec3 normals;
			in vec2 texcoords;
			out vec3 vnormals;
			out vec3 lightdir;
			out vec2 vtexcoords;

			uniform mat4 model;
			uniform mat4 view;
			uniform mat4 projection;

			void main() {
				mat4 mvp = projection * view * model;
				mat3 nmatrix = mat3(mvp);
				gl_Position = mvp * vec4(vertices,1);
				vnormals = nmatrix * normals;
				vtexcoords = texcoords;
				lightdir = mat3(view) * vec3(0,-1,0);
			}
		"}
		(fragment) {"
			#version 400 core
			in vec3 vnormals;
			in vec2 vtexcoords;
			in vec3 lightdir;
			out vec4 color1;

			uniform sampler2D colortex;

			void main() {
				vec3 normal = normalize(vnormals);
				float diffuse = dot(normal,lightdir);
				float maxambient = 0.3;
				float minambient = 0.1;
				float ambient = minambient + (maxambient-minambient)*(1+diffuse)/(1+maxambient);
				float fac = max(diffuse,ambient);
				color1 = texture(colortex,vtexcoords) * fac;
			}
		"}
	}

	(pass) {
		id = "present2.pass";
		shader = "present2.shader";
		(matrices) {
			view = "present1.view";
			projection = "present1.projection";
		}
		(output) {
			rendertarget = "present1.target";
		}
	}

	(effect) {
		id = "present2.effect";
		(passes) {
			"present2.pass"
		}
	}

	/**
	 * present3
	 */

	(skeleton) {
		id = "skeletons.m0";
		mesh = "meshes.m1";

		(bone) {
			id="bone1";
			(parentTransform) {
				(rotate) {
					"1 2 3"
					angle = 2.4521;
				}
			}
			(bone) {
				id="bone2";
				(parentTransform) {
					(scale) {
						"2 3 1"
					}
				}
			}
			(bone) {
				id="bone3";
				(parentTransform) {
					(translate) {
						"4 5 6"
					}
				}
				(bone) {
					id="bone4";
					(parentTransform) {
						(translate) {
							"4 8 16"
						}
					}
				}
			}
		}

		(transformationset) {
			id=1;
			(transform) {
				id="fantasybone";
				input="fantasyinput";
				output="fantasyoutput";
			}
			(transform) {
				id="head";
				input="s0.input.head";
				output="s0.output.head";
			}
			(transform) {
				id="shoulder_left";
				input="s0.input.shoulder_left";
				output="s0.output.shoulder_left";
			}
			(transform) {
				id="shoulder_right";
				input="s0.input.shoulder_right";
				output="s0.output.shoulder_right";
			}
			(transform) {
				id="upperarm_left";
				input="s0.input.upperarm_left";
				output="s0.output.upperarm_left";
			}
			(transform) {
				id="upperarm_right";
				input="s0.input.upperarm_right";
				output="s0.output.upperarm_right";
			}
			(transform) {
				id="underarm_left";
				input="s0.input.underarm_left";
				output="s0.output.underarm_left";
			}
			(transform) {
				id="underarm_right";
				input="s0.input.underarm_right";
				output="s0.output.underarm_right";
			}
			(transform) {
				id="pelvis_left";
				input="s0.input.pelvis_left";
				output="s0.output.pelvis_left";
			}
			(transform) {
				id="pelvis_right";
				input="s0.input.pelvis_right";
				output="s0.output.pelvis_right";
			}
			(transform) {
				id="thigh_left";
				input="s0.input.thigh_left";
				output="s0.output.thigh_left";
			}
			(transform) {
				id="thigh_right";
				input="s0.input.thigh_right";
				output="s0.output.thigh_right";
			}
			(transform) {
				id="calf_left";
				input="s0.input.calf_left";
				output="s0.output.calf_left";
			}
			(transform) {
				id="calf_right";
				input="s0.input.calf_right";
				output="s0.output.calf_right";
			}
		}
		(transformationset) {
			id=2;
			(transform) {
				id="shoulder_left";
				input="s01.input.shoulder_left";
			}
			(transform) {
				id="upperarm_left";
				input="s01.input.upperarm_left";
				output="s01.output.upperarm_left";
			}
		}
	}

	(skeleton) {
		id = "skeletons.m1";
		mesh = "meshes.m1";

		(transformationset) {
			id=1;
			(transform) {
				id="head";
				input="s1.input.head";
				output="s1.output.head";
			}
			(transform) {
				id="shoulder_left";
				input="s1.input.shoulder_left";
				output="s1.output.shoulder_left";
			}
			(transform) {
				id="shoulder_right";
				input="s1.input.shoulder_right";
				output="s1.output.shoulder_right";
			}
			(transform) {
				id="upperarm_left";
				input="s1.input.upperarm_left";
				output="s1.output.upperarm_left";
			}
			(transform) {
				id="upperarm_right";
				input="s1.input.upperarm_right";
				output="s1.output.upperarm_right";
			}
			(transform) {
				id="underarm_left";
				input="s1.input.underarm_left";
				output="s1.output.underarm_left";
			}
			(transform) {
				id="underarm_right";
				input="s1.input.underarm_right";
				output="s1.output.underarm_right";
			}
			(transform) {
				id="pelvis_left";
				input="s1.input.pelvis_left";
				output="s1.output.pelvis_left";
			}
			(transform) {
				id="pelvis_right";
				input="s1.input.pelvis_right";
				output="s1.output.pelvis_right";
			}
			(transform) {
				id="thigh_left";
				input="s1.input.thigh_left";
				output="s1.output.thigh_left";
			}
			(transform) {
				id="thigh_right";
				input="s1.input.thigh_right";
				output="s1.output.thigh_right";
			}
			(transform) {
				id="calf_left";
				input="s1.input.calf_left";
				output="s1.output.calf_left";
			}
			(transform) {
				id="calf_right";
				input="s1.input.calf_right";
				output="s1.output.calf_right";
			}
		}
	}

	(object) {
		id = "objects.m3";
		mesh = "meshes.m1";
		(matrices) {
			model = "objects.m3.transform";

			thead = "s1.output.head";
			tshoulder_left = "s1.output.shoulder_left";
			tupperarm_left = "s1.output.upperarm_left";
			tunderarm_left = "s1.output.underarm_left";
			tshoulder_right = "s1.output.shoulder_right";
			tupperarm_right = "s1.output.upperarm_right";
			tunderarm_right = "s1.output.underarm_right";
			tpelvis_left = "s1.output.pelvis_left";
			tthigh_left = "s1.output.thigh_left";
			tcalf_left = "s1.output.calf_left";
			tpelvis_right = "s1.output.pelvis_right";
			tthigh_right = "s1.output.thigh_right";
			tcalf_right = "s1.output.calf_right";
		}
		(passes) {
			"present3.pass"
		}
	}

	(matrix) {
		id = "objects.m3.transform";
		(value) {
			(rotate) {
				angle = -0.7853;
				"0 0 1"
			}
		}
	}

	(shader) {
		id = "present3.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec3 normals;
			out vec3 vnormals;
			out vec3 lightdir;

			uniform mat4 model;
			uniform mat4 view;
			uniform mat4 projection;

			in float head;
			in float shoulder_left;
			in float upperarm_left;
			in float underarm_left;
			in float shoulder_right;
			in float upperarm_right;
			in float underarm_right;
			in float pelvis_left;
			in float thigh_left;
			in float calf_left;
			in float pelvis_right;
			in float thigh_right;
			in float calf_right;

			uniform mat4 thead;
			uniform mat4 tshoulder_left;
			uniform mat4 tupperarm_left;
			uniform mat4 tunderarm_left;
			uniform mat4 tshoulder_right;
			uniform mat4 tupperarm_right;
			uniform mat4 tunderarm_right; 
			uniform mat4 tpelvis_left;
			uniform mat4 tthigh_left;
			uniform mat4 tcalf_left;
			uniform mat4 tpelvis_right;
			uniform mat4 tthigh_right;
			uniform mat4 tcalf_right;

			void main() {
				float stillBone = 1
					- shoulder_left - upperarm_left - underarm_left
					- shoulder_right - upperarm_right - underarm_right
					- pelvis_left - thigh_left - calf_left	
					- pelvis_right - thigh_right - calf_right;
				mat4 mvp = projection * view * model * (
						mat4(1.0)*stillBone 
						+ thead*head
						+ tshoulder_left*shoulder_left + tupperarm_left*upperarm_left + tunderarm_left*underarm_left
						+ tshoulder_right*shoulder_right + tupperarm_right*upperarm_right + tunderarm_right*underarm_right
						+ tpelvis_left*pelvis_left + tthigh_left*thigh_left + tcalf_left*calf_left
						+ tpelvis_right*pelvis_right + tthigh_right*thigh_right + tcalf_right*calf_right
						);
				mat3 nmatrix = mat3(mvp);
				gl_Position = mvp * vec4(vertices,1);
				vnormals = nmatrix * normals;
				lightdir = mat3(view) * vec3(0,-1,0);
			}
		"}
		(fragment) {"
			#version 400 core
			in vec3 vnormals;
			in vec3 lightdir;
			out vec4 color1;

			void main() {
				vec3 normal = normalize(vnormals);
				float diffuse = dot(normal,lightdir);
				float maxambient = 0.3;
				float minambient = 0.1;
				float ambient = minambient + (maxambient-minambient)*(1+diffuse)/(1+maxambient);
				float fac = max(diffuse,ambient);
				color1 = vec4(0,1,0,1)*fac;
			}
		"}
	}

	(pass) {
		id = "present3.pass";
		shader = "present3.shader";
		(matrices) {
			view = "present1.view";
			projection = "present1.projection";
		}
		(output) {
			rendertarget = "present1.target";
		}
	}

	(effect) {
		id = "present3.effect";
		(skeletons) {
			"skeletons.m1"
		}
		(passes) {
			"present3.pass"
		}
	}

}
