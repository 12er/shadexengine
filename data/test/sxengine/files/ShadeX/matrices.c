(matrices) {

	(vector) {
		id = "vec.v1";
		(value) {"
			1 2 3 4
		"}
	}

	(matrix) {
		id = "mat.m1";
		(value) {
			(matrix) {
				"1 2 3 4
				5 6 7 8
				9 10 11 12
				13 14 15 16"
			}
			(matrix) {
				"1 2 3
				4 5 6
				7 8 9"
			}
			(matrix) {
				"1 2
				3 4"
			}
			(matrix) {
				"2"
			}
			(matrix) {
			}
		}
	}

	(matrix) {
		id = "mat.m2";
		(value) {
			(rotate) {
				angle = 1.5345;
				"-1 0.5 0.3 1"
			}
		}
	}

	(matrix) {
		id = "mat.m3";
		(value) {
			(translate) {
				"4 1 3 1"
			}
		}
	}

	(matrix) {
		id = "mat.m4";
		(value) {
			(scale) {
				"55 6 36 1"
			}
		}
	}

	(matrix) {
		id = "mat.m5";
		(value) {
			(shear) {
				"3 -1 2.5 1"
			}
		}
	}

	(matrix) {
		id = "mat.m6";
		(value) {
			(orthographic) {
				left = -533;
				right = 210;
				bottom = 2;
				top = 113;
				znear = 0.2;
				zfar = 621;
			}
		}
	}

	(matrix) {
		id = "mat.m7";
		(value) {
			(view) {
				(position) {
					"3 5.23 10.1"
				}
				(view) {
					"0.4 2.5 0.13"
				}
				(up) {
					"1.5 0.5 1.32"
				}
			}
		}
	}

	(matrix) {
		id = "mat.m8";
		(value) {
			(perspective) {
				angle = 2.321;
				znear = 0.3;
				zfar = 654;
				width = 1920;
				height = 1200;
			}
		}
	}

	(matrix) {
		id = "mat.m9";
		(value) {
			(translate) { "4 2 3" }
			(rotate) { angle=3.982; "0.5 0.75 2.1" }
			(translate) { "9 -8 12" }
			(perspective) {
				angle=1.5;
				znear=1;
				zfar=500;
				width=1024;
				height=800;
			}
		}
	}

}
