(sound) {

	(audiobuffer) {
		id = "buffer.ambience";
		path = "data/audio/ambience.wav";
	}

	(audiobuffer) {
		id = "buffer.noise";
		path = "data/audio/noise.wav";
	}

	(audiobuffer) {
		id = "buffer.boom1";
		path = "data/audio/boom1.wav";
	}

	(audiobuffer) {
		id = "buffer.boom2";
		path = "data/audio/boom2.wav";
	}

	(audiobuffer) {
		id = "buffer.boom3";
		path = "data/audio/boom3.wav";
	}

	(vector) {
		id = "pos.ambience";
		(value) {"0 0 0"}
	}

	(float) {
		id = "radius.ambience";
		value = 5;
	}

	(float) {
		id = "max.ambience";
		value = 15;
	}

	(float) {
		id = "exp.ambience";
		value = 3;
	}

	(float) {
		id = "pitch.ambience";
		value = 1;
	}

	(float) {
		id = "volume.ambience";
		value = 1;
	}

	(audioobject) {
		id = "object.ambience";
		buffer = "buffer.ambience";
		looping = "true";

		position = "pos.ambience";
		referenceRadius = "radius.ambience";
		maxRadius = "max.ambience";
		distanceExponent = "exp.ambience";
		pitch = "pitch.ambience";
		volume = "volume.ambience";

		(passes) {
			"audio.pass"
		}
	}

	(audioobject) {
		id = "object.noise";
		buffer = "buffer.noise";
	}

	(audioobject) {
		id = "object.boom1";
		buffer = "buffer.boom1";
		(passes) {
			"audio.pass"
		}
	}

	(audioobject) {
		id = "object.boom2";
		buffer = "buffer.boom2";
	}

	(audioobject) {
		id = "object.boom3";
		buffer = "buffer.boom3";
	}

	(vector) {
		id = "pos.listener";
		(value) {"0 0 0"}
	}

	(vector) {
		id = "view.listener";
		(value) {"1 0 0"}
	}

	(vector) {
		id = "up.listener";
		(value) {"0 0 -1"}
	}

	(audiolistener) {
		id = "audio.listener";
		position = "pos.listener";
		view = "view.listener";
		up = "up.listener";
	}

	(audiopass) {
		id = "audio.pass";
		listener = "audio.listener";
		(objects) {
			"object.noise"
			"object.boom2"
			"object.boom3"
		}
	}

	(audiobuffer) {
		id = "buffer.matrix";
		path = "data/audio/matrix.wav";
	}

	(audioobject) {
		id = "object.matrix";
		buffer = "buffer.matrix";
		looping = "true";
	}

	(audiopass) {
		id = "audio.pass2";
		(objects) {
			"object.matrix"
		}
	}

	//some visual content rendered
	//at the same time sound is active
	
	(float) {
		id = "present.volumeindex";
		value = 0;
	}
	
	(volume) {
		id = "present.volume";
		(paths) {
			"data/test/sxengine/files/ShadeX/audio0.png"
			"data/test/sxengine/files/ShadeX/audio1.png"
			"data/test/sxengine/files/ShadeX/audio2.png"
			"data/test/sxengine/files/ShadeX/audio3.png"
			"data/test/sxengine/files/ShadeX/audio4.png"
			"data/test/sxengine/files/ShadeX/audio5.png"
			"data/test/sxengine/files/ShadeX/audio6.png"
			"data/test/sxengine/files/ShadeX/audio7.png"
			"data/test/sxengine/files/ShadeX/audio8.png"
			"data/test/sxengine/files/ShadeX/audio9.png"
			"data/test/sxengine/files/ShadeX/audio10.png"
			"data/test/sxengine/files/ShadeX/audio11.png"
			"data/test/sxengine/files/ShadeX/audio12.png"
			"data/test/sxengine/files/ShadeX/audio13.png"
		}
	}

	(shader) {
		id = "present.shader";
		(vertex) {"
			#version 400 core
			in vec3 vertices;
			in vec2 texcoords;
			out vec2 quadcolor;
			out vec2 text_coords;
			out float tindex;

			uniform mat4 transform;
			uniform float text_index;
			uniform float number_texts;

			void main() {
				gl_Position = vec4(vertices,1);
				quadcolor = texcoords;
				vec4 t = transform * vec4(vertices,1);
				text_coords = t.xy;
				tindex = (1/number_texts)*0.5 + (text_index/number_texts);
			}
		"}

		(fragment) {"
			#version 400 core
			in vec2 quadcolor;
			in vec2 text_coords;
			in float tindex;
			out vec4 fragment;

			uniform sampler3D text;

			void main() {
				vec2 tc = clamp(text_coords,0,1);
				fragment = max(
						vec4(0,quadcolor.x,quadcolor.y,1),
						texture(text,vec3(tc,tindex))	
					      );
			}
		"}
	}

	(rendertarget) {
		id = "present.target";
		width = 600;
		height = 400;
		renderToDisplay = "true";
	}

	(pass) {
		id = "present.pass";
		shader = "present.shader";
		backgroundQuad = "true";
		(floats) {
			text_index = "present.volumeindex";
			number_texts = "present.number_texts";
		}
		(volumes) {
			text = "present.volume";
		}
		(matrices) {
			transform = "present.transform";
		}
		(output) {
			rendertarget = "present.target";
		}
	}

	(effect) {
		id = "present.effect";
		(passes) {
			"present.pass"
		}
	}
	
}
