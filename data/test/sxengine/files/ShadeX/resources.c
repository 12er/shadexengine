/**
 * some resources
 */
(resources) {

	//scene objects
	(mesh) {
		id = "mesh.objects";
		path = "data/test/sxengine/files/ShadeX/sphere.ply";
	}

	(texture) {
		id = "texture.sceneball";
		path = "data/test/sxengine/files/ShadeX/background.jpg";
	}

	(object) {
		id = "scene.obj1";
		mesh = "mesh.objects";
		(matrices) {
			translate = "matrices.obj1.translate";
			scale = "matrices.obj1.scale";
		}
		(textures) {
			tex = "texture.sceneball";
		}
		(passes) {
			"pass.scene"
		}
	}


	(object) {
		id = "scene.obj2";
		mesh = "mesh.objects";
		(matrices) {
			translate = "matrices.obj2.translate";
			scale = "matrices.obj2.scale";
		}
		(textures) {
			tex = "texture.sceneball";
		}
		(passes) {
			"pass.scene"
		}
	}

	(object) {
		id = "scene.obj3";
		mesh = "mesh.objects";
		(matrices) {
			translate = "matrices.obj3.translate";
			scale = "matrices.obj3.scale";
		}
		(textures) {
			tex = "texture.sceneball";
		}
		(passes) {
			"pass.scene"
		}
	}

	(object) {
		id = "scene.obj4";
		mesh = "mesh.objects";
		(matrices) {
			translate = "matrices.obj4.translate";
			scale = "matrices.obj4.scale";
		}
		(textures) {
			tex = "texture.sceneball";
		}
		(passes) {
			"pass.scene"
		}
	}

	(object) {
		id = "scene.obj5";
		mesh = "mesh.objects";
		(matrices) {
			translate = "matrices.obj5.translate";
			scale = "matrices.obj5.scale";
		}
		(textures) {
			tex = "texture.sceneball";
		}
		(passes) {
			"pass.scene"
		}
	}

	(object) {
		id = "scene.obj6";
		mesh = "mesh.objects";
		(matrices) {
			translate = "matrices.obj6.translate";
			scale = "matrices.obj6.scale";
		}
		(textures) {
			tex = "texture.sceneball";
		}
		(passes) {
			"pass.scene"
		}
	}

	(audiobuffer) {
		id = "audio.buffer.1";
		path = "data/audio/matrix.wav";
	}

	(float) {
		id = "audio.object.1.volume";
		value = 1;
	}

	(audioobject) {
		id = "audio.object.1";
		buffer = "audio.buffer.1";
		volume = "audio.object.1.volume";
		(passes) {
			"audio.pass.1"
		}
	}

}
