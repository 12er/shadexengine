/**
 * ShadeX Engine Demo
 * (c) 2014 by Tristan Bauer
 */


/**
 * scene specific data
 */
(scene) {
	backgroundmusic = "data/audio/HeartOfCourage.wav";
	terrainheightmap = "data/texture/Terrain_Mountains.png";
	maxlogotime = 15;

	(renderstages) {
		(fluidlightshaft) {
			time = 0;
			emitsmoke = "true";
			lightshaftintensity = 1;
			sunrotationspeed = 0;
			(smokeposition) {"4.9253 -95.0002 3.38956"}
			(fireflyattractor) {"84.693 -62.5805 46.331"}
		}
		(fluidlightshaft) {
			time = 33;
			(smokeposition) {"-80.6319 -5.02448 25.2815"}
		}

		(fluidlightshaft) {
			time = 53;
			emitsmoke = "false";
		}
		(fluidlightshaft) {
			time = 60;
			emitsmoke = "true";
			(smokeposition) {"106.91 112.208 22.8242"}
			(fireflyattractor) {"106.91 112.208 40"}
		}
		(fluidlightshaft) {
			time = 103;
			sunrotationspeed = 0.1;
		}
		(fluidlightshaft) {
			time = 108;
			(smokeposition) {"115.593 16.7599 20.366"}
		}
	}

	(camerapath) {
		(keypoint) {
			time = 0;
			(position) {"3.10612 -180.848 6.10432"}
			(view) {"0.0461376 0.998793 -0.0168698"}
		}
		(keypoint) {
			time = 5.465;
			(position) {"-1.93673 -148.698 7.67039"}
			(view) {"-0.112383 0.959946 -0.264458"}
		}
		(keypoint) {
			time = 8.636;
			(position) {"-24.5869 -151.931 8.36889"}
			(view) {"0.239939 0.96632 0.0933755"}
		}
		(keypoint) {
			time = 10;
			(position) {"-21.6157 -149.989 5.9092"}
			(view) {"0.371714 0.926505 -0.0584617"}
		}
		(keypoint) {
			time = 12.5;
			(position) {"-30.4529 -137.827 5.44092"}
			(view) {"0.371714 0.926505 -0.0584617"}
		}
		(keypoint) {
			time = 15.79999;
			(position) {"-32.7943 -128.964 6.38772"}
			(view) {"0.586143 0.809928 0.0212744"}
		}
		(keypoint) {
			time = 21.453;
			(position) {"-12.9859 -114.319 6.30798"}
			(view) {"0.841108 0.535718 -0.0744546"}
		}
		(keypoint) {
			time = 24.642;
			(position) {"-17.0057 -117.257 13.0915"}
			(view) {"0.847892 0.312851 -0.409325"}
		}
		(keypoint) {
			time = 26.198;
			(position) {"-11.91 -117.414 12.678"}
			(view) {"0.768031 0.639703 0.030139"}
		}
		(keypoint) {
			time = 28.764;
			(position) {"-9.39129 -117.153 12.6879"}
			(view) {"0.735277 0.677481 -0.019658"}
		}
		(keypoint) {
			time = 32.213;
			(position) {"13.0552 -91.4502 9.24479"}
			(view) {"0.143676 0.9896 0.0200688"}
		}
		(keypoint) {
			time = 35.8219;
			(position) {"15.4463 -53.8538 12.2958"}
			(view) {"-0.485827 0.857014 -0.198987"}
		}
		(keypoint) {
			time = 41.9548;
			(position) {"31.6497 -2.77475 23.8003"}
			(view) {"-0.818541 -0.369355 -0.439964"}
		}
		(keypoint) {
			time = 43.7737;
			(position) {"13.6006 9.38677 21.1304"}
			(view) {"-0.929268 -0.368386 -0.0274476"}
		}
		(keypoint) {
			time = 46;
			(position) {"-5.30075 25.0974 20.1126"}
			(view) {"-0.93608 -0.344046 -0.0733934"}
		}
		(keypoint) {
			time = 50.2526;
			(position) {"-42.7589 -28.6615 21.2102"}
			(view) {"-0.997614 -0.0256029 -0.0500835"}
		}
		(keypoint) {
			time = 53.5747;
			(position) {"-83.6549 -37.3748 31.8027"}
			(view) {"0.159492 0.959137 0.234279"}
		}
		(keypoint) {
			time = 55.4397;
			(position) {"-98.1763 -18.6396 34.8077"}
			(view) {"0.288167 0.945283 0.152969"}
		}
		(keypoint) {
			time = 58.7117;
			(position) {"-78.2321 -0.129671 28.4757"}
			(view) {"0.98603 0.0526679 -0.0557561"}
		}
		(keypoint) {
			time = 61.1856;
			(position) {"-51.61 9.6217 32.706"}
			(view) {"0.970074 -0.197672 0.141004"}
		}
		(keypoint) {
			time = 68.1455;
			(position) {"18.0259 34.4256 34.8044"}
			(view) {"0.940362 0.332208 0.0731894"}
		}
		(keypoint) {
			time = 71.5405;
			(position) {"62.224 40.7925 35.2781"}
			(view) {"0.0475783 0.993806 0.107975"}
		}
		(keypoint) {
			time = 78.3355;
			(position) {"88.3585 104.391 25.2839"}
			(view) {"0.481846 -0.868538 -0.118623"}
		}
		(keypoint) {
			time = 80;
			(position) {"74.994 127.108 33.4223"}
			(view) {"0.933984 -0.280885 -0.220856"}
		}
		(keypoint) {
			time = 84.161;
			(position) {"111.029 144.561 32.6829"}
			(view) {"-0.298244 -0.943131 -0.155414"}
		}
		(keypoint) {
			time = 87.487;
			(position) {"120.406 122.989 28.2537"}
			(view) {"-0.686071 -0.714608 -0.136535"}
		}
		(keypoint) {
			time = 93.637;
			(position) {"107.304 107.845 59.4867"}
			(view) {"0.0794959 0.174145 -0.981506"}
		}
		(keypoint) {
			time = 99.81;
			(position) {"114.087 122.693 73.0209"}
			(view) {"-0.0141661 -0.120814 -0.992574"}
		}
		(keypoint) {
			time = 102.843;
			(position) {"108.922 117.509 44.4123"}
			(view) {"-0.616419 -0.358007 -0.701327"}
		}
		(keypoint) {
			time = 104.652;
			(position) {"97.0976 109.885 33.1353"}
			(view) {"-0.692294 -0.467935 -0.549333"}
		}
		(keypoint) {
			time = 111.693;
			(position) {"96.66 91.1381 26.8675"}
			(view) {"-0.543142 -0.831206 -0.118717"}
		}
		(keypoint) {
			time = 120;
			(position) {"81.5534 55.143 25.4756"}
			(view) {"0.49529 -0.804575 -0.218546"}
		}
		(keypoint) {
			time = 133;
			(position) {"70.7683 36.2925 22.1179"}
			(view) {"0.593928 -0.787337 0.165377"}
		}
	}

}
