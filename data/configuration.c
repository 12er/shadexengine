/**
 * ShadeX Engine Demo
 * (c) 2014 by Tristan Bauer
 */


/**
 * scene configuration
 */
(configuration) {
	fullscreen = "false";
	usescreendimensions = "false";
	width = 1024;
	height = 800;
	map = "data/map1.c";
	//map = "data/map2.c";
}
