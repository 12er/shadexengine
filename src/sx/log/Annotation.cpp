#ifndef _SX_LOG_ANNOTATION_CPP_
#define _SX_LOG_ANNOTATION_CPP_

/**
 * Log annotation class
 * (c) 2012 by Tristan Bauer
 */
#include <sx/Log4SX.h>

namespace sx {

	Annotation::Annotation(std::string annotation) {
		this->annotation = annotation;
	}

	Annotation::Annotation(const sx::Annotation &a) {
		this->annotation = a.annotation;
	}

	Annotation &Annotation::operator = (const Annotation &a) {
		if(this == &a) {
			return this [0];
		}
		this->annotation = a.annotation;
		return this [0];
	}

	string Annotation::getAnnotation() const {
		return annotation;
	}

}

#endif