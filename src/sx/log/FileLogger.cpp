#ifndef _SX_LOG_FILELOGGER_CPP_
#define _SX_LOG_FILELOGGER_CPP_

/**
 * Logger saving log in a file
 * (c) 2012 by Tristan Bauer
 */

#include <sx/Log4SX.h>
#include <sstream>
#include <chrono>
using namespace std;
using namespace std::chrono;

namespace sx {

	FileLogger::FileLogger(std::string filename) {
		try {
			errorbit = false;
			file.open(filename.c_str());
			auto currentTime = system_clock::now();
			file << 
				"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n"
				"<html>\n"
				"	<head>\n"
				"		<title>Log4SX Log Messages</title>\n"
				"		<style type=\"text/css\">\n"
				"			<!--\n"
				"			body, table {\n"
				"				font-family: arial,sans-serif;\n"
				"				font-size: x-small;\n"
				"			}\n"
				"			markup {\n"
				"				font-family: arial,sans-serif;\n"
				"				font-size: large;\n"
				"			}\n"
				"			reporttitle {\n"
				"				font-family: arial,sans-serif;\n"
				"				font-size: large;\n"
				"			}\n"
				"			th {\n"
				"				background: #336699;\n"
				"				color: #FFFFFF;\n"
				"				text-align: left;\n"
				"			}\n"
				"			.warning {\n"
				"				background-color: #fff200;\n"
				"			}\n"
				"			.error {\n"
				"				background-color: #fdbf2f;\n"
				"			}\n"
				"			.fatalerror {\n"
				"				background-color: #ed1c24;\n"
				"			}\n"
				"			-->\n"
				"		</style>\n"
				"	</head>\n"
				"	<body bg-color=\"#FFFFFF\" topmargin=\"6\" leftmargin=\"6\">\n"
				"		<hr size=\"1\" noshade/>\n"
				"		<reporttitle>Log Report</reporttitle><br/>\n"
				"		Log session start time " << TimeMeasurement::timepointToString(currentTime) << "<br/><br/>\n"
				;
			state = FIRST_TABLE;
			timeMeasurement.restart();
		} catch(...) {
			errorbit = true;
		}
	}

	string FileLogger::INTERNAL_TABLE_PRINT_FUNCTION() {
		return
			"		<table cellspacing=\"0\" cellpadding=\"4\" border = \"1\" bordercolor = \"#224466\" width = \"100%\">\n"
			"			<tr>\n"
			"				<th>Annotation</th>\n"
			"				<th>Time</th>\n"
			"				<th>Level</th>\n"
			"				<th>Message</th>\n"
			"			</tr>\n"
			"			<tr>\n"
			"				<td>"
			;
	}

	string FileLogger::INTERNAL_SEPARATE_ANNOTATION() {
		stringstream collect;
		collect << 
			"</td>\n"
			"				<td>" << timeMeasurement.elapsed() << "</td>\n"
			"				<td"
			;
		if(level == L_NONE) {
			collect << ">NONE</td>\n";
		} else if(level == L_HARMLESS) {
			collect << ">HARMLESS</td>\n";
		} else if(level == L_WARNING) {
			collect << " class=\"warning\">WARNING</td>\n";
		} else if(level == L_ERROR) {
			collect << " class=\"error\">ERROR</td>\n";
		} else {
			collect << " class=\"fatalerror\">FATAL ERROR</td>\n";
		}
		collect << "				<td>";
		return collect.str();
	}

	string FileLogger::INTERNAL_FINISH_TABLE() {
		return
			"</td>\n"
			"			</th>\n"
			"		</table>\n"
			;
	}

	FileLogger::~FileLogger() {
		close();
	}

	bool FileLogger::hasErrors() const {
		return errorbit;
	}

	void FileLogger::close() {
		try {
			if(state == COLLECT_ANNOTATIONS) {
				file << INTERNAL_SEPARATE_ANNOTATION();
			}
			if(state == COLLECT_ANNOTATIONS || state == COLLECT_MESSAGES) {
				file << INTERNAL_FINISH_TABLE();
			}
			file <<
				"	</body>\n"
				"</html>\n"
				;
			file.close();
		} catch(...) {
			errorbit = true;
		}
	}

	Logger &FileLogger::operator <<(const sx::Level l) {
		return Logger::operator <<(l);
	}

	Logger &FileLogger::operator <<(const sx::LogMarkup m) {
		if(!hasErrors()) {
			try {
				if(state == COLLECT_ANNOTATIONS) {
					file << INTERNAL_SEPARATE_ANNOTATION();
				}
				if(state == COLLECT_ANNOTATIONS || state == COLLECT_MESSAGES) {
					file << INTERNAL_FINISH_TABLE();
				}
				state = COLLECT_ANNOTATIONS;
				file << "			<br/><br/><markup>" << m.getMessage() << "</markup>\n";
				file << INTERNAL_TABLE_PRINT_FUNCTION();
			} catch(...) {
				errorbit = true;
			}
		}
		return this [0];
	}

	Logger &FileLogger::operator <<(const sx::Annotation a) {
		if(!hasErrors()) {
			try {
				if(state == COLLECT_MESSAGES) {
					file << 
						"</td>\n" <<
						"			</tr>\n"
						"			<tr>\n"
						"				<td>"
						;
				}
				if(state == FIRST_TABLE) {
					file << INTERNAL_TABLE_PRINT_FUNCTION();
				}
				state = COLLECT_ANNOTATIONS;
				file << a.getAnnotation();
			} catch(...) {
				errorbit = true;
			}
		}
		return this [0];
	}

	Logger &FileLogger::operator <<(char value) {
		if(takeMessage() && !hasErrors()) {
			try {
				if(state == FIRST_TABLE) {
					file << INTERNAL_TABLE_PRINT_FUNCTION();
					state = COLLECT_ANNOTATIONS;
				}
				if(state == COLLECT_ANNOTATIONS) {
					file << INTERNAL_SEPARATE_ANNOTATION();
				}
				state = COLLECT_MESSAGES;
				file << value;
			} catch(...) {
				errorbit = true;
			}
		}
		return this [0];
	}

	Logger &FileLogger::operator <<(unsigned char value) {
		if(takeMessage() && !hasErrors()) {
			try {
				if(state == FIRST_TABLE) {
					file << INTERNAL_TABLE_PRINT_FUNCTION();
					state = COLLECT_ANNOTATIONS;
				}
				if(state == COLLECT_ANNOTATIONS) {
					file << INTERNAL_SEPARATE_ANNOTATION();
				}
				state = COLLECT_MESSAGES;
				file << value;
			} catch(...) {
				errorbit = true;
			}
		}
		return this [0];
	}

	Logger &FileLogger::operator <<(short value) {
		if(takeMessage() && !hasErrors()) {
			try {
				if(state == FIRST_TABLE) {
					file << INTERNAL_TABLE_PRINT_FUNCTION();
					state = COLLECT_ANNOTATIONS;
				}
				if(state == COLLECT_ANNOTATIONS) {
					file << INTERNAL_SEPARATE_ANNOTATION();
				}
				state = COLLECT_MESSAGES;
				file << value;
			} catch(...) {
				errorbit = true;
			}
		}
		return this [0];
	}

	Logger &FileLogger::operator <<(unsigned short value) {
		if(takeMessage() && !hasErrors()) {
			try {
				if(state == FIRST_TABLE) {
					file << INTERNAL_TABLE_PRINT_FUNCTION();
					state = COLLECT_ANNOTATIONS;
				}
				if(state == COLLECT_ANNOTATIONS) {
					file << INTERNAL_SEPARATE_ANNOTATION();
				}
				state = COLLECT_MESSAGES;
				file << value;
			} catch(...) {
				errorbit = true;
			}
		}
		return this [0];
	}

	Logger &FileLogger::operator <<(int value) {
		if(takeMessage() && !hasErrors()) {
			try {
				if(state == FIRST_TABLE) {
					file << INTERNAL_TABLE_PRINT_FUNCTION();
					state = COLLECT_ANNOTATIONS;
				}
				if(state == COLLECT_ANNOTATIONS) {
					file << INTERNAL_SEPARATE_ANNOTATION();
				}
				state = COLLECT_MESSAGES;
				file << value;
			} catch(...) {
				errorbit = true;
			}
		}
		return this [0];
	}

	Logger &FileLogger::operator <<(unsigned int value) {
		if(takeMessage() && !hasErrors()) {
			try {
				if(state == FIRST_TABLE) {
					file << INTERNAL_TABLE_PRINT_FUNCTION();
					state = COLLECT_ANNOTATIONS;
				}
				if(state == COLLECT_ANNOTATIONS) {
					file << INTERNAL_SEPARATE_ANNOTATION();
				}
				state = COLLECT_MESSAGES;
				file << value;
			} catch(...) {
				errorbit = true;
			}
		}
		return this [0];
	}

	Logger &FileLogger::operator <<(long value) {
		if(takeMessage() && !hasErrors()) {
			try {
				if(state == FIRST_TABLE) {
					file << INTERNAL_TABLE_PRINT_FUNCTION();
					state = COLLECT_ANNOTATIONS;
				}
				if(state == COLLECT_ANNOTATIONS) {
					file << INTERNAL_SEPARATE_ANNOTATION();
				}
				state = COLLECT_MESSAGES;
				file << value;
			} catch(...) {
				errorbit = true;
			}
		}
		return this [0];
	}

	Logger &FileLogger::operator <<(unsigned long value) {
		if(takeMessage() && !hasErrors()) {
			try {
				if(state == FIRST_TABLE) {
					file << INTERNAL_TABLE_PRINT_FUNCTION();
					state = COLLECT_ANNOTATIONS;
				}
				if(state == COLLECT_ANNOTATIONS) {
					file << INTERNAL_SEPARATE_ANNOTATION();
				}
				state = COLLECT_MESSAGES;
				file << value;
			} catch(...) {
				errorbit = true;
			}
		}
		return this [0];
	}

	Logger &FileLogger::operator <<(float value) {
		if(takeMessage() && !hasErrors()) {
			try {
				if(state == FIRST_TABLE) {
					file << INTERNAL_TABLE_PRINT_FUNCTION();
					state = COLLECT_ANNOTATIONS;
				}
				if(state == COLLECT_ANNOTATIONS) {
					file << INTERNAL_SEPARATE_ANNOTATION();
				}
				state = COLLECT_MESSAGES;
				file << value;
			} catch(...) {
				errorbit = true;
			}
		}
		return this [0];
	}

	Logger &FileLogger::operator <<(double value) {
		if(takeMessage() && !hasErrors()) {
			try {
				if(state == FIRST_TABLE) {
					file << INTERNAL_TABLE_PRINT_FUNCTION();
					state = COLLECT_ANNOTATIONS;
				}
				if(state == COLLECT_ANNOTATIONS) {
					file << INTERNAL_SEPARATE_ANNOTATION();
				}
				state = COLLECT_MESSAGES;
				file << value;
			} catch(...) {
				errorbit = true;
			}
		}
		return this [0];
	}

	Logger &FileLogger::operator <<(std::string value) {
		if(takeMessage() && !hasErrors()) {
			try {
				if(state == FIRST_TABLE) {
					file << INTERNAL_TABLE_PRINT_FUNCTION();
					state = COLLECT_ANNOTATIONS;
				}
				if(state == COLLECT_ANNOTATIONS) {
					file << INTERNAL_SEPARATE_ANNOTATION();
				}
				state = COLLECT_MESSAGES;
				file << value;
			} catch(...) {
				errorbit = true;
			}
		}
		return this [0];
	}

}

#endif