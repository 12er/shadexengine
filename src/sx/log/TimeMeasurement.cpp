#ifndef _SX_TIMEMEASUREMENT_CPP_
#define _SX_TIMEMEASUREMENT_CPP_

/**
 * TimeMeasurement class
 * (c) 2015 by Tristan Bauer
 */

#include <sx/Log4SX.h>
#include <sstream>
#include <iomanip>
#include <ctime>
using namespace std;
using namespace std::chrono;

namespace sx {

	TimeMeasurement::TimeMeasurement() {
		startTime = system_clock::now();
	}

	TimeMeasurement::TimeMeasurement(const TimeMeasurement &tm) {
		this->startTime = tm.startTime;
	}

	TimeMeasurement &TimeMeasurement::operator = (const TimeMeasurement &tm) {
		if (this != &tm) {
			this->startTime = tm.startTime;
		}
		return *this;
	}

	TimeMeasurement::~TimeMeasurement() {

	}

	void TimeMeasurement::restart() {
		startTime = system_clock::now();
	}

	double TimeMeasurement::elapsed() const {
		auto endTime = system_clock::now();
		return (double)duration_cast<nanoseconds>(endTime - startTime).count() / 1000000000.0;
	}

	std::chrono::time_point <std::chrono::system_clock, std::chrono::system_clock::duration> TimeMeasurement::getStartTime() const {
		return startTime;
	}

	string TimeMeasurement::timepointToString(const std::chrono::time_point <std::chrono::system_clock, std::chrono::system_clock::duration> &t) {
		stringstream str;
		auto in_time_t = system_clock::to_time_t(t);

#ifdef _PLATFORM_WINDOWS_
		//windows routine
		struct tm _tm;
		localtime_s(&_tm, &in_time_t);
		str << put_time(&_tm, "%d.%m.%Y %X");
#else
		//linux routine
		struct tm *_tm = localtime(&in_time_t);
		str << _tm->tm_mday << "." << (_tm->tm_mon+1) << "." << (_tm->tm_year+1900) << " " << _tm->tm_hour << ":" << _tm->tm_min << ":" << _tm->tm_sec;
#endif

		return str.str();
	}

}

#endif
