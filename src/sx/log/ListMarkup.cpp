#ifndef _SX_LOG_LISTMARKUP_CPP_
#define _SX_LOG_LISTMARKUP_CPP_

/**
 * Markup for List Loggers
 * (c) 2012 by Tristan Bauer
 */
#include <sx/Log4SX.h>

namespace sx {

	ListMarkup::ListMarkup(std::string message): LogMarkup(message) {
	}

	ListMarkup::ListMarkup(const sx::ListMarkup &m): LogMarkup(m.message) {
		chars = m.chars;
		uchars = m.uchars;
		shorts = m.shorts;
		ushorts = m.ushorts;
		ints = m.ints;
		uints = m.uints;
		longs = m.longs;
		ulongs = m.ulongs;
		floats = m.floats;
		doubles = m.doubles;
		strings = m.strings;
		annotations = m.annotations;
	}

	ListMarkup &ListMarkup::operator = (const ListMarkup &m) {
		if(this == &m) {
			return this [0];
		}
		LogMarkup::operator = (m);
		chars = m.chars;
		uchars = m.uchars;
		shorts = m.shorts;
		ushorts = m.ushorts;
		ints = m.ints;
		uints = m.uints;
		longs = m.longs;
		ulongs = m.ulongs;
		floats = m.floats;
		doubles = m.doubles;
		strings = m.strings;
		annotations = m.annotations;
		return this [0];
	}

	ListMarkup::~ListMarkup() {
	}

	vector<char> &ListMarkup::getChars() {
		return chars;
	}

	vector<unsigned char> &ListMarkup::getUchars() {
		return uchars;
	}

	vector<short> &ListMarkup::getShorts() {
		return shorts;
	}

	vector<unsigned short> &ListMarkup::getUshorts() {
		return ushorts;
	}

	vector<int> &ListMarkup::getInts() {
		return ints;
	}

	vector<unsigned int> &ListMarkup::getUints() {
		return uints;
	}

	vector<long> &ListMarkup::getLongs() {
		return longs;
	}

	vector<unsigned long> &ListMarkup::getUlongs() {
		return ulongs;
	}

	vector<float> &ListMarkup::getFloats() {
		return floats;
	}

	vector<double> &ListMarkup::getDoubles() {
		return doubles;
	}

	vector<string> &ListMarkup::getStrings() {
		return strings;
	}

	vector<Annotation> &ListMarkup::getAnnotations() {
		return annotations;
	}

}

#endif