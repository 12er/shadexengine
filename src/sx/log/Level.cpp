#ifndef _SX_LOG_LEVEL_CPP_
#define _SX_LOG_LEVEL_CPP_

/**
 * Loglevel class
 * (c) 2012 by Tristan Bauer
 */

#include <sx/Log4SX.h>

namespace sx {

	Level::Level(sx::LogLevel level) {
		this->level = level;
	}

	Level::Level(const sx::Level &l) {
		this->level = l.level;
	}

	Level &Level::operator =(const sx::Level &l) {
		if(this == &l) {
			return this [0];
		}
		this->level = l.level;
		return this [0];
	}

	LogLevel Level::getLevel() const {
		return level;
	}

}

#endif