#ifndef _SX_ENGINE_CORE_AUDIOPASS_CPP_
#define _SX_ENGINE_CORE_AUDIOPASS_CPP_

/**
 * Audio Pass class
 * (c) 2013 by Tristan Bauer
 */
#include <sx/SXCore.h>
#include <sx/Log4SX.h>
#include <sx/Exception.h>
#include <sstream>
using namespace std;

namespace sx {

	AudioPass::AudioPass(const string &id) {
		this->id = id;
		this->listener = 0;
		this->paused = false;
	}

	AudioPass::~AudioPass() {
		unload();
	}

	void AudioPass::setPause(bool pause) {
		if(this->paused == pause) {
			return;
		}
		this->paused = pause;
		if(paused) {
			pausedObjects.reserve(objects.size());
			unpausedObjects.reserve(objects.size());
			for(unordered_map<string,AudioObject *>::iterator iter = objects.begin() ; iter != objects.end() ; iter++) {
				AudioObject *obj = (*iter).second;
				obj->unlock();
				if(obj->isPlaying()) {
					obj->pause();
					pausedObjects.push_back(obj);
				} else {
					unpausedObjects.push_back(obj);
				}
				obj->lock(1000000000);
			}
		} else {
			for(vector<AudioObject *>::iterator iter = pausedObjects.begin() ; iter != pausedObjects.end() ; iter++) {
				AudioObject *obj = (*iter);
				obj->unlock();
				obj->start();
			}
			for(vector<AudioObject *>::iterator iter = unpausedObjects.begin() ; iter != unpausedObjects.end() ; iter++) {
				AudioObject *obj = (*iter);
				obj->unlock();
			}
			pausedObjects.clear();
			unpausedObjects.clear();
		}
	}

	bool AudioPass::isPaused() const {
		return paused;
	}

	void AudioPass::load() {
		//nothing to do
	}
	void AudioPass::unload() {
		//nothing to do
	}

	bool AudioPass::isLoaded() const {
		return true;
	}

	void AudioPass::setAudioListener(AudioListener &listener) {
		this->listener = &listener;
		listener.update();
	}

	void AudioPass::addAudioObject(AudioObject &object) {
		const string &objID = object.getID();
		objects[objID] = &object;
	}

	void AudioPass::removeAudioObject(const string &id) {
		unordered_map<string,AudioObject *>::iterator iter = objects.find(id);
		if(iter != objects.end()) {
			objects.erase(iter);
			for(vector<AudioObject *>::iterator iter2 = pausedObjects.begin() ; iter2 != pausedObjects.end() ;) {
				if((*iter).second == (*iter2)) {
					//increment by erase
					pausedObjects.erase(iter2);
				} else {
					iter2++;
				}
			}
			for(vector<AudioObject *>::iterator iter2 = unpausedObjects.begin() ; iter2 != unpausedObjects.end() ;) {
				if((*iter).second == (*iter2)) {
					//increment by erase
					unpausedObjects.erase(iter2);
				} else {
					iter2++;
				}
			}
		}
	}

	void AudioPass::removeAudioObjects() {
		objects.clear();
		pausedObjects.clear();
	}

	void AudioPass::update() {
		if(paused) {
			return;
		}
		if(listener != 0) {
			listener->update();
		} else if(AudioObject::isInitialized()) {
			float p[] = {0,0,0};
			alListenerfv(AL_POSITION,p);

			float ori[6] = {1,0,0, 0,0,1};
			alListenerfv(AL_ORIENTATION,ori);

			float v[] = {0,0,0};
			alListenerfv(AL_VELOCITY,v);

			alListenerf(AL_GAIN,1);
		}
		for(unordered_map<string,AudioObject *>::iterator iter = objects.begin() ; iter != objects.end() ; iter++) {
			AudioObject *obj = (*iter).second;
			obj->update();
		}
	}

}

#endif