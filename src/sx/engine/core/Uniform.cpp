#ifndef _SX_ENGINE_CORE_UNIFORM_CPP_
#define _SX_ENGINE_CORE_UNIFORM_CPP_

/**
 * Uniform variable class
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SXCore.h>
#include <sx/SX.h>
#include <sx/SXParser.h>

namespace sx {

	Uniform::Uniform(const string &id) {
		this->id = id;
		vector<string> tokens = tokenizeString(id);
		for (vector<string>::iterator tokenIter = tokens.begin(); tokenIter != tokens.end(); tokenIter++) {
			const string &token = (*tokenIter);
			//use last token as idToken
			idToken = token;
		}
	}

	Uniform::~Uniform() {
	}

	void Uniform::setUniformName(const string &name, const string &id) {
		uniformNames[id] = name;
	}

	const string &Uniform::getUniformName(const string &id) const {
		unordered_map<string,string>::const_iterator iter = uniformNames.find(id);
		if(iter != uniformNames.end()) {
			return (*iter).second;
		}
		//no uniform name set,
		//hence the name is the last token
		//of the id
		return idToken;
	}

}

#endif