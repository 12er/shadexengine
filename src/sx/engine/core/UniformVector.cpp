#ifndef _SX_ENGINE_CORE_UNIFORMVECTOR_CPP_
#define _SX_ENGINE_CORE_UNIFORMVECTOR_CPP_

/**
 * Uniform vector class
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SXCore.h>
#include <sx/SX.h>

namespace sx {

	UniformVector::UniformVector(const string &id): Uniform(id), Vector() {
		
	}

	Vector &UniformVector::operator = (const Vector &v) {
		return Vector::operator = (v);
	}

	void UniformVector::load() {
	}

	bool UniformVector::isLoaded() const {
		return true;
	}

	void UniformVector::use(Shader &shader, const string &identifyer) {
		if(!shader.isLoaded()) {
			//shader isn't loaded
			//, hence nothing can be done
			return;
		}
		const string &name = getUniformName(identifyer);
		int loc = shader.getUniformLocation(name);
		if(loc >= 0) {
			//uniform used in shader,
			//hence copy value to uniform
			SXGL()glUniform4f(loc, (GLfloat)elements[0], (GLfloat)elements[1], (GLfloat)elements[2], (GLfloat)elements[3]);
		}
	}

}

#endif