#ifndef _SX_ENGINE_CORE_SXRESOURCE_CPP_
#define _SX_ENGINE_CORE_SXRESOURCE_CPP_

/**
 * SX resource class
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SXCore.h>

namespace sx {

	SXResource::~SXResource() {
	}

	const string &SXResource::getID() const {
		return id;
	}

}

#endif