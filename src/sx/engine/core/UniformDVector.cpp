#ifndef _SX_ENGINE_CORE_UNIFORMDVECTOR_CPP_
#define _SX_ENGINE_CORE_UNIFORMDVECTOR_CPP_

/**
 * Uniform vector class
 * (c) 2013 by Tristan Bauer
 */
#include <sx/SXCore.h>
#include <sx/SX.h>

namespace sx {

	UniformDVector::UniformDVector(const string &id): Uniform(id), DVector() {
		
	}

	DVector &UniformDVector::operator = (const DVector &v) {
		return DVector::operator = (v);
	}

	void UniformDVector::load() {
	}

	bool UniformDVector::isLoaded() const {
		return true;
	}

	void UniformDVector::use(Shader &shader, const string &identifyer) {
		if(!shader.isLoaded()) {
			//shader isn't loaded
			//, hence nothing can be done
			return;
		}
		const string &name = getUniformName(identifyer);
		int loc = shader.getUniformLocation(name);
		if(loc >= 0) {
			//uniform used in shader,
			//hence copy value to uniform
			SXGL()glUniform4d(loc, (GLdouble)elements[0], (GLdouble)elements[1], (GLdouble)elements[2], (GLdouble)elements[3]);
		}
	}

}

#endif