#ifndef _SX_ENGINE_CORE_UNIFORMMATRIX_CPP_
#define _SX_ENGINE_CORE_UNIFORMMATRIX_CPP_

/**
 * Uniform matrix class
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SXCore.h>
#include <sx/SX.h>

namespace sx {

	UniformMatrix::UniformMatrix(const string &id): Uniform(id), Matrix() {
		
	}

	Matrix &UniformMatrix::operator = (const Matrix &m) {
		return Matrix::operator = (m);
	}

	void UniformMatrix::load() {
	}

	bool UniformMatrix::isLoaded() const {
		return true;
	}

	void UniformMatrix::use(Shader &shader, const string &identifyer) {
		if(!shader.isLoaded()) {
			//shader isn't loaded
			//, hence nothing can be done
			return;
		}
		const string &name = getUniformName(identifyer);
		int loc = shader.getUniformLocation(name);
		if(loc >= 0) {
			//uniform used in shader,
			//hence copy value to uniform
			SXGL()glUniformMatrix4fv(loc,1,GL_FALSE,(GLfloat *)elements);
		}
	}

}

#endif