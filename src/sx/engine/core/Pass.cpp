#ifndef _SX_ENGINE_CORE_PASS_CPP_
#define _SX_ENGINE_CORE_PASS_CPP_

/**
 * Pass class
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SXCore.h>
#include <sx/Log4SX.h>

namespace sx {

	void Pass::applyWireframe(bool wireframe) {
		if(wireframe) {
			SXGL()glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		} else {
			SXGL()glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		}
	}

	Pass::Pass(const string &id) {
		this->id = id;
		visible = true;
		shader = 0;
		wireframe = false;
		clearColorbuffer = true;
		clearDepthbuffer = true;
		fragmentBlendFactor = ONE;
		targetBlendFactor = ZERO;
		depthTest = ACCEPT_LESS;
		writeDepthBuffer = true;
		renderTarget = 0;
		layerCoordinate = 0;
		startLayer = 0;
		endLayer = 0;
		geometryTarget = 0;
		float buffer1Array[] = {
			-1, -1, 0,		1, -1, 0,		1, 1, 0,
			-1, -1, 0,		1, 1, 0,		-1, 1, 0
		};
		float buffer2Array[] = {
			0, 0,		1, 0,		1, 1,
			0, 0,		1, 1,		0, 1
		};
		vector<float> buffer1(buffer1Array, buffer1Array + 18);
		vector<float> buffer2(buffer2Array, buffer2Array + 12);
		backgroundQuadMesh = new BufferedMesh("backgroundQuadMesh");
		backgroundQuadMesh->setFaceSize(3);
		backgroundQuadMesh->addBuffer("vertices",buffer1,3);
		backgroundQuadMesh->addBuffer("texcoords",buffer2,2);
		backgroundQuadMesh->load();
		backgroundQuad = new RenderObject("sx.engine.core.Pass.cpp.backgroundQuad");
		backgroundQuad->setMesh(*backgroundQuadMesh);
		backgroundQuad->load();
	}

	Pass::~Pass() {
		delete backgroundQuadMesh;
		delete backgroundQuad;
	}

	void Pass::load() {
	}

	bool Pass::isLoaded() const {
		return true;
	}

	void Pass::setVisible(bool visible) {
		this->visible = visible;
	}

	bool Pass::isVisible() const {
		return visible;
	}

	void Pass::setShader(Shader *shader) {
		this->shader = shader;
	}

	Shader *Pass::getShader() {
		return shader;
	}

	void Pass::addRenderObject(RenderObject &object) {
		const string &objID = object.getID();
		renderObjects[objID] = &object;

		SXout(LogMarkup("Pass::addRenderObject"));
		SXout((unsigned int)renderObjects.size());
	}

	void Pass::removeRenderObject(const string &id) {
		unordered_map<string,RenderObject *>::iterator iter = renderObjects.find(id);
		if(iter != renderObjects.end()) {
			renderObjects.erase(iter);
		}

		SXout(LogMarkup("Pass::removeRenderObject"));
		SXout((unsigned int)renderObjects.size());
	}

	void Pass::removeRenderObjects() {
		renderObjects.clear();

		SXout(LogMarkup("Pass::removeRenderObjects"));
		SXout((unsigned int)renderObjects.size());
	}

	void Pass::useBackgroundQuad(bool use) {
		if(use) {
			unordered_map<string,RenderObject *>::iterator iter = renderObjects.find(backgroundQuad->getID());
			if(iter == renderObjects.end()) {
				//the id of backgroundQuad is still free
				//hence add backgroundQuad to renderObjects
				renderObjects[backgroundQuad->getID()] = backgroundQuad;
			}
		} else {
			unordered_map<string,RenderObject *>::iterator iter = renderObjects.find(backgroundQuad->getID());
			if(iter != renderObjects.end()) {
				//a RenderObject with the id of backgroundQuad
				//is part of renderObjects
				RenderObject *found = (*iter).second;
				if(found == backgroundQuad) {
					//only remove the found RenderObject, if it's
					//RenderObject backgroundQuad
					renderObjects.erase(iter);
				}
			}
		}
		SXout(LogMarkup("Pass::useBackgroundQuad"));
		SXout((unsigned int)renderObjects.size());
	}

	void Pass::addTempRenderObjects(const vector<RenderObject *> &temp) {
		if(temp.size() == 0) {
			return;
		}
		tempRenderObjects.push_back(vector<RenderObject *>());
		vector<RenderObject *> &rObjs = tempRenderObjects[tempRenderObjects.size() - 1];
		for(vector<RenderObject *>::const_iterator iter = temp.begin() ; iter != temp.end() ; iter++) {
			rObjs.push_back((*iter));
		}
	}

	vector<RenderObject *> &Pass::getTempRenderObjects() {
		tempRenderObjects.push_back(vector<RenderObject *>());
		return tempRenderObjects[tempRenderObjects.size() - 1];
	}

	void Pass::addUniform(Uniform &uniform) {
		const string &name = uniform.getUniformName(id);
		uniforms[name] = &uniform;

		SXout(LogMarkup("Pass::addUniform"));
		SXout((unsigned int)uniforms.size());
	}

	void Pass::removeUniform(const string &name) {
		unordered_map<string,Uniform *>::iterator iter = uniforms.find(name);
		if(iter != uniforms.end()) {
			uniforms.erase(iter);
		}

		SXout(LogMarkup("Pass::removeUniform"));
		SXout((unsigned int)uniforms.size());
	}

	void Pass::removeUniforms() {
		uniforms.clear();

		SXout(LogMarkup("Pass::removeUniforms"));
		SXout((unsigned int)uniforms.size());
	}

	void Pass::setClearColorBuffer(bool clear) {
		clearColorbuffer = clear;
	}

	bool Pass::isClearingColorBuffer() const {
		return clearColorbuffer;
	}

	void Pass::setWireframe(bool w) {
		this->wireframe = w;
	}
	
	bool Pass::isWireframe() const {
		return wireframe;
	}

	void Pass::setClearDepthBuffer(bool clear) {
		clearDepthbuffer = clear;
	}

	bool Pass::isClearingDepthBuffer() const {
		return clearDepthbuffer;
	}

	void Pass::setFragmentBlendFactor(BlendFactor factor) {
		this->fragmentBlendFactor = factor;
	}

	BlendFactor Pass::getFragmentBlendFactor() const {
		return fragmentBlendFactor;
	}

	void Pass::setTargetBlendFactor(BlendFactor factor) {
		this->targetBlendFactor = factor;
	}

	BlendFactor Pass::getTargetBlendFactor() const {
		return targetBlendFactor;
	}

	void Pass::setDepthTest(DepthTest test) {
		this->depthTest = test;
	}

	DepthTest Pass::getDepthTest() {
		return depthTest;
	}

	void Pass::setWriteDepthBuffer(bool write) {
		this->writeDepthBuffer = write;
	}

	bool Pass::isWritingDepthBuffer() {
		return writeDepthBuffer;
	}

	void Pass::setOutput(RenderTarget &target, vector<Texture *> t) {
		renderTarget = &target;
		targets.clear();
		volumeTargets.clear();
		for(vector<Texture *>::iterator iter = t.begin() ; iter != t.end() ; iter++) {
			targets.push_back((*iter));
		}
	}

	void Pass::setOutput(RenderTarget &target, vector<Volume *> t) {
		renderTarget = &target;
		targets.clear();
		volumeTargets.clear();
		for(vector<Volume *>::iterator iter = t.begin() ; iter != t.end() ; iter++) {
			volumeTargets.push_back((*iter));
		}
	}

	void Pass::setOutput(BufferedMesh *target) {
		geometryTarget = target;
	}

	void Pass::setLayerCoordinate(UniformFloat *layerCoordinate) {
		this->layerCoordinate = layerCoordinate;

		SXout(LogMarkup("Pass::setLayerCoordinate"));
	}

	void Pass::setStartLayer(UniformFloat *startLayer) {
		this->startLayer = startLayer;

		SXout(LogMarkup("Pass::setStartLayer"));
	}

	void Pass::setEndLayer(UniformFloat *endLayer) {
		this->endLayer = endLayer;
	
		SXout(LogMarkup("Pass::setEndLayer"));
	}

	void Pass::render() {
		if(!visible || (renderTarget == 0 && geometryTarget == 0)) {
			//invisible passes are not rendered
			//passes without target are not rendered
			SXout(LogMarkup("Pass::render-discard"));
			SXout("discard");
			return;
		}
		if(volumeTargets.size() > 0) {
			//check 3D texture targets for integrity
			int layercount = volumeTargets[0]->getDepth();
			if(layercount < 1) {
				SXout(LogMarkup("Pass::render-discard"));
				SXout("3D texture targets have at least one layer");
				return;
			}
			for(vector<Volume *>::iterator iter = volumeTargets.begin() ; iter != volumeTargets.end() ; iter++) {
				if(layercount != (*iter)->getDepth()) {
					SXout(LogMarkup("Pass::render-discard"));
					SXout("3D texture targets must have the same amount of layers");
					return;
				}
			}
		}
		Shader *beginRenderShader = shader;
		if(beginRenderShader == 0) {
			//problem with design: currently rendertarget requires a shader
			//workaround:
			//no shader used by pass ->
			//use first render object
			//for beginRender
			Shader *robjShader = 0;
			for(unordered_map<string,RenderObject *>::iterator iter = renderObjects.begin() ; iter != renderObjects.end() ; iter++) {
				Shader *rShader = (*iter).second->getShader();
				if(rShader != 0) {
					beginRenderShader = rShader;
					break;
				}
			}
			if(beginRenderShader == 0) {
				//no shader found yet, continue search
				for(vector<vector<RenderObject *>>::iterator iter = tempRenderObjects.begin() ; iter != tempRenderObjects.end() ; iter++) {
					for(vector<RenderObject *>::iterator rIter = (*iter).begin() ; rIter != (*iter).end() ; rIter++) {
						Shader *rShader = (*rIter)->getShader();
						if(rShader != 0) {
							beginRenderShader = rShader;
							break;
						}
					}
					if(beginRenderShader != 0) {
						break;
					}
				}
			}
			if(beginRenderShader == 0) {
				//no shaders present at all
				// -> nothing can be rendered
				SXout(LogMarkup("Pass::render-discard"));
				SXout("discard");

				return;
			}
		}

		//apply wireframe
		applyWireframe(wireframe);
		//apply blend factors
		RenderTarget::applyBlendFactors(fragmentBlendFactor, targetBlendFactor);
		//apply depth test
		RenderTarget::applyDepthTest(depthTest);
		//enable/disable writing to deptbuffer, depending on writeDepthBuffer
		RenderTarget::applyWriteDepthBuffer(writeDepthBuffer);
		
		if(shader != 0) {
			//bind uniforms
			shader->use();
			for(unordered_map<string,Uniform *>::iterator iter = uniforms.begin() ; iter != uniforms.end() ; iter++) {
				(*iter).second->use(*shader,id);
			}
			shader->lockTextureSlots();
		}
		Shader *usedShader = shader;

		//startlayer has a positive value iff
		//pass is rendering to a 3D texture
		int startlayer = -1;
		int endlayer = -1;
		float layercount = 1.0f;
		float deltalayer = 0.0f;
		float halfdeltalayer = 0.0f;
		if(volumeTargets.size() > 0) {
			//prepare rendering to the layers of
			//a 3D texture
			float slayer = -1.0f;
			float elayer = (float)volumeTargets[0]->getDepth();
			if(startLayer != 0) {
				(*startLayer) >> slayer;
			}
			if(endLayer != 0) {
				(*endLayer) >> elayer;
			}
			startlayer = max(0,(int)floor(slayer));
			endlayer = min(volumeTargets[0]->getDepth() - 1,max((int)floor(elayer),startlayer));
			layercount = (float)volumeTargets[0]->getDepth();
			deltalayer = 1.0f/layercount;
			halfdeltalayer = deltalayer / 2.0f;
		}

		if(geometryTarget != 0 && shader != 0 && shader->isTransformFeedback()) {
			//a geometry target is used, and a shader with transformfeedback
			//output variables is present
			geometryTarget->beginCapture(*shader);
		}

		//loop for each layer of the target
		for(int currentlayer = startlayer ; currentlayer <= endlayer ; currentlayer++) {
			if(layerCoordinate != 0) {
				//calculate coordinate of layer
				if(startlayer < 0) {
					//2D rendertarget
					//hence use zero for the layerCoordinate
					(*layerCoordinate) << 0.0f;
				} else {
					//3D layer coordinate
					//layercoordinates are elements of interval [0,1]
					//the first layer is adressed by layercoordinate 1/(layercount * 2)
					//the n. layer is adressed by layercoordinate 1/(layercount * 2) + (n-1) / layercount
					(*layerCoordinate) << halfdeltalayer + ((float)currentlayer) * deltalayer;
				}
				
				//shader uses a layer coordinate
				if(shader != 0) {
					shader->use();
					layerCoordinate->use(*shader,id);
				}
			}

			//set rendertarget
			if(renderTarget != 0 && startlayer < 0) {
				//2D target
				renderTarget->beginRender(*beginRenderShader,targets,id);
			} else if(renderTarget != 0) {
				//3D target, select current layer
				renderTarget->beginRender(*beginRenderShader,volumeTargets,currentlayer,id);
			}
			//set viewport, and clear buffers, if necessary
			if(renderTarget != 0) {
				renderTarget->setViewport();

				if(clearDepthbuffer && clearColorbuffer) {
					renderTarget->clearTarget();

					SXout(LogMarkup("Pass::render-clear"));
					SXout("clear(depth|color)");
				} else if(clearDepthbuffer) {
					renderTarget->clearDepthBuffer();

					SXout(LogMarkup("Pass::render-clear"));
					SXout("clear(depth)");
				} else if(clearColorbuffer) {
					renderTarget->clearColorBuffer();

					SXout(LogMarkup("Pass::render-clear"));
					SXout("clear(color)");
				}
			}

			//render renderObjects
			for(unordered_map<string,RenderObject *>::iterator iter = renderObjects.begin() ; iter != renderObjects.end() ; iter++) {
				RenderObject *object = (*iter).second;
				if(!object->isVisible()) {
					//invisible object -> nothing needs
					//to be rendered
					continue;
				}
				if(object->getShader() != 0) {
					//renderobject has its own shader
					Shader *oldShader = usedShader;
					usedShader = object->getShader();
					if(usedShader != oldShader) {
						//another shader -> bind it
						usedShader->use();
						for(unordered_map<string,Uniform *>::iterator uIter = uniforms.begin() ; uIter != uniforms.end() ; uIter++) {
							(*uIter).second->use(*usedShader,id);
						}
					}
					if(layerCoordinate != 0 && (usedShader != oldShader || iter == renderObjects.begin())) {
						//shader uses a layer coordinate
						layerCoordinate->use(*usedShader,id);
					}
					usedShader->lockTextureSlots();
				} else {
					Shader *oldShader = usedShader;
					usedShader = shader;
					if(oldShader != usedShader && usedShader != 0) {
						//another shader -> bind it
						usedShader->use();
					}
				}

				if(usedShader != 0) {
					//rendering can only be performed
					//with a shader

					object->render(usedShader);

					//get rid of renderobject's textures
					usedShader->freeTextureSlots();
					if(usedShader != shader) {
						//make an exception for shader,
						//as its variables are used throughout render()
						usedShader->unlockTextureSlots();
					}
				}
			}

			if(renderTarget != 0 && startlayer >= 0) {
				//end rendering when rendering to a 3D texture
				//to proceed with the next layer
				renderTarget->endRender();
			}
		}

		//render temporary renderobjects
		//loop for each layer of the target
		for(int currentlayer = startlayer ; currentlayer <= endlayer ; currentlayer++) {
			if(startlayer >= 0) {
				//only 3D texture require a repeated setup of the rendertarget
				if(layerCoordinate != 0) {
					//3D layer coordinate
					//layercoordinates are elements of interval [0,1]
					//the first layer is adressed by layercoordinate 1/(layercount * 2)
					//the n. layer is adressed by layercoordinate 1/(layercount * 2) + (n-1) / layercount
					(*layerCoordinate) << halfdeltalayer + ((float)currentlayer) * deltalayer;
					
					//shader uses a layer coordinate
					if(shader != 0) {
						shader->use();
						layerCoordinate->use(*shader,id);
					}
				}

				//3D target, select current layer
				if(renderTarget != 0) {
					renderTarget->beginRender(*beginRenderShader,volumeTargets,currentlayer,id);
				
					//set viewport
					renderTarget->setViewport();
				}
			}

			for(vector<vector<RenderObject *>>::iterator iter = tempRenderObjects.begin() ; iter != tempRenderObjects.end() ; iter++) {
				for(vector<RenderObject *>::iterator rIter = (*iter).begin() ; rIter != (*iter).end() ; rIter++) {
					RenderObject *object = (*rIter);
					if(!object->isVisible()) {
						//object invisible -> nothing needs to be rendered
						continue;
					}
					if(object->getShader() != 0) {
						//renderobject has its own shader
						Shader *oldShader = usedShader;
						usedShader = object->getShader();
						if(usedShader != oldShader) {
							//another shader -> bind it
							usedShader->use();
							for(unordered_map<string,Uniform *>::iterator uIter = uniforms.begin() ; uIter != uniforms.end() ; uIter++) {
								(*uIter).second->use(*usedShader,id);
							}
						}
						if(layerCoordinate != 0 && (usedShader != oldShader || iter == tempRenderObjects.begin())) {
							//shader uses a layer coordinate
							layerCoordinate->use(*usedShader,id);
						}
						usedShader->lockTextureSlots();
					} else {
						Shader *oldShader = usedShader;
						usedShader = shader;
						if(oldShader != usedShader && usedShader != 0) {
							//another shader -> bind it
							usedShader->use();
						}
					}

					if(usedShader != 0) {
						//rendering can only be performed
						//with a shader

						object->render(usedShader);

						//get rid of renderobject's textures
						usedShader->freeTextureSlots();
						if(usedShader != shader) {
							//make an exception for shader,
							//as its variables are used throughout render()
							usedShader->unlockTextureSlots();
						}
					}
				}
			}

			if(renderTarget != 0 && startlayer >= 0) {
				//end rendering when rendering to a 3D texture
				//to proceed with the next layer
				renderTarget->endRender();
			}
		}

		if(renderTarget != 0 && startlayer < 0) {
			//if a 2D target is used,
			//the endRender() call has not
			//been called yet
			renderTarget->endRender();
		}

		if(geometryTarget != 0 && shader != 0 && shader->isTransformFeedback()) {
			//a geometry target is used, and a shader with transformfeedback
			//output variables is present
			geometryTarget->endCapture(*shader);
		}

		if(shader != 0) {
			shader->unlockTextureSlots();
		}

		//set standard state

		//apply wireframe
		applyWireframe(false);
		//apply blend factors
		RenderTarget::applyBlendFactors(ONE, ZERO);
		//apply depth test
		RenderTarget::applyDepthTest(ACCEPT_LESS);
		//enable/disable writing to deptbuffer, depending on writeDepthBuffer
		RenderTarget::applyWriteDepthBuffer(true);

		//tempRenderObjects are rendered only once
		tempRenderObjects.clear();
	}

	void Pass::execute(vector<string> &input) {
		render();
	}

}

#endif