#ifndef _SX_ENGINE_CORE_VOLUME_CPP_
#define _SX_ENGINE_CORE_VOLUME_CPP_

/**
 * 3d texture class
 * (c) 2013 by Tristan Bauer
 */
#include <sx/SXCore.h>
#include <sx/SX.h>
#include <sx/Log4SX.h>
#include <sx/SXParser.h>
#include <sstream>

namespace sx {

	Volume::Volume(const string &id): Uniform(id) {
		loaded = false;
		newWidth = -1;
		newHeight = -1;
		newDepth = -1;
		newPixelFormat = FLOAT_RGBA;
		width = -1;
		height = -1;
		depth = -1;
		pixelFormat = FLOAT_RGBA;
	}

	Volume::~Volume() {
		unload();
	}

	void Volume::unload() {
		if(loaded) {
			SXGL()glDeleteTextures(1, &texture);
			loaded = false;
		}
	}

	void Volume::load() {
		//path not implemented yet
		//if(newPath.size() == 0 && (newWidth <= 0 || newHeight <= 0)) {
		if(newPaths.size() == 0 && (newWidth <= 0 || newHeight <= 0 || newDepth <= 0)) {
			//no changes specified, hence don't
			//do anything
			return;
		}
		unload();

		if(newPaths.size() > 0) {
			//a path has been specified
			//, load image from path
			Texture first("tex");
			first.setPath(newPaths[0]);
			first.load();
			if(!first.isLoaded()) {
				//image not loaded
				//clear resource requests
				newPaths.clear();
				width = height = depth = newWidth = newHeight = newDepth = -1;
				newByteBuffer.clear();
				newFloatBuffer.clear();
				return;
			}
			width = (int)first.getWidth();
			height = (int)first.getHeight();
			depth = (int)newPaths.size();
			pixelFormat = first.getPixelFormat();

			//create 3D texture
			
			SXGL()glEnable(GL_TEXTURE_3D);
			SXGL()glGenTextures(1, (GLuint *)&texture);
			SXGL()glBindTexture(GL_TEXTURE_3D, texture);
			if(pixelFormat == BYTE_RGBA) {
				SXGL()glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA, width, height, depth, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
			} else if(pixelFormat == FLOAT16_RGBA){
				SXGL()glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA16F, width, height, depth, 0, GL_RGBA, GL_FLOAT, 0);
			} else if(pixelFormat == FLOAT_RGBA) {
				SXGL()glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA, width, height, depth, 0, GL_RGBA, GL_FLOAT, 0);
			}
			SXGL()glGenerateMipmap(GL_TEXTURE_3D);
			SXGL()glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			SXGL()glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			GLfloat aLargest;
			SXGL()glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &aLargest);
			SXGL()glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MAX_ANISOTROPY_EXT, aLargest);
			SXGL()glBindTexture(GL_TEXTURE_3D, 0);

			//init resources to render the layers into
			//the 3D texture

			//other resources interacting with
			//volumes during copying the images
			//into the 3D texture need the volume
			//to be loaded
			//hence set loaded true, and change
			//the value to false, if errors occure
			loaded = true;

			RenderTarget target("target");
			target.setWidth(width);
			target.setHeight(height);
			target.load();

			string frag_part1 =
				"#version 400 core				\n"
				"in vec2 vtc;					\n"
				"out vec4 ";
			string frag_part2 = 
				";				\n"
				"uniform sampler2D tex;			\n"
				"								\n"
				"void main() {					\n"
				"	"
				;
			string frag_part3 = 
				" = texture2D(tex,vtc);	\n"
				"}								\n"
				;
			//use idToken as the name of the rendertarget
			//in the fragmentshader
			stringstream frag_code;
			frag_code << frag_part1 << idToken << frag_part2 << idToken << frag_part3;
			Shader shader("shader");
			shader.addShader(
				"#version 400 core			\n"
				"in vec4 vertices;			\n"
				"in vec2 texcoords;			\n"
				"out vec2 vtc;				\n"
				"							\n"
				"void main() {				\n"
				"	gl_Position = vertices; \n"
				"	vtc = texcoords;		\n"
				"}							\n"
				,VERTEX);
			shader.addShader(
				frag_code.str()
				,FRAGMENT);
			shader.load();

			float meshVArray[] = {
			-1,-1,0,1,
			1,-1,0,1,
			1,1,0,1,
			-1,-1,0,1,
			1,1,0,1,
			-1,1,0,1
			};
			float meshTArray[] = {
			0,0,
			1,0,
			1,1,
			0,0,
			1,1,
			0,1
			};
			vector<float> meshVList(meshVArray,meshVArray + 24);
			vector<float> meshTList(meshTArray,meshTArray + 12);
			BufferedMesh mesh("mesh");
			mesh.addBuffer("vertices", meshVList, 4);
			mesh.addBuffer("texcoords",meshTList, 2);
			mesh.setFaceSize(3);
			mesh.load();

			int layer = 0;
			for(vector<string>::iterator iter = newPaths.begin() ; iter != newPaths.end() ; iter++) {
				Texture source("tex");
				if(iter != newPaths.begin()) {
					//first texture is loaded already
					source.setPath((*iter));
					source.load();
					if(!source.isLoaded()) {
						//image can't be loaded
						//clear resource requests
						loaded = false;
						SXGL()glDeleteTextures(1, &texture);
						newPaths.clear();
						width = height = depth = newWidth = newHeight = newDepth = -1;
						newByteBuffer.clear();
						newFloatBuffer.clear();
						return;
					}
				}
				shader.use();
				if(iter == newPaths.begin()) {
					//first layer -> use texture first for layer
					first.use(shader,"vol4");
				} else {
					//otherwise use texture source
					source.use(shader,"vol4");
				}

				vector<Volume *> volumes;
				volumes.push_back(this);
				target.beginRender(shader,volumes,layer,"vol4");
				target.setViewport();
				RenderTarget::clearTarget();

				mesh.render(shader);

				target.endRender();

				layer++;
			}

			//clear resource requests
			newPaths.clear();
			newWidth = newHeight = newDepth = -1;
			newByteBuffer.clear();
			newFloatBuffer.clear();
			return;
		} else {
			//create image with specified width, height and depth
			width = newWidth;
			height = newHeight;
			depth = newDepth;
			pixelFormat = newPixelFormat;
			//upload data to vram
			SXGL()glEnable(GL_TEXTURE_3D);
			SXGL()glGenTextures(1, (GLuint *)&texture);
			SXGL()glBindTexture(GL_TEXTURE_3D, texture);
			if(pixelFormat == BYTE_RGBA) {
				//texture with color channels made of unsigned bytes
				if(newByteBuffer.size() == width * height * depth * 4) {
					//byte buffer's size exactly fits texture format
					//, hence use byte buffer as data source
					unsigned char *data = new unsigned char[width * height * depth * 4];
					unsigned int index = 0;
					for(vector<unsigned char>::iterator iter = newByteBuffer.begin() ; iter != newByteBuffer.end() ; iter++) {
						data[index] = (*iter);
						index++;
					}
					SXGL()glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA, width, height, depth, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
					delete data;
				} else {
					//byte buffer's size doesn't fit texture format
					//, hence create a texture without uploading data
					SXGL()glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA, width, height, depth, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
				}
			} else if(pixelFormat == FLOAT16_RGBA) {
				//texture with color channels made of 16 bit floating point numbers
				if(newFloatBuffer.size() == width * height * depth * 4) {
					//float buffer's size exactly fits texture format
					//, hence use byte buffer as data source
					float *data = new float[width * height * depth * 4];
					unsigned int index = 0;
					for(vector<float>::iterator iter = newFloatBuffer.begin() ; iter != newFloatBuffer.end() ; iter++) {
						data[index] = (*iter);
						index++;
					}
					SXGL()glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA16F, width, height, depth, 0, GL_RGBA, GL_FLOAT, data);
					delete data;
				} else {
					//float buffer's size doesn't fit texture format
					//, hence create a texture without uploading data
					SXGL()glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA16F, width, height, depth, 0, GL_RGBA, GL_FLOAT, 0);
				}
			} else if(pixelFormat == FLOAT_RGBA) {
				//texture with color channels made of floating point numbers
				if(newFloatBuffer.size() == width * height * depth * 4) {
					//float buffer's size exactly fits texture format
					//, hence use byte buffer as data source
					float *data = new float[width * height * depth * 4];
					unsigned int index = 0;
					for(vector<float>::iterator iter = newFloatBuffer.begin() ; iter != newFloatBuffer.end() ; iter++) {
						data[index] = (*iter);
						index++;
					}
					SXGL()glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA32F, width, height, depth, 0, GL_RGBA, GL_FLOAT, data);
					delete data;
				} else {
					//float buffer's size doesn't fit texture format
					//, hence create a texture without uploading data
					SXGL()glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA32F, width, height, depth, 0, GL_RGBA, GL_FLOAT, 0);
				}
			}
			SXGL()glGenerateMipmap(GL_TEXTURE_3D);
			SXGL()glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			SXGL()glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			GLfloat aLargest;
			SXGL()glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &aLargest);
			SXGL()glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MAX_ANISOTROPY_EXT, aLargest);
			SXGL()glBindTexture(GL_TEXTURE_3D, 0);
			loaded = true;
			//clear resource requests
			newPaths.clear();
			newWidth = newHeight = newDepth = -1;
			newByteBuffer.clear();
			newFloatBuffer.clear();
			return;
		}
	}

	bool Volume::isLoaded() const {
		return loaded;
	}

	void Volume::save(const string &filenamepart) {
		if(!loaded) {
			//volume has to be loaded
			stringstream errmsg;
			errmsg << "Error: Volume " << id << " can't be saved, because the volume is not loaded" << endl;
			throw Exception(errmsg.str(),EX_NODATA);
		}
		if(filenamepart.size() < 4) {
			stringstream errmsg;
			errmsg << "Error: Volume " << id << " supports the save operation only for bmp, jpg, jpeg or png files" << endl;
			throw Exception(errmsg.str(),EX_NODATA);
		}
		string firstpart = filenamepart.substr(0,filenamepart.size()-4);
		string extension = filenamepart.substr(filenamepart.size()-4);
		if(extension.compare(".bmp") != 0 && extension.compare(".jpg") != 0 && extension.compare(".png") != 0) {
			if(filenamepart.size() >= 5) {
				//test, if file has extension .jpeg
				firstpart = filenamepart.substr(0,filenamepart.size()-5);
				extension = filenamepart.substr(filenamepart.size()-5);
			}
			if(extension.compare(".jpeg") != 0) {
				stringstream errmsg;
				errmsg << "Error: Volume " << id << " supports the save operation only for bmp, jpg, jpeg or png files" << endl;
				throw Exception(errmsg.str(),EX_NODATA);
			}
		}
		SXGL()glBindTexture(GL_TEXTURE_3D, texture);
		if(pixelFormat == BYTE_RGBA) {
			unsigned char *data = new unsigned char[width * height * depth * 4];
			SXGL()glGetTexImage(GL_TEXTURE_3D, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
			for(unsigned int layer = 0 ; layer < (unsigned int)depth ; layer++) {
				unsigned int layerAddress = layer*width*height*4;
				Bitmap image(width,height);
				unsigned char *img = image.getData();
				for(unsigned int i=0 ; i<(unsigned int)width*height*4 ; i++) {
					img[i] = data[layerAddress + i];
				}
				try {
					stringstream filename;
					filename << firstpart << layer << extension;
					image.save(filename.str());
				} catch(Exception &e) {
					delete data;
					throw e;
				}
			}
			delete data;
		} else if(pixelFormat == FLOAT16_RGBA || pixelFormat == FLOAT_RGBA) {
			float *data = new float[width * height * depth * 4];
			SXGL()glGetTexImage(GL_TEXTURE_3D, 0, GL_RGBA, GL_FLOAT, data);
			for(unsigned int layer=0 ; layer < (unsigned int)depth ; layer++) {
				unsigned int layerAddress = layer*width*height*4;
				Bitmap image(width,height,16);
				float *img = (float *)image.getData();
				for(unsigned int i=0 ; i<(unsigned int)width*height*4 ; i++) {
					img[i] = data[layerAddress + i];
				}
				try {
					stringstream filename;
					filename << firstpart << layer << extension;
					image.save(filename.str());
				} catch(Exception &e) {
					delete data;
					throw e;
				}
			}
			delete data;
		}
	}

	void Volume::use(Shader &shader, const string &id) {
		if(!loaded || !shader.isLoaded()) {
			//texture or shader isn't loaded
			//, hence nothing can be done
			return;
		}
		const string &name = getUniformName(id);
		int loc = shader.getUniformLocation(name);
		if(loc >= 0) {
			//uniform used in shader,
			//hence copy value to uniform
			//use next free slot for the texture (multitexturing requires
			//the textures used simultaneously being associated with unique slots
			//... sadly :(, simply binding them as uniform variables
			//without the redundant slot mechanism would be more elegant)
			unsigned int slot = shader.createTextureSlot();
			SXGL()glActiveTexture(GL_TEXTURE0 + slot);
			SXGL()glBindTexture(GL_TEXTURE_3D, texture);
			SXGL()glUniform1i(loc, slot);
		}
	}

	unsigned int Volume::getTextureID() const {
		return texture;
	}

	void Volume::addPath(const string &path) {
		this->newPaths.push_back(path);
	}

	void Volume::setPaths(const vector<string> &paths) {
		this->newPaths = paths;
	}

	void Volume::setWidth(int width) {
		this->newWidth = width;
	}

	int Volume::getWidth() const {
		return width;
	}

	void Volume::setHeight(int height) {
		this->newHeight = height;
	}

	int Volume::getHeight() const {
		return height;
	}

	void Volume::setDepth(int depth) {
		this->newDepth = depth;
	}

	int Volume::getDepth() {
		return depth;
	}

	void Volume::setPixelFormat(PixelFormat pixelFormat) {
		this->newPixelFormat = pixelFormat;
	}

	PixelFormat Volume::getPixelFormat() const {
		return pixelFormat;
	}

	void Volume::setByteBuffer(const vector<unsigned char> &buffer) {
		newByteBuffer.clear();
		for(vector<unsigned char>::const_iterator iter = buffer.begin() ; iter != buffer.end() ; iter++) {
			newByteBuffer.push_back((*iter));
		}
		newPixelFormat = BYTE_RGBA;
	}

	void Volume::setFloatBuffer(const vector<float> &buffer) {
		newFloatBuffer.clear();
		for(vector<float>::const_iterator iter = buffer.begin() ; iter != buffer.end() ; iter++) {
			newFloatBuffer.push_back((*iter));
		}
		newPixelFormat = FLOAT_RGBA;
	}

}

#endif