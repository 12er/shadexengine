#ifndef _SX_ENGINE_CORE_MESH_CPP_
#define _SX_ENGINE_CORE_MESH_CPP_

/**
 * Mesh class
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SX.h>

namespace sx {

	Mesh::~Mesh() {
	}

	void Mesh::setFaceSize(unsigned int faceSize) {
		this->newFaceSize = faceSize;
	}

	unsigned int Mesh::getFaceSize() const {
		return faceSize;
	}

	unsigned int Mesh::getVertexCount() const {
		return vertexCount;
	}

}

#endif