#ifndef _SX_ENGINE_CORE_SKELETON_CPP_
#define _SX_ENGINE_CORE_SKELETON_CPP_

/**
 * skeleton class
 * (c) 2013 by Tristan Bauer
 */
#include <sx/SXCore.h>
#include <sx/Log4SX.h>
#include <sstream>
#include <algorithm>
using namespace std;

namespace sx {

	void Skeleton::initBoneMap() {
		boneMap.clear();
		skeletonDepth = 1;

		//traverse skeleton
		vector<vector<Bone>::iterator> iterStack;
		iterStack.push_back(bones.begin());
		vector<vector<Bone> *> arrayStack;
		arrayStack.push_back(&bones);
		while(iterStack.size() > 0) {
			vector<Bone>::iterator currIter = iterStack[iterStack.size()-1];
			vector<Bone> *currArray = arrayStack[arrayStack.size()-1];
			if(currIter != currArray->end()) {
				Bone &currBone = (*currIter);

				map<string,Bone *>::iterator searchBoneIter = boneMap.find(currBone.ID);
				if(searchBoneIter != boneMap.end()) {
					//another bone already has id boneID
					stringstream str;
					str << "Error: found multiple bones having ID " << currBone.ID;
					throw Exception(str.str(),EX_AMBIGUOUS);
				}
				//insert this bone into the map
				boneMap[currBone.ID] = &currBone;

				//explore the childbones
				iterStack.push_back(currBone.bones.begin());
				arrayStack.push_back(&currBone.bones);
				//update skeletonDepth
				skeletonDepth = max((unsigned int)skeletonDepth,(unsigned int)iterStack.size());

				//advance to next element
				iterStack[iterStack.size()-2]++;
			} else {
				//processed iterator, pop it
				iterStack.pop_back();
				arrayStack.pop_back();
			}
		}
	}

	void Skeleton::initBoneTransorms() {
		boneTransforms.clear();
		boneTransformsIndices.clear();
		if(bones.size() == 0) {
			//no bone transforms
			//or empty skeleton
			//hence nothing needs to be done
			return;
		}

		//inorder traversal
		//of skeleton
		//stacks for traversal
		vector<Matrix> bindPoseStack;
		bindPoseStack.push_back(skeletonMatrix);
		vector<vector<Bone>::iterator> traversalStack;
		traversalStack.push_back(bones.begin());
		vector<vector<Bone> *> traversalStackArrays;
		traversalStackArrays.push_back(&bones);

		//inorder traversal
		//of skeleton
		unsigned int boneTransformsIndex = 0;
		while(traversalStack.size() != 0) {
			//evaluate top element of traversalStack
			vector<Bone>::iterator current = traversalStack[traversalStack.size()-1];
			vector<Bone> *currentArray = traversalStackArrays[traversalStackArrays.size()-1];
			if(current != currentArray->end()) {
				//consider last element only
				//if it hasn't been iterated through completely
				Bone &currentBone = (*current);
				Matrix &lastMatrix = bindPoseStack[bindPoseStack.size()-1];
				//update inverse bindpose matrix
				Matrix currentMatrix = lastMatrix * currentBone.parentTransform;
				currentBone.inverseBindPoseMatrix = currentMatrix;
				currentBone.inverseBindPoseMatrix.inverse();

				//push the place of the bone in  an inorder traversal
				boneTransformsIndices[currentBone.ID] = boneTransformsIndex;
				boneTransformsIndex++;

				//consider kid elements of current bone
				traversalStack.push_back(currentBone.bones.begin());
				traversalStackArrays.push_back(&currentBone.bones);
				bindPoseStack.push_back(currentMatrix);

				//move to next bone
				traversalStack[traversalStack.size()-2]++;
			} else {
				//iteration completed
				//pop last element
				traversalStack.pop_back();
				traversalStackArrays.pop_back();
				bindPoseStack.pop_back();
			}
		}

		//init for each bone transformation set
		//the bone transformations affecting the current
		//skeleton
		for(map<int,map<string,BoneTransform *> >::iterator transSetIter = boneTransformMap.begin() ; transSetIter != boneTransformMap.end() ; transSetIter++) {
			initBoneTransformSet(transSetIter);
		}
	}

	void Skeleton::initBoneTransformSet(map<int,map<string,BoneTransform *> >::iterator setIter) {
		//precondition: set with setID has not been initialized yet
		int setID = (*setIter).first;
		map<string,BoneTransform *> &currentMap = (*setIter).second;
		boneTransforms[setID] = vector<BoneTransform *>();
		map<int,vector<BoneTransform *> >::iterator currentTransformsIter = boneTransforms.find(setID);
		vector<BoneTransform *> &currentTransforms = (*currentTransformsIter).second;

		//fill in bone transformations
		currentTransforms.reserve(boneTransformsIndices.size());
		for(unsigned int i=0 ; i<boneTransformsIndices.size() ; i++) {
			currentTransforms.push_back(0);
		}
		for(map<string,BoneTransform *>::iterator boneTransformIter = currentMap.begin() ; boneTransformIter != currentMap.end() ; boneTransformIter++) {
			const string &boneID = (*boneTransformIter).first;
			BoneTransform *boneTransform = (*boneTransformIter).second;
			map<string,unsigned int>::iterator indexIter = boneTransformsIndices.find(boneID);
			if(indexIter != boneTransformsIndices.end()) {
				//the boneTransform has a bone associated with it
				//in the current skeleton
				//place the boneTransform at the found index
				unsigned int index = (*indexIter).second;
				currentTransforms[index] = boneTransform;
			} else {
				//the BoneTransform has no bone
				//associated to it. So list the boneID in vector unmatchedBones
				unmatchedBones.push_back(boneID);
				Logger::get() << LogMarkup("Skeleton::load") << Annotation("Skeleton") << Annotation(id) << Annotation(": Warning: a boneTransform's boneID ") << Annotation(boneID) << Annotation(" doesn't have a bone with the same ID");
			}
		}
	}

	Skeleton::Skeleton(const string &id) {
		this->id = id;
		loaded = false;
		skeletonDepth = 0;
	}

	Skeleton::~Skeleton() {
		//delete boneTransforms
		for(map<int,map<string,BoneTransform *> >::iterator setIter = boneTransformMap.begin() ; setIter != boneTransformMap.end() ; setIter++) {
			map<string,BoneTransform *> &setMap = (*setIter).second;
			for(map<string,BoneTransform *>::iterator tIter = setMap.begin() ; tIter != setMap.end() ; tIter++) {
				BoneTransform *t = (*tIter).second;
				delete t;
			}
		}
		unload();
	}

	void Skeleton::addBones(const string &path) {
		newPaths.push_back(path);
	}

	void Skeleton::addBones(BufferedMesh &mesh) {
		newMeshes.push_back(&mesh);
	}

	void Skeleton::setSkeletonMatrix(const Matrix &transform) {
		skeletonMatrix = transform;
	}

	const Matrix &Skeleton::getSkeletonMatrix() const {
		return skeletonMatrix;
	}

	void Skeleton::addRootBone(Bone &bone) {
		newBones.push_back(bone);
	}

	const Bone &Skeleton::getBone(const string &id) const {
		map<string,Bone *>::const_iterator foundBone = boneMap.find(id);
		if(foundBone == boneMap.end()) {
			//no bone with the given id exists
			//hence throw exception
			stringstream str;
			str << "Error: no bone with id " << id << " found";
			throw Exception(str.str(),EX_NODATA);
		}
		return *(*foundBone).second;
	}

	vector<Bone> Skeleton::getRootBones() const {
		vector<Bone> rBones;

		if(loaded) {
			rBones.insert(rBones.end(),bones.begin(),bones.end());
		}

		return rBones;
	}

	void Skeleton::load() {
		if(newPaths.size() == 0 && newMeshes.size() == 0 && newBones.size() == 0) {
			//no changes specified, hence don't
			//do anything
			return;
		}
		unload();

		//add new bones
		for(vector<string>::iterator iter = newPaths.begin() ; iter != newPaths.end() ; iter++) {
			const string path = (*iter);
			try {
				pair<vector<Bone>,Matrix> boneData = parseColladaSkeletonFile(path);
				vector<Bone> &b = boneData.first;
				skeletonMatrix = boneData.second;

				//add bones to skeleton
				for(vector<Bone>::iterator bIter = b.begin() ; bIter != b.end() ; bIter++) {
					Bone &bone = (*bIter);
					bones.push_back(bone);
				}
			} catch(Exception &e) {
				//some bones could not be loaded
				Logger::get() << LogMarkup("Skeleton::load") << Annotation("skeleton ") << Annotation(id) << e.getMessage();

				bones.clear();
				boneMap.clear();
				return;
			}
		}

		for(vector<BufferedMesh *>::iterator iter = newMeshes.begin() ; iter != newMeshes.end() ; iter++) {
			BufferedMesh *mesh = (*iter);
			try {
				if(mesh == 0) {
					throw Exception("Error: tried loading bones from nullpointer",EX_NODATA);
				}
				if(!mesh->isLoaded()) {
					//mesh not loaded yet
					//try to load it
					mesh->load();
					if(!mesh->isLoaded()) {
						//loading mesh did not work
						//there is no use in processing this mesh
						stringstream str;
						str << "Error: could not load skeleton from mesh " << mesh->getID();
						throw Exception(str.str(),EX_NODATA);
					}
				}

				vector<Bone> b = mesh->getSkeleton();
				skeletonMatrix = mesh->getSkeletonMatrix();

				//add bones to skeleton
				for(vector<Bone>::iterator bIter = b.begin() ; bIter != b.end() ; bIter++) {
					Bone &bone = (*bIter);
					bones.push_back(bone);
				}
			} catch(Exception &e) {
				//some bones could not be loaded
				Logger::get() << LogMarkup("Skeleton::load") << Annotation("skeleton ") << Annotation(id) << e.getMessage();

				bones.clear();
				boneMap.clear();
				return;
			}
		}

		bones.insert(bones.end(),newBones.begin(),newBones.end());

		try {
			initBoneMap();
		} catch(Exception &e) {
			//some bones are not unique
			Logger::get() << LogMarkup("Skeleton::load") << Annotation("skeleton ") << Annotation(id) << e.getMessage();

			unmatchedBones.clear();
			bones.clear();
			boneMap.clear();
			return;
		}

		//calculate inverse bind pose matrices
		//and setup bone transforms
		initBoneTransorms();

		//adding new bones done,
		//reset new data
		newPaths.clear();
		newMeshes.clear();
		newBones.clear();
		loaded = true;
	}

	void Skeleton::unload() {
		if(loaded) {
			//stuff has been loaded
			//unload it
			boneMap.clear();
			bones.clear();
			boneTransformsIndices.clear();
			boneTransforms.clear();
			unmatchedBones.clear();
			loaded = false;
			skeletonDepth = 0;
		}
	}

	bool Skeleton::isLoaded() const {
		return loaded;
	}

	vector<string> Skeleton::getUnmatchedBones() const {
		return unmatchedBones;
	}

	void Skeleton::addBoneTransform(int setID, const BoneTransform &transform) {
		map<int,map<string,BoneTransform *> >::iterator setIter = boneTransformMap.find(setID);
		if(setIter != boneTransformMap.end()) {
			//set with setID already exists
			//insert bone transform
			map<string,BoneTransform *> &tSet = (*setIter).second;
			BoneTransform *t = 0;
			map<string,BoneTransform *>::iterator findTransform = tSet.find(transform.boneID);
			if(findTransform == tSet.end()) {
				t = new BoneTransform();
				tSet[transform.boneID] = t;
			} else {
				t = (*findTransform).second;
			}
			t->boneID = transform.boneID;
			t->input = transform.input;
			t->output = transform.output;
			map<string,unsigned int>::iterator indexIter = boneTransformsIndices.find(t->boneID);
			if(indexIter != boneTransformsIndices.end()) {
				//a bone associated to the transform exists
				//insert the transform at the place of the bone in an inorder traversal
				unsigned int index = (*indexIter).second;
				boneTransforms[setID][index] = t;
			} else if(loaded) {
				unmatchedBones.push_back(t->boneID);
				Logger::get() << LogMarkup("Skeleton::addBoneTransform") << Annotation("Skeleton") << Annotation(id) << Annotation(": Warning: a boneTransform's boneID ") << Annotation(t->boneID) << Annotation(" doesn't have a bone with the same ID");
			}
		} else {
			//create a set with setID
			map<string,BoneTransform *> &tMap = boneTransformMap[setID] = map<string,BoneTransform *>();
			BoneTransform *t = 0;
			map<string,BoneTransform *>::iterator findTransform = tMap.find(transform.boneID);
			if(findTransform == tMap.end()) {
				t = new BoneTransform();
				tMap[transform.boneID] = t;
			} else {
				t = (*findTransform).second;
			}
			t->boneID = transform.boneID;
			t->input = transform.input;
			t->output = transform.output;
			setIter = boneTransformMap.find(setID);
			initBoneTransformSet(setIter);
			if(!loaded) {
				unmatchedBones.clear();
			}
		}
	}

	BoneTransform &Skeleton::getBoneTransform(int setID, const string &boneID) {
		map<int,map<string,BoneTransform *> >::iterator setIter = boneTransformMap.find(setID);
		if(setIter == boneTransformMap.end()) {
			//bone transform set with setID does not exist
			stringstream str;
			str << "Error: bone transform set " << setID << " does not exist";
			throw Exception(str.str(),EX_NODATA);
		}
		map<string,BoneTransform *> &tSet = (*setIter).second;
		map<string,BoneTransform *>::iterator transformIter = tSet.find(boneID);
		if(transformIter == tSet.end()) {
			//bone transform with boneID does not exist
			stringstream str;
			str << "Error: bone transform in set " << setID << " with boneID " << boneID << " does not exist";
			throw Exception(str.str(),EX_NODATA);
		}
		return *(*transformIter).second;
	}

	void Skeleton::removeBoneTransform(int setID, const string &boneID) {
		map<int,map<string,BoneTransform *> >::iterator setIter = boneTransformMap.find(setID);
		if(setIter == boneTransformMap.end()) {
			//bone transform set with setID does not exist
			return;
		}
		map<string,BoneTransform *> &tSet = (*setIter).second;
		map<string,BoneTransform *>::iterator transformIter = tSet.find(boneID);
		if(transformIter == tSet.end()) {
			//bone transform with boneID does not exist
			return;
		}
		delete (*transformIter).second;
		tSet.erase(transformIter);
		map<string,unsigned int>::iterator indexIter = boneTransformsIndices.find(boneID);
		if(indexIter != boneTransformsIndices.end()) {
			//the transform is linked in the boneTransforms
			//undo link
			unsigned int index = (*indexIter).second;
			boneTransforms[setID][index] = 0;
		}
		if(tSet.size() == 0) {
			//the transformation set is
			//empty
			//remove it completely
			boneTransformMap.erase(setIter);
			map<int,vector<BoneTransform *> >::iterator btSetIter = boneTransforms.find(setID);
			boneTransforms.erase(btSetIter);
		}
	}

	void Skeleton::removeBoneTransforms(int setID) {
		map<int,map<string,BoneTransform *> >::iterator setIter = boneTransformMap.find(setID);
		if(setIter == boneTransformMap.end()) {
			//bone transform set with setID does not exist
			return;
		}
		//delete BoneTransforms
		map<string,BoneTransform *> &setMap = (*setIter).second;
		for(map<string,BoneTransform *>::iterator iter = setMap.begin() ; iter != setMap.end() ; iter++) {
			BoneTransform *t = (*iter).second;
			delete t;
		}
		boneTransformMap.erase(setIter);
		map<int,vector<BoneTransform *> >::iterator btSetIter = boneTransforms.find(setID);
		boneTransforms.erase(btSetIter);
	}

	void Skeleton::removeBoneTransforms() {
		//delete BoneTransforms
		for(map<int,map<string,BoneTransform *> >::iterator setIter = boneTransformMap.begin() ; setIter != boneTransformMap.end() ; setIter++) {
			map<string,BoneTransform *> &setMap = (*setIter).second;
			for(map<string,BoneTransform *>::iterator tIter = setMap.begin() ; tIter != setMap.end() ; tIter++) {
				BoneTransform *t = (*tIter).second;
				delete t;
			}
		}
		boneTransforms.clear();
		boneTransformMap.clear();
	}

	void Skeleton::updateBoneTransforms() {
		if(!loaded || boneTransformMap.size() == 0 || bones.size() == 0) {
			//no bone transforms
			//or empty skeleton
			//hence nothing needs to be done
			return;
		}
		vector<Matrix> poseStack;
		vector<vector<Bone>::iterator> traversalStack;
		vector<vector<Bone> *> traversalStackArrays;
		//allocate space of stacks in order to save time
		//as push_back would reallocate the vectors as a whole
		//each time the sizes increase
		poseStack.reserve(skeletonDepth);
		traversalStack.reserve(skeletonDepth);
		traversalStackArrays.reserve(skeletonDepth);
		for(map<int,vector<BoneTransform *> >::iterator setIter = boneTransforms.begin() ; setIter != boneTransforms.end() ; setIter++) {
			vector<BoneTransform *> &tSet = (*setIter).second;

			//inorder traversal
			//of skeleton
			//stacks for traversal
			vector<BoneTransform *>::iterator tIter = tSet.begin();
			poseStack.clear();
			poseStack.push_back(skeletonMatrix);
			traversalStack.clear();
			traversalStack.push_back(bones.begin());
			traversalStackArrays.clear();
			traversalStackArrays.push_back(&bones);
			//inorder traversal
			//of skeleton
			while(traversalStack.size() > 0) {
				vector<Bone>::iterator boneIter = traversalStack[traversalStack.size()-1];
				vector<Bone> *boneArray = traversalStackArrays[traversalStackArrays.size()-1];
				if(boneIter != boneArray->end()) {
					//iterator has not reached end yet
					Bone &bone = (*boneIter);
					Matrix &lastMatrix = poseStack[poseStack.size()-1];
					Matrix currentMatrix = lastMatrix * bone.parentTransform;					
					BoneTransform *boneTransform = (*tIter);
					if(boneTransform != 0) {
						if(boneTransform->input != 0) {
							//apply change to bone with its join in the origin
							currentMatrix = currentMatrix * (*boneTransform->input);
						}
						if(boneTransform->output != 0) {
							//calculate transformation of the vertices
							//bound to this bone from joint space into model space
							(*boneTransform->output) = currentMatrix * bone.inverseBindPoseMatrix;
						}
					}

					//consider childhood of bone
					traversalStack.push_back(bone.bones.begin());
					traversalStackArrays.push_back(&bone.bones);
					poseStack.push_back(currentMatrix);

					//move to next bone
					traversalStack[traversalStack.size()-2]++;
					//move to next transform
					tIter++;
				} else {
					//iterator reached end, remove it
					traversalStack.pop_back();
					traversalStackArrays.pop_back();
					poseStack.pop_back();
				}
			}
		}
	}

	void Skeleton::execute(vector<string> &input) {
		updateBoneTransforms();
	}

}

#endif
