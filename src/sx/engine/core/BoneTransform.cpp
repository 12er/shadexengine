#ifndef _SX_ENGINE_CORE_BONETRANSFORM_CPP_
#define _SX_ENGINE_CORE_BONETRANSFORM_CPP_

/**
 * Bone Transform class
 * (c) 2013 by Tristan Bauer
 */
#include <sx/SXCore.h>
#include <sx/Log4SX.h>

namespace sx {

	BoneTransform::BoneTransform() {
		input = 0;
		output = 0;
	}

}

#endif