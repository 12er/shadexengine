#ifndef _SX_ENGINE_CORE_SXOPENGL_CPP_
#define _SX_ENGINE_CORE_SXOPENGL_CPP_

/**
 * SX OpenGL manager
 * (c) 2015 by Tristan Bauer
 */
#include <sx/SXCore.h>

namespace sx {

	QOpenGLFunctions_4_2_Core *SXOpenGL::sxgl = 0;

	bool SXOpenGL::init() {
#ifdef _PLATFORM_WINDOWS_
		if (sxgl == 0) {
#endif
			sxgl = new QOpenGLFunctions_4_2_Core();
#ifdef _PLATFORM_WINDOWS_
		}
#endif
		return sxgl->initializeOpenGLFunctions();
	}

	QOpenGLFunctions_4_2_Core &SXOpenGL::get() {
		if (sxgl == 0) {
			sxgl = new QOpenGLFunctions_4_2_Core();
			sxgl->initializeOpenGLFunctions();
		}
		return *sxgl;
	}

}

#endif
