#ifndef _SX_ENGINE_CORE_AUDIOLISTENER_CPP_
#define _SX_ENGINE_CORE_AUDIOLISTENER_CPP_

/**
 * Audio Listener class
 * (c) 2013 by Tristan Bauer
 */
#include <sx/SXCore.h>
#include <sx/Log4SX.h>
#include <sx/Exception.h>
#include <sstream>
using namespace std;

namespace sx {

	AudioListener::AudioListener(const string &id) {
		this->id = id;
		this->position = 0;
		this->view = 0;
		this->up = 0;
		this->velocity = 0;
		this->volume = 0;
	}

	AudioListener::~AudioListener() {
		unload();
	}

	void AudioListener::load() {
		//nothing to do except trying to update
		update();
	}

	void AudioListener::unload() {
		//nothing to do
	}

	bool AudioListener::isLoaded() const {
		return true;
	}

	void AudioListener::setPosition(const UniformVector *pos) {
		position = pos;
		if(position != 0 && AudioObject::isInitialized()) {
			alListenerfv(AL_POSITION,position->elements);
		} else if(AudioObject::isInitialized()) {
			float p[] = {0,0,0};
			alListenerfv(AL_POSITION,p);
		}
	}

	void AudioListener::setView(const UniformVector *view) {
		this->view = view;
		if(AudioObject::isInitialized()) {
			float ori[6] = {1,0,0, 0,0,1};
			if(view != 0) {
				for(unsigned int i=0 ; i<3 ; i++) {
					ori[i] = (*view)[i];
				}
			}
			if(up != 0) {
				for(unsigned int i=0 ; i<3 ; i++) {
					ori[i+3] = (*up)[i];
				}
			}
			alListenerfv(AL_ORIENTATION,ori);
		}
	}

	void AudioListener::setUp(const UniformVector *up) {
		this->up = up;
		if(AudioObject::isInitialized()) {
			float ori[6] = {1,0,0, 0,0,1};
			if(view != 0) {
				for(unsigned int i=0 ; i<3 ; i++) {
					ori[i] = (*view)[i];
				}
			}
			if(up != 0) {
				for(unsigned int i=0 ; i<3 ; i++) {
					ori[i+3] = (*up)[i];
				}
			}
			alListenerfv(AL_ORIENTATION,ori);
		}
	}

	void AudioListener::setVelocity(const UniformVector *vel) {
		velocity = vel;
		if(velocity != 0 && AudioObject::isInitialized()) {
			alListenerfv(AL_VELOCITY,velocity->elements);
		} else if(AudioObject::isInitialized()) {
			float v[] = {0,0,0};
			alListenerfv(AL_VELOCITY,v);
		}
	}

	void AudioListener::setVolume(const UniformFloat *v) {
		volume = v;
		if(volume != 0 && AudioObject::isInitialized()) {
			alListenerf(AL_GAIN,volume->value);
		} else if(AudioObject::isInitialized()) {
			alListenerf(AL_GAIN,1);
		}
	}

	void AudioListener::update() {
		if(!AudioObject::isInitialized()) {
			return;
		}
		if(position != 0) {
			alListenerfv(AL_POSITION,position->elements);
		} else {
			float p[] = {0,0,0};
			alListenerfv(AL_POSITION,p);
		}
		float ori[6] = {1,0,0, 0,0,1};
		if(view != 0) {
			for(unsigned int i=0 ; i<3 ; i++) {
				ori[i] = (*view)[i];
			}
		}
		if(up != 0) {
			for(unsigned int i=0 ; i<3 ; i++) {
				ori[i+3] = (*up)[i];
			}
		}
		alListenerfv(AL_ORIENTATION,ori);
		if(velocity != 0) {
			alListenerfv(AL_VELOCITY,velocity->elements);
		} else {
			float v[] = {0,0,0};
			alListenerfv(AL_VELOCITY,v);
		}
		if(volume != 0) {
			alListenerf(AL_GAIN,volume->value);
		} else {
			alListenerf(AL_GAIN,1);
		}
	}

}

#endif