#ifndef _SX_ENGINE_CORE_UNIFORMFLOAT_CPP_
#define _SX_ENGINE_CORE_UNIFORMFLOAT_CPP_

/**
 * Uniform float class
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SXCore.h>
#include <sx/SX.h>

namespace sx {

	UniformFloat::UniformFloat(const string &id): Uniform(id) {
		value = 0;
	}
	
	UniformFloat &UniformFloat::operator = (float v) {
		value = v;
		return this [0];
	}

	UniformFloat &UniformFloat::operator << (float v) {
		value = v;
		return this [0];
	}

	const UniformFloat &UniformFloat::operator >> (float &v) const {
		v = value;
		return this [0];
	}

	void UniformFloat::load() {
	}

	bool UniformFloat::isLoaded() const {
		return true;
	}

	void UniformFloat::use(Shader &shader, const string &identifyer) {
		if(!shader.isLoaded()) {
			//shader isn't loaded
			//, hence nothing can be done
			return;
		}
		const string &name = getUniformName(identifyer);
		int loc = shader.getUniformLocation(name);
		if(loc >= 0) {
			//uniform used in shader,
			//hence copy value to uniform
			SXGL()glUniform1f(loc,(GLfloat)value);
		}
	}
}

#endif