#ifndef _SX_ENGINE_SXWINDOW_CPP_
#define _SX_ENGINE_SXWINDOW_CPP_

/**
 * SX window class
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SX.h>
#include <sx/Log4SX.h>
#include <sx/Exception.h>
//#include <GL/freeglut.h>
#include <locale>
using namespace std;

namespace sx {

	/*map<int,int> SXWindow::keys_glut2sx = SXWindow::initKeys_glut2sx();

	map<int,int> SXWindow::keys_glutnotspecial2sx = SXWindow::initKeys_glutnotspecial2sx();

	map<int,SXWindow *> SXWindow::windows;

	double SXWindow::mouseDeltaX = 0;

	double SXWindow::mouseDeltaY = 0;

	int SXWindow::argc = 0;

	char **SXWindow::argv = 0;

	char *SXWindow::argv0 = "SXWindow";

	map<int,int> SXWindow::initKeys_glut2sx() {
		map<int,int> mapping;
		mapping.insert(pair<int,int>(GLUT_KEY_F1,SX_F1));
		mapping.insert(pair<int,int>(GLUT_KEY_F2,SX_F2));
		mapping.insert(pair<int,int>(GLUT_KEY_F3,SX_F3));
		mapping.insert(pair<int,int>(GLUT_KEY_F4,SX_F4));
		mapping.insert(pair<int,int>(GLUT_KEY_F5,SX_F5));
		mapping.insert(pair<int,int>(GLUT_KEY_F6,SX_F6));
		mapping.insert(pair<int,int>(GLUT_KEY_F7,SX_F7));
		mapping.insert(pair<int,int>(GLUT_KEY_F8,SX_F8));
		mapping.insert(pair<int,int>(GLUT_KEY_F9,SX_F9));
		mapping.insert(pair<int,int>(GLUT_KEY_F10,SX_F10));
		mapping.insert(pair<int,int>(GLUT_KEY_F11,SX_F11));
		mapping.insert(pair<int,int>(GLUT_KEY_F12,SX_F12));
		mapping.insert(pair<int,int>(GLUT_KEY_INSERT,SX_INSERT));
		mapping.insert(pair<int,int>(GLUT_KEY_HOME,SX_HOME));
		mapping.insert(pair<int,int>(GLUT_KEY_END,SX_END));
		mapping.insert(pair<int,int>(GLUT_KEY_DELETE,SX_DELETE));
		mapping.insert(pair<int,int>(GLUT_KEY_PAGE_UP,SX_PAGEUP));
		mapping.insert(pair<int,int>(GLUT_KEY_PAGE_DOWN,SX_PAGEDOWN));
		mapping.insert(pair<int,int>(GLUT_KEY_LEFT,SX_LEFT));
		mapping.insert(pair<int,int>(GLUT_KEY_RIGHT,SX_RIGHT));
		mapping.insert(pair<int,int>(GLUT_KEY_DOWN,SX_DOWN));
		mapping.insert(pair<int,int>(GLUT_KEY_UP,SX_UP));
		mapping.insert(pair<int,int>(GLUT_KEY_NUM_LOCK,SX_NUMLOCK));
		mapping.insert(pair<int,int>(GLUT_KEY_CTRL_L,SX_CTRL));
		mapping.insert(pair<int,int>(GLUT_KEY_SHIFT_L,SX_SHIFT));
		mapping.insert(pair<int,int>(GLUT_KEY_ALT_L,SX_ALT));

		return mapping;
	}

	map<int,int> SXWindow::initKeys_glutnotspecial2sx() {
		map<int,int> mapping;
		mapping.insert(pair<int,int>(27,SX_ESC));
		mapping.insert(pair<int,int>(13,SX_ENTER));
		mapping.insert(pair<int,int>(13,SX_RETURN));
		mapping.insert(pair<int,int>(8,SX_BACKSPACE));
		return mapping;
	}

	void windowKeyUp(unsigned char key, int x, int y) {
		int modifyer = glutGetModifiers();
		int k = (int)toupper(key);
		map<int,int>::iterator keyiter = SXWindow::keys_glutnotspecial2sx.find(key);
		if(keyiter != SXWindow::keys_glutnotspecial2sx.end()) {
			//special key
			//translate from glut special key
			//to sx
			k = (*keyiter).second;
		}
		int currWindow = glutGetWindow();
		map<int,SXWindow *>::iterator iter = SXWindow::windows.find(currWindow);
		if(iter != SXWindow::windows.end()) {
			(*iter).second->removeKey(k);
			if(modifyer != GLUT_ACTIVE_SHIFT) {
				(*iter).second->removeKey(SX_CAPSLOCK);
				(*iter).second->removeKey(SX_SHIFT);
			}
		}
	}

	void windowKeyDown(unsigned char key, int x, int y) {
		int modifyer = glutGetModifiers();
		int k = (int)toupper(key);
		map<int,int>::iterator keyiter = SXWindow::keys_glutnotspecial2sx.find(key);
		if(keyiter != SXWindow::keys_glutnotspecial2sx.end()) {
			//special key
			//translate from glut special key
			//to sx
			k = (*keyiter).second;
		}
		int currWindow = glutGetWindow();
		map<int,SXWindow *>::iterator iter = SXWindow::windows.find(currWindow);
		if(iter != SXWindow::windows.end()) {
			(*iter).second->addKey(k);
			if(modifyer == GLUT_ACTIVE_SHIFT) {
				(*iter).second->addKey(SX_CAPSLOCK);
				(*iter).second->addKey(SX_SHIFT);
			}
		}
	}

	void windowSpecialKeyUp(int key, int x, int y) {
		int modifyer = glutGetModifiers();
		int k = key;
		map<int,int>::iterator keyiter = SXWindow::keys_glut2sx.find(key);
		if(keyiter != SXWindow::keys_glut2sx.end()) {
			//special key
			//translate from glut special key
			//to sx
			k = (*keyiter).second;
		}
		int currWindow = glutGetWindow();
		map<int,SXWindow *>::iterator iter = SXWindow::windows.find(currWindow);
		if(iter != SXWindow::windows.end()) {
			(*iter).second->removeKey(k);
			if(modifyer != GLUT_ACTIVE_SHIFT) {
				(*iter).second->removeKey(SX_CAPSLOCK);
				(*iter).second->removeKey(SX_SHIFT);
			}
		}
	}

	void windowSpecialKeyDown(int key, int x, int y) {
		int modifyer = glutGetModifiers();
		int k = key;
		map<int,int>::iterator keyiter = SXWindow::keys_glut2sx.find(key);
		if(keyiter != SXWindow::keys_glut2sx.end()) {
			//special key
			//translate from glut special key
			//to sx
			k = (*keyiter).second;
		}
		int currWindow = glutGetWindow();
		map<int,SXWindow *>::iterator iter = SXWindow::windows.find(currWindow);
		if(iter != SXWindow::windows.end()) {
			(*iter).second->addKey(k);
			if(modifyer == GLUT_ACTIVE_SHIFT) {
				(*iter).second->addKey(SX_CAPSLOCK);
				(*iter).second->addKey(SX_SHIFT);
			}
		}
	}

	void windowMouseKey(int button, int state, int x, int y) {
		int currWindow = glutGetWindow();
		map<int,SXWindow *>::iterator iter = SXWindow::windows.find(currWindow);
		if(iter != SXWindow::windows.end()) {
			(*iter).second->setMouseX(x);
			(*iter).second->setMouseY(glutGet(GLUT_WINDOW_HEIGHT) - y);
			MouseButton key = MOUSE_LEFT;
			bool useKey = false;
			if(button == GLUT_LEFT_BUTTON) {
				key = MOUSE_LEFT;
				useKey = true;
			} else if(button == GLUT_MIDDLE_BUTTON) {
				key = MOUSE_MIDDLE;
				useKey = true;
			} else if(button == GLUT_RIGHT_BUTTON) {
				key = MOUSE_RIGHT;
				useKey = true;
			}
			if(useKey && state == GLUT_DOWN) {
				(*iter).second->addMouseKey(key);
			} else if(useKey && state == GLUT_UP) {
				(*iter).second->removeMouseKey(key);
			}
		}
	}

	void windowMouseMotion(int x, int y) {
		int currWindow = glutGetWindow();
		map<int,SXWindow *>::iterator iter = SXWindow::windows.find(currWindow);
		if(iter != SXWindow::windows.end()) {
			(*iter).second->setMouseX(x);
			(*iter).second->setMouseY(glutGet(GLUT_WINDOW_HEIGHT) - y);
		}
	}

	void windowPassiveMotion(int x, int y) {
		int currWindow = glutGetWindow();
		map<int,SXWindow *>::iterator iter = SXWindow::windows.find(currWindow);
		if(iter != SXWindow::windows.end()) {
			(*iter).second->setMouseX(x);
			(*iter).second->setMouseY(glutGet(GLUT_WINDOW_HEIGHT) - y);
		}
	}

	void reshapeAll(int width, int height) {
		int currWindow = glutGetWindow();
		map<int,SXWindow *>::iterator iter = SXWindow::windows.find(currWindow);
		if(iter != SXWindow::windows.end()) {
			(*iter).second->setWidth(width);
			(*iter).second->setHeight(height);
			(*iter).second->listenersReshape();
		}
	}

	void renderAll() {
		int currWindow = glutGetWindow();
		map<int,SXWindow *>::iterator iter = SXWindow::windows.find(currWindow);
		if(iter != SXWindow::windows.end()) {
			(*iter).second->updateTime();
			(*iter).second->listenersRender();
		}
	}

	void stopAll() {
		int currWindow = glutGetWindow();
		map<int,SXWindow *>::iterator iter = SXWindow::windows.find(currWindow);
		if(iter != SXWindow::windows.end()) {
			(*iter).second->listenersStop();
		}
	}

	void SXWindow::listenersReshape() {
		if(argc == 0) {
			return;
		}
		if(initListeners.size() == 0) {
			for(vector<SXRenderListener *>::iterator iter = listeners.begin() ; iter != listeners.end() ; iter++) {
				(*iter)->reshape(*this);
			}
		} else {
			for(vector<SXRenderListener *>::iterator iter = initListeners.begin() ; iter != initListeners.end() ; iter++) {
				(*iter)->create(*this);
			}
			initListeners.clear();
		}
	}

	void SXWindow::listenersRender() {
		if(argc == 0) {
			return;
		}
		for(vector<SXRenderListener *>::iterator iter = listeners.begin() ; iter != listeners.end() ; iter++) {
			(*iter)->render(*this);
		}
		glutSwapBuffers();
	}

	void SXWindow::listenersStop() {
		if(argc == 0) {
			return;
		}
		for(vector<SXRenderListener *>::iterator iter = listeners.begin() ; iter != listeners.end() ; iter++) {
			vector<SXRenderListener *>::iterator noInit = initListeners.end();
			for(vector<SXRenderListener *>::iterator i = initListeners.begin() ; i != initListeners.end() ; i++) {
				if((*i) == (*iter)) {
					noInit = i;
				}
			}
			if(noInit == initListeners.end()) {
				(*iter)->stop(*this);
			}
		}
	}

	SXWindow::SXWindow(const string &name, unsigned int width, unsigned int height) {
		if(argc == 0) {
			argc = 1;
			argv = new char *[1];
			argv[0] = argv0;
			glutInit(&argc, argv);
		}
		removedWindow = false;
		glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
		glutInitWindowPosition(100,100);
		glutInitWindowSize(width,height);
		windowID = glutCreateWindow(name.c_str());
		GLenum err = glewInit();
		if(GLEW_OK != err) {
			Logger::get() << LogMarkup("SXWindow()") << Annotation("glew support") << "Error: " << string((char *)glewGetErrorString(err));
			glutDestroyWindow(windowID);
			removedWindow = true;
			throw Exception("no glew support", EX_NODATA);
		}
		int glVersion[2] = {-1,-1};
		glGetIntegerv(GL_MAJOR_VERSION, &glVersion[0]);
		if(glVersion[0] < 4) {
			Logger::get() << LogMarkup("SXWindow()") << Annotation("opengl 4.x support") << "no opengl 4.x support";
			glutDestroyWindow(windowID);
			removedWindow = true;
			throw Exception("no opengl 4.x support", EX_NODATA);
		}
		windows.insert(pair<int,SXWindow *>(windowID, this));
		glutSpecialFunc(windowSpecialKeyDown);
		glutSpecialUpFunc(windowSpecialKeyUp);
		glutKeyboardFunc(windowKeyDown);
		glutKeyboardUpFunc(windowKeyUp);
		glutMouseFunc(windowMouseKey);
		glutMotionFunc(windowMouseMotion);
		glutPassiveMotionFunc(windowPassiveMotion);
		glutReshapeFunc(reshapeAll);
		glutDisplayFunc(renderAll);
		glutCloseFunc(stopAll);
		glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);
		glutIdleFunc(renderAll);

		glEnable(GL_DEPTH_TEST);

		currentTimeRendering = 0;
		lastTimeRendered = -1;
		width = height = 0;
		mouseDeltaX = mouseDeltaY = 0;
		mouseX = mouseY = 0;

		//start time measurement
		ptime time(second_clock::local_time());
	}

	SXWindow::~SXWindow() {
		if(removedWindow) {
			return;
		}
		deleteRenderListeners();
		removeWindow();
	}

	void SXWindow::removeWindow() {
		if(removedWindow && argc > 0) {
			glutSetWindow(windowID);
			glutDestroyWindow(windowID);
		}
		removedWindow = true;
	}

	void SXWindow::addRenderListener(SXRenderListener &l) {
		if(removedWindow) {
			return;
		}
		listeners.push_back(&l);
		initListeners.push_back(&l);
	}

	void SXWindow::deleteRenderListener(SXRenderListener &l) {
		vector<vector<SXRenderListener *>::iterator> deletables;
		for(vector<SXRenderListener *>::iterator iter = listeners.begin() ; iter != listeners.end() ; iter++) {
			if((*iter) == &l) {
				deletables.push_back(iter);
			}
		}
		for(vector<vector<SXRenderListener *>::iterator>::iterator iter = deletables.begin() ; iter != deletables.end() ; iter++) {
			listeners.erase((*iter));
		}
		deletables.clear();
		for(vector<SXRenderListener *>::iterator iter = initListeners.begin() ; iter != initListeners.end() ; iter++) {
			if((*iter) == &l) {
				deletables.push_back(iter);
			}
		}
		for(vector<vector<SXRenderListener *>::iterator>::iterator iter = deletables.begin() ; iter != deletables.end() ; iter++) {
			initListeners.erase((*iter));
		}
		delete &l;
	}

	void SXWindow::deleteRenderListeners() {
		for(vector<SXRenderListener *>::iterator iter = listeners.begin() ; iter != listeners.end() ; iter++) {
			delete (*iter);
		}
		listeners.clear();
		initListeners.clear();
	}

	double SXWindow::getTime() const {
		return currentTimeRendering;
	}

	void SXWindow::updateTime() {
		if(lastTimeRendered == -1) {
			timeMeasurement.restart();
		}
		lastTimeRendered = currentTimeRendering;
		currentTimeRendering = timeMeasurement.elapsed();
	}

	void SXWindow::setMouseX(int x) {
		mouseX = x;
	}

	void SXWindow::setMouseY(int y) {
		mouseY = y;
	}

	void SXWindow::addKey(int key) {
		keys[key] = key;
	}

	void SXWindow::removeKey(int key) {
		unordered_map<int,int>::iterator iter = keys.find(key);
		if(iter != keys.end()) {
			keys.erase(iter);
		}
	}

	void SXWindow::addMouseKey(MouseButton key) {
		int k = (int)key;
		mouseKeys[k] = k;
	}

	void SXWindow::removeMouseKey(MouseButton key) {
		int k = (int)key;
		unordered_map<int,int>::const_iterator iter = mouseKeys.find(k);
		if(iter != mouseKeys.end()) {
			mouseKeys.erase(iter);
		}
	}

	double SXWindow::getDeltaTime() const {
		if(lastTimeRendered < 0) {
			return 0;
		}
		return getTime() - lastTimeRendered;
	}

	void SXWindow::setWidth(int width) {
		this->width = width;
	}

	void SXWindow::setHeight(int height) {
		this->height = height;
	}

	int SXWindow::getMouseX() const {
		return mouseX;
	}

	int SXWindow::getMouseY() const {
		return mouseY;
	}

	double SXWindow::getMouseDeltaX() const {
		return mouseDeltaX;
	}

	double SXWindow::getMouseDeltaY() const {
		return mouseDeltaY;
	}

	bool SXWindow::hasKey(int key) const {
		unordered_map<int,int>::const_iterator iter = keys.find(key);
		return iter != keys.end();
	}

	bool SXWindow::hasMouseKey(MouseButton key) const {
		int k = (int)key;
		unordered_map<int,int>::const_iterator iter = mouseKeys.find(k);
		return iter != mouseKeys.end();
	}

	int SXWindow::getWidth() const {
		return width;
	}

	int SXWindow::getHeight() const {
		return height;
	}

	void SXWindow::run() {
		glutMainLoop();
		argc = 0;
		delete argv;
		argv = 0;
		for(map<int,SXWindow *>::iterator iter = windows.begin() ; iter != windows.end() ; iter++) {
			(*iter).second->removedWindow = true;
			(*iter).second->deleteRenderListeners();
			(*iter).second->removeWindow();
		}
		windows.clear();
	}

	void SXWindow::stopRendering() {
		glutLeaveMainLoop();
	}

	void SXWindow::setMousePointer(int x, int y) {
		int currWindow = glutGetWindow();
		map<int,SXWindow *>::iterator iter = windows.find(currWindow);
		if(iter != windows.end()) {
			mouseDeltaX = (*iter).second->getMouseX() - x;
			mouseDeltaY = (*iter).second->getMouseY() - y;
		}
		glutWarpPointer(x, glutGet(GLUT_WINDOW_HEIGHT) - y);
	}

	void SXWindow::setShowCursor(bool showCursor) {
		if(showCursor) {
			glutSetCursor(GLUT_CURSOR_RIGHT_ARROW);
		} else {
			glutSetCursor(GLUT_CURSOR_NONE);
		}
	}*/

}

#endif