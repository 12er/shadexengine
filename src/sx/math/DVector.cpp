#ifndef _SX_MATH_DVECTOR_CPP_
#define _SX_MATH_DVECTOR_CPP_

/**
 * double precision vector class
 * (c) 2013 by Tristan Bauer
 */

#include <sx/SXMath.h>
#include <sx/Log4SX.h>
#include <sx/Exception.h>
#include <cmath>
using namespace std;

namespace sx {
	
	DVector::DVector() {
		elements = new double[4];
		for(unsigned int i=0 ; i<3 ; i++) {
			elements[i] = 0.0;
		}
		elements[3] = 1.0;
	}

	DVector::DVector(double v) {
		elements = new double[4];
		for(unsigned int i=0 ; i<3 ; i++) {
			elements[i] = v;
		}
		elements[3] = 1.0;
	}

	DVector::DVector(double x,double y) {
		elements = new double[4];
		elements[0] = x;
		elements[1] = y;
		elements[2] = 0.0;
		elements[3] = 1.0;
	}

	DVector::DVector(double x,double y,double z) {
		elements = new double[4];
		elements[0] = x;
		elements[1] = y;
		elements[2] = z;
		elements[3] = 1.0;
	}

	DVector::DVector(double x,double y,double z,double w) {
		elements = new double[4];
		elements[0] = x;
		elements[1] = y;
		elements[2] = z;
		elements[3] = w;
	}

	DVector::DVector(const double *v) {
		elements = new double[4];
		for(unsigned int i=0 ; i<4 ; i++) {
			elements[i] = v[i];
		}
	}

	DVector::DVector(const Vector &vector) {
		elements = new double[4];
		for(unsigned int i=0 ; i<4 ; i++) {
			elements[i] = (double)vector.elements[i];
		}
	}

	DVector::DVector(const DVector &vector) {
		elements = new double[4];
		for(unsigned int i=0 ; i<4 ; i++) {
			elements[i] = vector.elements[i];
		}
	}

	DVector &DVector::operator = (const DVector &v) {
		if(this == &v) {
			return this [0];
		}
		for(unsigned int i=0 ; i<4 ; i++) {
			elements[i] = v.elements[i];
		}
		return this [0];
	}

	DVector::~DVector() {
		SXout(LogMarkup("~DVector"));
		SXout("~DVector");
		delete [] elements;
	}

	double &DVector::operator [] (unsigned int index) {
		if(index >= 4) {
			throw Exception("out of bounds",EX_BOUNDS);
		}
		return elements[index];
	}

	double DVector::operator [] (unsigned int index) const {
		if(index > 4) {
			throw Exception("out of bounds",EX_BOUNDS);
		}
		return elements[index];
	}

	DVector &DVector::operator << (const DVector &v) {
		if(this == &v) {
			return this [0];
		}
		for(unsigned int i=0 ; i<4 ; i++) {
			elements[i] = v.elements[i];
		}
		return this [0];
	}

	DVector &DVector::operator << (const double *v) {
		for(unsigned int i=0 ; i<4 ; i++) {
			elements[i] = v[i];
		}
		return this [0];
	}

	const DVector &DVector::operator >> (DVector &v) const {
		if(this == &v) {
			return this [0];
		}
		for(unsigned int i=0 ; i<4 ; i++) {
			v.elements[i] = elements[i];
		}
		return this [0];
	}

	const DVector &DVector::operator >> (double *v) const {
		for(unsigned int i=0 ; i<4 ; i++) {
			v[i] = elements[i];
		}
		return this [0];
	}

	DVector &DVector::add(const DVector &v) {
		for(unsigned int i=0 ; i<3 ; i++) {
			elements[i] += v.elements[i];
		}
		return this [0];
	}

	DVector &DVector::add(double x) {
		for(unsigned int i=0 ; i<3 ; i++) {
			elements[i] += x;
		}
		return this [0];
	}

	DVector operator +(const DVector &v1, const DVector &v2) {
		DVector v;
		for(unsigned int i=0 ; i<3 ; i++) {
			v.elements[i] = v1.elements[i] + v2.elements[i];
		}
		return v;
	}

	DVector operator +(const DVector &v, double x) {
		DVector w;
		for(unsigned int i=0 ; i<3 ; i++) {
			w.elements[i] = v.elements[i] + x;
		}
		return w;
	}

	DVector operator +(double x, const DVector &v) {
		DVector w;
		for(unsigned int i=0 ; i<3 ; i++) {
			w.elements[i] = v.elements[i] + x;
		}
		return w;
	}

	DVector &DVector::crossmult(const DVector &v) {
		DVector w;
		w.elements[0] = elements[1] * v.elements[2] - elements[2] * v.elements[1];
		w.elements[1] = -(elements[0] * v.elements[2] - elements[2] * v.elements[0]);
		w.elements[2] = elements[0] * v.elements[1] - elements[1] * v.elements[0];
		for(unsigned int i=0 ; i<3 ; i++) {
			elements[i] = w.elements[i];
		}
		return this [0];
	}

	DVector operator %(const DVector &v1, const DVector &v2) {
		DVector w;
		w.elements[0] = v1.elements[1] * v2.elements[2] - v1.elements[2] * v2.elements[1];
		w.elements[1] = -(v1.elements[0] * v2.elements[2] - v1.elements[2] * v2.elements[0]);
		w.elements[2] = v1.elements[0] * v2.elements[1] - v1.elements[1] * v2.elements[0];
		return w;
	}

	DVector &DVector::scalarmult(double s) {
		for(unsigned i=0 ; i<3 ; i++) {
			elements[i] *= s;
		}
		return this [0];
	}

	DVector operator *(const DVector &v, double x) {
		DVector w = v;
		for(unsigned int i=0 ; i<3 ; i++) {
			w.elements[i] *= x;
		}
		return w;
	}

	DVector operator *(double x, const DVector &v) {
		DVector w = v;
		for(unsigned int i=0 ; i<3 ; i++) {
			w.elements[i] *= x;
		}
		return w;
	}

	DVector operator --(const DVector &v) {
		DVector w = v;
		for(unsigned int i=0 ; i<3 ; i++) {
			w.elements[i] *= -1.0f;
		}
		return w;
	}

	double DVector::innerprod(const DVector &v) const {
		double ip = 0.0;
		for(unsigned int i=0 ; i<3 ; i++) {
			ip += elements[i] * v.elements[i];
		}
		return ip;
	}

	double operator *(const DVector &v1, const DVector &v2) {
		double ip = 0.0f;
		for(unsigned int i=0 ; i<3 ; i++) {
			ip += v1.elements[i] * v2.elements[i];
		}
		return ip;
	}

	DVector &DVector::leftmult(const DMatrix &m) {
		DVector w;
		w[3] = 0.0;
		for(unsigned int outComponent = 0 ; outComponent < 4 ; outComponent++) {
			for(unsigned int inComponent = 0 ; inComponent < 4 ; inComponent++) {
				w.elements[outComponent] += m.elements[outComponent + inComponent*4] * elements[inComponent];
			}
		}
		for(unsigned int i=0 ; i<4 ; i++) {
			elements[i] = w.elements[i];
		}
		return this [0];
	}

	DVector &DVector::rightmult(const DMatrix &m) {
		DVector w;
		w[3] = 0.0;
		for(unsigned int outComponent = 0 ; outComponent < 4 ; outComponent++) {
			for(unsigned int inComponent = 0 ; inComponent < 4 ; inComponent++) {
				w.elements[outComponent] += elements[inComponent] * m[inComponent + outComponent*4];
			}
		}
		for(unsigned int i=0 ; i<4 ; i++) {
			elements[i] = w.elements[i];
		}
		return this [0];
	}

	DVector operator *(const DMatrix &m, const DVector &v) {
		DVector w;
		w[3] = 0.0;
		for(unsigned int outComponent = 0 ; outComponent < 4 ; outComponent++) {
			for(unsigned int inComponent = 0 ; inComponent < 4 ; inComponent++) {
				w.elements[outComponent] += m.elements[outComponent + inComponent*4] * v.elements[inComponent];
			}
		}
		return w;
	}

	DVector operator *(const DVector &v, const DMatrix &m) {
		DVector w;
		w[3] = 0.0;
		for(unsigned int outComponent = 0 ; outComponent < 4 ; outComponent++) {
			for(unsigned int inComponent = 0 ; inComponent < 4 ; inComponent++) {
				w.elements[outComponent] += v.elements[inComponent] * m.elements[inComponent + outComponent*4];
			}
		}
		return w;
	}

	DVector &DVector::normalize() {
		double size = 0.0;
		for(unsigned int i=0 ; i<3 ; i++) {
			size += elements[i]*elements[i];
		}
		size = sqrt(size);
		if(size != 0) {
			for(unsigned i=0 ; i<3 ; i++) {
				elements[i] /= size;
			}
		}
		return this [0];
	}

	double DVector::distance(const DVector &v) const {
		DVector w;
		for(unsigned int i=0 ; i<3 ; i++) {
			w.elements[i] = elements[i] - v.elements[i];
		}
		double size = 0.0;
		for(unsigned int i=0 ; i<3 ; i++) {
			size += w.elements[i]*w.elements[i];
		}
		return sqrt(size);
	}

	double DVector::length() const {
		double size = 0.0;
		for(unsigned int i=0 ; i<3 ; i++) {
			size += elements[i]*elements[i];
		}
		return sqrt(size);
	}

	DVector &DVector::random() {
		for(unsigned int i=0 ; i<3 ; i++) {
			elements[i] = ((double)rand())/((double)RAND_MAX);
		}
		elements[3] = 1.0;
		return this [0];
	}

	DVector &DVector::homogenize() {
		if(elements[3] == 0.0) {
			return this [0];
		}
		double hom = elements[3];
		for(unsigned int i=0 ; i<4 ; i++) {
			elements[i] /= hom;
		}
		return this [0];
	}

	bool DVector::equals(const DVector &v) const {
		for(unsigned int i=0 ; i<4 ; i++) {
			if(elements[i] != v.elements[i]) {
				return false;
			}
		}
		return true;
	}

	bool DVector::equals(const DVector &v, double epsilon) const {
		for(unsigned int i=0 ; i<4 ; i++) {
			if(abs(elements[i] - v.elements[i]) > epsilon) {
				return false;
			}
		}
		return true;
	}

}

#endif