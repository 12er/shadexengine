#ifndef _SX_WIDGET_SXWIDGET_CPP_
#define _SX_WIDGET_SXWIDGET_CPP_

/**
 * SX Widget class for the Qt framework
 * (c) 2015 by Tristan Bauer
 */

#include <sx/Log4SX.h>
#include <sx/SXWidget.h>
#include <sx/Exception.h>
#include <QGridLayout>
#include <QMouseEvent>
#include <QCursor>
#include <QGLFormat>
#include <cmath>
#include <sstream>
#include <algorithm>
using namespace std;

namespace sx {

	map<int,int> SXInternalWidget::keys_qt2sx = SXInternalWidget::initKeys_qt2sx();

	map<int,int> SXInternalWidget::initKeys_qt2sx() {
		map<int,int> mapping;
		mapping.insert(pair<int,int>(Qt::Key_F1,SX_F1));
		mapping.insert(pair<int,int>(Qt::Key_F2,SX_F2));
		mapping.insert(pair<int,int>(Qt::Key_F3,SX_F3));
		mapping.insert(pair<int,int>(Qt::Key_F4,SX_F4));
		mapping.insert(pair<int,int>(Qt::Key_F5,SX_F5));
		mapping.insert(pair<int,int>(Qt::Key_F6,SX_F6));
		mapping.insert(pair<int,int>(Qt::Key_F7,SX_F7));
		mapping.insert(pair<int,int>(Qt::Key_F8,SX_F8));
		mapping.insert(pair<int,int>(Qt::Key_F9,SX_F9));
		mapping.insert(pair<int,int>(Qt::Key_F10,SX_F10));
		mapping.insert(pair<int,int>(Qt::Key_F11,SX_F11));
		mapping.insert(pair<int,int>(Qt::Key_F12,SX_F12));
		mapping.insert(pair<int,int>(Qt::Key_Escape,SX_ESC));
		mapping.insert(pair<int,int>(Qt::Key_Insert,SX_INSERT));
		mapping.insert(pair<int,int>(Qt::Key_Pause,SX_PAUSE));
		mapping.insert(pair<int,int>(Qt::Key_Home,SX_HOME));
		mapping.insert(pair<int,int>(Qt::Key_End,SX_END));
		mapping.insert(pair<int,int>(Qt::Key_Delete,SX_DELETE));
		mapping.insert(pair<int,int>(Qt::Key_PageUp,SX_PAGEUP));
		mapping.insert(pair<int,int>(Qt::Key_PageDown,SX_PAGEDOWN));
		mapping.insert(pair<int,int>(Qt::Key_Left,SX_LEFT));
		mapping.insert(pair<int,int>(Qt::Key_Right,SX_RIGHT));
		mapping.insert(pair<int,int>(Qt::Key_Down,SX_DOWN));
		mapping.insert(pair<int,int>(Qt::Key_Up,SX_UP));
		mapping.insert(pair<int,int>(Qt::Key_NumLock,SX_NUMLOCK));
		mapping.insert(pair<int,int>(Qt::Key_Clear,SX_CLEAR));
		mapping.insert(pair<int,int>(Qt::Key_Enter,SX_ENTER));
		mapping.insert(pair<int,int>(Qt::Key_Control,SX_CTRL));
		mapping.insert(pair<int,int>(Qt::Key_Shift,SX_SHIFT));
		mapping.insert(pair<int,int>(Qt::Key_CapsLock,SX_CAPSLOCK));
		mapping.insert(pair<int,int>(Qt::Key_Meta,SX_START));
		mapping.insert(pair<int,int>(Qt::Key_Alt,SX_ALT));
		mapping.insert(pair<int,int>(Qt::Key_Return,SX_RETURN));
		mapping.insert(pair<int,int>(Qt::Key_Backspace,SX_BACKSPACE));
		return mapping;
	}

	void SXInternalWidget::keyPressEvent(QKeyEvent * e) {
		if(!e->isAutoRepeat()) {
			int key = e->key();
			map<int,int>::iterator iter = keys_qt2sx.find(e->key());
			if(iter != keys_qt2sx.end()) {
				//it's a special key
				//translate it to sx keymap
				key = (*iter).second;
			}
			sxWidget->addKey(key);
		}
	}

	void SXInternalWidget::keyReleaseEvent(QKeyEvent * e) {
		if(!e->isAutoRepeat()) {
			int key = e->key();
			map<int,int>::iterator iter = keys_qt2sx.find(e->key());
			if(iter != keys_qt2sx.end()) {
				//it's a special key
				//translate it to sx keymap
				key = (*iter).second;
			}
			sxWidget->removeKey(key);
		}
	}

	void SXInternalWidget::mousePressEvent(QMouseEvent * e) {
		MouseButton button = MOUSE_LEFT;
		switch(e->button()) {
			case Qt::LeftButton:
				button = MOUSE_LEFT;
				break;
			case Qt::MiddleButton:
				button = MOUSE_MIDDLE;
				break;
			case Qt::RightButton:
				button = MOUSE_RIGHT;
				break;
		}
		setFocus();
		sxWidget->addMouseKey(button);
		sxWidget->mouseX = e->x();
		sxWidget->mouseY = this->size().height() - 1 - e->y();
	}

	void SXInternalWidget::mouseReleaseEvent(QMouseEvent * e) {
		MouseButton button = MOUSE_LEFT;
		switch(e->button()) {
			case Qt::LeftButton:
				button = MOUSE_LEFT;
				break;
			case Qt::MiddleButton:
				button = MOUSE_MIDDLE;
				break;
			case Qt::RightButton:
				button = MOUSE_RIGHT;
				break;
		}
		sxWidget->removeMouseKey(button);
		sxWidget->mouseX = e->x();
		sxWidget->mouseY = this->size().height() - 1 - e->y();
	}

	void SXInternalWidget::mouseMoveEvent(QMouseEvent * e) {
		sxWidget->mouseX = e->x();
		sxWidget->mouseY = this->size().height() - 1 - e->y();
	}

	void SXInternalWidget::wheelEvent(QWheelEvent * e) {
		float degreesTimesEight = (float)e->delta();
		float degrees = degreesTimesEight / 8.0f;
		this->sxWidget->wheelRotates(degrees);
	}

	SXInternalWidget::SXInternalWidget(SXWidget *sxWidget, const QGLFormat &format): QGLWidget(format) {
		this->sxWidget = sxWidget;
	}

	SXInternalWidget::~SXInternalWidget() {
	}

	void SXInternalWidget::initializeGL() {
		makeCurrent();
		if (!SXGLinit()) {
			throw Exception("no OpenGL support", EX_NODATA);
		}
		int glVersion[2] = {-1,-1};
		SXGL()glGetIntegerv(GL_MAJOR_VERSION, &glVersion[0]);
		SXGL()glGetIntegerv(GL_MINOR_VERSION, &glVersion[1]);
		if(glVersion[0] < 4 || glVersion[1] < 2) {
			stringstream errmsg;
			errmsg << "OpenGL " << glVersion[0] << "." << glVersion[1] << " supported, but OpenGL 4.2 support required";
			throw Exception(errmsg.str(),EX_NODATA);
		}
		SXGL()glEnable(GL_DEPTH_TEST);
		setAutoBufferSwap(true);
		setMouseTracking(true);
		setFocus();
	}

	void SXInternalWidget::resizeGL(int width, int height) {
		if(!sxWidget->constructed) {
			return;
		}
		sxWidget->processingListeners = true;
		if(sxWidget->initListeners.size() > 0) {
			for(vector<SXRenderListener *>::iterator iter = sxWidget->initListeners.begin() ; iter != sxWidget->initListeners.end() ; iter++) {
				(*iter)->create(*sxWidget);
			}
			sxWidget->initListeners.clear();
		}
		for(vector<SXRenderListener *>::iterator iter = sxWidget->listeners.begin() ; iter != sxWidget->listeners.end() ; iter++) {
			(*iter)->reshape(*sxWidget);
		}
		sxWidget->processingListeners = false;
	}

	void SXInternalWidget::paintGL() {
		if(!sxWidget->constructed) {
			return;
		}
		sxWidget->updateTime();
		if(sxWidget->callStopAfterRender) {
			//stopRendering can only process
			//if listeners aren't processed
			sxWidget->callStopAfterRender = false;
			sxWidget->stopRendering();
			return;
		}
		sxWidget->processingListeners = true;
		for(vector<SXRenderListener *>::iterator iter = sxWidget->listeners.begin() ; iter != sxWidget->listeners.end() ; iter++) {
			if(sxWidget->initListeners.size() > 0) {
				vector<SXRenderListener *>::iterator noInit = std::find(sxWidget->initListeners.begin(),sxWidget->initListeners.end(),(*iter));
				if(noInit != sxWidget->initListeners.end()) {
					//listener not initialized yet
					//, so don't render
					continue;
				}
			}
			(*iter)->render(*sxWidget);
		}
		sxWidget->processingListeners = false;
		if(sxWidget->callStopAfterRender) {
			//stopRendering can only process
			//if listeners aren't processed
			sxWidget->callStopAfterRender = false;
			sxWidget->stopRendering();
			return;
		}
	}

}

#endif
