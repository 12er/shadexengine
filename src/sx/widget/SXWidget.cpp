#ifndef _SX_WIDGET_SXWIDGET_CPP_
#define _SX_WIDGET_SXWIDGET_CPP_

/**
 * SX Widget class for the Qt framework
 * (c) 2012 by Tristan Bauer
 */

#include <sx/Log4SX.h>
#include <sx/SXWidget.h>
#include <sx/Exception.h>
#include <QGridLayout>
#include <QMouseEvent>
#include <QCursor>
#include <QGLFormat>
#include <cmath>
#include <sstream>
#include <algorithm>
using namespace std;

namespace sx {

	void SXWidget::updateTime() {
		if(lastTimeRendered == -1) {
			timeMeasurement.restart();
		}
		lastTimeRendered = currentTimeRendering;
		currentTimeRendering = timeMeasurement.elapsed();
	}

	void SXWidget::addKey(int key) {
		keys[key] = key;
	}

	void SXWidget::removeKey(int key) {
		unordered_map<int,int>::iterator iter = keys.find(key);
		if(iter != keys.end()) {
			keys.erase(iter);
		}
	}

	void SXWidget::addMouseKey(MouseButton key) {
		int k = (int)key;
		mouseKeys[k] = k;
	}

	void SXWidget::removeMouseKey(MouseButton key) {
		int k = (int)key;
		unordered_map<int,int>::iterator iter = mouseKeys.find(k);
		if(iter != mouseKeys.end()) {
			mouseKeys.erase(iter);
		}
	}

	SXWidget::SXWidget(): QWidget() {
		setMinimumSize(130,100);
		constructed = true;
		processingListeners = false;
		callStopAfterRender = false;
		lastTimeRendered = -1;
		currentTimeRendering = 0;
		mouseDeltaX = mouseDeltaY = 0;
		lastSetMouseTime = 0;
		mouseX = mouseY = 0;
		QGLFormat format;
		format.setVersion(4, 2);
		format.setProfile(QGLFormat::CoreProfile);
		internalWidget = 0;
		internalWidget = new SXInternalWidget(this, format);
		connect(&timer,SIGNAL(timeout()),internalWidget,SLOT(repaint()));
		QGridLayout *layout = new QGridLayout();
		layout->setContentsMargins(0,0,0,0);
		layout->addWidget(internalWidget,1,1,1,1);
		setLayout(layout);
	}

	SXWidget::~SXWidget() {
		if(constructed) {
			stopRendering();
		}
		deleteRenderListeners();
	}

	void SXWidget::renderPeriodically(float time) {
		int millies = (int)ceil(time * 1000);
		timer.start(millies);
	}

	void SXWidget::stopRenderPeriodically() {
		timer.stop();
	}

	void SXWidget::addRenderListener(SXRenderListener &l) {
		if(!constructed) {
			return;
		}
		listeners.push_back(&l);
		initListeners.push_back(&l);
	}

	void SXWidget::deleteRenderListener(SXRenderListener &l) {
		processingListeners = true;
		if(constructed) {
			vector<SXRenderListener *>::iterator noInit = std::find(initListeners.begin(),initListeners.end(),&l);
			if(noInit == initListeners.end()) {
				//iter has been initialized
				l.stop(*this);
			}
		}
		vector<vector<SXRenderListener *>::iterator> deletables;
		for(vector<SXRenderListener *>::iterator iter = listeners.begin() ; iter != listeners.end() ; iter++) {
			if((*iter) == &l) {
				deletables.push_back(iter);
			}
		}
		for(vector<vector<SXRenderListener *>::iterator>::iterator iter = deletables.begin() ; iter != deletables.end() ; iter++) {
			listeners.erase((*iter));
		}
		deletables.clear();
		for(vector<SXRenderListener *>::iterator iter = initListeners.begin() ; iter != initListeners.end() ; iter++) {
			if((*iter) == &l) {
				deletables.push_back(iter);
			}
		}
		for(vector<vector<SXRenderListener *>::iterator>::iterator iter = deletables.begin() ; iter != deletables.end() ; iter++) {
			initListeners.erase((*iter));
		}
		delete &l;
		processingListeners = false;
	}

	void SXWidget::deleteRenderListeners() {
		processingListeners = true;
		if(constructed) {
			unordered_map<SXRenderListener *,SXRenderListener *> initializedListeners;
			for(vector<SXRenderListener *>::iterator iter = listeners.begin() ; iter != listeners.end() ; iter++) {
				vector<SXRenderListener *>::iterator noInit = std::find(initListeners.begin(),initListeners.end(),(*iter));
				if(noInit == initListeners.end()) {
					//iter has been initialized
					initializedListeners[(*iter)] = (*iter);
				}
			}
			for(unordered_map<SXRenderListener *,SXRenderListener *>::iterator iter = initializedListeners.begin() ; iter != initializedListeners.end() ; iter++) {
				(*iter).first->stop(*this);
			}
			constructed = false;
		}
		unordered_map<SXRenderListener *,SXRenderListener *> deletableListeners;
		for(vector<SXRenderListener *>::iterator iter = listeners.begin() ; iter != listeners.end() ; iter++) {
			deletableListeners[(*iter)] = (*iter);
		}
		for(unordered_map<SXRenderListener *,SXRenderListener *>::iterator iter = deletableListeners.begin() ; iter != deletableListeners.end() ; iter++) {
			Logger::get() << LogMarkup("SXWidget::deleteRenderListeners/listener") << (unsigned int)1;
			delete (*iter).first;
		}
		listeners.clear();
		initListeners.clear();
		processingListeners = false;
		Logger::get() << LogMarkup("SXWidget::deleteRenderListeners/listeners") << (unsigned int)listeners.size();
		Logger::get() << LogMarkup("SXWidget::deleteRenderListeners/initListeners") << (unsigned int)initListeners.size();
	}

	double SXWidget::getTime() const {
		return currentTimeRendering;
	}

	double SXWidget::getDeltaTime() const {
		if(lastTimeRendered < 0) {
			return 0;
		}
		return currentTimeRendering - lastTimeRendered;
	}

	int SXWidget::getMouseX() const {
		return mouseX;
	}

	int SXWidget::getMouseY() const {
		return mouseY;
	}

	double SXWidget::getMouseDeltaX() const {
		return mouseDeltaX;
	}

	double SXWidget::getMouseDeltaY() const {
		return mouseDeltaY;
	}

	bool SXWidget::hasKey(int key) const {
		unordered_map<int,int>::const_iterator iter = keys.find(key);
		return iter != keys.end();
	}

	bool SXWidget::hasMouseKey(MouseButton key) const {
		int k = (int)key;
		unordered_map<int,int>::const_iterator iter = mouseKeys.find(k);
		return iter != mouseKeys.end();
	}

	int SXWidget::getWidth() const {
		return size().width();
	}

	int SXWidget::getHeight() const {
		return size().height();
	}

	void SXWidget::stopRendering() {
		if(processingListeners) {
			callStopAfterRender = true;
			return;
		}
		processingListeners = true;
		for(vector<SXRenderListener *>::iterator iter = listeners.begin() ; iter != listeners.end() ; iter++) {
			vector<SXRenderListener *>::iterator noInit = std::find(initListeners.begin(),initListeners.end(),(*iter));
			if(noInit == initListeners.end()) {
				//iter has been initialized
				(*iter)->stop(*this);
			}
		}
		processingListeners = false;
		if(constructed) {
			constructed = false;
			finishedRendering();
		}
	}

	void SXWidget::setMousePointer(int x, int y) {
		double currentTime = timeMeasurement.elapsed();
		if(currentTime - lastSetMouseTime < 0.01) {
			//don't poll too often
			//to optain good results
			return;
		}
		lastSetMouseTime = currentTime;
		float mDX = getMouseX() - x;
		float mDY = getMouseY() - y;
		//simply taking the mouse changes for each frame
		//would result in random behaviour for small mouse
		//changes, because changing the mouseposition for a distance
		//of one pixel leaves only nine possible positions for the mouse
		//to go
		//therefore the changes are smoothed
		setMouseDeltas.push_back(Vector(mDX,mDY));
		if(setMouseDeltas.size() > 5) {
			//keep a finite history of changes in
			//the mouseposition
			setMouseDeltas.erase(setMouseDeltas.begin());
		}
		//calculate mouseDeltaX/Y by averaging a subsequence of
		//the history of mousechanges
		Vector avgDelta;
		float n = 0;
		float sumLength = 0;
		for(int i=setMouseDeltas.size()-1 ; i>=0 ; i--) {
			Vector &d = setMouseDeltas[i];
			avgDelta = avgDelta + d;
			n++;
			sumLength += d.length();
			if(sumLength > 25) {
				//measurements are pretty accurate
				//if the mouse has travelled over
				//larger distances
				//hence don't consider more data
				//if the considered path of the mouse
				//is large enough
				break;
			}
		}
		avgDelta.scalarmult(1/n);
		mouseDeltaX = avgDelta[0];
		mouseDeltaY = avgDelta[1];
		QPoint widgetOrigin = mapToGlobal(QPoint(0,size().height() - 1));
		QCursor::setPos(widgetOrigin.x() + x,widgetOrigin.y() - y);
	}

	void SXWidget::setShowCursor(bool showCursor) {
		if(showCursor) {
			setCursor(QCursor(Qt::ArrowCursor));
		} else {
			setCursor(QCursor(Qt::BlankCursor));
		}
	}

}

#endif
