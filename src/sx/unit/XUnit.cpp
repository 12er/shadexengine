#ifndef _SX_UNIT_XUNIT_CPP_
#define _SX_UNIT_XUNIT_CPP_

/**
 * Unit class
 * (c) 2012 by Tristan Bauer
 */
#include <sx/Log4SX.h>
#include <sx/XUnit.h>
#include <sstream>
#include <iostream>
#include <cmath>
using namespace std;

namespace sx {

	XUnit::XUnit(sx::Logger *logger) {
		this->logger = logger;
		this->passed = true;
	}

	XUnit::~XUnit() {
		(*logger) << LogMarkup("Summary") << Annotation("Summary");
		if(passed) {
			(*logger) << Level(L_HARMLESS) << "Success, all tests worked without errors!" << Logger::newLine();
		} else {
			(*logger) << Level(L_ERROR) << "Error, some tests did not work!" << Logger::newLine();
		}
		delete logger;
	}

	void XUnit::addMarkup(std::string comment) {
		(*logger) << LogMarkup(comment);
	}

	void XUnit::addAnnotation(std::string annotation) {
		(*logger) << Annotation(annotation);
	}

	void XUnit::assertTrue(bool value) {
		(*logger) << Annotation("assertTrue: ");
		if(value) {
			(*logger) << Level(L_HARMLESS) << "success" << Logger::newLine();
		} else {
			(*logger) << Level(L_ERROR) << "fail" << Logger::newLine();
			passed = false;
		}
	}

	void XUnit::assertEquals(char v1, char v2) {
		_INTERN_ASSERTEQUALS_(v1,v2);
	}

	void XUnit::assertEquals(unsigned char v1, unsigned char v2) {
		_INTERN_ASSERTEQUALS_(v1,v2);
	}

	void XUnit::assertEquals(short v1, short v2) {
		_INTERN_ASSERTEQUALS_(v1,v2);
	}

	void XUnit::assertEquals(unsigned short v1, unsigned short v2) {
		_INTERN_ASSERTEQUALS_(v1,v2);
	}

	void XUnit::assertEquals(int v1, int v2) {
		_INTERN_ASSERTEQUALS_(v1,v2);
	}

	void XUnit::assertEquals(unsigned int v1, unsigned int v2) {
		_INTERN_ASSERTEQUALS_(v1,v2);
	}

	void XUnit::assertEquals(long v1, long v2) {
		_INTERN_ASSERTEQUALS_(v1,v2);
	}

	void XUnit::assertEquals(unsigned long v1, unsigned long v2) {
		_INTERN_ASSERTEQUALS_(v1,v2);
	}

	void XUnit::assertEquals(float v1, float v2) {
		_INTERN_ASSERTEQUALS_(v1,v2);
	}

	void XUnit::assertEquals(float v1, float v2, float epsilon) {
		stringstream ss;
		ss << "assertEquals(epsilon=" << epsilon << "): ";
		(*logger) << Annotation(ss.str());
		if(abs(v1-v2) <= abs(epsilon)) {
			(*logger) << Level(L_HARMLESS) << "success" << Logger::newLine();
		} else {
			(*logger) << Level(L_ERROR) << "fail: param1 is " << v1 << ", but param2 is " << v2 << ", such that |param1 - param2| = " << abs(v1-v2) << Logger::newLine();
			passed = false;
		}
	}

	void XUnit::assertEquals(double v1, double v2) {
		_INTERN_ASSERTEQUALS_(v1,v2);
	}

	void XUnit::assertEquals(double v1, double v2, double epsilon) {
		stringstream ss;
		ss << "assertEquals(epsilon=" << epsilon << "): ";
		(*logger) << Annotation(ss.str());
		if(abs(v1-v2) <= abs(epsilon)) {
			(*logger) << Level(L_HARMLESS) << "success" << Logger::newLine();
		} else {
			(*logger) << Level(L_ERROR) << "fail: param1 is " << v1 << ", but param2 is " << v2 << ", such that |param1 - param2| = " << abs(v1-v2) << Logger::newLine();
			passed = false;
		}
	}

	void XUnit::assertEquals(std::string v1, std::string v2) {
		(*logger) << Annotation("assertEquals: ");
		if(v1.compare(v2) == 0) {
			(*logger) << Level(L_HARMLESS) << "success" << Logger::newLine();
		} else {
			(*logger) << Level(L_ERROR) << "fail: param1 is " << v1 << ", but param2 is " << v2 << Logger::newLine();
			passed = false;
		}
	}

	void XUnit::assertInputYes(std::string message) {
		string input;
		cout << endl << message << " (y,n)";
		cin >> input;
		(*logger) << Annotation(message) << Annotation(" - ") << Annotation("assertInputYes: ");
		if(input.compare("y") == 0) {
			(*logger) << Level(L_HARMLESS) << "success" << Logger::newLine();
		} else {
			(*logger) << Level(L_ERROR) << "fail: user didn't input 'y'" << Logger::newLine();
			passed = false;
		}
	}

}

#endif