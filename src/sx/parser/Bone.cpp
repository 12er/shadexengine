#ifndef _SX_PARSER_BONE_CPP_
#define _SX_PARSER_BONE_CPP_

/**
 * bone class
 * (c) 2013 by Tristan Bauer
 */
#include <sx/SXParser.h>

namespace sx {

	Bone::Bone() {
	}

	Bone::Bone(const sx::Bone &bone) {
		if(this == &bone) {
			//copying itself not necessary
			return;
		}
		this->ID = bone.ID;
		this->parentTransform = bone.parentTransform;
		this->inverseBindPoseMatrix = bone.inverseBindPoseMatrix;
		this->bones = bone.bones;
	}

}

#endif