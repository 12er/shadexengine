#ifndef _SX_PARSER_SXTOKENIZER_CPP_
#define _SX_PARSER_SXTOKENIZER_CPP_

/**
 * tokenizer function
 * (c) 2015 by Tristan Bauer
 */

#include <sx/SXParser.h>
#include <sx/Log4SX.h>
using namespace std;

namespace sx {

	vector<string> tokenizeString(const string &str, const char *delimiter) {
		vector<string> tokenizedStr;

		size_t previous = 0;
		size_t next = 0;
		while((next = str.find_first_of(delimiter, previous)) != string::npos) {
			tokenizedStr.push_back(str.substr(previous, next-previous));
			previous = next + 1;
		}
		if (previous < str.size()) {
			tokenizedStr.push_back(str.substr(previous));
		}

		return tokenizedStr;
	}
}

#endif
