#ifndef _SX_PARSER_INTERNAL__XNODE__CPP_
#define _SX_PARSER_INTERNAL__XNODE__CPP_

/**
 * internal parser datastructures
 * (c) 2015 by Tristan Bauer
 */

#include <internal/Parsers.h>

_XMLNODE_::_XMLNODE_() {
	columnno = lineno = 1;
	deconstructChildren = true;
}

void _XMLNODE_::newText(unsigned int lineno, unsigned int columnno) {
	type = _XTEXT_;
	this->lineno = lineno;
	this->columnno = columnno;
}

void _XMLNODE_::newTag(const string &lname, const string &rname, unsigned int lineno, unsigned int columnno) {
	type = _XTAG_;
	this->name = lname;
	this->endname = rname;
	this->lineno = lineno;
	this->columnno = columnno;
}

void _XMLNODE_::newStrAttrib(const string &ID, unsigned int lineno, unsigned int columnno) {
	type = _S_ATTRIB_;
	this->strID = ID;
	this->lineno = lineno;
	this->columnno = columnno;
}

void _XMLNODE_::addText(char text) {
	this->text << text;
}

void _XMLNODE_::addAttrib(char text) {
	this->strAttrib << text;
}

_XMLNODE_::~_XMLNODE_() {
	if (deconstructChildren) {
		for (vector<_XMLNODE_ *>::iterator nodeIter = nodes.begin(); nodeIter != nodes.end(); nodeIter++) {
			_XMLNODE_ *node = (*nodeIter);
			if (node != 0) {
				delete node;
			}
		}
		for (vector<_XMLNODE_ *>::iterator nodeIter = ats.begin(); nodeIter != ats.end(); nodeIter++) {
			_XMLNODE_ *node = (*nodeIter);
			if (node != 0) {
				delete node;
			}
		}
	}
	nodes.clear();
}

#endif

