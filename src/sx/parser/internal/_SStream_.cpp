#ifndef _SX_PARSER_INTERNAL__SSTREAM__CPP_
#define _SX_PARSER_INTERNAL__SSTREAM__CPP_

/**
* internal ply parser utility
* (c) 2015 by Tristan Bauer
*/

#include <internal/Parsers.h>

void _SStream_::appendChar(char c) {
	str << c;
}

#endif