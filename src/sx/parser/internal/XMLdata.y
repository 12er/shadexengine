%{
#include <internal/Parsers.h>
#include <b_XMLdata.h>
#include <f_XMLdata.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <string>
using namespace std;

int XMLdataerror(_XMLNODE_ **finalTag, XMLdatascan_t scanner, const char *msg)
{
	return 1;
}
%}

%code requires
{
#ifndef XMLDATA_TYPEDEF_XMLDATA_SCANNER_T
#define XMLDATA_TYPEDEF_XMLDATA_SCANNER_T
typedef void* XMLdatascan_t;
#endif
}

%output "b_XMLdata.cpp"
%defines "b_XMLdata.h"

%define api.pure
%lex-param { XMLdatascan_t scanner }
%parse-param { _XMLNODE_ **finalTag }
%parse-param { XMLdatascan_t scanner }

%union
{
	char character;
	char *text;
	_XMLNODE_ *node;
}

%token<character> TEXT_CHAR_T
%token TAG_L_T
%token TAG_R_T
%token CLOSE_TAG_T
%token STRING_L_T
%token STRING_R_T
%token<character> STRING_CHAR_T
%token ASSIGN_T
%token<text> VAR_T
%token LEXERROR_T

%type<node> tag
%type<node> ltag
%type<text> rtag
%type<node> attributelist
%type<node> attribute
%type<node> str
%type<node> strlist
%type<node> nodelist
%type<node> tagtext
%type<node> tagtextlist

%%

finaltag:
	TAG_L_T tag TAG_R_T
		{
		*finalTag = $2;
		}
	| tagtextlist TAG_L_T tag TAG_R_T
		{
		*finalTag = $3;
		delete $1;
		$1 = 0;
		}
	| TAG_L_T tag TAG_R_T tagtextlist
		{
		*finalTag = $2;
		delete $4;
		$4 = 0;
		}
	| tagtextlist TAG_L_T tag TAG_R_T tagtextlist
		{
		*finalTag = $3;
		delete $1;
		delete $5;
		$1 = $5 = 0;
		}
	;

tag:
	VAR_T CLOSE_TAG_T
		{
		_XMLNODE_ *node = new _XMLNODE_();
		node->newTag(string($1), string($1), getXMLdataLineno(), getXMLdataColumnno());
		free($1);
		$1 = 0;
		$$ = node;
		}
	| VAR_T attributelist CLOSE_TAG_T
		{
		$2->newTag(string($1), string($1), getXMLdataLineno(), getXMLdataColumnno());
		free($1);
		$1 = 0;
		$$ = $2;
		}
	| ltag nodelist rtag
		{
		$2->ats = $1->ats;
		$2->newTag($1->name, string($3), getXMLdataLineno(), getXMLdataColumnno());
		$1->deconstructChildren = false;
		delete $1;
		$1 = 0;
		free($3);
		$3 = 0;
		$$ = $2;
		}
	;

ltag:
	VAR_T
		{
		_XMLNODE_ *node = new _XMLNODE_();
		node->name = string($1);
		free($1);
		$1 = 0;
		$$ = node;
		}
	| VAR_T attributelist
		{
		$2->name = string($1);
		free($1);
		$1 = 0;
		$$ = $2;
		}
	;

rtag:
	CLOSE_TAG_T VAR_T
		{
		$$ = $2;
		}
	;

attributelist:
	attribute
		{
		_XMLNODE_ *node = new _XMLNODE_();
		node->ats.push_back($1);
		$$ = node;
		}
	| attributelist attribute
		{
		$1->ats.push_back($2);
		$$ = $1;
		}
	;

attribute:
	VAR_T ASSIGN_T str
		{
		$3->newStrAttrib(string($1), getXMLdataLineno(), getXMLdataColumnno());
		free($1);
		$1 = 0;
		$$ = $3;
		}
	;

str:
	STRING_L_T STRING_R_T
		{
		_XMLNODE_ *node = new _XMLNODE_();
		$$ = node;
		}
	| STRING_L_T strlist STRING_R_T
		{
		$$ = $2;
		}
	;

strlist:
	STRING_CHAR_T
		{
		_XMLNODE_ *node = new _XMLNODE_();
		node->addAttrib($1);
		$$ = node;
		}
	| strlist STRING_CHAR_T
		{
		$1->addAttrib($2);
		$$ = $1;
		}
	;

nodelist:
	tagtext
		{
		_XMLNODE_ *node = new _XMLNODE_();
		node->nodes.push_back($1);
		$$ = node;
		}
	| nodelist tag tagtext
		{
		$1->nodes.push_back($2);
		$1->nodes.push_back($3);
		$$ = $1;
		}
	;

tagtext:
	TAG_R_T TAG_L_T
		{
		_XMLNODE_ *node = new _XMLNODE_();
		node->newText(getXMLdataLineno(), getXMLdataColumnno());
		$$ = node;
		}
	| TAG_R_T tagtextlist TAG_L_T
		{
		$2->newText(getXMLdataLineno(), getXMLdataColumnno());
		$$ = $2;
		}
	;

tagtextlist:
	TEXT_CHAR_T
		{
		_XMLNODE_ *node = new _XMLNODE_();
		node->addText($1);
		$$ = node;
		}
	| tagtextlist TEXT_CHAR_T
		{
		$1->addText($2);
		$$ = $1;
		}
	;

%%


