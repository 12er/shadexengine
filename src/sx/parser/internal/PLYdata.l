%{
#include <internal/Parsers.h>
#include <b_PLYdata.h>
#include <stdio.h>

unsigned int PLYdatalineno = 1;
unsigned int PLYdatacolumnno = 1;

void resetPLYdataNo()
{
	PLYdatalineno = PLYdatacolumnno = 1;
}

unsigned int getPLYdataLineno()
{
	return PLYdatalineno;
}

unsigned int getPLYdataColumnno()
{
	return PLYdatacolumnno;
}
%}

%option outfile="f_PLYdata.cpp" header-file="f_PLYdata.h"
%option warn nodefault
%option reentrant noyywrap never-interactive nounistd
%option bison-bridge

UCHAR	"uchar"
CHAR	"char"
USHORT	"ushort"
SHORT	"short"
UINT	"uint"
INT		"int"
ULONG	"ulong"
LONG	"long"
FLOAT	"float"
DOUBLE	"double"

ELEMENT	"element"
PROPERTY	"property"
LIST		"list"
END_HEADER	"end_header"

COMMENT	"comment"
PLY		"ply"
FORMAT	"format"

ULONGNUM	[+]?[0-9]+
NUM	[+-]?[0-9]*[.]?[0-9]+([eE][+-]?[0-9]+)?
VAR	[_a-zA-Z:.][_a-zA-Z0-9:.]*

SPACE	[ \t]*

%state LINECOMMENT

%%

<INITIAL>{UCHAR}	{ PLYdatacolumnno += strlen(yytext); return UCHAR_T; }
<INITIAL>{CHAR}		{ PLYdatacolumnno += strlen(yytext); return CHAR_T; }
<INITIAL>{USHORT}	{ PLYdatacolumnno += strlen(yytext); return USHORT_T; }
<INITIAL>{SHORT}	{ PLYdatacolumnno += strlen(yytext); return SHORT_T; }
<INITIAL>{UINT}		{ PLYdatacolumnno += strlen(yytext); return UINT_T; }
<INITIAL>{INT}		{ PLYdatacolumnno += strlen(yytext); return INT_T; }
<INITIAL>{ULONG}	{ PLYdatacolumnno += strlen(yytext); return ULONG_T; }
<INITIAL>{LONG}		{ PLYdatacolumnno += strlen(yytext); return LONG_T; }
<INITIAL>{FLOAT}	{ PLYdatacolumnno += strlen(yytext); return FLOAT_T; }
<INITIAL>{DOUBLE}	{ PLYdatacolumnno += strlen(yytext); return DOUBLE_T; }
<INITIAL>{ELEMENT}	{ PLYdatacolumnno += strlen(yytext); return ELEMENT_T; }
<INITIAL>{PROPERTY}	{ PLYdatacolumnno += strlen(yytext); return PROPERTY_T; }
<INITIAL>{LIST}		{ PLYdatacolumnno += strlen(yytext); return LIST_T; }
<INITIAL>{END_HEADER}	{ PLYdatacolumnno += strlen(yytext); return END_HEADER_T; }
<INITIAL>{COMMENT}	{ PLYdatacolumnno += strlen(yytext); BEGIN(LINECOMMENT); }
<INITIAL>{PLY}		{ PLYdatacolumnno += strlen(yytext); BEGIN(LINECOMMENT); }
<INITIAL>{FORMAT}	{ PLYdatacolumnno += strlen(yytext); BEGIN(LINECOMMENT); }
<INITIAL>{ULONGNUM}	{ yylval->ulong = strtol(yytext, NULL, 10); PLYdatacolumnno += strlen(yytext); return ULONGNUM_T; }
<INITIAL>{NUM}	{ yylval->number = stringToDouble(yytext); PLYdatacolumnno += strlen(yytext); return NUM_T; }
<INITIAL>{VAR}	{
				yylval->text = (char *)malloc(sizeof(char) * (1+strlen(yytext)));
				strcpy(yylval->text, yytext);
				PLYdatacolumnno += strlen(yytext);
				return VAR_T;
				}
<INITIAL>{SPACE}	{ PLYdatacolumnno += strlen(yytext); }
<INITIAL>\n	{ PLYdatacolumnno = 1; PLYdatalineno++; return SEPARATOR_T; }
<INITIAL>\r	{ }
<INITIAL>.	{ PLYdatacolumnno += strlen(yytext); return LEXERROR_T; }

<LINECOMMENT>\n	{ PLYdatacolumnno = 1; PLYdatalineno++; BEGIN(INITIAL); return SEPARATOR_T; }
<LINECOMMENT>\r	{ }
<LINECOMMENT>.	{ PLYdatacolumnno += strlen(yytext); }

%%
