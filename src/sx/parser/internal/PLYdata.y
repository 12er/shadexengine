%{
#include <internal/Parsers.h>
#include <b_PLYdata.h>
#include <f_PLYdata.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <string>
using namespace std;

int PLYdataerror(_XMESH_ **finalMesh, PLYdatascan_t scanner, const char *msg)
{
	return 1;
}
%}

%code requires
{
#ifndef PLYDATA_TYPEDEF_PLYDATA_SCANNER_T
#define PLYDATA_TYPEDEF_PLYDATA_SCANNER_T
typedef void* PLYdatascan_t;
#endif
}

%output "b_PLYdata.cpp"
%defines "b_PLYdata.h"

%define api.pure
%lex-param { PLYdatascan_t scanner }
%parse-param { _XMESH_ **finalMesh }
%parse-param { PLYdatascan_t scanner }

%union
{
	double number;
	unsigned long ulong;
	int typecode;
	char *text;
	VectorDouble *numberlist;
	VectorInt *typelist;
	_ELEMENT_ *element;
	VectorProperty *propertylist;
	_PROPERTY_ *property;
	_XMESH_ *mesh;
}

%token UCHAR_T
%token CHAR_T
%token USHORT_T
%token SHORT_T
%token UINT_T
%token INT_T
%token ULONG_T
%token LONG_T
%token FLOAT_T
%token DOUBLE_T
%token ELEMENT_T
%token PROPERTY_T
%token LIST_T
%token END_HEADER_T
%token<ulong> ULONGNUM_T
%token<number> NUM_T
%token<text> VAR_T
%token SEPARATOR_T
%token LEXERROR_T

%type<mesh> mesh
%type<mesh> headersection
%type<number> separatorlist
%type<element> element
%type<element> elementheader
%type<propertylist> propertylist
%type<property> property
%type<typelist> propertytypelist
%type<typecode> propertytype
%type<mesh> datasection
%type<numberlist> numberlist

%%

finalmesh:
	mesh
		{
		*finalMesh = $1;
		}
	;

mesh:
	headersection END_HEADER_T datasection
		{
		$3->elements = $1->elements;
		delete $1;
		$1 = 0;
		$$ = $3;
		}
	| separatorlist headersection END_HEADER_T datasection
		{
		$4->elements = $2->elements;
		delete $2;
		$2 = 0;
		$$ = $4;
		}
	;

separatorlist:
	SEPARATOR_T
		{
		$$ = 0;
		}
	| separatorlist SEPARATOR_T
		{
		$$ = 0;
		}
	;

headersection:
	element
		{
		_XMESH_ *mesh = new _XMESH_();
		mesh->elements.push_back(*$1);
		delete $1;
		$1 = 0;
		$$ = mesh;
		}
	| headersection element
		{
		$1->elements.push_back(*$2);
		delete $2;
		$2 = 0;
		$$ = $1;
		}
	;

element:
	elementheader propertylist
		{
		$1->properties = $2->v;
		delete $2;
		$2 = 0;
		$$ = $1;
		}
	;

elementheader:
	ELEMENT_T VAR_T ULONGNUM_T SEPARATOR_T
		{
		_ELEMENT_ *e = new _ELEMENT_();
		e->name = string($2);
		e->count = $3;
		free($2);
		$2 = 0;
		$$ = e;
		}
	;

propertylist:
	SEPARATOR_T
		{
		$$ = new VectorProperty();
		}
	| property SEPARATOR_T
		{
		VectorProperty *p = new VectorProperty();
		p->v.push_back(*$1);
		delete $1;
		$1 = 0;
		$$ = p;
		}
	| propertylist SEPARATOR_T
		{
		$$ = $1;
		}
	| propertylist property SEPARATOR_T
		{
		$1->v.push_back(*$2);
		delete $2;
		$2 = 0;
		$$ = $1;
		}
	;

property:
	PROPERTY_T propertytype VAR_T
		{
		_PROPERTY_ *p = new _PROPERTY_();
		p->multiplicity = M_SINGLE;
		p->name = string($3);
		p->types.push_back($2);
		free($3);
		$3 = 0;
		$$ = p;
		}
	| PROPERTY_T LIST_T propertytypelist VAR_T
		{
		_PROPERTY_ *p = new _PROPERTY_();
		p->multiplicity = M_LIST;
		p->name = string($4);
		p->types = $3->v;
		delete $3;
		free($4);
		$3 = 0;
		$4 = 0;
		$$ = p;
		}
	;

propertytypelist:
	propertytype
		{
		VectorInt *v = new VectorInt();
		v->v.push_back($1);
		$$ = v;
		}
	| propertytypelist propertytype
		{
		$1->v.push_back($2);
		$$ = $1;
		}
	;

propertytype:
	UCHAR_T
		{
		$$ = T_UCHAR;
		}
	| CHAR_T
		{
		$$ = T_CHAR;
		}
	| USHORT_T
		{
		$$ = T_USHORT;
		}
	| SHORT_T
		{
		$$ = T_SHORT;
		}
	| UINT_T
		{
		$$ = T_UINT;
		}
	| INT_T
		{
		$$ = T_INT;
		}
	| ULONG_T
		{
		$$ = T_ULONG;
		}
	| LONG_T
		{
		$$ = T_LONG;
		}
	| FLOAT_T
		{
		$$ = T_FLOAT;
		}
	| DOUBLE_T
		{
		$$ = T_DOUBLE;
		}
	;

datasection:
	SEPARATOR_T
		{
		$$ = new _XMESH_();
		}
	| SEPARATOR_T numberlist
		{
		_XMESH_ *mesh = new _XMESH_();
		mesh->data.push_back($2->v);
		delete $2;
		$2 = 0;
		$$ = mesh;
		}
	| datasection SEPARATOR_T
		{
		$$ = $1;
		}
	| datasection SEPARATOR_T numberlist
		{
		$1->data.push_back($3->v);
		delete $3;
		$3 = 0;
		$$ = $1;
		}
	;

numberlist:
	ULONGNUM_T
		{
		VectorDouble *list = new VectorDouble();
		list->v.push_back((double)$1);
		$$ = list;
		}
	| NUM_T
		{
		VectorDouble *list = new VectorDouble();
		list->v.push_back($1);
		$$ = list;
		}
	| numberlist ULONGNUM_T
		{
		$1->v.push_back((double)$2);
		$$ = $1;
		}
	| numberlist NUM_T
		{
		$1->v.push_back($2);
		$$ = $1;
		}
	;

%%


