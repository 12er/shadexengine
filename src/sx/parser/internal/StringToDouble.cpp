#ifndef _SX_PARSER_INTERNAL_STRINGTODOUBLE_CPP_
#define _SX_PARSER_INTERNAL_STRINGTODOUBLE_CPP_

/**
* internal string to double converter
* (c) 2015 by Tristan Bauer
*/

#include <internal/Parsers.h>
#include <QString>

double stringToDouble(char *c) {
	QString qstr(c);
	double q = qstr.toDouble();
	return q;
}

#endif

