%{
#include <internal/Parsers.h>
#include <b_XMLdata.h>
#include <stdio.h>

unsigned int XMLdatalineno = 1;
unsigned int XMLdatacolumnno = 1;

void resetXMLdataNo()
{
	XMLdatalineno = XMLdatacolumnno = 1;
}

unsigned int getXMLdataLineno()
{
	return XMLdatalineno;
}

unsigned int getXMLdataColumnno()
{
	return XMLdatacolumnno;
}
%}

%option outfile="f_XMLdata.cpp" header-file="f_XMLdata.h"
%option warn nodefault
%option reentrant noyywrap never-interactive nounistd

%option bison-bridge

ASSIGN	"="

VAR	[_a-zA-Z:.][_a-zA-Z0-9:.]*

SPACE	[ \t]*

STRINGBORDER	\"

STARTTAG_L	"<?"
STARTTAG_R	"?>"

COMMENT1_L	"<!--"
COMMENT1_R	"-->"

COMMENT2_L	"<!"

TAG_L	"<"
TAG_R	">"
CLOSE_TAG	"/"

%state STARTTAG
%state COMMENT1
%state COMMENT2

%state OMITTEXT
%state USESTRING_OMITTEXT
%state STARTTAG_OMITTEXT
%state COMMENT1_OMITTEXT
%state COMMENT2_OMITTEXT

%%

<INITIAL>{STARTTAG_L}	{ XMLdatacolumnno += strlen(yytext); BEGIN(STARTTAG); }
<INITIAL>{COMMENT1_L}	{ XMLdatacolumnno += strlen(yytext); BEGIN(COMMENT1); }
<INITIAL>{COMMENT2_L}	{ XMLdatacolumnno += strlen(yytext); BEGIN(COMMENT2); }
<INITIAL>{TAG_L}	{ XMLdatacolumnno += strlen(yytext); BEGIN(OMITTEXT); return TAG_L_T; }
<INITIAL>{SPACE}	{ yylval->character = yytext[0]; XMLdatacolumnno += strlen(yytext); return TEXT_CHAR_T; }
<INITIAL>\n			{ yylval->character = yytext[0]; XMLdatacolumnno = 1; XMLdatalineno++; return TEXT_CHAR_T; }
<INITIAL>\r			{ yylval->character = yytext[0]; return TEXT_CHAR_T; }
<INITIAL>.	{ yylval->character = yytext[0]; XMLdatacolumnno += strlen(yytext); return TEXT_CHAR_T; }

<STARTTAG>{STARTTAG_R}	{ XMLdatacolumnno += strlen(yytext); BEGIN(INITIAL); }
<STARTTAG>\n		{ XMLdatacolumnno = 1; XMLdatalineno++; }
<STARTTAG>\r		{ }
<STARTTAG>.		{ XMLdatacolumnno += strlen(yytext); }

<COMMENT1>{COMMENT1_R}	{ XMLdatacolumnno += strlen(yytext); BEGIN(INITIAL); }
<COMMENT1>\n		{ XMLdatacolumnno = 1; XMLdatalineno++; }
<COMMENT1>\r		{ }
<COMMENT1>.		{ XMLdatacolumnno += strlen(yytext); }

<COMMENT2>{TAG_R}	{ XMLdatacolumnno += strlen(yytext); BEGIN(INITIAL); }
<COMMENT2>\n		{ XMLdatacolumnno = 1; XMLdatalineno++; }
<COMMENT2>\r		{ }
<COMMENT2>.		{ XMLdatacolumnno += strlen(yytext); }

<OMITTEXT>{STRINGBORDER}	{ XMLdatacolumnno += strlen(yytext); BEGIN(USESTRING_OMITTEXT); return STRING_L_T; }
<OMITTEXT>{STARTTAG_L}	{ XMLdatacolumnno += strlen(yytext); BEGIN(STARTTAG_OMITTEXT); }
<OMITTEXT>{COMMENT1_L}	{ XMLdatacolumnno += strlen(yytext); BEGIN(COMMENT1_OMITTEXT); }
<OMITTEXT>{COMMENT2_L}	{ XMLdatacolumnno += strlen(yytext); BEGIN(COMMENT2_OMITTEXT); }
<OMITTEXT>{ASSIGN}	{ XMLdatacolumnno += strlen(yytext); return ASSIGN_T; };
<OMITTEXT>{VAR}	{
				yylval->text = (char *)malloc(sizeof(char) * (1+strlen(yytext)));
				strcpy(yylval->text, yytext);
				XMLdatacolumnno += strlen(yytext);
				return VAR_T;
				}
<OMITTEXT>{TAG_R}	{ XMLdatacolumnno += strlen(yytext); BEGIN(INITIAL); return TAG_R_T; }
<OMITTEXT>{CLOSE_TAG}	{ XMLdatacolumnno += strlen(yytext); return CLOSE_TAG_T; }
<OMITTEXT>{SPACE}	{ XMLdatacolumnno += strlen(yytext); }
<OMITTEXT>\n			{ XMLdatacolumnno = 1; XMLdatalineno++; }
<OMITTEXT>\r			{ }
<OMITTEXT>.	{ XMLdatacolumnno += strlen(yytext); return LEXERROR_T; }

<USESTRING_OMITTEXT>{STRINGBORDER}	{ XMLdatacolumnno += strlen(yytext); BEGIN(OMITTEXT); return STRING_R_T; }
<USESTRING_OMITTEXT>\n	{ XMLdatacolumnno = 1; XMLdatalineno++; yylval->character = '\n'; return STRING_CHAR_T; }
<USESTRING_OMITTEXT>\r	{ yylval->character = '\r'; return STRING_CHAR_T; }
<USESTRING_OMITTEXT>.	{ XMLdatacolumnno += strlen(yytext); yylval->character = yytext[0]; return STRING_CHAR_T; }

<STARTTAG_OMITTEXT>{STARTTAG_R}	{ XMLdatacolumnno += strlen(yytext); BEGIN(OMITTEXT); }
<STARTTAG_OMITTEXT>\n		{ XMLdatacolumnno = 1; XMLdatalineno++; }
<STARTTAG_OMITTEXT>\r		{ }
<STARTTAG_OMITTEXT>.		{ XMLdatacolumnno += strlen(yytext); }

<COMMENT1_OMITTEXT>{COMMENT1_R}	{ XMLdatacolumnno += strlen(yytext); BEGIN(OMITTEXT); }
<COMMENT1_OMITTEXT>\n		{ XMLdatacolumnno = 1; XMLdatalineno++; }
<COMMENT1_OMITTEXT>\r		{ }
<COMMENT1_OMITTEXT>.		{ XMLdatacolumnno += strlen(yytext); }

<COMMENT2_OMITTEXT>{TAG_R}	{ XMLdatacolumnno += strlen(yytext); BEGIN(OMITTEXT); }
<COMMENT2_OMITTEXT>\n		{ XMLdatacolumnno = 1; XMLdatalineno++; }
<COMMENT2_OMITTEXT>\r		{ }
<COMMENT2_OMITTEXT>.		{ XMLdatacolumnno += strlen(yytext); }

%%


