%{
#include <internal/Parsers.h>
#include <b_SXdata.h>
#include <stdio.h>

unsigned int SXdatalineno = 1;
unsigned int SXdatacolumnno = 1;

void resetSXdataNo()
{
	SXdatalineno = SXdatacolumnno = 1;
}

unsigned int getSXdataLineno()
{
	return SXdatalineno;
}

unsigned int getSXdataColumnno()
{
	return SXdatacolumnno;
}
%}

%option outfile="f_SXdata.cpp" header-file="f_SXdata.h"
%option warn nodefault
%option reentrant noyywrap never-interactive nounistd
%option bison-bridge

CODE_L	"{"
CODE_R	"}"
PAR_L	"("
PAR_N	")"
SEMIC	";"
ASSIGN	"="

NUM	[+-]?[0-9]*[.]?[0-9]+([eE][+-]?[0-9]+)?
VAR	[_a-zA-Z:.][_a-zA-Z0-9:.]*

SPACE	[ \t]*

STRINGBORDER	\"

COMMENT_L	"/*"
COMMENT_R	"*/"

LINECOMMENT_L	"//"

%state USESTRING
%state COMMENT
%state LINECOMMENT

%%

<INITIAL>{CODE_L}	{ SXdatacolumnno += strlen(yytext); return CODE_L_T; };
<INITIAL>{CODE_R}	{ SXdatacolumnno += strlen(yytext); return CODE_R_T; };
<INITIAL>{PAR_L}	{ SXdatacolumnno += strlen(yytext); return PAR_L_T; };
<INITIAL>{PAR_N}	{ SXdatacolumnno += strlen(yytext); return PAR_R_T; };
<INITIAL>{SEMIC}	{ SXdatacolumnno += strlen(yytext); return SEMIC_T; };
<INITIAL>{ASSIGN}	{ SXdatacolumnno += strlen(yytext); return ASSIGN_T; };

<INITIAL>{NUM}	{ yylval->number =stringToDouble(yytext); SXdatacolumnno += strlen(yytext); return NUM_T; }
<INITIAL>{VAR}	{
				yylval->text = (char *)malloc(sizeof(char) * (1+strlen(yytext)));
				strcpy(yylval->text, yytext);
				SXdatacolumnno += strlen(yytext);
				return VAR_T;
				}

<INITIAL>{STRINGBORDER}	{ SXdatacolumnno += strlen(yytext); BEGIN(USESTRING); return STRING_L_T; }
<INITIAL>{COMMENT_L}	{ SXdatacolumnno += strlen(yytext); BEGIN(COMMENT); }
<INITIAL>{LINECOMMENT_L}	{ SXdatacolumnno += strlen(yytext); BEGIN(LINECOMMENT); }

<INITIAL>{SPACE}	{ SXdatacolumnno += strlen(yytext); }
<INITIAL>\n			{ SXdatacolumnno = 1; SXdatalineno++; }
<INITIAL>\r			{ }
<INITIAL>.	{ SXdatacolumnno += strlen(yytext); return LEXERROR_T; }

<USESTRING>{STRINGBORDER}	{ SXdatacolumnno += strlen(yytext); BEGIN(INITIAL); return STRING_R_T; }
<USESTRING>\n	{ SXdatacolumnno = 1; SXdatalineno++; yylval->character = '\n'; return STRING_CHAR_T; }
<USESTRING>\r	{ yylval->character = '\r'; return STRING_CHAR_T; }
<USESTRING>.	{ SXdatacolumnno += strlen(yytext); yylval->character = yytext[0]; return STRING_CHAR_T; }

<COMMENT>{COMMENT_R}	{ SXdatacolumnno += strlen(yytext); BEGIN(INITIAL); }
<COMMENT>\n		{ SXdatacolumnno = 1; SXdatalineno++; }
<COMMENT>\r		{ }
<COMMENT>.		{ SXdatacolumnno += strlen(yytext); }

<LINECOMMENT>\n		{ SXdatacolumnno = 1; SXdatalineno++; BEGIN(INITIAL); }
<LINECOMMENT>\r		{ BEGIN(INITIAL); }
<LINECOMMENT>.		{ SXdatacolumnno += strlen(yytext); }

%%


