%{
#include <internal/Parsers.h>
#include <b_SXdata.h>
#include <f_SXdata.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <string>
using namespace std;

int SXdataerror(_XNODE_ **finalTag, SXdatascan_t scanner, const char *msg)
{
	return 1;
}
%}

%code requires
{
#ifndef SXDATA_TYPEDEF_SXDATA_SCANNER_T
#define SXDATA_TYPEDEF_SXDATA_SCANNER_T
typedef void* SXdatascan_t;
#endif
}

%output "b_SXdata.cpp"
%defines "b_SXdata.h"

%define api.pure
%lex-param { SXdatascan_t scanner }
%parse-param { _XNODE_ **finalTag }
%parse-param { SXdatascan_t scanner }

%union
{
	double number;
	char character;
	char *text;
	_XNODE_ *node;
	_SStream_ *concatenator;
}

%token CODE_L_T
%token CODE_R_T
%token PAR_L_T
%token PAR_R_T
%token SEMIC_T
%token ASSIGN_T
%token<number> NUM_T
%token<text> VAR_T
%token STRING_L_T
%token<character> STRING_CHAR_T
%token STRING_R_T
%token LEXERROR_T

%type<node> tag
%type<node> nodelist
%type<node> nodelistarg
%type<node> textnode
%type<node> rattrib
%type<node> strattrib
%type<concatenator> str
%type<concatenator> strlist

%%

finaltag:
	tag	{ *finalTag = $1; }
	;

tag:
	PAR_L_T VAR_T PAR_R_T
		{
		_XNODE_ *node = new _XNODE_();
		node->newTag(string($2), getSXdataLineno(), getSXdataColumnno());
		$$ = node;
		free($2);
		$2 = 0;
		}
	| PAR_L_T VAR_T PAR_R_T CODE_L_T CODE_R_T
		{
		_XNODE_ *node = new _XNODE_();
		node->newTag(string($2), getSXdataLineno(), getSXdataColumnno());
		$$ = node;
		free($2);
		$2 = 0;
		}
	| PAR_L_T VAR_T PAR_R_T CODE_L_T nodelist CODE_R_T
		{
		$5->newTag(string($2), getSXdataLineno(), getSXdataColumnno());
		$$ = $5;
		free($2);
		$2 = 0;
		}
	;

nodelist:
	nodelistarg
		{
		_XNODE_ *node = new _XNODE_();
		node->nodes.push_back($1);
		$$ = node;
		}
	| nodelist nodelistarg
		{
		$1->nodes.push_back($2);
		$$ = $1;
		}
	;

nodelistarg:
	tag		{ $$ = $1; }
	| textnode	{ $$ = $1; }
	| rattrib	{ $$ = $1; }
	| strattrib	{ $$ = $1; }
	;

textnode:
	str
		{
		_XNODE_ *node = new _XNODE_();
		node->newText($1->str.str(), getSXdataLineno(), getSXdataColumnno());
		$$ = node;
		delete $1;
		$1 = 0;
		}
	;

rattrib:
	VAR_T ASSIGN_T NUM_T SEMIC_T
			{
			_XNODE_ *node = new _XNODE_();
			node->newRealAttrib(string($1), $3, getSXdataLineno(), getSXdataColumnno());
			$$ = node;
			free($1);
			$1 = 0;
			}
	;

strattrib:
	VAR_T ASSIGN_T str SEMIC_T
			{
			_XNODE_ *node = new _XNODE_();
			node->newStrAttrib(string($1), $3->str.str(), getSXdataLineno(), getSXdataColumnno());
			$$ = node;
			free($1);
			$1 = 0;
			delete $3;
			$3 = 0;
			}
	;

str:
	STRING_L_T STRING_R_T				{ $$ = new _SStream_(); }
	| STRING_L_T strlist STRING_R_T		{ $$ = $2; }
	;

strlist:
	STRING_CHAR_T	{
			_SStream_ *ss = new _SStream_();
			ss->appendChar($1);
			$$ = ss;
			}
	| strlist STRING_CHAR_T
			{
			$1->appendChar($2);
			$$ = $1;
			}
	;

%%


