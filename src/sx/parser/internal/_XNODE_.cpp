#ifndef _SX_PARSER_INTERNAL__XNODE__CPP_
#define _SX_PARSER_INTERNAL__XNODE__CPP_

/**
 * internal ply parser utility
 * (c) 2015 by Tristan Bauer
 */

#include <internal/Parsers.h>

_XNODE_::_XNODE_() {
	rAttrib = 0;
	columnno = lineno = 1;
}

void _XNODE_::newText(const string &text, unsigned int lineno, unsigned int columnno) {
	type = _XTEXT_;
	this->text = text;
	this->lineno = lineno;
	this->columnno = columnno;
}

void _XNODE_::newTag(const string &name, unsigned int lineno, unsigned int columnno) {
	type = _XTAG_;
	this->name = name;
	this->lineno = lineno;
	this->columnno = columnno;
}

void _XNODE_::newStrAttrib(const string &ID, const string &value, unsigned int lineno, unsigned int columnno) {
	type = _S_ATTRIB_;
	this->strID = ID;
	this->strAttrib = value;
	this->lineno = lineno;
	this->columnno = columnno;
}

void _XNODE_::newRealAttrib(const string &ID, double value, unsigned int lineno, unsigned int columnno) {
	type = _R_ATTRIB_;
	this->rID = ID;
	this->rAttrib = value;
	this->lineno = lineno;
	this->columnno = columnno;
}

_XNODE_::~_XNODE_() {
	for(vector<_XNODE_ *>::iterator nodeIter = nodes.begin() ; nodeIter != nodes.end() ; nodeIter++) {
		_XNODE_ *node = (*nodeIter);
		if(node != 0) {
			delete node;
		}
	}
	nodes.clear();
}

#endif
