%{
#include <internal/Parsers.h>
#include <b_pNumbers.h>
#include <stdio.h>

unsigned int pNumberslineno = 1;
unsigned int pNumberscolumnno = 1;

void resetPNumbersNo()
{
	pNumberslineno = pNumberscolumnno = 1;
}

unsigned int getPNumbersLineno()
{
	return pNumberslineno;
}

unsigned int getPNumbersColumnno()
{
	return pNumberscolumnno;
}
%}

%option outfile="f_pNumbers.cpp" header-file="f_pNumbers.h"
%option warn nodefault
%option reentrant noyywrap never-interactive nounistd
%option bison-bridge

NUM	[+-]?[0-9]*[.]?[0-9]+([eE][+-]?[0-9]+)?

SPACE	[ \t]*

%%

<INITIAL>{NUM}	{ yylval->number = stringToDouble(yytext); pNumberscolumnno += strlen(yytext); return NUM_T; }
<INITIAL>{SPACE}	{ pNumberscolumnno += strlen(yytext); }
<INITIAL>\n			{ pNumberscolumnno = 1; pNumberslineno++; }
<INITIAL>\r			{ }
<INITIAL>.	{ pNumberscolumnno += strlen(yytext); return LEXERROR_T; }

%%

