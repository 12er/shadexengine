%{
#include <internal/Parsers.h>
#include <b_pNumbers.h>
#include <f_pNumbers.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <string>

int pNumberserror(VectorDouble **finalList, pNumbersscan_t scanner, const char *msg)
{
	return 1;
}
%}

%code requires
{
#ifndef PNUMBERS_TYPEDEF_PNUMBERS_SCANNER_T
#define PNUMBERS_TYPEDEF_PNUMBERS_SCANNER_T
typedef void* pNumbersscan_t;
#endif
}

%output "b_pNumbers.cpp"
%defines "b_pNumbers.h"

%define api.pure
%lex-param { pNumbersscan_t scanner }
%parse-param { VectorDouble **finalList }
%parse-param { pNumbersscan_t scanner }

%union
{
	double number;
	VectorDouble *list;
}

%token<number> NUM_T

%token LEXERROR_T

%type<list> numlist

%%

finallist:
	numlist { *finalList = $1; }
	;

numlist:
		{ $$ = new VectorDouble(); }
	| numlist NUM_T	{ $1->v.push_back($2); $$ = $1; }
	;

%%

