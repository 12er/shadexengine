#ifndef _SX_PARSER_PARSESXDATA_CPP_
#define _SX_PARSER_PARSESXDATA_CPP_

/**
 * sx parser
 * (c) 2012 by Tristan Bauer
 */

#include <sx/SXParser.h>
#include <sx/Exception.h>
#include <sx/Log4SX.h>
#include <internal/Parsers.h>
#include <b_SXdata.h>
#include <f_SXdata.h>
#include <vector>
#include <sstream>
using namespace std;

extern int SXdataparse(_XNODE_ **outcome, SXdatascan_t scanner);

namespace sx {

	/**
	 * converts _XNODE_ to XTag
	 */
	XTag *SXToXNode(_XNODE_ &source,bool &deconstruct,Exception &ex) {
		XTag *tag = new XTag();
		tag->name = source.name;
		for (vector<_XNODE_ *>::iterator nodeIter = source.nodes.begin(); nodeIter != source.nodes.end(); nodeIter++) {
			_XNODE_ &node = *(*nodeIter);
			if(deconstruct) {
				//if an error occurred, leave node
				return tag;
			}
			if(node.type == _XTAG_) {
				//take child under any circumstances
				XTag *child = SXToXNode(node,deconstruct,ex);
				tag->nodes.push_back(child);
			} else if(node.type == _XTEXT_) {
				XText *text = new XText();
				text->text = node.text;
				tag->nodes.push_back(text);
			} else if(node.type == _S_ATTRIB_) {
				map<string,string>::iterator iter = tag->stringAttribs.find(node.strID);
				if(iter == tag->stringAttribs.end()) {
					tag->stringAttribs.insert(pair<string,string>(node.strID,node.strAttrib));
				} else {
					//attribute IDs must not appear multiple times in the same tag
					stringstream errmsg;
					errmsg << "Error: attribute ID " << node.strID << " at line " << node.lineno << ", column " << node.columnno << " appears multiple times";
					deconstruct = true;
					ex.setMessage(errmsg.str());
					ex.setType(EX_SYNTAX);
				}
			} else if(node.type == _R_ATTRIB_) {
				map<string,double>::iterator iter = tag->realAttribs.find(node.rID);
				if(iter == tag->realAttribs.end()) {
					tag->realAttribs.insert(pair<string,double>(node.rID,node.rAttrib));
				} else {
					//attribute IDs must not appear multiple times in the same tag
					stringstream errmsg;
					errmsg << "Error: attribute ID " << node.rID << " at line " << node.lineno << ", column " << node.columnno << " appears multiple times";
					deconstruct = true;
					ex.setMessage(errmsg.str());
					ex.setType(EX_SYNTAX);
				}
			}
		}
		return tag;
	}

	XTag *parseSXdata(const string &data) {
		XTag *sxData = 0;
		resetSXdataNo();

		//parse with flex, bison
		_XNODE_ *outcome = 0;
		SXdatascan_t scanner;
		YY_BUFFER_STATE state;
		if (SXdatalex_init(&scanner)) {
			throw Exception("Error: couldn't initialize SXdata lexer");
		}
		state = SXdata_scan_string(data.c_str(), scanner);
		if (SXdataparse(&outcome, scanner)) {
			SXdata_delete_buffer(state, scanner);
			SXdatalex_destroy(scanner);
			stringstream errstr;
			errstr << "Error: couldn't parse SX data at line " << getSXdataLineno() << ", column " << getSXdataColumnno();
			throw Exception(errstr.str());
		}
		SXdata_delete_buffer(state, scanner);
		SXdatalex_destroy(scanner);
		if (outcome == 0) {
			stringstream errstr;
			errstr << "Error: couldn't parse SX data at line " << getSXdataLineno() << ", column " << getSXdataColumnno();
			throw Exception(errstr.str());
		}

		//translate _XNODE_ to XTag
		bool deconstruct = false;
		Exception ex;
		XTag *tag = SXToXNode(*outcome, deconstruct, ex);
		delete outcome;
		if (deconstruct) {
			delete tag;
			throw ex;
		}
		return tag;
	}

	XTag *parseSXFile(const string path) {
		string content = readFile(path);
		return parseSXdata(content);
	}

}

#endif