#ifndef _SX_PARSER_BITMAP_CPP_
#define _SX_PARSER_BITMAP_CPP_

/**
 * bitmap utilities
 * (c) 2012 by Tristan Bauer
 */

#include <sx/SXParser.h>
#include <sx/Log4SX.h>
#include <sx/Exception.h>
#include <FreeImage.h>
#include <algorithm>
#include <sstream>
using namespace std;

namespace sx {
	
	Bitmap::Bitmap(unsigned int width, unsigned int height) {
		this->width = width;
		this->height = height;
		this->bytesPerPixel = 4;
		this->data = 0;
		if(width != 0 && height != 0) {
			this->data = new unsigned char[width * height * 4];
		}
	}
	
	Bitmap::Bitmap(unsigned int width, unsigned int height, unsigned int bytesPerPixel) {
		if(bytesPerPixel != 4 && bytesPerPixel != 16) {
			stringstream errmsg;
			errmsg << "Error: " << bytesPerPixel << " bytes per pixel not supported";
			throw Exception(errmsg.str(),EX_IO);
		}
		this->width = width;
		this->height = height;
		this->bytesPerPixel = bytesPerPixel;
		this->data = 0;
		if(width != 0 && height != 0) {
			this->data = new unsigned char[width * height * bytesPerPixel];
		}
	}

	Bitmap::Bitmap(const string filename) {
		bytesPerPixel = 4;
		FreeImage_Initialise();
		FREE_IMAGE_FORMAT format = FIF_UNKNOWN;
		FIBITMAP *bitmap = 0;
		//load bitmap into memory
		format = FreeImage_GetFileType(filename.c_str());
		if(format == FIF_UNKNOWN) {
			format = FreeImage_GetFIFFromFilename(filename.c_str());
		}
		if((format != FIF_UNKNOWN) && FreeImage_FIFSupportsReading(format)) {
			bitmap = FreeImage_Load(format,filename.c_str());
			if(bitmap == 0) {
				FreeImage_DeInitialise();
				stringstream errmsg;
				errmsg << "Error: could not load " << filename;
				throw Exception(errmsg.str(),EX_IO);
			}
		} else {
			FreeImage_DeInitialise();
			stringstream errmsg;
			errmsg << "Error: format of " << filename << " not supported";
			throw Exception(errmsg.str(),EX_IO);
		}
		//adapt format
		//from here on, bitmap must always be unloaded
		//if an exception is thrown
		unsigned int bpp = FreeImage_GetBPP(bitmap);
		unsigned char *internalData = (unsigned char *)FreeImage_GetBits(bitmap);
		if(bpp == 8) {
			width = FreeImage_GetWidth(bitmap);
			height = FreeImage_GetHeight(bitmap);
			data = new unsigned char[width * height * 4];
			//counters for 4 bytes alignment
			unsigned int lineCounter = 0;
			unsigned int additional = 0;
			for(unsigned int i=0 ; i<width*height ; i++) {
				data[i*4] = internalData[i+additional];
				data[i*4+1] = internalData[i+additional];
				data[i*4+2] = internalData[i+additional];
				data[i*4+3] = 255;
				lineCounter++;
				if(lineCounter == width) {
					//end of image row reached
					//add bytes to current reading position
					//until the next row's index is divisible by 4
					while(((i+1)+additional)%4 != 0) {
						additional++;
					}
					lineCounter = 0;
				}
			}
		} else if(bpp == 16) {
			width = FreeImage_GetWidth(bitmap);
			height = FreeImage_GetHeight(bitmap);
			data = new unsigned char[width * height * 4];
			//counters for 4 bytes alignment
			unsigned int lineCounter = 0;
			unsigned int additional = 0;
			for(unsigned int i=0 ; i<width*height ; i++) {
				data[i*4] = internalData[i*2+additional];
				data[i*4+1] = internalData[i*2+1+additional];
				data[i*4+2] = 0;
				data[i*4+3] = 255;
				lineCounter++;
				if(lineCounter == width) {
					//end of image row reached
					//add bytes to current reading position
					//until the next row's index is divisible by 4
					while(((i+1)*2+additional)%4 != 0) {
						additional++;
					}
					lineCounter = 0;
				}
			}
		} else if(bpp == 24) {
			width = FreeImage_GetWidth(bitmap);
			height = FreeImage_GetHeight(bitmap);
			data = new unsigned char[width * height * 4];
			//counters for 4 bytes alignment
			unsigned int lineCounter = 0;
			unsigned int additional = 0;
			for(unsigned int i=0 ; i<width*height ; i++) {
				data[i*4] = internalData[i*3+2+additional];
				data[i*4+1] = internalData[i*3+1+additional];
				data[i*4+2] = internalData[i*3+additional];
				data[i*4+3] = 255;
				lineCounter++;
				if(lineCounter == width) {
					//end of image row reached
					//add bytes to current reading position
					//until the next row's index is divisible by 4
					while(((i+1)*3+additional)%4 != 0) {
						additional++;
					}
					lineCounter = 0;
				}
			}
		} else if(bpp == 32) {
			width = FreeImage_GetWidth(bitmap);
			height = FreeImage_GetHeight(bitmap);
			data = new unsigned char[width * height * 4];
			for(unsigned int i=0 ; i<width*height ; i++) {
				//FreeImage uses bgra colors
				// but Bitmap uses rgba colors
				data[i*4] = internalData[i*4+2];
				data[i*4+1] = internalData[i*4+1];
				data[i*4+2] = internalData[i*4];
				data[i*4+3] = internalData[i*4+3];
			}
		} else if(bpp == 48) {
			bytesPerPixel = 16;
			width = FreeImage_GetWidth(bitmap);
			height = FreeImage_GetHeight(bitmap);
			data = new unsigned char[width * height * 16];
			FIRGB16 *source = (FIRGB16 *)internalData;
			float *destination = (float *)data;
			for(unsigned int i=0 ; i<width*height ; i++) {
				FIRGB16 &pixel = source[i];
				destination[i*4] = ((float)pixel.red)/65535.0f;
				destination[i*4+1] = ((float)pixel.green)/65535.0f;
				destination[i*4+2] = ((float)pixel.blue)/65535.0f;
				destination[i*4+3] = 1;
			}
		} else if(bpp == 64) {
			bytesPerPixel = 16;
			width = FreeImage_GetWidth(bitmap);
			height = FreeImage_GetHeight(bitmap);
			data = new unsigned char[width * height * 16];
			FIRGBA16 *source = (FIRGBA16 *)internalData;
			float *destination = (float *)data;
			for(unsigned int i=0 ; i<width*height ; i++) {
				FIRGBA16 &pixel = source[i];
				destination[i*4] = ((float)pixel.red)/65535.0f;
				destination[i*4+1] = ((float)pixel.green)/65535.0f;
				destination[i*4+2] = ((float)pixel.blue)/65535.0f;
				destination[i*4+3] = ((float)pixel.alpha)/65535.0f;
			}
		} else {
			FreeImage_Unload(bitmap);
			FreeImage_DeInitialise();
			stringstream errmsg;
			errmsg << "Error: " << bpp << " bits per pixel not supported";
			throw Exception(errmsg.str(),EX_IO);
		}
		FreeImage_Unload(bitmap);
		FreeImage_DeInitialise();
	}

	EXPA Bitmap::~Bitmap() {
		SXout(LogMarkup("~Bitmap"));
		SXout("~Bitmap");
		if(data != 0) {
			SXout("delete");
			delete [] data;
		}
	}

	EXPA void Bitmap::save(const string filename) {
		if(width == 0 || height == 0) {
			stringstream errmsg;
			errmsg << "Error: can't write to " << filename << ", because width and height must be positive";
			throw Exception(errmsg.str(),EX_IO);
		}
		FreeImage_Initialise();
		unsigned int bpp = 32;
		stringstream fname;
		fname << filename;
		FREE_IMAGE_FORMAT format = FIF_UNKNOWN;
		format = FreeImage_GetFIFFromFilename(filename.c_str());
		if(format == FIF_UNKNOWN) {
			format = FIF_PNG;
			fname << ".png";
			if(bytesPerPixel == 16) {
				bpp = 64;
			}
		} else if(format == FIF_JPEG || format == FIF_BMP) {
			bpp = 24;
		} else if(format == FIF_PNG) {
			if(bytesPerPixel == 16) {
				bpp = 64;
			}
		}
		FIBITMAP *bitmap = 0;
		if(bpp == 24 || bpp == 32) {
			bitmap = FreeImage_Allocate(width,height,bpp);
		} else if(bpp == 64) {
			bitmap = FreeImage_AllocateT(FIT_RGBA16,width,height,16);
		}
		if(bitmap == 0) {
			FreeImage_DeInitialise();
			stringstream errmsg;
			errmsg << "Error: can't allocate space for " << fname.str();
			throw Exception(errmsg.str(),EX_IO);
		}
		//from here on bitmap must be unloaded,
		//if an exception is raised
		unsigned char *internalData = (unsigned char *)FreeImage_GetBits(bitmap);
		if(bpp == 24) {
			if(bytesPerPixel == 4) {
				//counters for 4 bytes alignment
				unsigned int lineCounter = 0;
				unsigned int additional = 0;
				for(unsigned int i=0 ; i<width*height ; i++) {
					internalData[i*3+2+additional] = data[i*4];
					internalData[i*3+1+additional] = data[i*4+1];
					internalData[i*3+additional] = data[i*4+2];
					lineCounter++;
					if(lineCounter == width) {
						//end of image row reached
						//add bytes to current reading position
						//until the next row's index is divisible by 4
						while(((i+1)*3+additional)%4 != 0) {
							additional++;
						}
						lineCounter = 0;
					}
				}
			} else if(bytesPerPixel == 16) {
				const float *source = (const float *)data;
				
				//counters for 4 bytes alignment
				unsigned int lineCounter = 0;
				unsigned int additional = 0;
				for(unsigned int i=0 ; i<width*height ; i++) {
					internalData[i*3+2+additional] = (unsigned char)(255 * max(min(source[i*4],1.0f),0.0f));
					internalData[i*3+1+additional] = (unsigned char)(255 * max(min(source[i*4+1],1.0f),0.0f));
					internalData[i*3+additional] = (unsigned char)(255 * max(min(source[i*4+2],1.0f),0.0f));
					lineCounter++;
					if(lineCounter == width) {
						//end of image row reached
						//add bytes to current reading position
						//until the next row's index is divisible by 4
						while(((i+1)*3+additional)%4 != 0) {
							additional++;
						}
						lineCounter = 0;
					}
				}
			}
		} else if(bpp == 32) {
			if(bytesPerPixel == 4) {
				for(unsigned int i=0 ; i<width*height ; i++) {
					internalData[i*4] = data[i*4+2];
					internalData[i*4+1] = data[i*4+1];
					internalData[i*4+2] = data[i*4];
					internalData[i*4+3] = data[i*4+3];
				}
			} else if(bytesPerPixel == 16) {
				const float *source = (const float *)data;
				for(unsigned int i=0 ; i<width*height ; i++) {
					internalData[i*4] = (unsigned char)(255 * max(min(source[i*4+2],1.0f),0.0f));
					internalData[i*4+1] = (unsigned char)(255 * max(min(source[i*4+1],1.0f),0.0f));
					internalData[i*4+2] = (unsigned char)(255 * max(min(source[i*4],1.0f),0.0f));
					internalData[i*4+3] = (unsigned char)(255 * max(min(source[i*4+3],1.0f),0.0f));
				}
			}
		} else if(bpp == 64) {
			const float *source = (const float *)data;
			FIRGBA16 *destination = (FIRGBA16 *)internalData;
			for(unsigned int i=0 ; i<width*height ; i++) {
				destination[i].red = (unsigned short)(65535 * max(min(source[i*4],1.0f),0.0f));
				destination[i].green = (unsigned short)(65535 * max(min(source[i*4+1],1.0f),0.0f));
				destination[i].blue = (unsigned short)(65535 * max(min(source[i*4+2],1.0f),0.0f));
				destination[i].alpha = (unsigned short)(65535 * max(min(source[i*4+3],1.0f),0.0f));
			}
		}
		BOOL saved = FreeImage_Save(format,bitmap,fname.str().c_str());
		if(!saved) {
			FreeImage_Unload(bitmap);
			FreeImage_DeInitialise();
			stringstream errmsg;
			errmsg << "Error: " << fname.str() << " could not be saved";
			throw Exception(errmsg.str(),EX_IO);
		}
		FreeImage_Unload(bitmap);
		FreeImage_DeInitialise();
	}

	EXPA unsigned int Bitmap::getWidth() const {
		return width;
	}

	EXPA unsigned int Bitmap::getHeight() const {
		return height;
	}

	EXPA unsigned int Bitmap::getPixelSize() const {
		return bytesPerPixel;
	}

	EXPA unsigned char *Bitmap::getData() {
		return data;
	}

}

#endif