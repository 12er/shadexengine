#ifndef _SX_PARSER_PARSEXMLDATA_CPP_
#define _SX_PARSER_PARSEXMLDATA_CPP_

/**
 * xml parser
 * (c) 2012 by Tristan Bauer
 */

#include <sx/SXParser.h>
#include <sx/Exception.h>
#include <sx/Log4SX.h>
#include <internal/Parsers.h>
#include <b_XMLdata.h>
#include <f_XMLdata.h>
#include <sstream>
using namespace std;

extern int XMLdataparse(_XMLNODE_ **outcome, XMLdatascan_t scanner);

namespace sx {

	/**
	 * converts _XMLNODE_ to XTag
	 */
	XTag *XMLToXNode(_XMLNODE_ &source,bool &deconstruct,Exception &ex) {
		XTag *tag = new XTag();
		tag->name = source.name;
		if(source.name.compare(source.endname) != 0) {
			//start and endtag must have the same name
			stringstream errmsg;
			errmsg << "Error: opening tag is " << source.name << ", but closing tag at line " << source.lineno << ", column " << source.columnno << " is " << source.endname;
			deconstruct = true;
			ex.setMessage(errmsg.str());
			ex.setType(EX_SYNTAX);
		}
		if(deconstruct) {
			//if an error occurred, leave node
			return tag;
		}
		for (vector<_XMLNODE_ *>::iterator nodeIter = source.ats.begin(); nodeIter != source.ats.end(); nodeIter++) {
			_XMLNODE_ &node = *(*nodeIter);
			map<string,string>::iterator iter = tag->stringAttribs.find(node.strID);
			if(iter == tag->stringAttribs.end()) {
				tag->stringAttribs[node.strID] = node.strAttrib.str();
			} else {
				//attribute IDs must not appear multiple times in the same tag
				stringstream errmsg;
				errmsg << "Error: attribute ID " << node.strID << " at line " << node.lineno << ", column " << node.columnno << " appears multiple times";
				deconstruct = true;
				ex.setMessage(errmsg.str());
				ex.setType(EX_SYNTAX);
			}
		}
		for (vector<_XMLNODE_ *>::iterator nodeIter = source.nodes.begin(); nodeIter != source.nodes.end(); nodeIter++) {
			_XMLNODE_ &node = *(*nodeIter);
			if(deconstruct) {
				//if an error occurred, leave node
				return tag;
			}
			if(node.type == _XTAG_) {
				//take child under any circumstances
				XTag *child = XMLToXNode(node,deconstruct,ex);
				tag->nodes.push_back(child);
			} else if(node.type == _XTEXT_) {
				XText *text = new XText();
				text->text = node.text.str();
				tag->nodes.push_back(text);
			}
		}
		return tag;
	}

	XTag *parseXMLdata(const string &data) {
		XTag *sxData = 0;
		resetXMLdataNo();

		//parse with flex, bison
		_XMLNODE_ *outcome = 0;
		XMLdatascan_t scanner;
		YY_BUFFER_STATE state;
		if (XMLdatalex_init(&scanner)) {
			throw Exception("Error: couldn't initialize XMLdata lexer");
		}
		state = XMLdata_scan_string(data.c_str(), scanner);
		if (XMLdataparse(&outcome, scanner)) {
			XMLdata_delete_buffer(state, scanner);
			XMLdatalex_destroy(scanner);
			stringstream errstr;
			errstr << "Error: couldn't parse XML data at line " << getXMLdataLineno() << ", column " << getXMLdataColumnno();
			throw Exception(errstr.str());
		}
		XMLdata_delete_buffer(state, scanner);
		XMLdatalex_destroy(scanner);
		if (outcome == 0) {
			stringstream errstr;
			errstr << "Error: couldn't parse XML data at line " << getXMLdataLineno() << ", column " << getXMLdataColumnno();
			throw Exception(errstr.str());
		}

		//translate _XMLNODE_ to XTag
		bool deconstruct = false;
		Exception ex;
		XTag *tag = XMLToXNode(*outcome, deconstruct, ex);
		delete outcome;
		if (deconstruct) {
			delete tag;
			throw ex;
		}
		return tag;
	}

	XTag *parseXMLFile(const string path) {
		string content = readFile(path);
		return parseXMLdata(content);
	}

}

#endif