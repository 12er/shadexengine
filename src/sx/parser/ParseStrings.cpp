#ifndef _SX_PARSER_PARSESTRINGS_CPP_
#define _SX_PARSER_PARSESTRINGS_CPP_

/**
 * string parser
 * (c) 2013 by Tristan Bauer
 */

#include <sx/SXParser.h>
#include <sx/Exception.h>
#include <sx/Log4SX.h>

namespace sx {

	vector<string> parseStrings(const string &stringList) {
		vector<string> strings;

		parserInternal::parseStrings(stringList,strings);

		return strings;
	}

}

#endif