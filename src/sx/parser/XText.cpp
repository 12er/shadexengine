#ifndef _SX_PARSER_XTEXT_CPP_
#define _SX_PARSER_XTEXT_CPP_

/**
 * text node class
 * (c) 2012 by Tristan Bauer
 */

#include <sx/SXParser.h>
#include <sx/Log4SX.h>

namespace sx {

	XText::XText(): XNode() {
	}

	XText::~XText() {
		SXout(LogMarkup("~XText"));
		SXout(text);
	}

}

#endif