#ifndef _SX_PARSER_PARSENUMBERS_CPP_
#define _SX_PARSER_PARSENUMBERS_CPP_

/**
 * number parser
 * (c) 2013 by Tristan Bauer
 */

#include <sx/SXParser.h>
#include <sx/Exception.h>
#include <sx/Log4SX.h>
#include <internal/Parsers.h>
#include <b_pNumbers.h>
#include <f_pNumbers.h>
#include <sstream>
using namespace std;

extern int pNumbersparse(VectorDouble **outcome, pNumbersscan_t scanner);

namespace sx {

	vector<double> parseNumbers(const string &numberList) {
		vector<double> numbers;
		resetPNumbersNo();

		VectorDouble *outcome = 0;
		pNumbersscan_t scanner;
		YY_BUFFER_STATE state;
		if(pNumberslex_init(&scanner)) {
			throw Exception("Error: couldn't initialize pNumbers lexer");
		}
		state = pNumbers_scan_string(numberList.c_str(), scanner);
		if (pNumbersparse(&outcome, scanner)) {
			pNumbers_delete_buffer(state, scanner);
			pNumberslex_destroy(scanner);
			stringstream errstr;
			errstr << "Error: couldn't parse number list at line " << getPNumbersLineno() << ", column " << getPNumbersColumnno();
			throw Exception(errstr.str());
		}
		pNumbers_delete_buffer(state, scanner);
		pNumberslex_destroy(scanner);
		if(outcome == 0) {
			stringstream errstr;
			errstr << "Error: couldn't parse number list at line " << getPNumbersLineno() << ", column " << getPNumbersColumnno();
			throw Exception(errstr.str());
		}
		numbers.reserve(outcome->v.size());
		numbers.insert(numbers.end(), outcome->v.begin(), outcome->v.end());
		delete outcome;

		return numbers;
	}

}

#endif
