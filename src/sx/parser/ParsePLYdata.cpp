#ifndef _SX_PARSER_PARSEPLYDATA_CPP_
#define _SX_PARSER_PARSEPLYDATA_CPP_

/**
 * ply parser
 * (c) 2012 by Tristan Bauer
 */

#include <sx/SXParser.h>
#include <sx/Exception.h>
#include <sx/Log4SX.h>
#include <internal/Parsers.h>
#include <b_PLYdata.h>
#include <f_PLYdata.h>
#include <sstream>
#include <cmath>
#include <algorithm>
#include <vector>
using namespace std;

extern int PLYdataparse(_XMESH_ **outcome, PLYdatascan_t scanner);

namespace sx {

	/**
	 * converts _XMESH_ to XMesh
	 */
	XMesh *transformToMesh(_XMESH_ &source) {
		int vertexPos = -1;
		int facePos = -1;
		unsigned int vertexStart = 0;
		unsigned int faceStart = 0;
		//start of rows for the current element
		//in the data section
		unsigned int currentLocation = 0;
		unsigned int currentIndex = 0;

		//search for vertices and faces
		for (vector<_ELEMENT_>::iterator elemIter = source.elements.begin(); elemIter != source.elements.end(); elemIter++) {
			_ELEMENT_ &elem = (*elemIter);
			if(currentLocation + elem.count > source.data.size()) {
				//region of this element out of bounds
				throw Exception("Error: element out of bounds",EX_COMPILE);
			}
			if(elem.name.compare("vertex") == 0) {
				vertexPos = currentIndex;
				vertexStart = currentLocation;
			} else if(elem.name.compare("face") == 0) {
				facePos = currentIndex;
				faceStart = currentLocation;
			}
			currentLocation += elem.count;
			currentIndex++;
		}

		//collect vertex structure
		if(vertexPos == -1) {
			throw Exception("Error: no vertices found",EX_COMPILE);
		}
		_ELEMENT_ &vertexStructure = source.elements[vertexPos];
		if(vertexStructure.count == 0) {
			throw Exception("Error: no vertices found",EX_COMPILE);
		}
		//find out what attributes need to be extracted
		unsigned int vertex_size = 0;
		int x_column = -1;
		int y_column = -1;
		int z_column = -1;
		int w_column = -1;
		unsigned int normal_size = 0;
		int nx_column = -1;
		int ny_column = -1;
		int nz_column = -1;
		int nw_column = -1;
		unsigned int tex_size = 0;
		int s_column = -1;
		int t_column = -1;
		int p_column = -1;
		int q_column = -1;
		unsigned int color_size = 0;
		int red_column = -1;
		int green_column = -1;
		int blue_column = -1;
		int alpha_column = -1;
		vector<string> other_names;
		vector<unsigned int> other_columns;
		//determine if to divide attribute by 255.0 or not
		bool x_normalize = false;
		bool y_normalize = false;
		bool z_normalize = false;
		bool w_normalize = false;
		bool nx_normalize = false;
		bool ny_normalize = false;
		bool nz_normalize = false;
		bool nw_normalize = false;
		bool s_normalize = false;
		bool t_normalize = false;
		bool p_normalize = false;
		bool q_normalize = false;
		bool red_normalize = false;
		bool green_normalize = false;
		bool blue_normalize = false;
		bool alpha_normalize = false;
		vector<bool> other_normalize;
		unsigned int column_index = 0;
		for(vector<_PROPERTY_>::iterator propIter = vertexStructure.properties.begin(); propIter != vertexStructure.properties.end(); propIter++) {
			_PROPERTY_ &prop = (*propIter);
			if(prop.multiplicity == M_LIST) {
				throw Exception("Error: inconsistent property declaration",EX_COMPILE);
			}
			bool normalize = false;
			if(prop.types[0] == T_UCHAR) {
				normalize = true;
			}
			if(prop.name.compare("x") == 0) {
				if(x_column == -1) {
					vertex_size++;
				}
				x_column = column_index;
				x_normalize = normalize;
			} else if(prop.name.compare("y") == 0) {
				if(y_column == -1) {
					vertex_size++;
				}
				y_column = column_index;
				y_normalize = normalize;
			} else if(prop.name.compare("z") == 0) {
				if(z_column == -1) {
					vertex_size++;
				}
				z_column = column_index;
				z_normalize = normalize;
			} else if(prop.name.compare("w") == 0) {
				if(w_column == -1) {
					vertex_size++;
				}
				w_column = column_index;
				w_normalize = normalize;
			} else if(prop.name.compare("nx") == 0) {
				if(nx_column == -1) {
					normal_size++;
				}
				nx_column = column_index;
				nx_normalize = normalize;
			} else if(prop.name.compare("ny") == 0) {
				if(ny_column == -1) {
					normal_size++;
				}
				ny_column = column_index;
				ny_normalize = normalize;
			} else if(prop.name.compare("nz") == 0) {
				if(nz_column == -1) {
					normal_size++;
				}
				nz_column = column_index;
				nz_normalize = normalize;
			} else if(prop.name.compare("nw") == 0) {
				if(nw_column == -1) {
					normal_size++;
				}
				nw_column = column_index;
				nw_normalize = normalize;
			} else if(prop.name.compare("s") == 0) {
				if(s_column == -1) {
					tex_size++;
				}
				s_column = column_index;
				s_normalize = normalize;
			} else if(prop.name.compare("t") == 0) {
				if(t_column == -1) {
					tex_size++;
				}
				t_column = column_index;
				t_normalize = normalize;
			} else if(prop.name.compare("p") == 0) {
				if(p_column == -1) {
					tex_size++;
				}
				p_column = column_index;
				p_normalize = normalize;
			} else if(prop.name.compare("q") == 0) {
				if(q_column == -1) {
					tex_size++;
				}
				q_column = column_index;
				q_normalize = normalize;
			} else if(prop.name.compare("red") == 0) {
				if(red_column == -1) {
					color_size++;
				}
				red_column = column_index;
				red_normalize = normalize;
			} else if(prop.name.compare("green") == 0) {
				if(green_column == -1) {
					color_size++;
				}
				green_column = column_index;
				green_normalize = normalize;
			} else if(prop.name.compare("blue") == 0) {
				if(blue_column == -1) {
					color_size++;
				}
				blue_column = column_index;
				blue_normalize = normalize;
			} else if(prop.name.compare("alpha") == 0) {
				if(alpha_column == -1) {
					color_size++;
				}
				alpha_column = column_index;
				alpha_normalize = normalize;
			} else {
				other_names.push_back(prop.name);
				other_columns.push_back(column_index);
				other_normalize.push_back(normalize);
			}
			column_index++;
		}

		//init buffers to collect vertex attributes
		unsigned int count_columns = vertex_size + normal_size + tex_size + color_size + other_columns.size();
		vector<vector<double> > vertices;
		vector<vector<double> > normals;
		vector<vector<double> > texs;
		vector<vector<double> > colors;
		vector<vector<vector<double> > > others;
		for(unsigned int i=0 ; i<other_columns.size() ; i++) {
			others.push_back(vector<vector<double>>());
		}
		//collect vertex attributes
		for(unsigned int i=vertexStart ; i < vertexStart + vertexStructure.count ; i++) {
			vector<double> &currentRow = source.data[i];
			if(currentRow.size() != count_columns) {
				throw Exception("Error: wrong number of vertex attributes",EX_COMPILE);
			}
			if(vertex_size > 0) {
				//collect data of attribute vertex
				vertices.push_back(vector<double>());
				vector<double> &curr = vertices[vertices.size() - 1];
				if(x_column != -1) {
					double value = currentRow[x_column];
					if(x_normalize) {
						value /= 255.0;
					}
					curr.push_back(value);
				}
				if(y_column != -1) {
					double value = currentRow[y_column];
					if(y_normalize) {
						value /= 255.0;
					}
					curr.push_back(value);
				}
				if(z_column != -1) {
					double value = currentRow[z_column];
					if(z_normalize) {
						value /= 255.0;
					}
					curr.push_back(value);
				}
				if(w_column != -1) {
					double value = currentRow[w_column];
					if(w_normalize) {
						value /= 255.0;
					}
					curr.push_back(value);
				}
			}
			if(normal_size > 0) {
				//collect data of attribute normal
				normals.push_back(vector<double>());
				vector<double> &curr = normals[normals.size() - 1];
				if(nx_column != -1) {
					double value = currentRow[nx_column];
					if(nx_normalize) {
						value /= 255.0;
					}
					curr.push_back(value);
				}
				if(ny_column != -1) {
					double value = currentRow[ny_column];
					if(ny_normalize) {
						value /= 255.0;
					}
					curr.push_back(value);
				}
				if(nz_column != -1) {
					double value = currentRow[nz_column];
					if(nz_normalize) {
						value /= 255.0;
					}
					curr.push_back(value);
				}
				if(nw_column != -1) {
					double value = currentRow[nw_column];
					if(nw_normalize) {
						value /= 255.0;
					}
					curr.push_back(value);
				}
			}
			if(tex_size > 0) {
				//collect data of attribute tex
				texs.push_back(vector<double>());
				vector<double> &curr = texs[texs.size() - 1];
				if(s_column != -1) {
					double value = currentRow[s_column];
					if(s_normalize) {
						value /= 255.0;
					}
					curr.push_back(value);
				}
				if(t_column != -1) {
					double value = currentRow[t_column];
					if(t_normalize) {
						value /= 255.0;
					}
					curr.push_back(value);
				}
				if(p_column != -1) {
					double value = currentRow[p_column];
					if(p_normalize) {
						value /= 255.0;
					}
					curr.push_back(value);
				}
				if(q_column != -1) {
					double value = currentRow[q_column];
					if(q_normalize) {
						value /= 255.0;
					}
					curr.push_back(value);
				}
			}
			if(color_size > 0) {
				//collect data of attribute color
				colors.push_back(vector<double>());
				vector<double> &curr = colors[colors.size() - 1];
				if(red_column != -1) {
					double value = currentRow[red_column];
					if(red_normalize) {
						value /= 255.0;
					}
					curr.push_back(value);
				}
				if(green_column != -1) {
					double value = currentRow[green_column];
					if(green_normalize) {
						value /= 255.0;
					}
					curr.push_back(value);
				}
				if(blue_column != -1) {
					double value = currentRow[blue_column];
					if(blue_normalize) {
						value /= 255.0;
					}
					curr.push_back(value);
				}
				if(alpha_column != -1) {
					double value = currentRow[alpha_column];
					if(alpha_normalize) {
						value /= 255.0;
					}
					curr.push_back(value);
				}
			}
			//collect other attributes
			for(unsigned int j=0 ; j<other_columns.size() ; j++) {
				unsigned int otherIndex = other_columns[j];
				bool normalize = other_normalize[j];
				vector<vector<double>> &currBuffer = others[j];
				currBuffer.push_back(vector<double>());
				vector<double> &curr = currBuffer[currBuffer.size() - 1];
				double value = currentRow[otherIndex];
				if(normalize) {
					value /= 255.0;
				}
				curr.push_back(value);
			}
		}

		if(facePos != -1 && source.elements[facePos].count > 0) {
			//an index is given to define faces
			//evaluate face structure
			_ELEMENT_ &faceStructure = source.elements[facePos];
			int indexListIndex = -1;
			column_index = 0;
			for (vector<_PROPERTY_>::iterator propIter = faceStructure.properties.begin(); propIter != faceStructure.properties.end(); propIter++) {
				_PROPERTY_ &prop = (*propIter);
				if(prop.name.compare("vertex_indices") == 0) {
					indexListIndex = column_index;
				}
				column_index++;
			}
			if(indexListIndex == -1) {
				throw Exception("Error: index list declaration is inconsistent",EX_COMPILE);
			}
			_PROPERTY_ &indexListStructure = faceStructure.properties[indexListIndex];
			if(indexListStructure.multiplicity == M_SINGLE) {
				throw Exception("Error: index list declaration too short",EX_COMPILE);
			}
			if(indexListStructure.types.size() != 1 && indexListStructure.types.size() != 2) {
				throw Exception("Error: index list types declaration is inconsistent",EX_COMPILE);
			}
			for (vector<int>::iterator typeIter = indexListStructure.types.begin(); typeIter != indexListStructure.types.end(); typeIter++) {
				int type = (*typeIter);
				if(type == T_CHAR || type == T_SHORT || type == T_INT ||
					type == T_LONG || type == T_FLOAT ||type == T_DOUBLE) {
					throw Exception("Error: index list must be made of nonnegative integer numbers",EX_COMPILE);
				}
			}

			//output buffers to write the information into
			vector<vector<vector<double> > *> inputBuffers;
			vector<XBuffer *> outputBuffers;
			//initialize mesh, must be deconstructed
			//in the case of an error
			//as long as the face size is not known, let it
			//be zero
			XMesh *mesh = new XMesh();
			mesh->faceSize = 0;
			if(vertex_size > 0) {
				XBuffer *buffer = new XBuffer();
				buffer->name = "vertices";
				buffer->attributeSize = 0;
				if(x_column != -1) {
					buffer->attributeSize++;
				}
				if(y_column != -1) {
					buffer->attributeSize++;
				}
				if(z_column != -1) {
					buffer->attributeSize++;
				}
				if(w_column != -1) {
					buffer->attributeSize++;
				}
				mesh->buffers.insert(pair<string,XBuffer *>(buffer->name,buffer));
				inputBuffers.push_back(&vertices);
				outputBuffers.push_back(buffer);
			}
			if(normal_size > 0) {
				XBuffer *buffer = new XBuffer();
				buffer->name = "normals";
				buffer->attributeSize = 0;
				if(nx_column != -1) {
					buffer->attributeSize++;
				}
				if(ny_column != -1) {
					buffer->attributeSize++;
				}
				if(nz_column != -1) {
					buffer->attributeSize++;
				}
				if(nw_column != -1) {
					buffer->attributeSize++;
				}
				mesh->buffers.insert(pair<string,XBuffer *>(buffer->name,buffer));
				inputBuffers.push_back(&normals);
				outputBuffers.push_back(buffer);
			}
			if(tex_size > 0) {
				XBuffer *buffer = new XBuffer();
				buffer->name = "texcoords";
				buffer->attributeSize = 0;
				if(s_column != -1) {
					buffer->attributeSize++;
				}
				if(t_column != -1) {
					buffer->attributeSize++;
				}
				if(p_column != -1) {
					buffer->attributeSize++;
				}
				if(q_column != -1) {
					buffer->attributeSize++;
				}
				mesh->buffers.insert(pair<string,XBuffer *>(buffer->name,buffer));
				inputBuffers.push_back(&texs);
				outputBuffers.push_back(buffer);
			}
			if(color_size > 0) {
				XBuffer *buffer = new XBuffer();
				buffer->name = "colors";
				buffer->attributeSize = 0;
				if(red_column != -1) {
					buffer->attributeSize++;
				}
				if(green_column != -1) {
					buffer->attributeSize++;
				}
				if(blue_column != -1) {
					buffer->attributeSize++;
				}
				if(alpha_column != -1) {
					buffer->attributeSize++;
				}
				mesh->buffers.insert(pair<string,XBuffer *>(buffer->name,buffer));
				inputBuffers.push_back(&colors);
				outputBuffers.push_back(buffer);
			}
			if(other_columns.size() > 0) {
				for(unsigned int i=0 ; i < other_columns.size() ; i++) {
					const string &name = other_names[i];
					vector<vector<double>> &other_instance = others[i];
					map<string,XBuffer *>::iterator iter = mesh->buffers.find(name);
					if(iter != mesh->buffers.end()) {
						//two buffer with the same name can't exist
						delete mesh;
						stringstream errmsg;
						errmsg << "Error: two buffers with name " << name;
						throw Exception(errmsg.str(),EX_COMPILE);
					}
					XBuffer *buffer = new XBuffer();
					buffer->name = name;
					buffer->attributeSize = 1;
					mesh->buffers.insert(pair<string,XBuffer *>(buffer->name,buffer));
					inputBuffers.push_back(&other_instance);
					outputBuffers.push_back(buffer);
				}
			}

			//determine face size
			vector<double> &testBuffer = source.data[faceStart];
			if(testBuffer.size() - 1 > 0) {
				mesh->faceSize = min((unsigned int)3, (unsigned int)(testBuffer.size() - 1));
			} else {
				delete mesh;
				throw Exception("Error: faces of size zero",EX_COMPILE);
			}

			//create mesh indices
			vector<vector<unsigned int>> indices;
			if(mesh->faceSize < 3) {
				//mesh made of points or lines
				for(unsigned int faceIndex = faceStart ; faceIndex < faceStart + faceStructure.count ; faceIndex++) {
					//guarantee consistence of mesh indices
					vector<double> &indexRow = source.data[faceIndex];
					indices.push_back(vector<unsigned int>());
					vector<unsigned int> &curr = indices[indices.size() - 1];
					if(indexRow.size() - 1 != mesh->faceSize) {
						delete mesh;
						throw Exception("Error: inconsistency in faces",EX_COMPILE);
					}
					for(unsigned int i=1 ; i<indexRow.size() ; i++) {
						unsigned int ind = (unsigned int)floor(indexRow[i]);
						if(ind < vertexStart || ind >= vertexStart + vertexStructure.count) {
							delete mesh;
							throw Exception("Error: face index out of bounds",EX_COMPILE);
						}
						curr.push_back(ind);
					}
				}
				
			} else {
				//mesh made of polygons
				for(unsigned int faceIndex = faceStart ; faceIndex < faceStart + faceStructure.count ; faceIndex++) {
					//guarantee consistence of mesh indices
					vector<double> &indexRow = source.data[faceIndex];
					if(indexRow.size() - 1 < 3) {
						delete mesh;
						throw Exception("Error: inconsistency in faces",EX_COMPILE);
					}
					//split polygon into triangles
					for(unsigned int i=2 ; i<indexRow.size()-1 ; i++) {
						indices.push_back(vector<unsigned int>());
						vector<unsigned int> &curr = indices[indices.size() - 1];
						unsigned int ind1 = (unsigned int)floor(indexRow[1]);
						unsigned int ind2 = (unsigned int)floor(indexRow[i]);
						unsigned int ind3 = (unsigned int)floor(indexRow[i+1]);
						if(ind1 < vertexStart || ind1 >= vertexStart + vertexStructure.count
							|| ind2 < vertexStart || ind2 >= vertexStart + vertexStructure.count
							|| ind3 < vertexStart || ind3 >= vertexStart + vertexStructure.count
							) {
							delete mesh;
							throw Exception("Error: face index out of bounds",EX_COMPILE);
						}
						curr.push_back(ind1);
						curr.push_back(ind2);
						curr.push_back(ind3);
					}
				}
			}

			//iterate indices to fill the mesh with the values
			for(unsigned int i=0 ; i<outputBuffers.size() ; i++) {
				vector<vector<double> > *inputBuffer = inputBuffers[i];
				XBuffer *outputBuffer = outputBuffers[i];
				for (vector < vector<unsigned int> >::iterator faceIter = indices.begin(); faceIter != indices.end(); faceIter++) {
					vector<unsigned int> &face = (*faceIter);
					for (vector<unsigned int>::iterator indIter = face.begin(); indIter != face.end(); indIter++) {
						unsigned int ind = (*indIter);
						vector<double> &inputRow = (*inputBuffer)[ind];
						for (vector<double>::iterator valIter = inputRow.begin(); valIter != inputRow.end(); valIter++) {
							double val = (*valIter);
							outputBuffer->vertexAttributes.push_back(val);
						}
					}
				}
			}

			return mesh;
		} else {
			//no faces defined
			throw Exception("Error: no faces defined",EX_COMPILE);
		}
	}

	XMesh *parsePLYdata(const string &data) {
		XMesh *mesh = 0;
		resetPLYdataNo();

		//parse with flex, bison
		_XMESH_ *outcome = 0;
		PLYdatascan_t scanner;
		YY_BUFFER_STATE state;
		if (PLYdatalex_init(&scanner)) {
			throw Exception("Error: couldn't initialize PLYdata lexer");
		}
		state = PLYdata_scan_string(data.c_str(), scanner);
		if (PLYdataparse(&outcome, scanner)) {
			PLYdata_delete_buffer(state, scanner);
			PLYdatalex_destroy(scanner);
			stringstream errstr;
			errstr << "Error: couldn't parse PLY data at line " << getPLYdataLineno() << ", column " << getPLYdataColumnno();
			throw Exception(errstr.str());
		}
		PLYdata_delete_buffer(state, scanner);
		PLYdatalex_destroy(scanner);
		if (outcome == 0) {
			stringstream errstr;
			errstr << "Error: couldn't parse PLY data at line " << getPLYdataLineno() << ", column " << getPLYdataColumnno();
			throw Exception(errstr.str());
		}

		try {
			mesh = transformToMesh(*outcome);
		} catch (Exception &e) {
			delete outcome;
			throw e;
		}
		delete outcome;

		return mesh;
	}

	XMesh *parsePLYFile(const string &path) {
		string content = readFile(path);
		return parsePLYdata(content);
	}

}

#endif