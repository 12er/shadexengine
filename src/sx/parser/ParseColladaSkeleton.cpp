#ifndef _SX_PARSER_PARSECOLLADASKELETON_CPP_
#define _SX_PARSER_PARSECOLLADASKELETON_CPP_

/**
 * collada skeleton parser
 * (c) 2013 by Tristan Bauer
 */

#include <sx/SXParser.h>
#include <sx/Exception.h>
#include <sx/Log4SX.h>
#include <cmath>
using namespace std;

namespace sx {

	void _COLLECT_BONEHIERACHY_(Bone &currentBone, XTag *currentBoneData) {
		currentBone.ID = currentBoneData->getStrAttribute("id");
		XTag *matrixNode = currentBoneData->getFirst("matrix");
		vector<double> m = parseNumbers(matrixNode->getTexts());
		if(m.size() != 16) {
			throw Exception();
		}
		for(unsigned int i=0 ; i<4 ; i++) {
			//row
			for(unsigned int j=0 ; j<4 ; j++) {
				//column
				currentBone.parentTransform[i + j*4] = (float)m[i*4 + j];
			}
		}
		for(vector<XNode *>::iterator iter = currentBoneData->nodes.begin() ; iter != currentBoneData->nodes.end() ; iter++) {
			XTag *childData = dynamic_cast<XTag *>((*iter));
			if(childData != 0 && childData->name.compare("node") == 0 && childData->getStrAttribute("type").compare("JOINT") == 0) {
				//recursively collect child bones
				currentBone.bones.push_back(Bone());
				_COLLECT_BONEHIERACHY_(currentBone.bones[currentBone.bones.size()-1], childData);
			}
		}
	}

	void COLLADA_transformToSkeleton(XTag *collada, bool deleteCollada, pair<vector<Bone>,Matrix> &bones) {
		bones.first.clear();
		bones.second = Matrix();
		XTag *skeleton = 0;
		try {
			//lookup the name of the geometry
			//in the scene node with the id equal to this name
			//the identifier of the skeleton controller can be found
			XTag *geometry = collada->getFirst("library_geometries.geometry");
			string geometry_name = geometry->getStrAttribute("name");
			vector<XTag *> nodes;
			//lookup the identifier of the skeleton controller
			//the result will be put in skeletonID
			collada->getTags("library_visual_scenes.visual_scene.node",nodes);
			XTag *geoScene = 0;
			for(vector<XTag *>::iterator iter = nodes.begin() ; iter != nodes.end() ; iter++) {
				XTag *node = (*iter);
				if(node->name.compare("node") == 0 && node->getStrAttribute("id").compare(geometry_name) == 0) {
					geoScene = node;
					break;
				}
			}
			if(geoScene == 0) {
				throw Exception();
			}

			//get ids of root bones
			XTag *controllerLink = geoScene->getFirst("instance_controller");
			if(controllerLink == 0) {
				//controller not found
				//now root bones can't be explored
				throw Exception();
			}
			vector<XTag *> rootBoneLinks;
			controllerLink->getTags("skeleton",rootBoneLinks);
			vector<string> rootBoneIDs;
			for(vector<XTag *>::iterator iter = rootBoneLinks.begin() ; iter != rootBoneLinks.end() ; iter++) {
				string txt = (*iter)->getTexts();
				if(txt.size() < 2) {
					//each root bone id is composed
					//of character '#' and at least
					//one character for the bone id string
					throw Exception();
				}
				txt = txt.substr(1);
				rootBoneIDs.push_back(txt);
			}

			//now search for the node containing
			//JOINT nodes with rootbone IDs
			for(vector<XTag *>::iterator iter = nodes.begin() ; iter != nodes.end() ; iter++) {
				XTag *skeletonNode = (*iter);
				map<string,XTag *> candidateBones;
				//collect candidateBones
				for(vector<XNode *>::iterator cIter = skeletonNode->nodes.begin() ; cIter != skeletonNode->nodes.end() ; cIter++) {
					XTag *candidateBone = dynamic_cast<XTag *>((*cIter));
					if(candidateBone == 0) {
						//not a XTag, skip it
						continue;
					}
					if(candidateBone->name.compare("node") != 0) {
						//not a node tag, skip it
						continue;
					}
					string bID;
					try {
						bID = candidateBone->getStrAttribute("id");
					} catch(Exception &) {
						//node without id, skip it
						continue;
					}
					candidateBones[bID] = candidateBone;
				}
				//now check if the candidateBones' ids are the
				//root bones' names
				bool hasRootBones = true;
				for(vector<string>::iterator rbIter = rootBoneIDs.begin() ; rbIter != rootBoneIDs.end() ; rbIter++) {
					map<string,XTag *>::iterator findIter = candidateBones.find((*rbIter));
					if(findIter == candidateBones.end()) {
						//does not contain all root bones
						hasRootBones = false;
						break;
					}
				}
				if(hasRootBones) {
					//found tag with root bones
					skeleton = skeletonNode;
				}
			}
			if(skeleton == 0) {
				throw Exception();
			}
		} catch(Exception &) {
			if(deleteCollada) {
				delete collada;
			}
			throw Exception("No bones found in Collada mesh",EX_SYNTAX);
		}
		try {
			//extract matrix and bones
			for(vector<XNode *>::iterator iter = skeleton->nodes.begin() ; iter != skeleton->nodes.end() ; iter++) {
				XTag *tag = dynamic_cast<XTag *>((*iter));
				if(tag != 0 && tag->name.compare("translate") == 0) {
					//translate
					vector<double> trans = parseNumbers(tag->getTexts());
					Vector translate;
					for(unsigned int i=0 ; i<trans.size() && i<3 ; i++) {
						translate[i] = (float) trans[i];
					}
					bones.second = bones.second * Matrix().translate(translate);
				} else if(tag != 0 && tag->name.compare("rotate") == 0) {
					//rotate for a certain angle in degrees around a vector
					vector<double> rotAngle = parseNumbers(tag->getTexts());
					if(rotAngle.size() != 4) {
						throw Exception();
					}
					Vector axis((float)rotAngle[0], (float)rotAngle[1], (float)rotAngle[2]);
					float angle = (float) (sx::Pi * rotAngle[3] / 180.0);
					bones.second = bones.second * Matrix().rotate(axis,angle);
				} else if(tag != 0 && tag->name.compare("scale") == 0) {
					//scale
					vector<double> sc = parseNumbers(tag->getTexts());
					Vector scale(1,1,1);
					for(unsigned int i=0 ; i<sc.size() && i<3 ; i++) {
						scale[i] = (float) sc[i];
					}
					bones.second = bones.second * Matrix().scale(scale);
				} else if(tag != 0 && tag->name.compare("skew") == 0) {
					//skew
					vector<double> sc = parseNumbers(tag->getTexts());
					if(sc.size() != 7) {
						throw Exception();
					}
					float angle = (float)(sc[0] * Pi / 180.0);
					Vector rot((float)sc[1],(float)sc[2],(float)sc[3]);
					Vector shear((float)sc[4],(float)sc[5],(float)sc[6]);

					rot.normalize();
					shear.normalize();
					Vector a1 = shear * (shear.innerprod(rot));
					Vector a2 = rot + (--a1);
					a2.normalize();

					float an1 = rot.innerprod(a2);
					float an2 = rot.innerprod(shear);
					float rx = an1 * cos(angle) - an2 * sin(angle);
					float ry = an1 * sin(angle) + an2 * cos(angle);
					float alpha = (an1 == 0) ? 0 : (ry/rx - an2/an1);
					Matrix skewM(
						a2[0] * shear[0] * alpha + 1.0f , a2[0] * shear[1] * alpha , a2[0] * shear[2] * alpha ,
						a2[1] * shear[0] * alpha , a2[1] * shear[1] * alpha + 1.0f , a2[1] * shear[2] * alpha ,
						a2[2] * shear[0] * alpha , a2[2] * shear[1] * alpha , a2[2] * shear[2] * alpha + 1.0f
						);
					bones.second = bones.second * skewM;
				} else if(tag != 0 && tag->name.compare("matrix") == 0) {
					//matrix
					vector<double> mv = parseNumbers(tag->getTexts());
					if(mv.size() != 16) {
						throw Exception();
					}
					Matrix matr;
					for(unsigned int row = 0 ; row < 4 ; row++) {
						for(unsigned int column = 0 ; column < 4 ; column++) {
							unsigned int vIndex = row*4 + column;
							unsigned int mIndex = column*4 + row;
							matr[mIndex] = (float)mv[vIndex];
						}
					}
					bones.second = bones.second * matr;
				} else if(tag != 0 && tag->name.compare("node") == 0 && tag->getStrAttribute("type").compare("JOINT") == 0) {
					//it's a bone
					//recursively extract bone hierachy
					bones.first.push_back(Bone());
					_COLLECT_BONEHIERACHY_(bones.first[bones.first.size()-1], tag);
				}
			}
		} catch(Exception &) {
			if(deleteCollada) {
				delete collada;
			}
			throw Exception("Invalid bone structure",EX_SYNTAX);
		}
		if(bones.first.size() == 0) {
			if(deleteCollada) {
				delete collada;
			}
			throw Exception("No bones found in Collada mesh",EX_SYNTAX);
		}
	}

	pair<vector<Bone>,Matrix> parseColladaSkeleton(XTag &collada) {
		pair<vector<Bone>,Matrix> bones;
		COLLADA_transformToSkeleton(&collada, false, bones);
		return bones;
	}

	pair<vector<Bone>,Matrix> parseColladaSkeleton(const string &data) {
		pair<vector<Bone>,Matrix> bones;
		XTag *collada = parseXMLdata(data);
		COLLADA_transformToSkeleton(collada, true, bones);
		delete collada;
		return bones;
	}

	pair<vector<Bone>,Matrix> parseColladaSkeletonFile(const string &path) {
		string content = readFile(path);
		return parseColladaSkeleton(content);
	}

}

#endif
