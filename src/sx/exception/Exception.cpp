#ifndef _SX_EXCEPTION_EXCEPTION_CPP_
#define _SX_EXCEPTION_EXCEPTION_CPP_

/**
 * Exception class
 * (c) 2012 by Tristan Bauer
 */

#include <sx/Exception.h>

namespace sx {

	Exception::Exception(string message, int type) {
		this->message = message;
		this->type = type;
	}

	Exception::Exception(string message) {
		this->message = message;
		this->type = 0;
	}

	Exception::Exception() {
		this->message = "";
		this->type = 0;
	}

	Exception::Exception(const Exception &e) {
		this->message = e.message;
		this->type = e.type;
	}

	Exception &Exception::operator = (const Exception &e) {
		if(this == &e) {
			return this [0];
		}
		this->message = e.message;
		this->type = e.type;
		return this [0];
	}

	Exception::~Exception() throw() {
	}

	void Exception::setMessage(string message) {
		this->message = message;
	}

	string Exception::getMessage() const {
		return message;
	}

	void Exception::setType(int type) {
		this->type = type;
	}

	int Exception::getType() const {
		return type;
	}

}

#endif
