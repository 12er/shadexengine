#ifndef _DEMO_SHADEX_CPP_
#define _DEMO_SHADEX_CPP_

#include <demo/ShadeX.h>

/**
 * ShadeX Engine Demo
 * (c) 2012 by Tristan Bauer
 */
namespace sx {

	ShadeX SX::shadeX;

}

#endif