#ifndef _DEMO_RENDERER_CPP_
#define _DEMO_RENDERER_CPP_

/**
 * ShadeX Engine Demo
 * (c) 2014 by Tristan Bauer
 */
#include <demo/Entities.h>

namespace sx {

	Renderer *Renderer::renderer = 0;

}

#endif