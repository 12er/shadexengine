#ifndef _DEMO_ENTITIES_FIREFLY_CPP_
#define _DEMO_ENTITIES_FIREFLY_CPP_

/**
 * ShadeX Engine Demo
 * (c) 2012 by Tristan Bauer
 */
#include <demo/Entities.h>
#include <demo/ShadeX.h>
#include <sx/SXMath.h>
#include <cmath>
using namespace std;

namespace sx {

	Firefly::Firefly(int id) {
		this->id = id;
		model = new UniformMatrix("model");
		normal = new UniformMatrix("normalm");
		left = new UniformMatrix("left");
		right = new UniformMatrix("right");
		behind = new UniformMatrix("behind");
		middle = new UniformMatrix("middle");

		RenderObject &ffly = SX::shadeX.getRenderObject("mainobj.firefly");
		unordered_map<string, Uniform *> &uniforms = ffly.getUniformSet(id);
		uniforms["model"] = model;
		uniforms["normalm"] = normal;
		uniforms["left"] = left;
		uniforms["right"] = right;
		uniforms["behind"] = behind;
		uniforms["middle"] = middle;

		mmodel = new UniformMatrix("model");
		mnormal = new UniformMatrix("normalm");
		mleft = new UniformMatrix("left");
		mright = new UniformMatrix("right");
		mbehind = new UniformMatrix("behind");
		mmiddle = new UniformMatrix("middle");

		RenderObject &mffly = SX::shadeX.getRenderObject("mainobj.main.firefly");
		unordered_map<string, Uniform *> &muniforms = mffly.getUniformSet(id);
		muniforms["model"] = mmodel;
		muniforms["normalm"] = mnormal;
		muniforms["left"] = mleft;
		muniforms["right"] = mright;
		muniforms["behind"] = mbehind;
		muniforms["middle"] = mmiddle;

		changeAttractor = 10.0f * min(((float)rand() / (float)RAND_MAX),0.5f);
		speed = 10.0f * min(((float)rand()/(float)RAND_MAX) , 0.5f);
		wingspeed = 0.25f * ((float)rand()/(float)RAND_MAX) + 0.875f;
	}

	Firefly::~Firefly() {
		delete model;
		delete normal;
		delete left;
		delete right;
		delete behind;
		delete middle;
		delete mmodel;
		delete mnormal;
		delete mleft;
		delete mright;
		delete mbehind;
		delete mmiddle;
	}

	void Firefly::setSize(float size) {
		this->size = size;
	}

	void Firefly::setPosition(const Vector &pos) {
		fireflyposition = pos;
		attractor = fireflyposition + fireflydirection * 10.0f;
	}

	void Firefly::setDirection(const Vector &dir) {
		fireflydirection = dir;
		attractor = fireflyposition + fireflydirection * 10.0f;
	}

	void Firefly::calculate(SXRenderArea &area) {
		//movement of wings
		float wingsMoved = (float)(Tau * sin(area.getTime() * 5 * wingspeed) * 0.125);
		*left = Matrix().rotate(Vector(0,1,0),wingsMoved);
		*right = Matrix().rotate(Vector(0,-1,0),wingsMoved);
		*behind = Matrix().rotate(Vector(-1,0,0),wingsMoved);
		*middle = Matrix().translate(Vector(0,0,-wingsMoved*2.0f));

		//change attractor eventually
		changeAttractor -= (float)area.getDeltaTime();
		if(changeAttractor < 0) {
			Vector attractionPoint;
			if(Renderer::renderer != 0) {
				attractionPoint = Renderer::renderer->getFireflyAttractionPoint();
			}
			changeAttractor = speed * min(((float)rand() / (float)RAND_MAX),0.5f);
			attractor.random();
			attractor = Matrix().scale(Vector(100.0f,100.0f,50.0f)) * attractor + Vector(-50.0f,-50.0f,-25.0f) + attractionPoint;
		}

		//update position and viewdirection
		//viewdirection attracted to attractor
		Vector attract = (attractor + fireflyposition * -1.0f).normalize() * ((float)area.getDeltaTime());
		fireflydirection = fireflydirection + attract;
		fireflydirection.normalize();

		//update fireflyposition, take terrain height into account to avoid flying through the terrain
		fireflyposition = fireflyposition + fireflydirection * ((float)area.getDeltaTime() * size * 30);
		fireflyposition[2] += (((float)area.getDeltaTime() * 60.0f) * 0.07f * -wingsMoved / 0.125f / (float)Tau);
		fireflyposition[2] = max(fireflyposition[2] , terrainPosition[2] + 2.0f);

		//rotate to view dir
		fireflydirection.normalize();
		float sinval = fireflydirection.innerprod(Vector(-1,0,0));
		float cosval = fireflydirection.innerprod(Vector(0,1,0));
		float angle = acos(cosval);
		if(sinval < 0) {
			angle = -angle;
		}

		//interact with smoke
		if(Renderer::renderer != 0) {
			Renderer::renderer->addSmokeInteractor(fireflyposition,fireflydirection * size * 30.0f * 30.0f);
		}

		//request lookup of terrain height at the fireflyposition 
		if(Renderer::renderer != 0) {
			Renderer::renderer->addTerrainSamplePoint(fireflyposition,terrainPosition);
		}

		*model = Matrix().translate(fireflyposition) * Matrix().scale(size) * Matrix().rotate(Vector(0,0,1),angle);

		(*(Matrix *)normal) = *model;
		normal->submatrix();

		*mmodel = Matrix(*model);
		*mnormal = Matrix(*normal);
		*mleft = Matrix(*left);
		*mright = Matrix(*right);
		*mbehind = Matrix(*behind);
		*mmiddle = Matrix(*middle);
	}

}

#endif