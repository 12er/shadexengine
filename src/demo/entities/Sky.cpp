#ifndef _DEMO_ENTITIES_SKY_CPP_
#define _DEMO_ENTITIES_SKY_CPP_

/**
 * ShadeX Engine Demo
 * (c) 2012 by Tristan Bauer
 */
#include <demo/Entities.h>
#include <demo/ShadeX.h>
#include <sx/Log4SX.h>

namespace sx {

	Sky::Sky() {
		lightdir = &SX::shadeX.getUniformVector("mainscene.lightdir");
		ambient = &SX::shadeX.getUniformFloat("mainscene.ambient");
		position = &SX::shadeX.getUniformVector("mainscene.cam.position");
		model = &SX::shadeX.getUniformMatrix("mainobj.sky.model");
		daynightLightshafts = &SX::shadeX.getUniformFloat("daynightlightshafts");
	}

	Sky::~Sky() {
	}

	void Sky::setBlendLightshafts(float lightshaftfactor) {
		UniformFloat &blendlightshafts = SX::shadeX.getUniformFloat("blendlightshafts");
		blendlightshafts.value = lightshaftfactor;
	}

	void Sky::setLightdir(const Vector &ldir) {
		*lightdir = ldir;
	}

	Vector Sky::getLightdir() {
		return *lightdir;
	}

	void Sky::calculate(SXRenderArea &area) {
		UniformFloat &daytime = SX::shadeX.getUniformFloat("mainscene.daytime");
		Vector &skygroundcolor = SX::shadeX.getUniformVector("skygroundcolor");
		float angle = (Vector(*lightdir) * -1.0f).innerprod(Vector(0,0,1));
		daytime.value = 0.0f;
		if(angle >= 0.0) {
			daytime.value = (8.0f - angle*8.0f)/12.0f;
		}

		//set lightshaft influence depending on time of day
		daynightLightshafts->value = min(1.0f,max(0.0f, ((Vector(*lightdir) * (-1.0f)).innerprod(Vector(0,0,1)) + 0.1f)*30.0f ));

		//set ambient value depending on time of day
		float sunbrightness = (Vector(*lightdir) * (-1.0f)).innerprod(Vector(0,0,1));
		UniformFloat &ambient = SX::shadeX.getUniformFloat("mainscene.ambient");
		ambient.value = max(0.0f , min(0.25f , sunbrightness));
		UniformFloat &maxbrightness = SX::shadeX.getUniformFloat("mainscene.maxbrightness");
		maxbrightness.value = max(0.0f , min(1.0f , sunbrightness*3.0f));

		//calculate sun position in texture coordinates
		UniformFloat &sunlocaction = SX::shadeX.getUniformFloat("sunlocaction");
		Vector &eye = SX::shadeX.getUniformVector("mainscene.cam.position");
		Matrix &viewprojection = SX::shadeX.getUniformMatrix("mainscene.viewprojection");
		Vector &sunposition = SX::shadeX.getUniformVector("sunposition");
		Vector transfray = viewprojection * (-1.0f * (*lightdir) + eye);
		sunlocaction.value = transfray[2];
		transfray = transfray * (1.0f / transfray[3]);
		transfray[3] = 1.0f;
		sunposition = Matrix().translate(Vector(0.5f,0.5f,0)) * Matrix().scale(Vector(0.5f,0.5f,1)) * transfray;

		//read ground skycolor
		BufferedMesh &mesh = SX::shadeX.getBufferedMesh("skygroundcolor2.mesh");
		VertexBuffer *vbo = mesh.getBuffer("positions");
		const float *content = vbo->unlockRead();
		Vector skycolor = Vector(content[0],content[1],content[2]);
		vbo->lock();
		skygroundcolor = skycolor;

		*model = Matrix().translate(*position);

	}

}

#endif