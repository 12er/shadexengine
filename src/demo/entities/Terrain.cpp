#ifndef _DEMO_ENTITIES_TERRAIN_CPP_
#define _DEMO_ENTITIES_TERRAIN_CPP_

/**
 * ShadeX Engine Demo
 * (c) 2012 by Tristan Bauer
 */
#include <demo/Entities.h>
#include <demo/ShadeX.h>
#include <sx/Exception.h>
#include <sx/Log4SX.h>
#include <cmath>
#include <sstream>
using namespace std;

namespace sx {

	Terrain::Terrain() {
		viewerPosition = &SX::shadeX.getUniformVector("mainscene.cam.position");
		model = &SX::shadeX.getUniformMatrix("mainobj.terrain.model");
		projheightmap = &SX::shadeX.getUniformMatrix("mainobj.terrain.projheightmap");
		inputPointsMesh = &SX::shadeX.getBufferedMesh("terrain.inputpoints.mesh");
		terrainPointsMesh = &SX::shadeX.getBufferedMesh("terrain.terrainpoints.mesh");

		//read scene data
		bool criticalException = false;
		try {
			XTag *sceneTag = Renderer::renderer->getConfiguration();
			string heightmappath = sceneTag->getStrAttribute("terrainheightmap");
			try {
				Texture &heightmap = SX::shadeX.getTexture("mainobj.terrain.heightmap");
				heightmap.setPath(heightmappath);
				heightmap.load();

				Texture &normalmap = SX::shadeX.getTexture("mainobj.terrain.normals");
				normalmap.setWidth(heightmap.getWidth());
				normalmap.setHeight(heightmap.getHeight());
				normalmap.setPixelFormat(FLOAT_RGBA);
				normalmap.load();

				Texture &overlaymap = SX::shadeX.getTexture("terroverlay.texture");
				overlaymap.setWidth(heightmap.getWidth());
				overlaymap.setHeight(heightmap.getHeight());
				overlaymap.setPixelFormat(BYTE_RGBA);
				overlaymap.load();

				if(!heightmap.isLoaded()) {
					throw Exception();
				}
			} catch(Exception &) {
				criticalException = true;
				stringstream errmsg;
				errmsg << "Error: could not load heightmap from " << heightmappath;
				throw Exception(errmsg.str());
			}
		} catch(Exception &e) {
			if(criticalException) {
				Logger::get() << Level(L_ERROR) << e.getMessage();
				throw e;
			}
		}
		
		Effect &calcNormals = SX::shadeX.getEffect("calcterrnormals");
		calcNormals.render();

		Effect &calcOverlay = SX::shadeX.getEffect("calcterroverlay.effect");
		calcOverlay.render();

		Pass &genTerrain = SX::shadeX.getPass("terrain.generategeometry.pass");
		genTerrain.render();
	}

	Terrain::~Terrain() {
	}

	void Terrain::addSamplePoint(const Vector &inputPoint, Vector &terrainPoint) {
		inputPoints.push_back(inputPoint);
		terrainPoints.push_back(&terrainPoint);
		if(inputPoints.size() > inputPointsMesh->getMaxVertexCount()) {
			inputPointsMesh->setMaxVertexCount(inputPoints.size());
			vector<float> v1;
			inputPointsMesh->addBuffer("positions","opositions",v1,(unsigned int)3);
			inputPointsMesh->setFaceSize(1);
			inputPointsMesh->load();
			terrainPointsMesh->setMaxVertexCount(inputPoints.size());
			vector<float> v2;			
			terrainPointsMesh->addBuffer("positions","opositions",v2,(unsigned int)3);
			terrainPointsMesh->setFaceSize(1);
			terrainPointsMesh->load();
		}
		inputPointsMesh->setVertexCount(inputPoints.size());
		terrainPointsMesh->setVertexCount(inputPoints.size());
	}

	void Terrain::calculate(SXRenderArea &area) {
		float debugDistance = (float)area.getTime();

		//transform of world coordinates to terrain texture coordinates
		*projheightmap = Matrix().scale(Vector(0.5f,0.5f,0.5f,1)) * Matrix().translate(Vector(1,1)) * Matrix().orthographicPerspeciveMatrix(-150.0f,150.0f,-150.0f,150.0f,1,2.0f);

		//map intput points given in world coordinates to the points on the terrain under them
		VertexBuffer *ipVBO = inputPointsMesh->getBuffer("positions");
		float *ipBuffer = ipVBO->unlockWrite();
		for(unsigned int i=0 ; i<inputPoints.size() ; i++) {
			for(unsigned int j=0 ; j<3 ; j++) {
				ipBuffer[i*3 + j] = inputPoints[i][j];
			}
		}
		ipVBO->lock();
		Pass &readPointsPass = SX::shadeX.getPass("terrain.getheights.pass");
		readPointsPass.render();
		VertexBuffer *tpVBO = terrainPointsMesh->getBuffer("positions");
		const float *tpBuffer = tpVBO->unlockRead();
		for(unsigned int i=0 ; i<terrainPoints.size() ; i++) {
			for(unsigned int j=0 ; j<3 ; j++) {		
				(*terrainPoints[i])[j] = tpBuffer[i*3 + j];
			}
		}
		tpVBO->lock();
		inputPoints.clear();
		terrainPoints.clear();
	
		//the terrain is rendered by projecting a heightmap in the vertex shader on it
		//and changing the vertices' z-values
		//hence the mesh is a flat grid
		//the grid is always moved with the viewer
		//the terrain grid is assumed to have 300 x 300 equidistant vertices with coordinates in the quad with coordinates in [-300,300]^2 x {0}
		float delta = 300.0f/299.0f; //distance between adjacent vertices along an axis
		float indexX = floor(((*viewerPosition)[0] + (delta/2.0f))/delta);
		float indexY = floor(((*viewerPosition)[1] + (delta/2.0f))/delta);
		Vector newTerrainPosition(delta * indexX , delta * indexY);
		*model = Matrix().translate(newTerrainPosition);

		//each time the user leaves a certain area, the terrain geometry
		//in world space coordinates is generated such that the user is always
		//near the center of the terrain geometry
		if(!terrainPosition.equals(newTerrainPosition,0.1f)) {
			terrainPosition = newTerrainPosition;
			Pass &genTerrain = SX::shadeX.getPass("terrain.generategeometry.pass");
			genTerrain.render();
		}
	}

}

#endif
