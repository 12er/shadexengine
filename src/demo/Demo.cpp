#ifndef _DEMO_DEMO_CPP_
#define _DEMO_DEMO_CPP_

/**
 * ShadeX Engine Demo
 * (c) 2012 by Tristan Bauer
 */
#include <demo/Demo.h>
#include <sx/Log4SX.h>
#include <sx/Exception.h>
#include <demo/ShadeX.h>

namespace sx {

	Demo::Demo(const string &mapconfigfile) {
		configuration = 0;
		configuration = sx::parseSXFile(mapconfigfile);
		smoke = 0;
		sky = 0;
		useScreenDimensions = true;
	}

	Demo::~Demo() {
		if(configuration != 0) {
			delete configuration;
		}
	}

	XTag *Demo::getConfiguration() {
		return configuration;
	}

	void Demo::setUseScreenDimensions(bool use) {
		this->useScreenDimensions = use;
	}

	int Demo::getImageWidth() {
		return imageWidth;
	}

	int Demo::getImageHeight() {
		return imageHeight;
	}

	void Demo::setImageWidth(int width) {
		if(!useScreenDimensions) {
			this->imageWidth = width;
		}
	}

	void Demo::setImageHeight(int height) {
		if(!useScreenDimensions) {
			this->imageHeight = height;
		}
	}

	void Demo::setRenderStage(RenderStage renderStage) {
		this->renderStage = renderStage;
		if(smoke != 0 && renderStage != STAGE_USESMOKE && renderStage != STAGE_USESMOKELIGHTSHAFT) {
			this->smoke->setSimulateSmokeDynamics(false);
		} else if(smoke != 0) {
			this->smoke->setSimulateSmokeDynamics(true);
		}
	}

	RenderStage Demo::getRenderStage() const {
		return renderStage;
	}

	void Demo::setEmitSmoke(bool emitSmoke) {
		if(smoke != 0) {
			smoke->setEmitSmoke(emitSmoke);
		}
	}

	void Demo::setPreferredBottomPosition(const Vector &pos) {
		if(smoke != 0) {
			smoke->setPreferredBottomPosition(pos);
		}
	}

	Vector Demo::getPreferredBottomPosition() const {
		Vector bpos;
		if(smoke != 0) {
			bpos = smoke->getPreferredBottomPosition();
		}
		return bpos;
	}

	void Demo::addTerrainSamplePoint(const Vector &inputPoint, Vector &terrainPoint) {
		if(terrain != 0) {
			terrain->addSamplePoint(inputPoint, terrainPoint);
		}
	}

	void Demo::addSmokeInteractor(const Vector &position, const Vector &velocity) {
		if(smoke != 0) {
			smoke->addSmokeInteractor(position,velocity);
		}
	}

	void Demo::setFireflyAttractionPoint(const Vector &pos) {
		fireflyAttractionPoint = pos;
	}

	Vector Demo::getFireflyAttractionPoint() const {
		return fireflyAttractionPoint;
	}

	void Demo::setBlendLightshafts(float lightshaftfactor) {
		if(sky != 0) {
			sky->setBlendLightshafts(lightshaftfactor);
		}
	}

	void Demo::setLightdir(const Vector &lightdir) {
		if(sky != 0) {
			sky->setLightdir(lightdir);
		}
	}

	Vector Demo::getLightdir() {
		Vector lightdir(-1,-1,0);
		if(sky != 0) {
			lightdir = sky->getLightdir();
		}
		return lightdir;
	}

	void Demo::loadEntities() {
		viewer = new Viewer();
		entities.push_back(viewer);
		sky = new Sky();
		entities.push_back(sky);
		terrain = new Terrain();
		entities.push_back(terrain);
		entities.push_back(new Grass());
		entities.push_back(new Water());
		smoke = new Smoke();
		entities.push_back(smoke);

		Firefly *firefly1 = new Firefly(1);
		firefly1->setSize(0.25f);
		firefly1->setPosition(Vector(30,0,10));
		firefly1->setDirection(Vector(2,5));
		entities.push_back(firefly1);

		Firefly *firefly2 = new Firefly(2);
		firefly2->setSize(0.4f);
		firefly2->setPosition(Vector(10,-30,10));
		firefly2->setDirection(Vector(7,1));
		entities.push_back(firefly2);

		Firefly *firefly3 = new Firefly(3);
		firefly3->setSize(0.4f);
		firefly3->setPosition(Vector(90,-30,10));
		firefly3->setDirection(Vector(7,1));
		entities.push_back(firefly3);

		Firefly *firefly4 = new Firefly(4);
		firefly4->setSize(0.4f);
		firefly4->setPosition(Vector(80,100,10));
		firefly4->setDirection(Vector(7,1));
		entities.push_back(firefly4);

		Firefly *firefly5 = new Firefly(5);
		firefly5->setSize(0.4f);
		firefly5->setPosition(Vector(70,-70,10));
		firefly5->setDirection(Vector(7,1));
		entities.push_back(firefly5);

		Firefly *firefly6 = new Firefly(6);
		firefly6->setSize(0.4f);
		firefly6->setPosition(Vector(150,100,10));
		firefly6->setDirection(Vector(7,1));
		entities.push_back(firefly6);

		Firefly *firefly7 = new Firefly(7);
		firefly7->setSize(0.4f);
		firefly7->setPosition(Vector(100,-30,10));
		firefly7->setDirection(Vector(7,1));
		entities.push_back(firefly7);

		Firefly *firefly8 = new Firefly(8);
		firefly8->setSize(0.4f);
		firefly8->setPosition(Vector(10,-30,10));
		firefly8->setDirection(Vector(7,1));
		entities.push_back(firefly8);

		Firefly *firefly9 = new Firefly(9);
		firefly9->setSize(0.4f);
		firefly9->setPosition(Vector(10,-30,10));
		firefly9->setDirection(Vector(7,1));
		entities.push_back(firefly9);



		Firefly *firefly10 = new Firefly(10);
		firefly10->setSize(0.4f);
		firefly10->setPosition(Vector(10,-30,10));
		firefly10->setDirection(Vector(7,1));
		entities.push_back(firefly10);

		Firefly *firefly11 = new Firefly(11);
		firefly11->setSize(0.4f);
		firefly11->setPosition(Vector(90,-30,10));
		firefly11->setDirection(Vector(7,1));
		entities.push_back(firefly11);

		Firefly *firefly12 = new Firefly(12);
		firefly12->setSize(0.4f);
		firefly12->setPosition(Vector(80,100,10));
		firefly12->setDirection(Vector(7,1));
		entities.push_back(firefly12);

		Firefly *firefly13 = new Firefly(13);
		firefly13->setSize(0.4f);
		firefly13->setPosition(Vector(70,-70,10));
		firefly13->setDirection(Vector(7,1));
		entities.push_back(firefly13);

		Firefly *firefly14 = new Firefly(14);
		firefly14->setSize(0.4f);
		firefly14->setPosition(Vector(150,100,10));
		firefly14->setDirection(Vector(7,1));
		entities.push_back(firefly14);

		Firefly *firefly15 = new Firefly(15);
		firefly15->setSize(0.4f);
		firefly15->setPosition(Vector(100,-30,10));
		firefly15->setDirection(Vector(7,1));
		entities.push_back(firefly15);

		Firefly *firefly16 = new Firefly(16);
		firefly16->setSize(0.4f);
		firefly16->setPosition(Vector(10,-30,10));
		firefly16->setDirection(Vector(7,1));
		entities.push_back(firefly16);

		Firefly *firefly17 = new Firefly(17);
		firefly17->setSize(0.4f);
		firefly17->setPosition(Vector(10,-30,10));
		firefly17->setDirection(Vector(7,1));
		entities.push_back(firefly17);
	}

	void Demo::create(SXRenderArea &area) {
		this->renderStage = STAGE_USESMOKE;
		if(useScreenDimensions) {
			this->imageWidth = area.getWidth();
			this->imageHeight = area.getHeight();
		}
		try {
			//pipeline
			SX::shadeX.addResources("data/pipeline/effect.c");
			SX::shadeX.addResources("data/pipeline/resources.c");
			SX::shadeX.addResources("data/pipeline/audio.c");
			//shaders
			SX::shadeX.addResources("data/shader/sky.c");
			SX::shadeX.addResources("data/shader/firefly.c");
			SX::shadeX.addResources("data/shader/terrain.c");
			SX::shadeX.addResources("data/shader/water.c");
			SX::shadeX.addResources("data/shader/grass.c");
			SX::shadeX.addResources("data/shader/smoke.c");
			SX::shadeX.addResources("data/shader/lightshafts.c");
			SX::shadeX.addResources("data/shader/postscene.c");
			SX::shadeX.addResources("data/shader/SXlogo.c");
			//particlesystem
			SX::shadeX.addResources("data/mesh/particlesystem.c");
		} catch(Exception &e) {
			Logger::get() << LogMarkup("Demo::create") << e.getMessage();
			area.stopRendering();
			return;
		}
		try {
			SX::shadeX.load();
		} catch(Exception &e) {
			Logger::get() << LogMarkup("Demo::create") << e.getMessage();
		}
		try {
			loadEntities();
		} catch(Exception &e) {
			Logger::get() << Level(L_ERROR) << e.getMessage();
			area.stopRendering();
			return;
		}
	}

	void Demo::reshape(SXRenderArea &area) {
		if(useScreenDimensions) {
			this->imageWidth = area.getWidth();
			this->imageHeight = area.getHeight();
		}

		RenderTarget &mainsceneDisplay = SX::shadeX.getRenderTarget("mainscene.display");
		mainsceneDisplay.setWidth(imageWidth);
		mainsceneDisplay.setHeight(imageHeight);
		mainsceneDisplay.setRenderToDisplay(false);
		mainsceneDisplay.load();

		Texture &mainsceneDmap = SX::shadeX.getTexture("mainscene.dmap");
		mainsceneDmap.setWidth(imageWidth);
		mainsceneDmap.setHeight(imageHeight);
		mainsceneDmap.setPixelFormat(FLOAT16_RGBA);
		mainsceneDmap.load();

		Texture &mainsceneDmap2 = SX::shadeX.getTexture("mainscene.dmap2");
		mainsceneDmap2.setWidth(imageWidth);
		mainsceneDmap2.setHeight(imageHeight);
		mainsceneDmap2.setPixelFormat(FLOAT16_RGBA);
		mainsceneDmap2.load();

		Texture &mainsceneFrag1 = SX::shadeX.getTexture("mainscene.frag1");
		mainsceneFrag1.setWidth(imageWidth);
		mainsceneFrag1.setHeight(imageHeight);
		mainsceneFrag1.setPixelFormat(BYTE_RGBA);
		mainsceneFrag1.load();

		Texture &mainscene2Frag1 = SX::shadeX.getTexture("mainscene2.frag1");
		mainscene2Frag1.setWidth(imageWidth);
		mainscene2Frag1.setHeight(imageHeight);
		mainscene2Frag1.setPixelFormat(BYTE_RGBA);
		mainscene2Frag1.load();

		Texture &mainscene3Frag1 = SX::shadeX.getTexture("mainscene3.frag1");
		mainscene3Frag1.setWidth(imageWidth);
		mainscene3Frag1.setHeight(imageHeight);
		mainscene3Frag1.setPixelFormat(BYTE_RGBA);
		mainscene3Frag1.load();

		Texture &mainscene4Frag1 = SX::shadeX.getTexture("mainscene4.frag1");
		mainscene4Frag1.setWidth(imageWidth);
		mainscene4Frag1.setHeight(imageHeight);
		mainscene4Frag1.setPixelFormat(BYTE_RGBA);
		mainscene4Frag1.load();

		RenderTarget &refractionsceneRefrag = SX::shadeX.getRenderTarget("refractionscene.refrag");
		refractionsceneRefrag.setWidth(imageWidth);
		refractionsceneRefrag.setHeight(imageHeight);
		refractionsceneRefrag.setRenderToDisplay(false);
		refractionsceneRefrag.load();

		Texture &refractionsceneRefractionmap = SX::shadeX.getTexture("refractionscene.refractionmap");
		refractionsceneRefractionmap.setWidth(imageWidth);
		refractionsceneRefractionmap.setHeight(imageHeight);
		refractionsceneRefractionmap.setPixelFormat(FLOAT16_RGBA);
		refractionsceneRefractionmap.load();

		RenderTarget &reflectionsceneReflect = SX::shadeX.getRenderTarget("reflectionscene.reflect");
		reflectionsceneReflect.setWidth(imageWidth);
		reflectionsceneReflect.setHeight(imageHeight);
		reflectionsceneReflect.setRenderToDisplay(false);
		reflectionsceneReflect.load();

		Texture &reflectionsceneReflectionmap = SX::shadeX.getTexture("reflectionscene.reflectionmap");
		reflectionsceneReflectionmap.setWidth(imageWidth);
		reflectionsceneReflectionmap.setHeight(imageHeight);
		reflectionsceneReflectionmap.setPixelFormat(BYTE_RGBA);
		reflectionsceneReflectionmap.load();

		Texture &identifycastboxTexture = SX::shadeX.getTexture("identifycastbox.texture");
		identifycastboxTexture.setWidth(imageWidth);
		identifycastboxTexture.setHeight(imageHeight);
		identifycastboxTexture.setPixelFormat(FLOAT16_RGBA);
		identifycastboxTexture.load();

		RenderTarget &identifycastboxTarget = SX::shadeX.getRenderTarget("identifycastbox.target");
		identifycastboxTarget.setWidth(imageWidth);
		identifycastboxTarget.setHeight(imageHeight);
		identifycastboxTarget.setRenderToDisplay(false);
		identifycastboxTarget.load();

		Texture &postsceneShadexlogo = SX::shadeX.getTexture("postscene.shadexlogo");
		postsceneShadexlogo.setWidth(imageWidth);
		postsceneShadexlogo.setHeight(imageHeight);
		postsceneShadexlogo.setPixelFormat(FLOAT16_RGBA);
		postsceneShadexlogo.load();

		RenderTarget &postsceneShadexlogoTarget = SX::shadeX.getRenderTarget("postscene.shadexlogo.target");
		postsceneShadexlogoTarget.setWidth(imageWidth);
		postsceneShadexlogoTarget.setHeight(imageHeight);
		postsceneShadexlogoTarget.setRenderToDisplay(false);
		postsceneShadexlogoTarget.load();

		RenderTarget &displaytarget = SX::shadeX.getRenderTarget("displaytarget");
		displaytarget.setWidth(area.getWidth());
		displaytarget.setHeight(area.getHeight());
		displaytarget.setRenderToDisplay(true);
		displaytarget.load();
	}

	void Demo::render(SXRenderArea &area) {
		if(viewer == 0 || !viewer->isDemoMode()) {
			area.setMousePointer(area.getWidth()/2,area.getHeight()/2);
		}

		for(vector<Entity *>::iterator iter = entities.begin() ; iter != entities.end() ; iter++) {
			(*iter)->calculate(area);
		}

		UniformFloat &deltaTime = SX::shadeX.getUniformFloat("deltatime.float");
		deltaTime.value = (float)area.getDeltaTime();

		AudioPass &audio = SX::shadeX.getAudioPass("backgroundmusic.pass");
		audio.update();

		if(renderStage == STAGE_USESMOKE) {
			if(smoke != 0 && smoke->getCurrentTarget() == 0) {
				//cast volume "medium1.volume" on even frames
				Effect &effect = SX::shadeX.getEffect("maineffect.smoke.medium1");
				effect.render();
			} else {
				//cast volume "medium2.volume" on odd frames
				Effect &effect = SX::shadeX.getEffect("maineffect.smoke.medium2");
				effect.render();
			}
		} else if(renderStage == STAGE_USESMOKELIGHTSHAFT) {
			if(smoke != 0 && smoke->getCurrentTarget() == 0) {
				//cast volume "medium1.volume" on even frames
				Effect &effect = SX::shadeX.getEffect("maineffect.smoke.medium1.lightshaft");
				effect.render();
			} else {
				//cast volume "medium2.volume" on odd frames
				Effect &effect = SX::shadeX.getEffect("maineffect.smoke.medium2.lightshaft");
				effect.render();
			}
		} else if(renderStage == STAGE_USELIGHTSHAFT) {
			Effect &effect = SX::shadeX.getEffect("maineffect.lightshaft");
			effect.render();
		} else if(renderStage == STAGE_COMMON) {
			Effect &effect = SX::shadeX.getEffect("maineffect");
			effect.render();
		}

		if(area.hasKey(SX_ESC)) {
			//pressed ESC, quit
			area.stopRendering();
		}
	}

	void Demo::stop(SXRenderArea &area) {
		for(vector<Entity *>::iterator iter = entities.begin() ; iter != entities.end() ; iter++) {
			delete (*iter);
		}
		SX::shadeX.clear();
	}

}

#endif