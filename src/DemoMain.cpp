#ifndef _STARTDEMO_CPP_
#define _STARTDEMO_CPP_

/**
 * ShadeX Engine Demo
 * (c) 2012 by Tristan Bauer
 */
#include <demo/Demo.h>
#include <sx/Log4SX.h>
#include <sx/SX.h>
#include <sx/Exception.h>
#include <sx/SXWidget.h>
#include <QApplication>
#include <QSplashScreen>
#include <QDesktopWidget>
#include <QIcon>
#include <string>
using namespace std;

/**
 * Entry point of the demo
 */
int main(int argc, char **argv) {
	QApplication app(argc,argv);
	return sx::StartDemo(app);
}

#endif