#ifndef _TEST_SXPARSER_TESTNODES_CPP_
#define _TEST_SXPARSER_TESTNODES_CPP_

/**
 * test cases with nodes
 * (c) 2012 by Tristan Bauer
 */

#include <test/SXParser.h>
#include <sx/SXParser.h>
#include <sx/Log4SX.h>
#include <sx/XUnit.h>
#include <vector>
using namespace sx;
using namespace std;

namespace sxparser {

	/**
	 * generates the first test tree
	 */
	XTag *getTree1() {
		//construct a test tree
		//root
		XTag *bla_n = new XTag();
		bla_n->name = "bla";
		//first generation
		XTag *a_n = new XTag();
		a_n->name = "a";
		XTag *tristan_n = new XTag();
		tristan_n->name = "tristan";
		XText *text1 = new XText();
		text1->text = "someshit";
		bla_n->nodes.push_back(a_n);
		bla_n->nodes.push_back(tristan_n);
		bla_n->nodes.push_back(text1);
		//second generation
		XText *text2 = new XText();
		text2->text = "blafasl";
		XTag *otto_n = new XTag();
		otto_n->name = "otto";
		XText *text3 = new XText();
		text3->text = "bauer";
		XTag *bauer_n = new XTag();
		bauer_n->name = "bauer";
		XText *text4 = new XText();
		text4->text = "bauer";
		XTag *willie_n = new XTag();
		willie_n->name = "willie";
		XText *text5 = new XText();
		text5->text = "5. text";
		XTag *bauer_n2 = new XTag();
		bauer_n2->name = "bauer";
		bauer_n2->realAttribs.insert(pair<string,double>("id",1));
		bauer_n2->realAttribs.insert(pair<string,double>("bla",3));
		bauer_n2->realAttribs.insert(pair<string,double>("fasl",2));
		bauer_n2->realAttribs.insert(pair<string,double>("fuck",55));
		bauer_n2->realAttribs.insert(pair<string,double>("pi",100));
		bauer_n2->stringAttribs.insert(pair<string,string>("bla","hey"));
		bauer_n2->stringAttribs.insert(pair<string,string>("name","hi"));
		bauer_n2->stringAttribs.insert(pair<string,string>("sack","erl"));
		bauer_n2->stringAttribs.insert(pair<string,string>("tx","haha"));
		bauer_n2->stringAttribs.insert(pair<string,string>("t1","hhhh"));
		tristan_n->nodes.push_back(text2);
		tristan_n->nodes.push_back(otto_n);
		tristan_n->nodes.push_back(text3);
		tristan_n->nodes.push_back(bauer_n);
		tristan_n->nodes.push_back(text4);
		tristan_n->nodes.push_back(willie_n);
		tristan_n->nodes.push_back(text5);
		tristan_n->nodes.push_back(bauer_n2);
		//third generation
		XTag *a_n2 = new XTag();
		a_n2->name = "a";
		XTag *andreas_n = new XTag();
		andreas_n->name = "andreas";
		andreas_n->realAttribs.insert(pair<string,double>("pi",6));
		andreas_n->realAttribs.insert(pair<string,double>("fuck",3));
		andreas_n->stringAttribs.insert(pair<string,string>("tx","bla"));
		XTag *andreas_n2 = new XTag();
		andreas_n2->name = "andreas";
		andreas_n2->realAttribs.insert(pair<string,double>("pi",3.14159));
		andreas_n2->realAttribs.insert(pair<string,double>("id",3));
		andreas_n2->stringAttribs.insert(pair<string,string>("t1","yey"));
		andreas_n2->stringAttribs.insert(pair<string,string>("t2","lucky"));
		andreas_n2->stringAttribs.insert(pair<string,string>("t3","na"));
		andreas_n2->stringAttribs.insert(pair<string,string>("sack","gasse"));
		andreas_n2->stringAttribs.insert(pair<string,string>("name","bauer"));
		otto_n->nodes.push_back(a_n2);
		bauer_n->nodes.push_back(andreas_n);
		bauer_n2->nodes.push_back(andreas_n2);
		//fourth generation
		XText *text6 = new XText();
		text6->text = "fasdsda";
		XTag *b_n = new XTag();
		b_n->name = "b";
		XText *text7 = new XText();
		text7->text = "erdprokker";
		XTag *b_n2 = new XTag();
		b_n2->name = "b";
		XText *text8 = new XText();
		text8->text = "himmelskropper";
		XTag *c_n = new XTag();
		c_n->name = "c";
		XText *text9 = new XText();
		text9->text = "signanz";
		XTag *ali_n = new XTag();
		ali_n->name = "ali";
		XTag *c_n2 = new XTag();
		c_n2->name = "c";
		XTag *b_n3 = new XTag();
		b_n3->name = "b";
		XText *text10 = new XText();
		text10->text = "faslbla";
		XTag *c_n3 = new XTag();
		c_n3->name = "c";
		XText *text11 = new XText();
		text11->text = "blablalaasds";
		XTag *b_n4 = new XTag();
		b_n4->name = "b";
		XText *text12 = new XText();
		text12->text = "tristan";
		XTag *b_n5 = new XTag();
		b_n5->name = "b";
		b_n5->stringAttribs.insert(pair<string,string>("baby","spielzeug"));
		a_n2->nodes.push_back(text6);
		a_n2->nodes.push_back(b_n);
		a_n2->nodes.push_back(text7);
		andreas_n->nodes.push_back(b_n2);
		andreas_n->nodes.push_back(text8);
		andreas_n->nodes.push_back(c_n);
		andreas_n->nodes.push_back(text9);
		andreas_n2->nodes.push_back(ali_n);
		andreas_n2->nodes.push_back(c_n2);
		andreas_n2->nodes.push_back(b_n3);
		andreas_n2->nodes.push_back(text10);
		andreas_n2->nodes.push_back(c_n3);
		andreas_n2->nodes.push_back(text11);
		andreas_n2->nodes.push_back(b_n4);
		andreas_n2->nodes.push_back(text12);
		andreas_n2->nodes.push_back(b_n5);
		
		return bla_n;
	}

	/**
	 * testcases for XTag method getTags
	 */
	void testGetTags(XUnit &unit) {
		unit.addMarkup("test cases for XTag::getNodes");

		vector<XTag *> nodes;
		XTag *tree1 = getTree1();
		tree1->getTags("*",nodes);

		nodes.clear();
		tree1->getTags("*",nodes);
		unit.addAnnotation("Search for '*' - size of outcome - ");
		unit.assertEquals((unsigned int)nodes.size(),(unsigned int)2);
		if(nodes.size() >= 2) {
			unit.addAnnotation("Search for '*' - compare tagname - ");
			unit.assertEquals(nodes[0]->name,"a");
			unit.addAnnotation("Search for '*' - compare tagname - ");
			unit.assertEquals(nodes[1]->name,"tristan");
		}
		
		nodes.clear();
		tree1->getTags("tristan.bauer.andreas.*",nodes);
		unit.addAnnotation("Search for 'tristan.bauer.andreas.*' - size of outcome - ");
		unit.assertEquals((unsigned int)nodes.size(),(unsigned int)8);
		if(nodes.size() >= 8) {
			unit.addAnnotation("Search for 'tristan.bauer.andreas.*' - compare tagname - ");
			unit.assertEquals(nodes[1]->name,"c");
			unit.addAnnotation("Search for 'tristan.bauer.andreas.*' - compare tagname - ");
			unit.assertEquals(nodes[2]->name,"ali");
			unit.addAnnotation("Search for 'tristan.bauer.andreas.*' - compare tagname - ");
			unit.assertEquals(nodes[5]->name,"c");
			unit.addAnnotation("Search for 'tristan.bauer.andreas.*' - compare tagname - ");
			unit.assertEquals(nodes[7]->name,"b");
		}

		tree1->getTags("tristan.*.*.b",nodes);
		unit.addAnnotation("Search for 'tristan.*.*.b', and append outcome to former result - size of outcome - ");
		unit.assertEquals((unsigned int)nodes.size(),(unsigned int)13);
		if(nodes.size() >= 13) {
			unit.addAnnotation("Merged outcome - compare tagname - ");
			unit.assertEquals(nodes[2]->name,"ali");
			unit.addAnnotation("Merged outcome - compare tagname - ");
			unit.assertEquals(nodes[7]->name,"b");
			for(unsigned int i = 8 ; i<13 ; i++) {
				unit.addAnnotation("Merged outcome - compare tagname - ");
				unit.assertEquals(nodes[8]->name,"b");
			}
		}

		delete tree1;
	}

	/**
	 * testcases for XTag method getFirst
	 */
	void testGetFirst(XUnit &unit) {
		unit.addMarkup("test cases for XTag::getFirst");
		XTag *tree1 = getTree1();

		try {
			XTag *outcome = tree1->getFirst("notexisting");
			unit.addAnnotation("getFirst - node should not exist - ");
			unit.assertTrue(false);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(true);
		}

		try {
			XTag *outcome = tree1->getFirst("tristan.bauer.andreas");
			unit.addAnnotation("getFirst(tristan.bauer.andreas) - compare name - ");
			unit.assertEquals(outcome->name,"andreas");
			unit.addAnnotation("getFirst - compare child node count - ");
			unit.assertEquals((unsigned int)outcome->nodes.size() , (unsigned int)4);
			XText *child = dynamic_cast<XText *>(outcome->nodes[3]);
			if(child != 0) {
				unit.addAnnotation("getFirst - compare 4. child text - ");
				unit.assertEquals(child->text,"signanz");
			} else {
				throw Exception("Error: 4. node is not XText");
			}
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}

		try {
			XTag *outcome = tree1->getFirst("tristan.*");
			unit.addAnnotation("getFirst(tristan.bauer.andreas) - compare name - ");
			unit.assertEquals(outcome->name,"otto");
			unit.addAnnotation("getFirst - compare child node count - ");
			unit.assertEquals((unsigned int)outcome->nodes.size() , (unsigned int)1);
			XTag *child = dynamic_cast<XTag *>(outcome->nodes[0]);
			if(child != 0) {
				unit.addAnnotation("getFirst - compare 4. child name - ");
				unit.assertEquals(child->name,"a");
			} else {
				throw Exception("Error: 1. node is not XTag");
			}
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}

		try {
			XTag *outcome = tree1->getFirst("tristan");
			unit.addAnnotation("getFirst(tristan.bauer.andreas) - compare name - ");
			unit.assertEquals(outcome->name,"tristan");
			unit.addAnnotation("getFirst - compare child node count - ");
			unit.assertEquals((unsigned int)outcome->nodes.size() , (unsigned int)8);
			XTag *child = dynamic_cast<XTag *>(outcome->nodes[3]);
			if(child != 0) {
				unit.addAnnotation("getFirst - compare 4. child name - ");
				unit.assertEquals(child->name,"bauer");
			} else {
				throw Exception("Error: 4. node is not XTag");
			}
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}

		try {
			XTag *outcome1 = tree1->getFirst("tristan.bauer.andreas.b");
			unit.addAnnotation("getFirst() - get father node to test getFirst() failing - ");
			unit.assertEquals(outcome1->name,"b");
			unit.addAnnotation("getFirst - compare child node count - ");
			unit.assertEquals((unsigned int)outcome1->nodes.size() , (unsigned int)0);
			try {
				outcome1->getFirst();
				unit.addAnnotation("getFirst - should not find any child nodes - ");
				unit.assertTrue(false);
			} catch(Exception &ex) {
				unit.addAnnotation(ex.getMessage());
				unit.assertTrue(true);
			}
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}

		try {
			XTag *outcome = tree1->getFirst();
			unit.addAnnotation("getFirst() - compare names - ");
			unit.assertEquals(outcome->name,"a");
			unit.addAnnotation("getFirst - compare child node count - ");
			unit.assertEquals((unsigned int)outcome->nodes.size() , (unsigned int)0);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}

		delete tree1;
	}

	void testGetAttribute(XUnit &unit) {
		unit.addMarkup("test cases for XTag::getStrAttribute and XTag::getRealAttribute");
		XTag *tree1 = getTree1();
		vector<XTag *> tags;
		tree1->getTags("tristan.bauer",tags);
		if(tags.size() < 2) {
			unit.addAnnotation("getAttribute - attributed node is missing - ");
			unit.assertTrue(false);
			delete tree1;
			return;
		}
		XTag *testNode = tags[1];

		try {
			string str = testNode->getStrAttribute("bla");
			unit.addAnnotation("getAttribute - compare values - ");
			unit.assertEquals(str,"hey");
			str = testNode->getStrAttribute("name");
			unit.addAnnotation("getAttribute - compare values - ");
			unit.assertEquals(str,"hi");
			str = testNode->getStrAttribute("t1");
			unit.addAnnotation("getAttribute - compare values - ");
			unit.assertEquals(str,"hhhh");
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}

		try {
			testNode->getStrAttribute("nothere");
			unit.addAnnotation("getAttribute - attribute should not exist - ");
			unit.assertTrue(false);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(true);
		}

		try {
			double val = testNode->getRealAttribute("id");
			unit.addAnnotation("getAttribute - compare values - ");
			unit.assertEquals(val,1.0);
			val = testNode->getRealAttribute("fuck");
			unit.addAnnotation("getAttribute - compare values - ");
			unit.assertEquals(val,55.0);
			val = testNode->getRealAttribute("pi");
			unit.addAnnotation("getAttribute - compare values - ");
			unit.assertEquals(val,100.0);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}

		try {
			testNode->getRealAttribute("nothere");
			unit.addAnnotation("getAttribute - attribute should not exist - ");
			unit.assertTrue(false);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(true);
		}

		delete tree1;
	}

	void testGetDirectTexts(XUnit &unit) {
		unit.addMarkup("test cases for XTag::getDirectTexts");
		XTag *tree1 = getTree1();
		XTag *testNode = 0;
		try {
			testNode = tree1->getFirst("tristan");
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
			delete tree1;
			return;
		}

		vector<string> texts;
		testNode->getDirectTexts(texts);
		unit.addAnnotation("getDirectTexts(texts) - compare node count - ");
		unit.assertEquals((unsigned int)texts.size(), (unsigned int)4);
		unit.addAnnotation("getDirectTexts - compare texts - ");
		unit.assertEquals(texts[0],"blafasl");
		unit.addAnnotation("getDirectTexts - compare texts - ");
		unit.assertEquals(texts[1],"bauer");
		unit.addAnnotation("getDirectTexts - compare texts - ");
		unit.assertEquals(texts[2],"bauer");
		unit.addAnnotation("getDirectTexts - compare texts - ");
		unit.assertEquals(texts[3],"5. text");

		try {
			string text = testNode->getDirectTexts();
			unit.addAnnotation("getDirectTexts() - compare text - ");
			unit.assertEquals(text,"blafaslbauerbauer5. text");
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}

		delete tree1;
	}

	void testGetTexts(XUnit &unit) {
		unit.addMarkup("test cases for XTag::getTexts");
		XTag *tree1 = getTree1();
		string srcTexts[] = {"blafasl","fasdsda","erdprokker","bauer","himmelskropper",
							"signanz","bauer","5. text","faslbla","blablalaasds","tristan","someshit"};
		vector<string> texts;
		tree1->getTexts(texts);
		unit.addAnnotation("getTexts(texts) - compare node count - ");
		unit.assertEquals((unsigned int)texts.size(), (unsigned int)12);
		if(texts.size() == 12) {
			for(int i=0 ; i<12 ; i++) {
				unit.addAnnotation("getTexts - compare texts - ");
				unit.assertEquals(texts[i],srcTexts[i]);
			}
		}

		try {
			string text = tree1->getTexts();
			unit.addAnnotation("getTexts() - compare text");
			unit.assertEquals("blafaslfasdsdaerdprokkerbauerhimmelskroppersignanzbauer5. textfaslblablablalaasdstristansomeshit",text);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}

		delete tree1;
	}

	void testFlattenTag(XUnit &unit) {
		unit.addMarkup("test cases for XTag::flattenTag");
		XTag *tree1 = getTree1();

		XTag tree2;
		map<string,vector<double>> ambiguousDoubles;
		map<string,vector<string>> ambiguousStrings;
		tree1->flattenTag(tree2,ambiguousDoubles,ambiguousStrings);
		unit.addAnnotation("Flatten tree1 - test name - ");
		unit.assertEquals(tree2.name,"bla");
		unit.addAnnotation("Flatten tree1 - test number of ambiguous real attributes - ");
		unit.assertEquals((unsigned int)ambiguousDoubles.size(),(unsigned int)3);
		unit.addAnnotation("Flatten tree1 - test number of ambiguous string attributes - ");
		unit.assertEquals((unsigned int)ambiguousStrings.size(),(unsigned int)4);
		unit.addAnnotation("Flatten tree1 - test number of real attributes - ");
		unit.assertEquals((unsigned int)tree2.realAttribs.size(),(unsigned int)5);
		unit.addAnnotation("Flatten tree1 - test number of string attributes - ");
		unit.assertEquals((unsigned int)tree2.stringAttribs.size(),(unsigned int)8);
		unit.addAnnotation("Flatten tree1 - test number of nodes - ");
		unit.assertEquals((unsigned int)tree2.nodes.size(),(unsigned int)23);

		try {
			map<string,double>::iterator iter = tree2.realAttribs.find("pi");
			if(iter == tree2.realAttribs.end()) {
				throw 1;
			}
			unit.addAnnotation("Flatten tree1 - test attribute value - ");
			unit.assertEquals((*iter).second,3.14159);
			iter = tree2.realAttribs.find("fuck");
			if(iter == tree2.realAttribs.end()) {
				throw 1;
			}
			unit.addAnnotation("Flatten tree1 - test attribute value - ");
			unit.assertEquals((*iter).second,55.0);
			iter = tree2.realAttribs.find("id");
			if(iter == tree2.realAttribs.end()) {
				throw 1;
			}
			unit.addAnnotation("Flatten tree1 - test attribute value - ");
			unit.assertEquals((*iter).second,3.0);
			iter = tree2.realAttribs.find("bla");
			if(iter == tree2.realAttribs.end()) {
				throw 1;
			}
			unit.addAnnotation("Flatten tree1 - test attribute value - ");
			unit.assertEquals((*iter).second,3.0);
		} catch(...) {
			unit.addAnnotation("Flatten tree1 - searched attribute not found - ");
			unit.assertTrue(false);
		}

		try {
			map<string,string>::iterator iter = tree2.stringAttribs.find("tx");
			if(iter == tree2.stringAttribs.end()) {
				throw 1;
			}
			unit.addAnnotation("Flatten tree1 - test attribute value - ");
			unit.assertEquals((*iter).second,"haha");
			iter = tree2.stringAttribs.find("baby");
			if(iter == tree2.stringAttribs.end()) {
				throw 1;
			}
			unit.addAnnotation("Flatten tree1 - test attribute value - ");
			unit.assertEquals((*iter).second,"spielzeug");
			iter = tree2.stringAttribs.find("sack");
			if(iter == tree2.stringAttribs.end()) {
				throw 1;
			}
			unit.addAnnotation("Flatten tree1 - test attribute value - ");
			unit.assertEquals((*iter).second,"gasse");
			iter = tree2.stringAttribs.find("t1");
			if(iter == tree2.stringAttribs.end()) {
				throw 1;
			}
			unit.addAnnotation("Flatten tree1 - test attribute value - ");
			unit.assertEquals((*iter).second,"yey");
		} catch(...) {
			unit.addAnnotation("Flatten tree1 - searched attribute not found - ");
			unit.assertTrue(false);
		}

		if(tree2.nodes.size() >= 23) {
			try {
				XTag *tag = 0;
				XText *text = 0;
				text = dynamic_cast<XText *>(tree2.nodes[5]);
				if(text == 0) {
					throw 1;
				}
				unit.addAnnotation("Flatten tree1 - test node value - ");
				unit.assertEquals(text->text,"bauer");
				tag = dynamic_cast<XTag *>(tree2.nodes[11]);
				if(tag == 0) {
					throw 1;
				}
				unit.addAnnotation("Flatten tree1 - test node value - ");
				unit.assertEquals(tag->name,"willie");
				tag = dynamic_cast<XTag *>(tree2.nodes[13]);
				if(tag == 0) {
					throw 1;
				}
				unit.addAnnotation("Flatten tree1 - test node value - ");
				unit.assertEquals(tag->name,"ali");
				text = dynamic_cast<XText *>(tree2.nodes[20]);
				if(text == 0) {
					throw 1;
				}
				unit.addAnnotation("Flatten tree1 - test node value - ");
				unit.assertEquals(text->text,"tristan");
			} catch(...) {
				unit.addAnnotation("Flatten tree1 - wrong node - ");
				unit.assertTrue(false);
			}
		}

		map<string,vector<double>>::iterator dAmb = ambiguousDoubles.find("pi");
		if(dAmb == ambiguousDoubles.end()) {
			unit.addAnnotation("Flatten tree1 - pi is supposed to be ambiguous - ");
			unit.assertTrue(false);
		} else {
			vector<double> &vals = (*dAmb).second;
			unit.addAnnotation("Flatten tree1 - pi is defined three times - ");
			unit.assertEquals((unsigned int)vals.size(),(unsigned int)3);
			if(vals.size() >= 3) {
				unit.addAnnotation("Flatten tree1 - test values of pi - ");
				unit.assertEquals(vals[0],6.0);
				unit.addAnnotation("Flatten tree1 - test values of pi - ");
				unit.assertEquals(vals[1],100.0);
				unit.addAnnotation("Flatten tree1 - test values of pi - ");
				unit.assertEquals(vals[2],3.14159);
			}
		}

		map<string,vector<string>>::iterator sAmb = ambiguousStrings.find("sack");
		if(sAmb == ambiguousStrings.end()) {
			unit.addAnnotation("Flatten tree1 - sack is supposed to be ambiguous - ");
			unit.assertTrue(false);
		} else {
			vector<string> &vals = (*sAmb).second;
			unit.addAnnotation("Flatten tree1 - sack is defined two times - ");
			unit.assertEquals((unsigned int)vals.size(),(unsigned int)2);
			if(vals.size() >= 2) {
				unit.addAnnotation("Flatten tree1 - test values of sack - ");
				unit.assertEquals(vals[0],"erl");
				unit.addAnnotation("Flatten tree1 - test values of sack - ");
				unit.assertEquals(vals[1],"gasse");
			}
		}

		delete tree1;
	}

	void testDeconstructor(XUnit &unit) {
		unit.addMarkup("test cases for XTag::~XTag");

		try {
			ListLogger &logger = dynamic_cast<ListLogger &>(Logger::get());
			logger.getMarkups().clear();

			XTag *tree1 = getTree1();
			delete tree1;
			
			map<string,ListMarkup>::iterator dXNode = logger.getMarkups().find("~XTag");
			if(dXNode == logger.getMarkups().end()) {
				unit.addAnnotation("deconstructor - no markup ~XTag - ");
				unit.assertTrue(false);
			} else {
				unit.addAnnotation("deconstructor - test deconstruction count - ");
				unit.assertEquals((unsigned int)(*dXNode).second.getStrings().size(),(unsigned int)19);
			}

			map<string,ListMarkup>::iterator dXText = logger.getMarkups().find("~XText");
			if(dXText == logger.getMarkups().end()) {
				unit.addAnnotation("deconstructor - no markup ~XText - ");
				unit.assertTrue(false);
			} else {
				unit.addAnnotation("deconstructor - test deconstruction count - ");
				unit.assertEquals((unsigned int)(*dXText).second.getStrings().size(),(unsigned int)12);
			}
		} catch(...) {
			unit.addAnnotation("deconstructor - could not optain list logger - ");
			unit.assertTrue(false);
		}
	}

	/**
	 * invocing all node test cases
	 */
	void testNodes(const string &logname) {
		XUnit unit(new FileLogger(logname));
		try {
			Logger::addLogger("testNodes",new ListLogger());
			Logger::setDefaultLogger("testNodes");

			testGetTags(unit);
			testGetFirst(unit);
			testGetAttribute(unit);
			testGetDirectTexts(unit);
			testGetTexts(unit);
			testFlattenTag(unit);
			testDeconstructor(unit);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}
	}

}

#endif
