#ifndef _TEST_SX_TESTPARSESTRINGS_CPP_
#define _TEST_SX_TESTPARSESTRINGS_CPP_

/**
 * string parser test cases
 * (c) 2013 by Tristan Bauer
 */
#include <test/SXParser.h>
#include <sx/XUnit.h>
#include <sx/Log4SX.h>
#include <sx/SXParser.h>
#include <vector>
using namespace sx;
using namespace std;

namespace sxparser {

	void testParseStrings(const string &logname) {
		XUnit unit(new FileLogger(logname));
		unit.addMarkup("Parsing lists of strings");

		unit.addAnnotation("parse an empty stringlist - size is zero - ");
		try {
			vector<string> strings = parseStrings("");
			unit.assertEquals((unsigned int)strings.size(),(unsigned int)0);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}

		unit.addAnnotation("parse a stringlist with one string - ");
		try {
			vector<string> strings = parseStrings("	 \n	\r\n3.14159asd.:sdFSA		\r\n ");
			unit.assertEquals((unsigned int)strings.size(),(unsigned int)1);

			unit.addAnnotation("stringlist with one string - check 1. string - ");
			unit.assertEquals(strings[0],"3.14159asd.:sdFSA");
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}

		unit.addAnnotation("parse a stringlist with multiple strings - ");
		try {
			vector<string> strings = parseStrings("	 \n	\r\n3.14159\r\nHey1_334PO#:.,L	\n D a		  FFF	ggg	dd");
			unit.assertEquals((unsigned int)strings.size(),(unsigned int)7);

			unit.addAnnotation("stringlist with one number - check 1. string - ");
			unit.assertEquals(strings[0],"3.14159");

			unit.addAnnotation("stringlist with one string - check 2. string - ");
			unit.assertEquals(strings[1],"Hey1_334PO#:.,L");

			unit.addAnnotation("stringlist with one string - check 3. string - ");
			unit.assertEquals(strings[2],"D");

			unit.addAnnotation("stringlist with one string - check 4. string - ");
			unit.assertEquals(strings[3],"a");

			unit.addAnnotation("stringlist with one string - check 5. string - ");
			unit.assertEquals(strings[4],"FFF");

			unit.addAnnotation("stringlist with one string - check 6. string - ");
			unit.assertEquals(strings[5],"ggg");

			unit.addAnnotation("stringlist with one string - check 7. string - ");
			unit.assertEquals(strings[6],"dd");
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}
	}
	
}

#endif
