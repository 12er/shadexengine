#ifndef _TEST_SX_SXPARSER_TESTPARSECOLLADASKELETON_CPP_
#define _TEST_SX_SXPARSER_TESTPARSECOLLADASKELETON_CPP_

/**
 * Collada skeleton parser test cases
 * (c) 2013 by Tristan Bauer
 */
#include <test/SXParser.h>
#include <sx/XUnit.h>
#include <sx/Log4SX.h>
#include <sx/SXParser.h>
#include <vector>
#include <sstream>
using namespace sx;
using namespace std;

namespace sxparser {

	void testParseColladaSkeleton(const string &logname) {
		XUnit unit(new FileLogger(logname));

		unit.addMarkup("Parsing nonexistent file - ");

		unit.addAnnotation("read a file, which does not exist - ");

		try {
			parseColladaSkeletonFile("i don't exist");
			unit.assertTrue(false);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(true);
		}

		unit.addMarkup("Parsing a file, which is not in Collada format - ");

		unit.addAnnotation("open an existing file, which is not a Collada file - ");

		try {
			parseColladaSkeletonFile("data/test/sxparser/files/xml4.xml");
			unit.assertTrue(false);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(true);
		}

		unit.addMarkup("Parsing a file, which is in Collada format, but does not contain any bones - ");

		unit.addAnnotation("open an existing file in Collada format without bones - ");

		try {
			parseColladaSkeletonFile("data/test/sxparser/files/bones0.dae");
			unit.assertTrue(false);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(true);
		}

		unit.addMarkup("Parsing collada skeleton bones1.dae - ");

		unit.addAnnotation("read and parse file - ");
		try {
			pair<vector<Bone>,Matrix> bones = parseColladaSkeletonFile("data/test/sxparser/files/bones1.dae");
			unit.assertTrue(true);

			Matrix m(
				2.03685343f, 0.05915352f, 3.5603741f, 4.5f,
				1.96696649f, 0.10540376f, -2.9962929f, 2,
				-2.0572483f, 0.15934524f, 0.6602763f, -0.9606076f,
				0, 0, 0, 1
				);
			for(unsigned int i=0 ; i<16 ; i++) {
				stringstream str;
				str << "compare matrix element " << i << " - ";
				unit.addAnnotation(str.str());
				unit.assertEquals(m[i],bones.second[i],0.0001f);
			}

			unit.addAnnotation("one root bone - ");
			unit.assertEquals((unsigned int)bones.first.size(), (unsigned int)1);

			unit.addAnnotation("bone called bottom - ");
			unit.assertEquals(bones.first[0].ID, "bottom");

			m = Matrix(1, 0, 0, 0, 0, 0, -1, 0, 0, 1, 0, 0, 0, 0, 0, 1);
			for(unsigned int i=0 ; i<16 ; i++) {
				stringstream str;
				str << "compare bottom element " << i << " - ";
				unit.addAnnotation(str.str());
				unit.assertEquals(m[i],bones.first[0].parentTransform[i],0.0001f);
			}

			unit.addAnnotation("one child bone - ");
			unit.assertEquals((unsigned int)bones.first[0].bones.size(), (unsigned int)1);

			Bone &top = bones.first[0].bones[0];
			unit.addAnnotation("bone called top - ");
			unit.assertEquals(top.ID,"top");

			m = Matrix(-1, 0, 0, 0, 0, 0.999999f, 0, 1, 0, 0, -1, 0, 0, 0, 0, 1);
			for(unsigned int i=0 ; i<16 ; i++) {
				stringstream str;
				str << "compare top element " << i << " - ";
				unit.addAnnotation(str.str());
				unit.assertEquals(m[i],top.parentTransform[i],0.0001f);
			}
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}

		unit.addMarkup("Parsing collada skeleton bones2.dae - ");

		unit.addAnnotation("read and parse file - ");
		try {
			pair<vector<Bone>,Matrix> bones = parseColladaSkeletonFile("data/test/sxparser/files/bones2.dae");
			unit.assertTrue(true);

			Matrix m(
				1, 0, 0, -0.3246693f,
				0, 1, 0, 0.01032972f,
				0, 0, 1, -0.8873341f,
				0, 0, 0, 1
				);
			for(unsigned int i=0 ; i<16 ; i++) {
				stringstream str;
				str << "compare matrix element " << i << " - ";
				unit.addAnnotation(str.str());
				unit.assertEquals(m[i],bones.second[i],0.0001f);
			}

			unit.addAnnotation("3 root bones - ");
			unit.assertEquals((unsigned int)bones.first.size(), (unsigned int)3);

			Bone &body = bones.first[0];
			Bone &pelvis_left = bones.first[1];
			Bone &pelvis_right = bones.first[2];

			unit.addAnnotation("name of bone body - ");
			unit.assertEquals(body.ID,"body");

			m = Matrix(1, 0, 0, 0.3205911f, 0, 0, -1, 0, 0, 1, 0, 0, 0, 0, 0, 1);
			for(unsigned int i=0 ; i<16 ; i++) {
				stringstream str;
				str << "compare body element " << i << " - ";
				unit.addAnnotation(str.str());
				unit.assertEquals(m[i],body.parentTransform[i],0.0001f);
			}

			unit.addAnnotation("childcount of body - ");
			unit.assertEquals((unsigned int)body.bones.size(), (unsigned int)3);

			Bone &head = body.bones[0];
			Bone &shoulder_left = body.bones[1];

			unit.addAnnotation("name of bone head - ");
			unit.assertEquals(head.ID,"head");

			unit.addAnnotation("childcount of head - ");
			unit.assertEquals((unsigned int)head.bones.size(), (unsigned int)0);

			unit.addAnnotation("name of bone shoulder_left - ");
			unit.assertEquals(shoulder_left.ID,"shoulder_left");

			unit.addAnnotation("childcount of shoulder_left - ");
			unit.assertEquals((unsigned int)shoulder_left.bones.size(), (unsigned int)1);

			Bone &upperarm_left = shoulder_left.bones[0];

			unit.addAnnotation("name of bone upperarm_left - ");
			unit.assertEquals(upperarm_left.ID,"upperarm_left");

			m = Matrix(1, 0, 0, 0, 0, 0.8738195f, 0.4862503f, 1.174714f, 0, -0.4862504f, 0.8738195f, 0, 0, 0, 0, 1);
			for(unsigned int i=0 ; i<16 ; i++) {
				stringstream str;
				str << "compare upperarm_left element " << i << " - ";
				unit.addAnnotation(str.str());
				unit.assertEquals(m[i],upperarm_left.parentTransform[i],0.0001f);
			}

			unit.addAnnotation("childcount of upperarm_left - ");
			unit.assertEquals((unsigned int)upperarm_left.bones.size(), (unsigned int)1);

			Bone &underarm_left = upperarm_left.bones[0];

			unit.addAnnotation("name of bone underarm_left - ");
			unit.assertEquals(underarm_left.ID,"underarm_left");

			unit.addAnnotation("childcount of underarm_left - ");
			unit.assertEquals((unsigned int)underarm_left.bones.size(), (unsigned int)0);

			m = Matrix(1, 0, 0, 0, 0, 0.9999492f, -0.01007736f, 1.010654f, 0, 0.01007732f, 0.9999492f, 0, 0, 0, 0, 1);
			for(unsigned int i=0 ; i<16 ; i++) {
				stringstream str;
				str << "compare underarm_left element " << i << " - ";
				unit.addAnnotation(str.str());
				unit.assertEquals(m[i],underarm_left.parentTransform[i],0.0001f);
			}

			unit.addAnnotation("name of bone pelvis_left - ");
			unit.assertEquals(pelvis_left.ID,"pelvis_left");

			m = Matrix(-1, 0, 0, 0.3205911f, 0, 0.8858316f, -0.464007f, 0, 0, -0.464007f, -0.8858316f, 0, 0, 0, 0, 1);
			for(unsigned int i=0 ; i<16 ; i++) {
				stringstream str;
				str << "compare pelvis_left element " << i << " - ";
				unit.addAnnotation(str.str());
				unit.assertEquals(m[i],pelvis_left.parentTransform[i],0.0001f);
			}

			unit.addAnnotation("name of bone pelvis_right - ");
			unit.assertEquals(pelvis_right.ID,"pelvis_right");

			m = Matrix(-1, 0, 0, 0.3205911f, 0, -0.8762159f, -0.481919f, 0, 0, -0.4819191f, 0.8762158f, 0, 0, 0, 0, 1);
			for(unsigned int i=0 ; i<16 ; i++) {
				stringstream str;
				str << "compare pelvis_right element " << i << " - ";
				unit.addAnnotation(str.str());
				unit.assertEquals(m[i],pelvis_right.parentTransform[i],0.0001f);
			}
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}
	}

}

#endif