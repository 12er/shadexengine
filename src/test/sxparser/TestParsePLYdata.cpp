#ifndef _TEST_SXPARSER_TESTPARSEPLYDATA_CPP_
#define _TEST_SXPARSER_TESTPARSEPLYDATA_CPP_

/**
 * ply parser test cases
 * (c) 2012 by Tristan Bauer
 */

#include <test/SXParser.h>
#include <sx/Log4SX.h>
#include <sx/XUnit.h>
#include <sx/SXParser.h>
#include <sstream>
using namespace sx;
using namespace std;

namespace sxparser {

	void testParsePLYdata(XUnit &unit) {
		//test, if tabs, spaces and empty rows are ignored
		unit.addMarkup("testcases for parsePLYdata - test accepting of spaces");
		try {
			XMesh *mesh = parsePLYFile("data/test/sxparser/files/ply1.ply");
			unit.addAnnotation("space - facetype - ");
			unit.assertEquals((unsigned int)mesh->faceSize,(unsigned int)1);
			unit.addAnnotation("space - attributecount - ");
			unit.assertEquals((unsigned int)mesh->buffers.size(),(unsigned int)5);

			map<string,XBuffer *>::iterator iter = mesh->buffers.find("vertices");
			if(iter == mesh->buffers.end()) {
				delete mesh;
				throw Exception("space - missing vertices - ");
			}
			unit.addAnnotation("space - vertex - ");
			unit.assertEquals((double)(*iter).second->vertexAttributes[8],3.0);
			unit.addAnnotation("space - vertex - ");
			unit.assertEquals((double)(*iter).second->vertexAttributes[9],3.1);
			unit.addAnnotation("space - vertex - ");
			unit.assertEquals((double)(*iter).second->vertexAttributes[10],3.2);
			unit.addAnnotation("space - vertex - ");
			unit.assertEquals((double)(*iter).second->vertexAttributes[11],3.3);
			iter = mesh->buffers.find("texcoords");

			if(iter == mesh->buffers.end()) {
				delete mesh;
				throw Exception("space - missing texcoords - ");
			}
			iter = mesh->buffers.find("colors");
			if(iter == mesh->buffers.end()) {
				delete mesh;
				throw Exception("space - missing colors - ");
			}

			iter = mesh->buffers.find("yey");
			if(iter == mesh->buffers.end()) {
				delete mesh;
				throw Exception("space - missing attribute yey - ");
			}
			unit.addAnnotation("space - yey - ");
			unit.assertEquals((double)(*iter).second->vertexAttributes[0],0.16);
			unit.addAnnotation("space - yey - ");
			unit.assertEquals((double)(*iter).second->vertexAttributes[2],3.16);
			unit.addAnnotation("space - yey - ");
			unit.assertEquals((double)(*iter).second->vertexAttributes[3],1.16);

			//test if constructors of XBuffer objects are called
			ListLogger &logger = dynamic_cast<ListLogger &>(Logger::get());
			logger.getMarkups().clear();
			delete mesh;
			map<string,ListMarkup>::iterator dXMesh = logger.getMarkups().find("~XMesh");
			map<string,ListMarkup>::iterator dXBuffer = logger.getMarkups().find("~XBuffer");
			if(dXMesh == logger.getMarkups().end() || dXBuffer == logger.getMarkups().end()) {
				throw Exception("space - test deconstructor - could not find log - ");
			}
			unit.addAnnotation("space - test ~XMesh - ");
			unit.assertEquals((unsigned int)(*dXMesh).second.getStrings().size(), (unsigned int)1);
			unit.addAnnotation("space - test ~XBuffer - ");
			unit.assertEquals((unsigned int)(*dXBuffer).second.getStrings().size(), (unsigned int)5);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}

		//tests if face indices are processed correctly
		unit.addMarkup("testcases for parsePLYdata - test faces");
		try {
			try {
				XMesh *mesh = parsePLYFile("data/test/sxparser/files/ply2.ply");
				delete mesh;
				throw Exception("faces - accepted inconsistent file - ");
			} catch(Exception &f) {
				if(f.getType() == EX_COMPILE) {
					stringstream str;
					str << "faces - throw exception as expected - " << f.getMessage() << " - ";
					unit.addAnnotation(str.str());
					unit.assertTrue(true);
				} else {
					throw Exception("faces - not a compilation exception - ");
				}
			}
			try {
				XMesh *mesh = parsePLYFile("data/test/sxparser/files/ply3.ply");
				delete mesh;
				throw Exception("faces - accepted inconsistent file - ");
			} catch(Exception &f) {
				if(f.getType() == EX_COMPILE) {
					stringstream str;
					str << "faces - throw exception as expected - " << f.getMessage() << " - ";
					unit.addAnnotation(str.str());
					unit.assertTrue(true);
				} else {
					throw Exception("faces - not a compilation exception - ");
				}
			}

			XMesh *mesh = parsePLYFile("data/test/sxparser/files/ply4.ply");
			unit.addAnnotation("faces - face size - ");
			unit.assertEquals((unsigned int)mesh->faceSize,(unsigned int)2);
			map<string,XBuffer *>::iterator iter = mesh->buffers.find("normals");
			if(iter == mesh->buffers.end()) {
				delete mesh;
				throw Exception("faces - no normals - ");
			}
			unit.addAnnotation("faces - normals - ");
			unit.assertEquals((*iter).second->vertexAttributes[4],2.2);
			unit.addAnnotation("faces - normals - ");
			unit.assertEquals((*iter).second->vertexAttributes[7],3.3);
			delete mesh;

			mesh = parsePLYFile("data/test/sxparser/files/ply5.ply");
			unit.addAnnotation("faces - face size - ");
			unit.assertEquals((unsigned int)mesh->faceSize,(unsigned int)3);
			iter = mesh->buffers.find("colors");
			if(iter == mesh->buffers.end()) {
				delete mesh;
				throw Exception("faces - no colors - ");
			}
			unit.addAnnotation("faces - colors - ");
			unit.assertEquals((*iter).second->vertexAttributes[72],4.2);
			unit.addAnnotation("faces - colors - ");
			unit.assertEquals((*iter).second->vertexAttributes[81],0.3);
			unit.addAnnotation("faces - colors - ");
			unit.assertEquals((*iter).second->vertexAttributes[66],2.4);
			unit.addAnnotation("faces - colors - ");
			unit.assertEquals((*iter).second->vertexAttributes[76],1.2);
			unit.addAnnotation("faces - colors - ");
			unit.assertEquals((*iter).second->vertexAttributes[54],3.4);
			unit.addAnnotation("faces - colors - ");
			unit.assertEquals((*iter).second->vertexAttributes[40],2.2);
			unit.addAnnotation("faces - colors - ");
			unit.assertEquals((*iter).second->vertexAttributes[44],3.2);
			unit.addAnnotation("faces - colors - ");
			unit.assertEquals((*iter).second->vertexAttributes[28],1.2);
			unit.addAnnotation("faces - colors - ");
			unit.assertEquals((*iter).second->vertexAttributes[16],3.2);
			delete mesh;
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}

		//tests if uchar properties are divided by 255
		unit.addMarkup("testcases for parsePLYdata - uchar property");
		try {
			XMesh *mesh = parsePLYFile("data/test/sxparser/files/ply6.ply");
			vector<double> &vertices = (*mesh->buffers.find("vertices")).second->vertexAttributes;
			vector<double> &normals = (*mesh->buffers.find("normals")).second->vertexAttributes;
			vector<double> &texcoords = (*mesh->buffers.find("texcoords")).second->vertexAttributes;
			vector<double> &colors = (*mesh->buffers.find("colors")).second->vertexAttributes;
			vector<double> &yey = (*mesh->buffers.find("yey")).second->vertexAttributes;
			for(unsigned int i=0 ; i<2 ; i++) {
				unit.addAnnotation("uchar - even - ");
				unit.assertEquals(vertices[i*2],1.0);
				unit.addAnnotation("uchar - uneven - ");
				unit.assertEquals(vertices[i*2+1],255.0);
				unit.addAnnotation("uchar - even - ");
				unit.assertEquals(normals[i*2],1.0);
				unit.addAnnotation("uchar - uneven - ");
				unit.assertEquals(normals[i*2+1],255.0);
				unit.addAnnotation("uchar - even - ");
				unit.assertEquals(texcoords[i*2],1.0);
				unit.addAnnotation("uchar - uneven - ");
				unit.assertEquals(texcoords[i*2+1],255.0);
				unit.addAnnotation("uchar - even - ");
				unit.assertEquals(colors[i*2],1.0);
				unit.addAnnotation("uchar - uneven - ");
				unit.assertEquals(colors[i*2+1],255.0);
			}
			unit.addAnnotation("uchar - yey - ");
			unit.assertEquals(yey[0],1.0);
			delete mesh;

			mesh = parsePLYFile("data/test/sxparser/files/ply7.ply");
			vector<double> &vertices2 = (*mesh->buffers.find("vertices")).second->vertexAttributes;
			vector<double> &normals2 = (*mesh->buffers.find("normals")).second->vertexAttributes;
			vector<double> &texcoords2 = (*mesh->buffers.find("texcoords")).second->vertexAttributes;
			vector<double> &colors2 = (*mesh->buffers.find("colors")).second->vertexAttributes;
			vector<double> &yey2 = (*mesh->buffers.find("yey")).second->vertexAttributes;
			for(unsigned int i=0 ; i<2 ; i++) {
				unit.addAnnotation("uchar - even - ");
				unit.assertEquals(vertices2[i*2],255.0);
				unit.addAnnotation("uchar - uneven - ");
				unit.assertEquals(vertices2[i*2+1],1.0);
				unit.addAnnotation("uchar - even - ");
				unit.assertEquals(normals2[i*2],255.0);
				unit.addAnnotation("uchar - uneven - ");
				unit.assertEquals(normals2[i*2+1],1.0);
				unit.addAnnotation("uchar - even - ");
				unit.assertEquals(texcoords2[i*2],255.0);
				unit.addAnnotation("uchar - uneven - ");
				unit.assertEquals(texcoords2[i*2+1],1.0);
				unit.addAnnotation("uchar - even - ");
				unit.assertEquals(colors2[i*2],255.0);
				unit.addAnnotation("uchar - uneven - ");
				unit.assertEquals(colors2[i*2+1],1.0);
			}
			unit.addAnnotation("uchar - yey - ");
			unit.assertEquals(yey2[0],255.0);
			delete mesh;
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}

		//tests if a large real world example is working
		unit.addMarkup("testcases for parsePLYdata - reading large file");
		try {
			XMesh *mesh = parsePLYFile("data/test/sxparser/files/ply8.ply");
			unit.addAnnotation("space - facetype - ");
			unit.assertEquals((unsigned int)mesh->faceSize,(unsigned int)3);
			unit.addAnnotation("space - attributecount - ");
			unit.assertEquals((unsigned int)mesh->buffers.size(),(unsigned int)3);

			vector<double> &vertices = (*mesh->buffers.find("vertices")).second->vertexAttributes;
			unit.addAnnotation("large file - vertex - ");
			unit.assertEquals(vertices[72639],4.168625);
			unit.addAnnotation("large file - vertex count - ");
			unit.assertEquals((unsigned int)vertices.size(), (unsigned int)74196);
			vector<double> &normals = (*mesh->buffers.find("normals")).second->vertexAttributes;
			unit.addAnnotation("large file - normal - ");
			unit.assertEquals(normals[19822],-0.982709);
			unit.addAnnotation("large file - normal count - ");
			unit.assertEquals((unsigned int)normals.size(), (unsigned int)74196);
			vector<double> &texcoords = (*mesh->buffers.find("texcoords")).second->vertexAttributes;
			unit.addAnnotation("large file - texcoord count - ");
			unit.assertEquals((unsigned int)texcoords.size(), (unsigned int)49464);

			delete mesh;
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}
	}

	void testParsePLYdata(const string &logname) {
		XUnit unit(new FileLogger(logname));
		try {
			Logger::addLogger("testParsePLY",new ListLogger());
			Logger::setDefaultLogger("testParsePLY");

			testParsePLYdata(unit);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}
	}

}

#endif