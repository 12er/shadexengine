#ifndef _TEST_SXPARSER_TESTBITMAP_CPP_
#define _TEST_SXPARSER_TESTBITMAP_CPP_

/**
 * bitmap test cases
 * (c) 2012 by Tristan Bauer
 */
#include <sx/SXParser.h>
#include <test/SXParser.h>
#include <sx/Log4SX.h>
#include <sx/XUnit.h>
#include <sx/Exception.h>
#include <sstream>
#include <stdio.h>
using namespace std;
using namespace sx;

namespace sxparser {

	/**
	 * executes unit test for a bitmap by comparing its content with
	 * a given dataset
	 *
	 * @param unit to execute the unit tests
	 * @param filename path to the bitmap, which is compared with the dataset
	 * @param width width of the dataset
	 * @param height height of the dataset
	 * @param dataset representation of an image to be compared with the image at path filename
	 * @param delta tolerable maximum error, if a component of a pixel differs more than delta from the dataset, an error is reported to unit
	 */
	void noisyCompareBitmaps(XUnit &unit, const string filename,unsigned int width, unsigned int height, const unsigned char *dataset, unsigned int delta = 0) {
		try {
			stringstream s1;
			stringstream s2;
			s1 << "test width of " << filename << " - ";
			s2 << "test height of " << filename << " - ";
			Bitmap image(filename);
			unit.addAnnotation(s1.str());
			unit.assertEquals(image.getWidth(),width);
			unit.addAnnotation(s2.str());
			unit.assertEquals(image.getHeight(),height);
			if(image.getWidth() != width || image.getHeight() != height) {
				throw Exception("wrong dimensions - ",EX_IO);
			}
			const unsigned char *data = image.getData();
			for(unsigned int i=0 ; i<image.getWidth()*image.getHeight()*image.getPixelSize() ; i++) {
				stringstream ss;
				ss << "test dataset of " << filename << " - " << i << " - ";
				unit.addAnnotation(ss.str());
				unit.assertEquals((float)data[i],(float)dataset[i],(float)delta);
			}
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}
	}

	void testBitmap(XUnit &unit) {
		unit.addMarkup("test trying to load a non-existent image");

		try {
			unit.addAnnotation("test loading nonexistent file - ");
			Bitmap image("doesnotexist :(");
			unit.addAnnotation("no exception thrown, although expected - ");
			unit.assertTrue(false);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(true);
		}

		unit.addMarkup("test loading images");
		unsigned char dataset1[] = {
			0xff,0x00,0xff,0xff,
			0xff,0xff,0xff,0xff,
			0x00,0xff,0xff,0xff,
			0xff,0xff,0x00,0xff,
			0x00,0x00,0x00,0x00,
			0xff,0x00,0x00,0xff,
			0x00,0x00,0x00,0x00,
			0x00,0xff,0x00,0xff,
			0x00,0xff,0x00,0xff,
			0x00,0xff,0x00,0xff,
			0xff,0x00,0x00,0xff,
			0x00,0x00,0xff,0xff,
			0x00,0x00,0xff,0xff,
			0x00,0x00,0x00,0xff,
			0xff,0xff,0xff,0xff,
			0x00,0xff,0x00,0xff
		};
		noisyCompareBitmaps(unit,"data/test/sxparser/files/img1.png",4,4,dataset1);

		unsigned char dataset2[] = {
			10,20,30,0xff,
			40,50,60,0xff
		};
		noisyCompareBitmaps(unit,"data/test/sxparser/files/img1.bmp",1,2,dataset2);

		unsigned char dataset3[] = {
			10,20,30,0xff,
			40,50,60,0xff,
			70,80,90,0xff,
			100,110,120,0xff
		};
		noisyCompareBitmaps(unit,"data/test/sxparser/files/img2.bmp",2,2,dataset3);

		unsigned char dataset4[] = {
			10,20,30,0xff,
			40,50,60,0xff,
			70,80,90,0xff,
			100,110,120,0xff,
			130,140,150,0xff,
			160,170,180,0xff
		};
		noisyCompareBitmaps(unit,"data/test/sxparser/files/img3.bmp",3,2,dataset4);

		unsigned char dataset5[] = {
			0x00,0x00,0xff,0xff,
			0xff,0xff,0x00,0xff,
			0xff,0x00,0x00,0xff,
			0xff,0x00,0xff,0xff,
			0xff,0xff,0x00,0xff,
			0x00,0xff,0xff,0xff,
			0xff,0x00,0x00,0xff,
			0x00,0xff,0x00,0xff,
			0x00,0x00,0xff,0xff
		};
		noisyCompareBitmaps(unit,"data/test/sxparser/files/img1.jpg",3,3,dataset5,20);

		unit.addMarkup("test loading a large image");
		try {
			Bitmap image("data/test/sxparser/files/img2.jpg");
			if(image.getWidth() != 4000 || image.getHeight() != 3000) {
				throw Exception("large image - image has wrong dimensions - ");
			}
			const unsigned char *data = image.getData();
			unit.addAnnotation("large image - pixel (2668,1601) - ");
			unit.assertEquals((float)data[(image.getWidth() * 1601 + 2668)*4],(float)105,5.0f);
			unit.addAnnotation("large image - pixel (2668,1601) - ");
			unit.assertEquals((float)data[(image.getWidth() * 1601 + 2668)*4 + 1],(float)130,5.0f);
			unit.addAnnotation("large image - pixel (2668,1601) - ");
			unit.assertEquals((float)data[(image.getWidth() * 1601 + 2668)*4 + 2],(float)160,5.0f);

			unit.addAnnotation("large image - pixel (1884,827) - ");
			unit.assertEquals((float)data[(image.getWidth() * 827 + 1884)*4],(float)77,5.0f);
			unit.addAnnotation("large image - pixel (1884,827) - ");
			unit.assertEquals((float)data[(image.getWidth() * 827 + 1884)*4 + 1],(float)86,5.0f);
			unit.addAnnotation("large image - pixel (1884,827) - ");
			unit.assertEquals((float)data[(image.getWidth() * 827 + 1884)*4 + 2],(float)31,5.0f);

			unit.addAnnotation("large image - pixel (675,1922) - ");
			unit.assertEquals((float)data[(image.getWidth() * 1922 + 675)*4],(float)119,5.0f);
			unit.addAnnotation("large image - pixel (675,1922) - ");
			unit.assertEquals((float)data[(image.getWidth() * 1922 + 675)*4 + 1],(float)139,5.0f);
			unit.addAnnotation("large image - pixel (675,1922) - ");
			unit.assertEquals((float)data[(image.getWidth() * 1922 + 675)*4 + 2],(float)140,5.0f);

			unit.addAnnotation("large image - pixel (679,1922) - ");
			unit.assertEquals((float)data[(image.getWidth() * 1922 + 679)*4],(float)8,5.0f);
			unit.addAnnotation("large image - pixel (679,1922) - ");
			unit.assertEquals((float)data[(image.getWidth() * 1922 + 679)*4 + 1],(float)26,5.0f);
			unit.addAnnotation("large image - pixel (679,1922) - ");
			unit.assertEquals((float)data[(image.getWidth() * 1922 + 679)*4 + 2],(float)26,5.0f);

			unit.addAnnotation("large image - pixel (3762,2736) - ");
			unit.assertEquals((float)data[(image.getWidth() * 2736 + 3762)*4],(float)146,5.0f);
			unit.addAnnotation("large image - pixel (3762,2736) - ");
			unit.assertEquals((float)data[(image.getWidth() * 2736 + 3762)*4 + 1],(float)185,5.0f);
			unit.addAnnotation("large image - pixel (3762,2736) - ");
			unit.assertEquals((float)data[(image.getWidth() * 2736 + 3762)*4 + 2],(float)226,5.0f);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}

		unit.addMarkup("test writing an image");
		try {
			srand((unsigned int)time(0));
			unsigned char dataSource[4*3*4];
			for(unsigned int i=0 ; i<4*3*4 ; i++) {
				dataSource[i] = rand() % 256;
			}
			Bitmap sourceImage(4,3);
			unsigned char *d = sourceImage.getData();
			for(unsigned int i=0 ; i<sourceImage.getWidth()*sourceImage.getHeight()*sourceImage.getPixelSize() ; i++) {
				d[i] = dataSource[i];
			}
			sourceImage.save("data/test/sxparser/files/img1_out.png");
			noisyCompareBitmaps(unit,"data/test/sxparser/files/img1_out.png",4,3,dataSource,0);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}
	}

	void testBitmap(const string &logname) {
		XUnit unit(new FileLogger(logname));
		try {
			Logger::addLogger("testBitmap",new ListLogger());
			Logger::setDefaultLogger("testBitmap");

			testBitmap(unit);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}
	}

}

#endif