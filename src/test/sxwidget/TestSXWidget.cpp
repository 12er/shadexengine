#ifndef _TEST_SXWIDGET_SXWIDGET_CPP_
#define _TEST_SXWIDGET_SXWIDGET_CPP_

/**
 * SX widget test cases
 * (c) 2012 by Tristan Bauer
 */

#include <test/SXWidget.h>
#include <sx/Log4SX.h>
#include <sx/XUnit.h>
#include <sx/Exception.h>

#include <QApplication>
#include <QGridLayout>
#include <QPushButton>
#include <sx/SX.h>
#include <sx/SXWidget.h>
#include <sstream>
using namespace std;
using namespace sx;

namespace sxwidget {

	class TestSXWidget1: public SXRenderListener {
	private:
		bool stopFrames;
		int frames;
		double frameTime;
		double lastFrameTime;
		XUnit *unit;
		TestSXWidget1(const TestSXWidget1 &);
		TestSXWidget1 &operator = (const TestSXWidget1 &);
	public:

		TestSXWidget1(XUnit &unit, bool stopFrames) {
			this->unit = &unit;
			this->stopFrames = stopFrames;
			frames = 10;
			frameTime = lastFrameTime = 0.0;
		}

		void create(SXRenderArea &area) {
			unit->addMarkup("creating widget");
			
			unit->addAnnotation("width - ");
			unit->assertEquals(area.getWidth() , 400);
			unit->addAnnotation("height - ");
			unit->assertEquals(area.getHeight() , 300);
			unit->addAnnotation("time - ");
			unit->assertEquals(area.getTime() , 0.0);
			unit->addAnnotation("timedelta - ");
			unit->assertEquals(area.getDeltaTime() , 0.0);
		}

		void reshape(SXRenderArea &area) {
			unit->addMarkup("reshaping widget");

			unit->addAnnotation("width - ");
			unit->assertEquals(area.getWidth() , 400);
			unit->addAnnotation("height - ");
			unit->assertEquals(area.getHeight() , 300);
			unit->addAnnotation("time - ");
			unit->assertEquals(area.getTime() , 0.0);
			unit->addAnnotation("timedelta - ");
			unit->assertEquals(area.getDeltaTime() , 0.0);
		}

		void render(SXRenderArea &area) {
			lastFrameTime = frameTime;
			frameTime = area.getTime();
			if(frames <= 0 && stopFrames) {
				area.stopRendering();
			}
			if(frames <= 0) {
				return;
			}

			stringstream markupText;
			markupText << "rendering widget for " << frames << " times";
			unit->addMarkup(markupText.str());

			unit->addAnnotation("width - ");
			unit->assertEquals(area.getWidth() , 400);
			unit->addAnnotation("height - ");
			unit->assertEquals(area.getHeight() , 300);
			if(frames == 10) {
				unit->addAnnotation("time - ");
				unit->assertEquals(area.getTime() , 0.0);
				unit->addAnnotation("timedelta - ");
				unit->assertEquals(area.getDeltaTime() , 0.0);
			} else if(frames == 9) {
				unit->addAnnotation("time - ");
				unit->assertTrue(area.getTime() >= 0.0);
				unit->addAnnotation("timedelta - ");
				unit->assertEquals(area.getDeltaTime(),frameTime - lastFrameTime);
			} else {
				unit->addAnnotation("time - ");
				unit->assertTrue(area.getTime() > 0.0);
				unit->addAnnotation("timedelta - ");
				unit->assertEquals(area.getDeltaTime(),frameTime - lastFrameTime);
			}

			frames--;
		}

		void stop(SXRenderArea &area) {
			unit->addMarkup("stop rendering widget");

			unit->addAnnotation("width - ");
			unit->assertEquals(area.getWidth() , 400);
			unit->addAnnotation("height - ");
			unit->assertEquals(area.getHeight() , 300);
			unit->addAnnotation("time - ");
			unit->assertTrue(area.getTime() > 0.0);
			unit->addAnnotation("timedelta - ");
			unit->assertEquals(area.getDeltaTime(),frameTime - lastFrameTime);
		}

	};

	pair<QWidget *,SXWidget *> createWidget(XUnit &unit,bool stopFrames) {
		QWidget *window = new QWidget();
		QGridLayout *layout = new QGridLayout();

		QPushButton *b = new QPushButton("(1,1)");
		b->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
		layout->addWidget(b,1,1,1,1);
		b = new QPushButton("(2,1)");
		b->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
		layout->addWidget(b,2,1,1,1);
		b = new QPushButton("(3,1)");
		b->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
		layout->addWidget(b,3,1,1,1);
		b = new QPushButton("(1,2)");
		b->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
		layout->addWidget(b,1,2,1,1);
		b = new QPushButton("(3,2)");
		b->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
		layout->addWidget(b,3,2,1,1);
		b = new QPushButton("(1,3)");
		b->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
		layout->addWidget(b,1,3,1,1);
		b = new QPushButton("(2,3)");
		b->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
		layout->addWidget(b,2,3,1,1);
		b = new QPushButton("(3,3)");
		b->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);
		layout->addWidget(b,3,3,1,1);

		SXWidget *my = new SXWidget();
		my->addRenderListener(*new TestSXWidget1(unit,stopFrames));
		my->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
		my->setMinimumSize(400,300);
		my->renderPeriodically(0.01f);
		layout->addWidget(my,2,2,1,1);

		window->setLayout(layout);

		return pair<QWidget *,SXWidget *>(window,my);
	}

	void testSXWidget(const string &logname, QApplication &app) {
		XUnit unit(new FileLogger(logname));
		try {
			Logger::addLogger("testsxwidget",new ListLogger());
			Logger::setDefaultLogger("testsxwidget");
			ListLogger &logger = dynamic_cast<ListLogger &>(Logger::get());

			pair<QWidget *,SXWidget *> window1 = createWidget(unit,true);
			//CloseWidget cw(window1.first,unit);
			//QObject::connect(window1.second,SIGNAL(finishedRendering()),&cw,SLOT(closeWidget()));
			try {
				window1.first->show();
				
				unit.addAnnotation("could create SXWidget - ");
				unit.assertTrue(true);
			} catch(Exception &e) {
				unit.addAnnotation(e.getMessage());
				unit.assertTrue(false);
			}

			pair<QWidget *,SXWidget *> window2 = createWidget(unit,true);
			try {
				window2.first->show();
				
				unit.addAnnotation("could create SXWidget - ");
				unit.assertTrue(true);
			} catch(Exception &e) {
				unit.addAnnotation(e.getMessage());
				unit.assertTrue(false);
			}

			pair<QWidget *,SXWidget *> window3 = createWidget(unit,false);
			try {
				window3.first->show();
				
				unit.addAnnotation("could create SXWidget - ");
				unit.assertTrue(true);
			} catch(Exception &e) {
				unit.addAnnotation(e.getMessage());
				unit.assertTrue(false);
			}

			unit.addMarkup("QApplication executed");
			unit.addAnnotation("QApplication execution success - ");
			unit.assertEquals(app.exec(),0);

			unit.addMarkup("QApplication stopped");

			logger.getMarkups().clear();
			delete window1.first;
			unit.addAnnotation("deleted listeners - ");
			unit.assertEquals(logger.getUintEntry("SXWidget::deleteRenderListeners/listener",0),(unsigned int)1);
			unit.addAnnotation("remaining listeners - ");
			unit.assertEquals(logger.getUintEntry("SXWidget::deleteRenderListeners/listeners",0),(unsigned int)0);
			unit.addAnnotation("remaining uninitialized listeners - ");
			unit.assertEquals(logger.getUintEntry("SXWidget::deleteRenderListeners/initListeners",0),(unsigned int)0);

			logger.getMarkups().clear();
			delete window2.first;
			unit.addAnnotation("deleted listeners - ");
			unit.assertEquals(logger.getUintEntry("SXWidget::deleteRenderListeners/listener",0),(unsigned int)1);
			unit.addAnnotation("remaining listeners - ");
			unit.assertEquals(logger.getUintEntry("SXWidget::deleteRenderListeners/listeners",0),(unsigned int)0);
			unit.addAnnotation("remaining uninitialized listeners - ");
			unit.assertEquals(logger.getUintEntry("SXWidget::deleteRenderListeners/initListeners",0),(unsigned int)0);

			logger.getMarkups().clear();
			delete window3.first;
			unit.addAnnotation("deleted listeners - ");
			unit.assertEquals(logger.getUintEntry("SXWidget::deleteRenderListeners/listener",0),(unsigned int)1);
			unit.addAnnotation("remaining listeners - ");
			unit.assertEquals(logger.getUintEntry("SXWidget::deleteRenderListeners/listeners",0),(unsigned int)0);
			unit.addAnnotation("remaining uninitialized listeners - ");
			unit.assertEquals(logger.getUintEntry("SXWidget::deleteRenderListeners/initListeners",0),(unsigned int)0);

			unit.assertInputYes("pink unicorn with wings?");

		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}
	}

}

#endif