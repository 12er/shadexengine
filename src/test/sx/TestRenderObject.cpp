#ifndef _TEST_SX_TESTRENDEROBJECT_CPP_
#define _TEST_SX_TESTRENDEROBJECT_CPP_

/**
 * RenderObject test cases
 * (c) 2012 by Tristan Bauer
 */
#include <test/SXEngine.h>
#include <sx/XUnit.h>
#include <sx/Log4SX.h>
#include <sx/SXCore.h>
#include <sx/SX.h>
#include <sx/SXWidget.h>
#include <QApplication>
#include <QDialog>
#include <QGridLayout>
#include <vector>
#include <cmath>
using namespace sx;
using namespace std;

namespace sxengine {

	class TestRenderObject1: public SXRenderListener {
	private:
		int width;
		int height;

		BufferedMesh *mesh;
		Shader *shader;
		Shader *foreignShader;
		Texture *tex1;
		Texture *tex2;
		Texture *tex3;
		Texture *tex4;
		UniformVector *tr0;
		UniformVector *tr1;
		UniformVector *tr2;
		RenderObject *object;

		XUnit *unit;
		ListLogger *list;
		TimeMeasurement timeMeasurement;
	public:

		TestRenderObject1(XUnit &unit, ListLogger &list) {
			this->unit = &unit;
			this->list = &list;
		}

		void create(SXRenderArea &area) {
			this->width = area.getWidth();
			this->height = area.getHeight();

			unit->addMarkup("test add/remove uniforms");

			RenderObject *ro1 = new RenderObject("robject1");
			UniformFloat *uf1 = new UniformFloat("ufloat1");
			UniformFloat *uf2 = new UniformFloat("ufloat2");
			UniformMatrix *um1 = new UniformMatrix("umatrix1");
			UniformVector *uv1 = new UniformVector("uvec1");
			Texture *t1 = new Texture("texture1");
			uf1->setUniformName("lala","robject1");
			um1->setUniformName("lala","robject1");
			t1->setUniformName("alo","robject1");
			ro1->addUniform(*uf1);
			list->getMarkups().clear();
			ro1->addUniform(*uf2);
			try {
				unit->addAnnotation("add uniforms - ");
				unit->assertEquals((unsigned int)list->getUintEntry("RenderObject::addUniform", 0), (unsigned int)2);
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}
			list->getMarkups().clear();
			ro1->addUniform(*um1);
			try {
				unit->addAnnotation("add uniforms - ");
				unit->assertEquals((unsigned int)list->getUintEntry("RenderObject::addUniform", 0), (unsigned int)2);
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}
			ro1->addUniform(*uv1);
			list->getMarkups().clear();
			ro1->addUniform(*t1);
			try {
				unit->addAnnotation("add uniforms - ");
				unit->assertEquals((unsigned int)list->getUintEntry("RenderObject::addUniform", 0), (unsigned int)4);
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}

			ro1->removeUniform("alo");
			ro1->removeUniform("uvec1");
			list->getMarkups().clear();
			ro1->removeUniform("notthere");
			try {
				unit->addAnnotation("remove uniforms - ");
				unit->assertEquals((unsigned int)list->getUintEntry("RenderObject::removeUniform", 0), (unsigned int)2);
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}
			list->getMarkups().clear();
			ro1->removeUniforms();
			try {
				unit->addAnnotation("remove uniforms - ");
				unit->assertEquals((unsigned int)list->getUintEntry("RenderObject::removeUniforms", 0), (unsigned int)0);
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}
			
			delete t1;
			delete uv1;
			delete um1;
			delete uf2;
			delete uf1;
			delete ro1;


			unit->addMarkup("loading phase");

			//init renderobject data
			float meshVArray[] = {
			-1,-1,0,1,
			-0.875f,-1,0,1,
			-0.875f,-0.875f,0,1,
			-1,-1,0,1,
			-0.875f,-0.875f,0,1,
			-1,-0.875f,0,1
			};
			float meshTArray[] = {
			0,0,
			1,0,
			1,1,
			0,0,
			1,1,
			0,1
			};
			vector<float> meshVList(meshVArray,meshVArray + 24);
			vector<float> meshTList(meshTArray,meshTArray + 12);
			mesh = new BufferedMesh("mesh");
			mesh->addBuffer("vertices",meshVList,4);
			mesh->addBuffer("texcoords",meshTList,2);
			mesh->setFaceSize(3);
			mesh->load();
			unit->addAnnotation("loaded mesh - ");
			unit->assertTrue(mesh->isLoaded());

			shader = new Shader("shader1");
			shader->addShaderFile("data/test/sxengine/files/object.vp",VERTEX);
			shader->addShaderFile("data/test/sxengine/files/object.fp",FRAGMENT);
			list->getMarkups().clear();
			shader->load();
			unit->addAnnotation("loaded shader - ");
			try {
				for(unsigned int i=0 ; true ; i++) {
					unit->addAnnotation(list->getStringEntry("Shader::load",i));
				}
			} catch(Exception &) {
			}
			unit->assertTrue(shader->isLoaded());

			foreignShader = new Shader("shader2");
			foreignShader->addShaderFile("data/test/sxengine/files/objectforeign.vp",VERTEX);
			foreignShader->addShaderFile("data/test/sxengine/files/objectforeign.fp",FRAGMENT);
			list->getMarkups().clear();
			foreignShader->load();
			unit->addAnnotation("loaded foreign shader - ");
			try {
				for(unsigned int i=0 ; true ; i++) {
					unit->addAnnotation(list->getStringEntry("Shader::load",i));
				}
			} catch(Exception &) {
			}
			unit->assertTrue(foreignShader->isLoaded());

			tex1 = new Texture("some.tex1");
			tex1->setPath("data/test/sxengine/files/ShadeXEngine.jpg");
			tex1->load();
			unit->addAnnotation("loaded tex1 - ");
			unit->assertTrue(tex1->isLoaded());

			tex2 = new Texture("some.other");
			tex2->setPath("data/test/sxengine/files/background.jpg");
			tex2->setUniformName("tex2","render.object");
			tex2->load();
			unit->addAnnotation("loaded tex2 - ");
			unit->assertTrue(tex2->isLoaded());

			tex3 = new Texture("some.t3");
			tex3->setPath("data/test/sxengine/files/t3.jpg");
			tex3->setUniformName("tex3","render.object");
			tex3->load();
			unit->addAnnotation("loaded tex3 - ");
			unit->assertTrue(tex3->isLoaded());

			tex4 = new Texture("some.t4");
			tex4->setPath("data/test/sxengine/files/t4.jpg");
			tex4->setUniformName("tex3","render.object");
			tex4->load();
			unit->addAnnotation("loaded tex4 - ");
			unit->assertTrue(tex4->isLoaded());

			tr0 = new UniformVector("t0.translate");
			*tr0 = Vector(0.625f,0.75f,0);
			tr1 = new UniformVector("t1.translate");
			*tr1 = Vector(0.5f,0.5f,0);
			tr2 = new UniformVector("t2.translate");
			*tr2 = Vector(0.75f,0.5f,0);

			unit->addMarkup("test render - discard");

			object = new RenderObject("render.object");
			list->getMarkups().clear();
			object->render(0);
			try {
				unit->addAnnotation("empty object - ");
				unit->assertEquals(list->getStringEntry("RenderObject::render",0),"discard");
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}
			delete object;

			object = new RenderObject("render.object");
			object->setMesh(*mesh);
			object->setShader(shader);
			object->setVisible(false);
			list->getMarkups().clear();
			object->render(0);
			try {
				unit->addAnnotation("rendering invisible object - ");
				unit->assertEquals(list->getStringEntry("RenderObject::render",0),"discard");
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}
			delete object;

			object = new RenderObject("render.object");
			object->setShader(shader);
			list->getMarkups().clear();
			object->render(0);
			try {
				unit->addAnnotation("rendering without mesh - ");
				unit->assertEquals(list->getStringEntry("RenderObject::render",0),"discard");
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}
			delete object;

			object = new RenderObject("render.object");
			object->setMesh(*mesh);
			list->getMarkups().clear();
			object->render(0);
			try {
				unit->addAnnotation("rendering without shaders - ");
				unit->assertEquals(list->getStringEntry("RenderObject::render",0),"discard");
			} catch(Exception &e) {
				unit->addAnnotation(e.getMessage());
				unit->assertTrue(false);
			}
			delete object;

			object = new RenderObject("render.object");
			object->setMesh(*mesh);
			list->getMarkups().clear();
			object->render(foreignShader);
			try {
				unit->addAnnotation("rendering without own shader - ");
				list->getStringEntry("RenderObject::render",0);
				unit->assertTrue(false);
			} catch(Exception &) {
				unit->addAnnotation("rendering without own shader - ");
				unit->assertTrue(true);
			}
			delete object;

			object = new RenderObject("render.object");
			object->setMesh(*mesh);
			object->setShader(shader);
			list->getMarkups().clear();
			object->render(0);
			try {
				unit->addAnnotation("rendering without foreign shader - ");
				list->getStringEntry("RenderObject::render",0);
				unit->assertTrue(false);
			} catch(Exception &) {
				unit->addAnnotation("rendering without foreign shader - ");
				unit->assertTrue(true);
			}


			unit->addMarkup("test render - texture slots");

			object->addUniform(*tex1);
			object->addUniform(*tex2);
			shader->freeTextureSlots();
			shader->createTextureSlot();
			unit->addAnnotation("lock two texture slots - ");
			unit->assertEquals((unsigned int)shader->createTextureSlot(), (unsigned int)1);
			shader->lockTextureSlots();
			list->getMarkups().clear();
			object->render(0);
			try {
				unit->addAnnotation("rendering with locked slots - ");
				list->getStringEntry("RenderObject::render",0);
				unit->assertTrue(false);
			} catch(Exception &) {
				unit->addAnnotation("rendering with locked slots - ");
				unit->assertTrue(true);
			}
			unit->addAnnotation("after render - ");
			unit->assertEquals((unsigned int)shader->createTextureSlot(), (unsigned int)4);
			shader->freeTextureSlots();
			unit->addAnnotation("after render - control locked slots - ");
			unit->assertEquals((unsigned int)shader->createTextureSlot(), (unsigned int)2);
			shader->unlockTextureSlots();

			object->setShader(0);
			foreignShader->freeTextureSlots();
			foreignShader->createTextureSlot();
			foreignShader->createTextureSlot();
			foreignShader->createTextureSlot();
			unit->addAnnotation("lock four texture slots - ");
			unit->assertEquals((unsigned int)foreignShader->createTextureSlot(), (unsigned int)3);
			foreignShader->lockTextureSlots();
			list->getMarkups().clear();
			object->render(foreignShader);
			try {
				unit->addAnnotation("rendering with locked slots - ");
				list->getStringEntry("RenderObject::render",0);
				unit->assertTrue(false);
			} catch(Exception &) {
				unit->addAnnotation("rendering with locked slots - ");
				unit->assertTrue(true);
			}
			unit->addAnnotation("after render - ");
			unit->assertEquals((unsigned int)foreignShader->createTextureSlot(), (unsigned int)6);
			foreignShader->freeTextureSlots();
			unit->addAnnotation("after render - control locked slots - ");
			unit->assertEquals((unsigned int)foreignShader->createTextureSlot(), (unsigned int)4);
			foreignShader->unlockTextureSlots();

			object->setShader(shader);
			unordered_map<string, Uniform *> &uSet1 = object->getUniformSet(1);
			uSet1[tex3->getUniformName("render.object")] = tex3;
			unordered_map<string, Uniform *> &uSet2 = object->getUniformSet(2);
			uSet2[tex4->getUniformName("render.object")] = tex4;
			shader->freeTextureSlots();
			shader->createTextureSlot();
			shader->createTextureSlot();
			unit->addAnnotation("lock three texture slots - ");
			unit->assertEquals((unsigned int)shader->createTextureSlot(), (unsigned int)2);
			shader->lockTextureSlots();
			list->getMarkups().clear();
			object->render(0);
			try {
				unit->addAnnotation("rendering with locked slots - ");
				list->getStringEntry("RenderObject::render",0);
				unit->assertTrue(false);
			} catch(Exception &) {
				unit->addAnnotation("rendering with locked slots - ");
				unit->assertTrue(true);
			}
			unit->addAnnotation("after render - ");
			unit->assertEquals((unsigned int)shader->createTextureSlot(), (unsigned int)6);
			shader->freeTextureSlots();
			unit->addAnnotation("after render - control locked slots - ");
			unit->assertEquals((unsigned int)shader->createTextureSlot(), (unsigned int)3);
			shader->unlockTextureSlots();
			

			//start measuring time
			timeMeasurement.restart();
		}

		void reshape(SXRenderArea &area) {
			this->width = area.getWidth();
			this->height = area.getHeight();
		}
		
		void render(SXRenderArea &area) {
			double millies = timeMeasurement.elapsed();

			RenderTarget::setViewport(0,0,width,height);
			RenderTarget::clearTarget();

			object->removeUniformSets();
			object->setShader(shader);
			object->addUniform(*tr0);
			object->addUniform(*tex1);
			object->render(foreignShader);

			object->removeUniform(tex3->getUniformName("render.object"));
			object->removeUniform(tr0->getUniformName("render.object"));
			unordered_map<string,Uniform *> &set1 = object->getUniformSet(1);
			set1[tr1->getUniformName("render.object")] = tr1;
			set1[tex3->getUniformName("render.object")] = tex3;
			unordered_map<string,Uniform *> &set2 = object->getUniformSet(2);
			set2[tr2->getUniformName("render.object")] = tr2;
			set2[tex4->getUniformName("render.object")] = tex4;
			object->setShader(0);
			object->render(foreignShader);

			object->setShader(shader);
			object->render(foreignShader);

			if(millies > 6) {
				//leave window after five seconds
				area.stopRendering();
			}
		}

		void stop(SXRenderArea &area) {
			unit->addMarkup("destruction phase");
			delete mesh;
			delete shader;
			delete foreignShader;
			delete tex1;
			delete tex2;
			delete tex3;
			delete tex4;
			delete tr0;
			delete tr1;
			delete tr2;
			delete object;
		}

	};

	void testRenderObject(const string &logname) {
		XUnit unit(new FileLogger(logname));
		try {
			Logger::addLogger("testrenderobject",new ListLogger());
			Logger::setDefaultLogger("testrenderobject");
			ListLogger &logger = dynamic_cast<ListLogger &>(Logger::get());

			QDialog dialog;
			QGridLayout *dialog_l = new QGridLayout();
			dialog.setLayout(dialog_l);
			SXWidget *widget = new SXWidget();
			dialog_l->addWidget(widget);
			widget->renderPeriodically(0.01f);
			widget->setMinimumSize(600,400);
			QObject::connect(widget, SIGNAL(finishedRendering()), &dialog, SLOT(close()));
			widget->addRenderListener(*new TestRenderObject1(unit,logger));
			dialog.show();

			unit.addAnnotation("execute dialog - ");
			unit.assertEquals(dialog.exec(),0);

			unit.addMarkup("image validation");
			unit.assertInputYes("pink unicorn with wings?");
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}
	}

}

#endif
