#ifndef _TEST_SX_TESTUNIFORMS_CPP_
#define _TEST_SX_TESTUNIFORMS_CPP_

/**
 * uniform variable test cases
 * (c) 2012 by Tristan Bauer
 */
#include <test/SXEngine.h>
#include <sx/XUnit.h>
#include <sx/Log4SX.h>
#include <sx/SXCore.h>
#include <sx/SX.h>
#include <sx/SXWidget.h>
#include <QApplication>
#include <QDialog>
#include <QGridLayout>
#include <vector>
#include <cmath>
using namespace sx;
using namespace std;

namespace sxengine {

	class TestUniforms1: public SXRenderListener {
	private:
		unsigned int width;
		unsigned int height;

		Texture *texture1;
		Texture *texture2;
		UniformFloat *uFloat;
		UniformVector *uVector;
		UniformMatrix *uMatrix;
		BufferedMesh *mesh;
		Shader *shader;

		XUnit *unit;
		ListLogger *list;
		TimeMeasurement timeMeasurement;
		TestUniforms1(const TestUniforms1 &);
		TestUniforms1 &operator = (const TestUniforms1 &);
	public:
		TestUniforms1(XUnit &unit, ListLogger &list) {
			this->unit = &unit;
			this->list = &list;
		}

		void create(SXRenderArea &area) {
			this->width = (unsigned int)area.getWidth();
			this->height = (unsigned int)area.getHeight();

			unit->addMarkup("loading phase");

			UniformFloat testName("this.is.some.textwithtokens");
			testName.setUniformName("uniform1","resource1");
			testName.setUniformName("uniform2","resource2");
			unit->addAnnotation("getUniformName - resource1 - ");
			unit->assertEquals(testName.getUniformName("resource1"),"uniform1");
			unit->addAnnotation("getUniformName - resource2 - ");
			unit->assertEquals(testName.getUniformName("resource2"),"uniform2");
			unit->addAnnotation("getUniformName - resource3 - ");
			unit->assertEquals(testName.getUniformName("resource3"),"textwithtokens");

			shader = new Shader("shader");
			shader->addShaderFile("data/test/sxengine/files/uniforms1.vp",VERTEX);
			shader->addShaderFile("data/test/sxengine/files/uniforms1.fp",FRAGMENT);
			list->getMarkups().clear();
			shader->load();
			unit->addAnnotation("loaded shader - ");
			try {
				for(unsigned int i=0 ; true ; i++) {
					unit->addAnnotation(list->getStringEntry("Shader::load",i));
				}
			} catch(Exception &) {
			}
			unit->assertTrue(shader->isLoaded());

			float meshVArray[] = {
			-1,-1,0,1,
			1,-1,0,1,
			1,1,0,1,
			-1,-1,0,1,
			1,1,0,1,
			-1,1,0,1
			};
			float meshTArray[] = {
			0,0,
			1,0,
			1,1,
			0,0,
			1,1,
			0,1
			};
			vector<float> meshVBuffer(meshVArray,meshVArray+24);
			vector<float> meshTBuffer(meshTArray,meshTArray+12);
			mesh = new BufferedMesh("somemesh");
			mesh->setFaceSize(3);
			mesh->addBuffer("vertices",meshVBuffer,4);
			mesh->addBuffer("texcoords",meshTBuffer,2);
			mesh->load();
			unit->addAnnotation("load mesh - ");
			unit->assertTrue(mesh->isLoaded());

			texture1 = new Texture("tex");
			texture1->load();
			unit->addAnnotation("load texture with nothing set - ");
			unit->assertTrue(!texture1->isLoaded());
			delete texture1;

			texture1 = new Texture("tex");
			texture1->setPath("invalid path");
			texture1->load();
			unit->addAnnotation("load texture from invalid path");
			unit->assertTrue(!texture1->isLoaded());
			delete texture1;

			texture1 = new Texture("tex");
			texture1->setPath("data/test/sxengine/files/ShadeXEngine.jpg");
			texture1->load();
			unit->addAnnotation("load texture from valid path");
			unit->assertTrue(texture1->isLoaded());
			delete texture1;

			texture1 = new Texture("tex");
			texture1->setWidth(300);
			texture1->setHeight(200);
			texture1->setPixelFormat(FLOAT_RGBA);
			texture1->load();
			unit->addAnnotation("load texture from with valid parameters");
			unit->assertTrue(texture1->isLoaded());
			delete texture1;

			texture1 = new Texture("tex");
			texture1->setWidth(300);
			texture1->setHeight(200);
			texture1->setPixelFormat(BYTE_RGBA);
			texture1->load();
			unit->addAnnotation("load texture from with valid parameters");
			unit->assertTrue(texture1->isLoaded());
			delete texture1;

			texture1 = new Texture("");
			texture1->setPath("data/test/sxengine/files/ShadeXEngine.jpg");
			texture1->load();
			unit->addAnnotation("load1: success - ");
			unit->assertTrue(texture1->isLoaded());
			int w = texture1->getWidth();
			int h = texture1->getHeight();
			texture1->load();
			unit->addAnnotation("load2: nothing changed - ");
			unit->assertTrue(texture1->isLoaded());
			unit->addAnnotation("width - ");
			unit->assertEquals(texture1->getWidth(),w);
			unit->addAnnotation("height - ");
			unit->assertEquals(texture1->getHeight(),h);
			texture1->setPath("not a valid path");
			texture1->load();
			unit->addAnnotation("load3: fail - ");
			unit->assertTrue(!texture1->isLoaded());
			texture1->setWidth(500);
			texture1->setHeight(300);
			texture1->setPixelFormat(BYTE_RGBA);
			texture1->load();
			unit->addAnnotation("load4: success - ");
			unit->assertTrue(texture1->isLoaded());
			delete texture1;

			texture1 = new Texture("special.textures.sx");
			texture1->setPath("data/test/sxengine/files/ShadeXEngine.jpg");
			texture1->load();
			unit->addAnnotation("load texture - ");
			unit->assertTrue(texture1->isLoaded());

			float texture2array[] = {
			1,0,1,1, 0.5f,0,1,1, 0,0,1,1,
			1,0,0.5f,1, 0.5f,0,0.5f,1, 0,0,0.5f,1,
			1,0,0,1, 0.5f,0,0,1, 0,1,0,1
			};
			vector<float> texture2buffer(texture2array,texture2array + 36);
			texture2 = new Texture("special.textures.tex");
			texture2->setFloatBuffer(texture2buffer);
			texture2->setWidth(3);
			texture2->setHeight(3);
			texture2->load();
			texture2->setUniformName("simple","resource");
			unit->addAnnotation("load texture - ");
			unit->assertTrue(texture2->isLoaded());

			uFloat = new UniformFloat("special.floats.var");
			uFloat->setUniformName("interpolate","resource");

			uVector = new UniformVector("special.vectors.translate");

			uMatrix = new UniformMatrix("special.matrices.rotate");

			//start measuring time
			timeMeasurement.restart();
		}

		void reshape(SXRenderArea &area) {
			this->width = (unsigned int)area.getWidth();
			this->height = (unsigned int)area.getHeight();
		}

		void render(SXRenderArea &area) {
			double millies = timeMeasurement.elapsed();

			//prepare surface
			RenderTarget::setViewport(0,0,width,height);
			RenderTarget::clearTarget();

			*uFloat = min((float)millies/2.0f,1.0f);

			if(millies >2 && millies<4) {
				//rotate between seconds 2 and 4
				uMatrix->rotate(Vector(0,0,1),(float)sx::Tau * ((float)millies - 2.0f)/2.0f);
			}
			if(millies > 4) {
				//translate between seconds 4 and 6
				float translation = ((float)millies - 4.0f)/1.0f;
				*uVector = Vector(translation,translation);
			}

			shader->use();
			texture1->use(*shader,"");
			texture2->use(*shader,"resource");
			uFloat->use(*shader,"resource");
			uMatrix->use(*shader,"");
			uVector->use(*shader,"");
			mesh->render(*shader);

			if(millies > 6) {
				//leave window after five seconds
				area.stopRendering();
			}
		}

		void stop(SXRenderArea &area) {
			unit->addMarkup("destruction phase");
			delete texture1;
			delete texture2;
			delete uFloat;
			delete uVector;
			delete uMatrix;
			delete mesh;
			delete shader;
		}

	};

	void testUniforms(const string &log) {
		XUnit unit(new FileLogger(log));
		try {
			Logger::addLogger("testuniforms",new ListLogger());
			Logger::setDefaultLogger("testuniforms");
			ListLogger &logger = dynamic_cast<ListLogger &>(Logger::get());

			QDialog dialog;
			QGridLayout *dialog_l = new QGridLayout();
			dialog.setLayout(dialog_l);
			SXWidget *widget = new SXWidget();
			dialog_l->addWidget(widget);
			widget->renderPeriodically(0.01f);
			widget->setMinimumSize(600,400);
			QObject::connect(widget, SIGNAL(finishedRendering()), &dialog, SLOT(close()));
			widget->addRenderListener(*new TestUniforms1(unit,logger));
			dialog.show();

			unit.addAnnotation("execute dialog - ");
			unit.assertEquals(dialog.exec(),0);

			unit.addMarkup("image validation");
			unit.assertInputYes("Did you see shades of blue with a green area, being replaced by the ShadeXEngine logo, which then rotates and moves away?");
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}
	}

}

#endif
