#ifndef _TEST_SX_TESTINSTANCING_CPP_
#define _TEST_SX_TESTINSTANCING_CPP_

/**
 * instanced rendering test cases
 * (c) 2013 by Tristan Bauer
 */
#include <test/SXEngine.h>
#include <sx/XUnit.h>
#include <sx/Log4SX.h>
#include <sx/SXCore.h>
#include <sx/SX.h>
#include <sx/SXWidget.h>
#include <QApplication>
#include <QDialog>
#include <QGridLayout>
#include <vector>
#include <cmath>
using namespace sx;
using namespace std;

namespace sxengine {

	class TestInstancing1: public SXRenderListener {
	private:
		XUnit *unit;
		ListLogger *list;
		float time;

		BufferedMesh *mesh1;
		BufferedMesh *mesh2;
		BufferedMesh *mesh3;
		BufferedMesh *mesh4;
		BufferedMesh *mesh5;
		Shader *program1;
		Shader *program2;

		TestInstancing1(const TestInstancing1 &);
		TestInstancing1 &operator = (const TestInstancing1 &);
	public:

		TestInstancing1(XUnit &unit, ListLogger &list) {
			this->unit = &unit;
			this->list = &list;
			time = 0;
		}

		~TestInstancing1() {
		}

		void create(SXRenderArea &area) {
			unit->addMarkup("loading phase");

			//test reading/writing buffers <-> uniforms

			UniformFloat uf7("uf7");
			UniformMatrix um7("um7");
			vector<Uniform *> uniforms;
			vector<string> bufferIDs;
			bool worked = true;
			uniforms.push_back(&uf7);
			uniforms.push_back(&um7);
			bufferIDs.push_back("buffer1");
			bufferIDs.push_back("buffer16");

			unsigned int vertexCount = 10;
			unsigned int maxVertexCount = 15;
			float m1b1[10];
			float m1b2[20];
			float m1b3[30];
			float m1b4[40];
			float m1b16[160];
			float mesh1b1Array [] = {
				3.1f,
				4.1f,
				5.9f,
				2.6f,
				5.3f,
				5.8f,
				9.7f,
				9.3f,
				2.3f,
				8.4f
			};
			float mesh1b2Array [] = {
				1.2f,		4.1f,
				3.1f,		5.1f,
				3.1415f,	2.0f,
				11.321f,	0.543f,
				132.0f,		44.0f,
				2.159f,		2.71828f,
				3.2145f,	1.5423f,
				4.4223f,	4.445f,
				2.159f,		4.444f,
				1.014f,		2.532f
			};
			float mesh1b3Array [] = {
				4.23f,		9.23f,		6.1f,
				4.44f,		2.22f,		1.11f,
				5.342f,		4.567f,		9.24f,
				11.111f,	12.12f,		14.14f,
				2.71828f,	3.14159f,	1.011f,
				4.01f,		5.05f,		7.42f,
				4.12f,		2.423f,		0.523f,
				6.544f,		6.53f,		9.98f,
				8.723f,		2.145f,		9.823f,
				5.055f,		7.942f,		9.988f
			};
			float mesh1b4Array [] = {
				4.5f,		87.5f,	9.9f,	4.2f,
				2.2f,		9.8f,	3.1f,	9.3f,
				8.534f,		67.43f,	9.98f,	12.14f,
				2.7f,		3.8f,	8.2f,	5.5f,
				1.1f,		9.9f,	12.2f,	14.4f,
				9.0f,		8.0f,	1.23f,	923.03f, 
				14.2f,		15.2f,	21.1f,	25.01f,
				101.03f,	23.3f,	14.4f,	25.5f,
				19.2f,		1.1f,	3.13f,	2.723f,
				20.5f,		23.42f,	32.1f,	13.31f
			};
			float mesh1b16Array [] = {
				3.1f,	4.1f,	5.9f,	2.6f,		5.3f,	5.8f,	9.7f,	9.3f,		2.3f,	8.4f,	6.2f,	6.4f,		3.3f,	8.3f,	2.7f,	9.5f,
				0.2f,	8.8f,	4.1f,	9.7f,		1.6f,	9.3f,	9.9f,	3.7f,		5.1f,	0.5f,	8.2f,	0.9f,		7.4f,	9.4f,	4.5f,	9.2f,
				3.0f,	7.8f,	1.6f,	4.0f,		6.2f,	8.6f,	2.0f,	8.9f,		9.8f,	6.2f,	8.0f,	3.4f,		8.2f,	5.3f,	4.2f,	1.1f,
				7.0f,	6.7f,	9.8f,	2.1f,		4.8f,	0.8f,	6.5f,	1.3f,		2.8f,	2.3f,	0.6f,	6.4f,		7.0f,	9.3f,	8.4f,	4.6f,
				0.9f,	5.5f,	0.5f,	8.2f,		2.3f,	1.7f,	2.5f,	3.5f,		9.4f,	0.8f,	1.2f,	8.4f,		8.1f,	1.1f,	7.4f,	5.0f,
				2.8f,	4.1f,	0.2f,	7.0f,		1.9f,	3.8f,	5.2f,	1.1f,		0.5f,	5.5f,	9.6f,	4.4f,		6.2f,	2.9f,	4.8f,	9.5f,
				4.9f,	3.0f,	3.8f,	1.9f,		6.4f,	4.2f,	8.8f,	1.0f,		9.7f,	5.6f,	6.5f,	9.3f,		3.4f,	4.6f,	1.2f,	8.4f,
				7.5f,	6.4f,	8.2f,	3.3f,		7.8f,	6.7f,	8.3f,	1.6f,		5.2f,	7.1f,	2.0f,	1.9f,		0.9f,	1.4f,	5.6f,	4.8f,
				5.6f,	6.9f,	2.3f,	4.6f,		0.3f,	4.8f,	6.1f,	0.4f,		5.4f,	3.2f,	6.6f,	4.8f,		2.1f,	3.3f,	9.3f,	6.0f,
				7.2f,	6.0f,	2.4f,	9.1f,		4.1f,	2.7f,	3.7f,	2.4f,		5.8f,	7.0f,	0.6f,	6.0f,		6.3f,	1.5f,	5.8f,	8.1f
			};
			for(unsigned int i=0 ; i<160 ; i++) {
				if(i<10) {
					m1b1[i] = mesh1b1Array[i];
				}
				if(i<20) {
					m1b2[i] = mesh1b2Array[i];
				}
				if(i<30) {
					m1b3[i] = mesh1b3Array[i];
				}
				if(i<40) {
					m1b4[i] = mesh1b4Array[i];
				}
				m1b16[i] = mesh1b16Array[i];
			}
			vector<float> mesh1b1(mesh1b1Array,mesh1b1Array + vertexCount);
			vector<float> mesh1b2(mesh1b2Array,mesh1b2Array + vertexCount*2);
			vector<float> mesh1b3(mesh1b3Array,mesh1b3Array + vertexCount*3);
			vector<float> mesh1b4(mesh1b4Array,mesh1b4Array + vertexCount*4);
			vector<float> mesh1b16(mesh1b16Array,mesh1b16Array + vertexCount*16);
			mesh1 = new BufferedMesh("mesh1");
			mesh1->addBuffer("buffer1",mesh1b1,1);
			mesh1->addBuffer("buffer2",mesh1b2,2);
			mesh1->addBuffer("buffer3",mesh1b3,3);
			mesh1->addBuffer("buffer4",mesh1b4,4);
			mesh1->addBuffer("buffer16",mesh1b16,16);
			mesh1->setMaxVertexCount(maxVertexCount);
			mesh1->setFaceSize(1);
			worked = mesh1->setUniforms(uniforms,bufferIDs,7);
			unit->addAnnotation("mesh1 setUniform before load - ");
			unit->assertTrue(!worked);
			mesh1->load();
			unit->addAnnotation("mesh1 load vertices - ");
			unit->assertTrue(mesh1->isLoaded());
			unit->addAnnotation("mesh1 vertex count - ");
			unit->assertEquals((unsigned int)mesh1->getVertexCount(), (unsigned int)vertexCount);
			unit->addAnnotation("mesh1 maximum vertex count - ");
			unit->assertEquals((unsigned int)mesh1->getMaxVertexCount(), (unsigned int)maxVertexCount);

			Texture ut7("ut7");
			ut7.setPath("data/test/sxengine/files/tex4.jpg");
			ut7.load();
			uniforms.push_back(&ut7);
			bufferIDs.push_back("buffer4");
			worked = mesh1->setUniforms(uniforms,bufferIDs,7);
			unit->addAnnotation("mesh1 setUniform with texture - ");
			unit->assertTrue(!worked);

			Volume uv7("uv7");
			uv7.setWidth(2);
			uv7.setHeight(2);
			uv7.setDepth(2);
			uv7.load();
			uniforms.clear();
			bufferIDs.clear();
			uniforms.push_back(&uv7);
			bufferIDs.push_back("buffer1");
			worked = mesh1->setUniforms(uniforms,bufferIDs,7);
			unit->addAnnotation("mesh1 setUniform with volume - ");
			unit->assertTrue(!worked);

			uniforms.clear();
			bufferIDs.clear();
			uniforms.push_back(&uf7);
			uniforms.push_back(&um7);
			bufferIDs.push_back("buffer1");
			bufferIDs.push_back("1337");
			worked = mesh1->setUniforms(uniforms,bufferIDs,7);
			unit->addAnnotation("mesh1 setUniform with nonexistent buffer - ");
			unit->assertTrue(!worked);

			uniforms.clear();
			bufferIDs.clear();
			uniforms.push_back(&uf7);
			uniforms.push_back(&um7);
			bufferIDs.push_back("buffer1");
			bufferIDs.push_back("buffer4");
			worked = mesh1->setUniforms(uniforms,bufferIDs,7);
			unit->addAnnotation("mesh1 setUniform with wrong dimensions - ");
			unit->assertTrue(!worked);

			uniforms.clear();
			bufferIDs.clear();
			uniforms.push_back(&uf7);
			uniforms.push_back(&um7);
			bufferIDs.push_back("buffer1");
			worked = mesh1->setUniforms(uniforms,bufferIDs,7);
			unit->addAnnotation("mesh1 setUniform with uniforms.size() != bufferIDs.size() - ");
			unit->assertTrue(!worked);

			UniformVector uve7("uve7");
			uniforms.clear();
			bufferIDs.clear();
			uniforms.push_back(&uf7);
			uniforms.push_back(&uve7);
			uniforms.push_back(&um7);
			bufferIDs.push_back("buffer1");
			bufferIDs.push_back("buffer2");
			bufferIDs.push_back("buffer16");
			worked = mesh1->setUniforms(uniforms,bufferIDs,15);
			unit->addAnnotation("mesh1 setUniform at wrong location - ");
			unit->assertTrue(!worked);

			UniformVector u2v5("u2v5");
			u2v5 << Vector(3,1,5,4);
			UniformMatrix um5("um5");
			um5 << Matrix(
				4,3,5,6,
				9,8,13,21,
				1,0,1,2,
				4,5,6,2
				);
			UniformVector u4v6("u4v6");
			u4v6 << Vector(3.1f,4.1f,5.9f,1.0f);
			UniformMatrix um6("um6");
			um6 << Matrix(
				3.1f,3.1f,2.8f,4,
				5.1f,6,7,8,
				2,3,0,3.14f,
				5.2f,3.2f,4,3
				);
			UniformFloat uf8("uf8");
			uf8 << 1.5131313f;
			UniformVector u3v11("u3v11");
			u3v11 << Vector(4.1f,1.3f,0.9f,2.2f);
			UniformVector u4v11("u4v11");
			u4v11 << Vector(19,13,2.2f,18);
			UniformFloat uf14("uf14");
			uf14 << 653.345f;
			UniformVector u2v14("u2v14");
			u2v14 << Vector(13,0.75f,144,99);
			UniformMatrix um14("um14");
			um14 << Matrix(
				28,2,34,6,
				9,23,5,23,
				13,35,3,5,
				3.14159f,1.8f,2,3
				);

			uniforms.clear();
			bufferIDs.clear();
			uniforms.push_back(&um5);
			uniforms.push_back(&u2v5);
			bufferIDs.push_back("buffer16");
			bufferIDs.push_back("buffer2");
			worked = mesh1->setUniforms(uniforms,bufferIDs,5);
			unit->addAnnotation("mesh1 setUniform location 5 - ");
			unit->assertTrue(worked);

			uniforms.clear();
			bufferIDs.clear();
			uniforms.push_back(&u4v6);
			uniforms.push_back(&um6);
			bufferIDs.push_back("buffer4");
			bufferIDs.push_back("buffer16");
			worked = mesh1->setUniforms(uniforms,bufferIDs,6);
			unit->addAnnotation("mesh1 setUniform location 6 - ");
			unit->assertTrue(worked);

			uniforms.clear();
			bufferIDs.clear();
			uniforms.push_back(&uf8);
			bufferIDs.push_back("buffer1");
			worked = mesh1->setUniforms(uniforms,bufferIDs,8);
			unit->addAnnotation("mesh1 setUniform location 8 - ");
			unit->assertTrue(worked);

			uniforms.clear();
			bufferIDs.clear();
			uniforms.push_back(&u4v11);
			uniforms.push_back(&u3v11);
			bufferIDs.push_back("buffer4");
			bufferIDs.push_back("buffer3");
			worked = mesh1->setUniforms(uniforms,bufferIDs,11);
			unit->addAnnotation("mesh1 setUniform location 11 - ");
			unit->assertTrue(worked);

			uniforms.clear();
			bufferIDs.clear();
			uniforms.push_back(&u4v11);
			uniforms.push_back(&u3v11);
			bufferIDs.push_back("buffer4");
			bufferIDs.push_back("buffer3");
			worked = mesh1->setUniforms(uniforms,bufferIDs,11);
			unit->addAnnotation("mesh1 setUniform location 11 - ");
			unit->assertTrue(worked);

			uniforms.clear();
			bufferIDs.clear();
			uniforms.push_back(&uf14);
			uniforms.push_back(&u2v14);
			uniforms.push_back(&um14);
			bufferIDs.push_back("buffer1");
			bufferIDs.push_back("buffer2");
			bufferIDs.push_back("buffer16");
			worked = mesh1->setUniforms(uniforms,bufferIDs,14);
			unit->addAnnotation("mesh1 setUniform location 14 - ");
			unit->assertTrue(worked);

			mesh1->loadUniformsToBuffers(6,20);

			const float *content = mesh1->getBuffer("buffer1")->unlockRead();
			bool equivalent = true;
			for(unsigned int i=0 ; i<vertexCount ; i++) {
				if(i != 8 && i != 14 && abs(m1b1[i] - content[i]) > 0.001f) {
					equivalent = false;
				}
			}
			equivalent = equivalent && (abs(content[8] - 1.5131313f) < 0.001f);
			equivalent = equivalent && (abs(content[14] - 653.345f) < 0.001f);
			unit->addAnnotation("mesh1 loadUniformsToBuffers(6,20) buffer1 - ");
			unit->assertTrue(equivalent);
			mesh1->getBuffer("buffer1")->lock();

			content = mesh1->getBuffer("buffer2")->unlockRead();
			equivalent = true;
			for(unsigned int i=0 ; i<vertexCount*2 ; i++) {
				if(i != 14*2 && i != 14*2+1 && abs(m1b2[i] - content[i]) > 0.001f) {
					equivalent = false;
				}
			}
			Vector v;
			v << Vector(content[14*2],content[14*2 + 1],0,1);
			Vector v2 = u2v14;
			v2[2] = 0;
			v2[3] = 1;
			equivalent = equivalent && v.equals(v2,0.001f);
			unit->addAnnotation("mesh1 loadUniformsToBuffers(6,20) buffer2 - ");
			unit->assertTrue(equivalent);
			mesh1->getBuffer("buffer2")->lock();

			content = mesh1->getBuffer("buffer3")->unlockRead();
			equivalent = true;
			for(unsigned int i=0 ; i<vertexCount*3 ; i++) {
				if((i<11*3 || i>=12*3) && abs(m1b3[i] - content[i]) > 0.001f) {
					equivalent = false;
				}
			}
			v << Vector(content[11*3],content[11*3+1],content[11*3+2],1);
			v2 = u3v11;
			v2[3] = 1.0;
			equivalent = equivalent && v.equals(v2,0.001f);
			unit->addAnnotation("mesh1 loadUniformsToBuffers(6,20) buffer3 - ");
			unit->assertTrue(equivalent);
			mesh1->getBuffer("buffer3")->lock();

			content = mesh1->getBuffer("buffer4")->unlockRead();
			equivalent = true;
			for(unsigned int i=0 ; i<vertexCount*4 ; i++) {
				if((i<6*4 || i>=7*4) && (i<11*4 || i>=12*4) && abs(m1b4[i] - content[i]) > 0.001f) {
					equivalent = false;
				}
			}
			v << &content[6*4];
			equivalent = equivalent && v.equals(u4v6,0.001f);
			v << &content[11*4];
			equivalent = equivalent && v.equals(u4v11,0.001f);
			unit->addAnnotation("mesh1 loadUniformsToBuffers(6,20) buffer4 - ");
			unit->assertTrue(equivalent);
			mesh1->getBuffer("buffer4")->lock();

			content = mesh1->getBuffer("buffer16")->unlockRead();
			equivalent = true;
			for(unsigned int i=0 ; i<vertexCount*16 ; i++) {
				if((i<6*16 || i>=7*16) && i<14*16 && abs(m1b16[i] - content[i]) > 0.001f) {
					equivalent = false;
				}
			}
			Matrix m;
			m << &content[6*16];
			equivalent = equivalent && m.equals(um6,0.001f);
			m << &content[14*16];
			equivalent = equivalent && m.equals(um14,0.001f);
			unit->addAnnotation("mesh1 loadUniformsToBuffers(6,20) buffer4 - ");
			unit->assertTrue(equivalent);
			mesh1->getBuffer("buffer16")->lock();

			um6 = Matrix(
				1,3,3,1,
				0,1,2,3,
				4.1f,2.2f,1.04f,5.5f,
				4,4,4,4);
			mesh1->loadUniformsToBuffer("buffer16",5,6);

			content = mesh1->getBuffer("buffer4")->unlockRead();
			equivalent = true;
			for(unsigned int i=0 ; i<vertexCount*4 ; i++) {
				if((i<6*4 || i>=7*4) && (i<11*4 || i>=12*4) && abs(m1b4[i] - content[i]) > 0.001f) {
					equivalent = false;
				}
			}
			v << &content[6*4];
			equivalent = equivalent && v.equals(u4v6,0.001f);
			v << &content[11*4];
			equivalent = equivalent && v.equals(u4v11,0.001f);
			unit->addAnnotation("mesh1 loadUniformsToBuffer(\"buffer16\",6,20) -> buffer4 not changed - ");
			unit->assertTrue(equivalent);
			mesh1->getBuffer("buffer4")->lock();

			content = mesh1->getBuffer("buffer16")->unlockRead();
			equivalent = true;
			for(unsigned int i=0 ; i<vertexCount*16 ; i++) {
				if((i<5*16 || i>=7*16) && i<14*16 && abs(m1b16[i] - content[i]) > 0.001f) {
					equivalent = false;
				}
			}
			m << &content[5*16];
			equivalent = equivalent && m.equals(um5,0.001f);
			m << &content[6*16];
			equivalent = equivalent && m.equals(um6,0.001f);
			m << &content[14*16];
			equivalent = equivalent && m.equals(um14,0.001f);
			unit->addAnnotation("mesh1 loadUniformsToBuffer(\"buffer16\",6,20) buffer16 - ");
			unit->assertTrue(equivalent);
			mesh1->getBuffer("buffer16")->lock();

			float *writeContent = mesh1->getBuffer("buffer1")->unlockWrite();
			for(unsigned int i=0 ; i<vertexCount ; i++) {
				writeContent[i] = m1b1[i];
			}
			for(unsigned int i=vertexCount ; i<maxVertexCount ; i++) {
				writeContent[i] = i;
			}
			mesh1->getBuffer("buffer1")->lock();
			writeContent = mesh1->getBuffer("buffer2")->unlockWrite();
			for(unsigned int i=0 ; i<vertexCount*2 ; i++) {
				writeContent[i] = m1b2[i];
			}
			for(unsigned int i=vertexCount*2 ; i<maxVertexCount*2 ; i++) {
				writeContent[i] = i;
			}
			mesh1->getBuffer("buffer2")->lock();
			writeContent = mesh1->getBuffer("buffer3")->unlockWrite();
			for(unsigned int i=0 ; i<vertexCount*3 ; i++) {
				writeContent[i] = m1b3[i];
			}
			for(unsigned int i=vertexCount*3 ; i<maxVertexCount*3 ; i++) {
				writeContent[i] = i;
			}
			mesh1->getBuffer("buffer3")->lock();
			writeContent = mesh1->getBuffer("buffer4")->unlockWrite();
			for(unsigned int i=0 ; i<vertexCount*4 ; i++) {
				writeContent[i] = m1b4[i];
			}
			for(unsigned int i=vertexCount*4 ; i<maxVertexCount*4 ; i++) {
				writeContent[i] = i;
			}
			mesh1->getBuffer("buffer4")->lock();
			writeContent = mesh1->getBuffer("buffer16")->unlockWrite();
			for(unsigned int i=0 ; i<vertexCount*16 ; i++) {
				writeContent[i] = m1b16[i];
			}
			for(unsigned int i=vertexCount*16 ; i<maxVertexCount*16 ; i++) {
				writeContent[i] = i;
			}
			mesh1->getBuffer("buffer16")->lock();

			mesh1->loadBuffersToUniforms(6,20);

			content = mesh1->getBuffer("buffer1")->unlockRead();
			equivalent = true;
			for(unsigned int i=0 ; i<vertexCount ; i++) {
				if(abs(m1b1[i] - content[i]) > 0.001f) {
					equivalent = false;
				}
			}
			for(unsigned int i=vertexCount ; i<maxVertexCount ; i++) {
				if(content[i] != i) {
					equivalent = false;
				}
			}
			float f;
			uf8 >> f;
			equivalent = equivalent && (abs(content[8] - f) < 0.001f);
			uf14 >> f;
			equivalent = equivalent && (abs(content[14] - f) < 0.001f);
			unit->addAnnotation("mesh1 loadBuffersToUniforms(6,20) buffer1 - ");
			unit->assertTrue(equivalent);
			mesh1->getBuffer("buffer1")->lock();

			content = mesh1->getBuffer("buffer2")->unlockRead();
			equivalent = true;
			for(unsigned int i=0 ; i<vertexCount*2 ; i++) {
				if(abs(m1b2[i] - content[i]) > 0.001f) {
					equivalent = false;
				}
			}
			for(unsigned int i=vertexCount*2 ; i<maxVertexCount*2 ; i++) {
				if(content[i] != i) {
					equivalent = false;
				}
			}
			v = Vector(3,1,5,4);
			equivalent = equivalent && v.equals(u2v5,0.001f);
			v = Vector(content[14*2],content[14*2+1],0,1);
			equivalent = equivalent && v.equals(u2v14,0.001f);
			unit->addAnnotation("mesh1 loadBuffersToUniforms(6,20) buffer2 - ");
			unit->assertTrue(equivalent);
			mesh1->getBuffer("buffer2")->lock();

			content = mesh1->getBuffer("buffer3")->unlockRead();
			equivalent = true;
			for(unsigned int i=0 ; i<vertexCount*3 ; i++) {
				if(abs(m1b3[i] - content[i]) > 0.001f) {
					equivalent = false;
				}
			}
			for(unsigned int i=vertexCount*3 ; i<maxVertexCount*3 ; i++) {
				if(content[i] != i) {
					equivalent = false;
				}
			}
			v = Vector(content[11*3],content[11*3+1],content[11*3+2],1);
			equivalent = equivalent && v.equals(u3v11,0.001f);
			unit->addAnnotation("mesh1 loadBuffersToUniforms(6,20) buffer3 - ");
			unit->assertTrue(equivalent);
			mesh1->getBuffer("buffer3")->lock();

			content = mesh1->getBuffer("buffer4")->unlockRead();
			equivalent = true;
			for(unsigned int i=0 ; i<vertexCount*4 ; i++) {
				if(abs(m1b4[i] - content[i]) > 0.001f) {
					equivalent = false;
				}
			}
			for(unsigned int i=vertexCount*4 ; i<maxVertexCount*4 ; i++) {
				if(content[i] != i) {
					equivalent = false;
				}
			}
			v << &content[6*4];
			equivalent = equivalent && v.equals(u4v6,0.001f);
			v << &content[11*4];
			equivalent = equivalent && v.equals(u4v11,0.001f);
			unit->addAnnotation("mesh1 loadBuffersToUniforms(6,20) buffer4 - ");
			unit->assertTrue(equivalent);
			mesh1->getBuffer("buffer4")->lock();

			content = mesh1->getBuffer("buffer16")->unlockRead();
			equivalent = true;
			for(unsigned int i=0 ; i<vertexCount*16 ; i++) {
				if(abs(m1b16[i] - content[i]) > 0.001f) {
					equivalent = false;
				}
			}
			for(unsigned int i=vertexCount*16 ; i<maxVertexCount*16 ; i++) {
				if(content[i] != i) {
					equivalent = false;
				}
			}
			m = Matrix(
				4,3,5,6,
				9,8,13,21,
				1,0,1,2,
				4,5,6,2
				);
			equivalent = equivalent && m.equals(um5,0.001f);
			m << &content[6*16];
			equivalent = equivalent && m.equals(um6,0.001f);
			m << &content[14*16];
			equivalent = equivalent && m.equals(um14,0.001f);
			unit->addAnnotation("mesh1 loadBuffersToUniforms(6,20) buffer16 - ");
			unit->assertTrue(equivalent);
			mesh1->getBuffer("buffer16")->lock();

			u2v5 = Vector(13,13,13,13);
			u2v14 = Vector(14,14,14,14);
			um5 = Matrix(3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3);
			um6 = Matrix(7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7);
			um14 = Matrix(9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9);

			mesh1->loadBufferToUniforms("buffer2",6,20);

			content = mesh1->getBuffer("buffer2")->unlockRead();
			equivalent = true;
			for(unsigned int i=0 ; i<vertexCount*2 ; i++) {
				if(abs(m1b2[i] - content[i]) > 0.001f) {
					equivalent = false;
				}
			}
			for(unsigned int i=vertexCount*2 ; i<maxVertexCount*2 ; i++) {
				if(content[i] != i) {
					equivalent = false;
				}
			}
			v = Vector(13,13,13,13);
			equivalent = equivalent && v.equals(u2v5,0.001f);
			v = Vector(content[14*2],content[14*2+1],0,1);
			equivalent = equivalent && v.equals(u2v14,0.001f);
			unit->addAnnotation("mesh1 loadBufferToUniforms(\"buffer2\",6,20) buffer2 - ");
			unit->assertTrue(equivalent);
			mesh1->getBuffer("buffer2")->lock();

			content = mesh1->getBuffer("buffer16")->unlockRead();
			equivalent = true;
			for(unsigned int i=0 ; i<vertexCount*16 ; i++) {
				if(abs(m1b16[i] - content[i]) > 0.001f) {
					equivalent = false;
				}
			}
			for(unsigned int i=vertexCount*16 ; i<maxVertexCount*16 ; i++) {
				if(content[i] != i) {
					equivalent = false;
				}
			}
			m = Matrix(3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3);
			equivalent = equivalent && m.equals(um5,0.001f);
			m = Matrix(7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7);
			equivalent = equivalent && m.equals(um6,0.001f);
			m = Matrix(9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9);
			equivalent = equivalent && m.equals(um14,0.001f);
			unit->addAnnotation("mesh1 loadBufferToUniforms(\"buffer2\",6,20) buffer16 - ");
			unit->assertTrue(equivalent);
			mesh1->getBuffer("buffer16")->lock();

			//init data for instanced rendering

			program1 = new Shader("program1");
			program1->addShader(
				"#version 400 core			\n"
				"in vec4 vertices;				\n"
				"in vec3 colors;				\n"
				"in mat4 pos;					\n"
				"in vec3 colorAdd;				\n"
				"out vec3 col;					\n"
				"								\n"
				"void main() {					\n"
				"	gl_Position = pos*vertices;	\n"
				"	col = colors*colorAdd;		\n"
				"}								\n"
				,VERTEX);
			program1->addShader(
				"#version 400 core			\n"
				"in vec3 col;					\n"
				"out vec4 frag1;				\n"
				"								\n"
				"void main() {					\n"
				"	frag1 = vec4(col,1);		\n"
				"}								\n"
				,FRAGMENT);
			program1->load();
			unit->addAnnotation("load program1 - ");
			unit->assertTrue(program1->isLoaded());
			unit->addAnnotation("program1 fragmendshading - ");
			unit->assertTrue(program1->isFragmentShading());
			unit->addAnnotation("program1 transformfeedback - ");
			unit->assertTrue(!program1->isTransformFeedback());

			float mesh2VArray[] = {
			0.5f,0,0,1,			0.4f,0.4f,0,1,		0.25f,0.25f,0,1,
			0.25f,0.25f,0,1,	0.4f,0.4f,0,1,		0,0.5f,0,1,
			0,0.5f,0,1,			-0.4f,0.4f,0,1,		-0.25f,0.25f,0,1,
			-0.25f,0.25f,0,1,	-0.4f,0.4f,0,1,		-0.5f,0,0,1,
			-0.5f,0,0,1,		-0.4f,-0.4f,0,1,	-0.25f,-0.25f,0,1,
			-0.25f,-0.25f,0,1,	-0.4f,-0.4f,0,1,	0,-0.5f,0,1,
			0,-0.5f,0,1,		0.4f,-0.4f,0,1,		0.25f,-0.25f,0,1,
			0.25f,-0.25f,0,1,	0.4f,-0.4f,0,1,		0.5f,0,0,1
			};
			float mesh2TArray[] = {
			1,1,1,		1,0,0,		0,0,1,
			0,0,1,		1,0,0,		0,1,0,
			0,1,0,		0,0,1,		1,0,0,
			1,0,0,		0,0,1,		0,1,0,
			0,1,0,		0,1,0,		1,0,0,
			1,0,0,		0,1,0,		0,0,1,
			0,0,1,		0,0,1,		1,0,0,
			1,0,0,		0,0,1,		1,1,1
			};
			vector<float> mesh2VList(mesh2VArray,mesh2VArray + 96);
			vector<float> mesh2TList(mesh2TArray,mesh2TArray + 72);
			mesh2 = new BufferedMesh("mesh2");
			mesh2->addBuffer("vertices",mesh2VList,4);
			mesh2->addBuffer("colors",mesh2TList,3);
			mesh2->setFaceSize(3);
			mesh2->load();
			unit->addAnnotation("mesh2 loaded - ");
			unit->assertTrue(mesh2->isLoaded());

			float mesh3Array1[] = {
				0.25f,0,0,0, 0,0.5f,0,0, 0,0,1,0, -1,-1,0,1,
				0.25f,0,0,0, 0,0.25f,0,0, 0,0,1,0, -0.8f,-0.8f,0.1f,1,
				0.5f,0,0,0, 0,0.25f,0,0, 0,0,1,0, -0.5f,0,0.2f,1,
				0.1f,0,0,0, 0,0.1f,0,0, 0,0,1,0, -0.25f,0.25f,0.3f,1,
				0.25f,0,0,0, 0,0.25f,0,0, 0,0,1,0, 0,0.5f,0.4f,1,
				0.1f,0,0,0, 0,0.25f,0,0, 0,0,1,0, 0.25f,0,0.5f,1,
				0.5f,0,0,0, 0,0.5f,0,0, 0,0,1,0, 0.5f,0.5f,0.6f,1,
				0.25f,0,0,0, 0,0.25f,0,0, 0,0,1,0, 0.8f,0.5f,0.7f,1,
				0.1f,0,0,0, 0,0.1f,0,0, 0,0,1,0, 0,-0.25f,0.8f,1,
			};
			vector<float> mesh3List1(mesh3Array1,mesh3Array1 + 144);
			mesh3 = new BufferedMesh("mesh3");
			mesh3->addBuffer("pos",mesh3List1,16);
			mesh3->setFaceSize(1);
			mesh3->load();
			unit->addAnnotation("mesh3 loaded - ");
			unit->assertTrue(mesh3->isLoaded());

			float mesh4Array1[] = {
				0.5f,0.5f,0.5f,
				0,0,1,
				0,1,0,
				0,1,1,
				1,0,0,
				1,0,1,
				1,1,0,
				1,1,1
			};
			vector<float> mesh4List1(mesh4Array1,mesh4Array1 + 24);
			mesh4 = new BufferedMesh("mesh4");
			mesh4->addBuffer("colorAdd",mesh4List1,3);
			mesh4->setFaceSize(1);
			mesh4->load();
			unit->addAnnotation("mesh4 loaded - ");
			unit->assertTrue(mesh4->isLoaded());

			program2 = new Shader("program2");
			program2->addShader(
				"#version 400 core			\n"
				"in vec4 vertices;				\n"
				"in vec3 col1;					\n"
				"in vec3 col2;					\n"
				"in vec3 col3;					\n"
				"in vec3 col4;					\n"
				"out vec3 col;					\n"
				"								\n"
				"void main() {					\n"
				"	gl_Position = vertices;		\n"
				"	col = col1 + col2 + col3 + col4;		\n"
				"}								\n"
				,VERTEX);
			program2->addShader(
				"#version 400 core			\n"
				"in vec3 col;					\n"
				"out vec4 frag1;				\n"
				"								\n"
				"void main() {					\n"
				"	frag1 = vec4(col,1);		\n"
				"}								\n"
				,FRAGMENT);
			program2->load();
			unit->addAnnotation("load program2 - ");
			unit->assertTrue(program2->isLoaded());
			unit->addAnnotation("program2 fragmendshading - ");
			unit->assertTrue(program2->isFragmentShading());
			unit->addAnnotation("program2 transformfeedback - ");
			unit->assertTrue(!program2->isTransformFeedback());

			float mesh5Array1[] = {
				0.5f,-0.5f,0.9f,1,
				1,-0.5f,0.9f,1,
				1,-1,0.9f,1,
				0.5f,-0.5f,0.9f,1,
				1,-1,0.9f,1,
				0.5f,-1,0.9f,1
			};
			float mesh5Array2[] = {
				0,0,1,
				0,0,0,
				0,0,0,
				0,0,1,
				0,0,0,
				0,0,0
			};
			float mesh5Array3[] = {
				0,0,0,
				1,0,1,
				0,0,0,
				0,0,0,
				0,0,0,
				0,0,0
			};
			float mesh5Array4[] = {
				0,0,0,
				0,0,0,
				1,0,0,
				0,0,0,
				1,0,0,
				0,0,0
			};
			float mesh5Array5[] = {
				0,0,0,
				0,0,0,
				0,0,0,
				0,0,0,
				0,0,0,
				0,0,0
			};
			vector<float> mesh5List1(mesh5Array1,mesh5Array1 + 24);
			vector<float> mesh5List2(mesh5Array2,mesh5Array2 + 18);
			vector<float> mesh5List3(mesh5Array3,mesh5Array3 + 18);
			vector<float> mesh5List4(mesh5Array4,mesh5Array4 + 18);
			vector<float> mesh5List5(mesh5Array5,mesh5Array5 + 18);
			mesh5 = new BufferedMesh("mesh5");
			mesh5->addBuffer("vertices",mesh5List1,4);
			mesh5->addBuffer("col1",mesh5List2,3);
			mesh5->addBuffer("col2",mesh5List3,3);
			mesh5->addBuffer("col3",mesh5List4,3);
			mesh5->addBuffer("col4",mesh5List5,3);
			mesh5->setFaceSize(3);
			mesh5->load();
			unit->addAnnotation("mesh5 loaded - ");
			unit->assertTrue(mesh5->isLoaded());
		}

		void reshape(SXRenderArea &area) {
		}

		void render(SXRenderArea &area) {
			time += area.getDeltaTime();
			if(time > 4) {
				area.stopRendering();
			}

			RenderTarget::setViewport(0,0,area.getWidth(),area.getHeight());
			RenderTarget::clearTarget();

			vector<BufferedMesh *> instanceBuffers;
			instanceBuffers.push_back(mesh3);
			instanceBuffers.push_back(mesh4);

			program1->use();
			mesh2->renderInstanced(*program1,instanceBuffers);

			program2->use();
			mesh5->render(*program2);
		}

		void stop(SXRenderArea &area) {
			unit->addMarkup("destruction phase");

			delete mesh1;
			delete mesh2;
			delete mesh3;
			delete mesh4;
			delete mesh5;
			delete program1;
			delete program2;
		}

	};
	
	void testInstancing(const string &logname) {
		XUnit unit(new FileLogger(logname));
		try {
			Logger::addLogger("testinstancing",new ListLogger());
			Logger::setDefaultLogger("testinstancing");
			ListLogger &list = dynamic_cast<ListLogger &>(Logger::get());

			QDialog dialog1;
			QGridLayout *dialog1_l = new QGridLayout();
			dialog1.setLayout(dialog1_l);
			SXWidget *widget1 = new SXWidget();
			dialog1_l->addWidget(widget1);
			widget1->renderPeriodically(0.01f);
			widget1->setMinimumSize(600,400);
			QObject::connect(widget1, SIGNAL(finishedRendering()), &dialog1, SLOT(close()));
			widget1->addRenderListener(*new TestInstancing1(unit,list));
			dialog1.show();

			unit.addAnnotation("execute app1 - ");
			unit.assertEquals(dialog1.exec(),0);

			unit.addMarkup("image validation");
			unit.assertInputYes("pink unicorn with wings?");
		}  catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}
	}

}

#endif
