#ifndef _TEST_SX_TESTRENDERTARGET_CPP_
#define _TEST_SX_TESTRENDERTARGET_CPP_

/**
 * test cases with frame buffers
 * (c) 2012 by Tristan Bauer
 */
#include <test/SXEngine.h>
#include <sx/XUnit.h>
#include <sx/Log4SX.h>
#include <sx/SXCore.h>
#include <sx/SX.h>
#include <sx/SXWidget.h>
#include <QApplication>
#include <QDialog>
#include <QGridLayout>
#include <vector>
#include <cmath>
using namespace sx;
using namespace std;

namespace sxengine {

	class TestRenderTarget1: public SXRenderListener {
	private:
		int width;
		int height;

		Shader *program1;
		Shader *program2;
		BufferedMesh *mesh;
		Texture *logo;
		RenderTarget *target;
		UniformFloat *useFrag1;
		Texture *target1;
		Texture *target2;

		XUnit *unit;
		ListLogger *list;
		TimeMeasurement timeMeasurement;
	public:

		TestRenderTarget1(XUnit &unit, ListLogger &list) {
			this->unit = &unit;
			this->list = &list;
		}

		void create(SXRenderArea &area) {
			this->width = area.getWidth();
			this->height = area.getHeight();

			unit->addMarkup("loading phase");

			target = new RenderTarget("bla");
			target->load();
			unit->addAnnotation("load without params fails - ");
			unit->assertTrue(!target->isLoaded());
			delete target;

			target = new RenderTarget("bla");
			target->setWidth(800);
			target->setHeight(300);
			target->load();
			unit->addAnnotation("load successfully - ");
			unit->assertTrue(target->isLoaded());
			delete target;

			target = new RenderTarget("bla");
			target->setWidth(5);
			target->setHeight(100);
			target->load();
			int w = target->getWidth();
			int h = target->getHeight();
			unit->addAnnotation("load 1 - success - ");
			unit->assertTrue(target->isLoaded());
			target->setWidth(-1);
			target->setHeight(50);
			target->load();
			unit->addAnnotation("load 2 - don't change - ");
			unit->assertTrue(target->isLoaded());
			unit->addAnnotation("width - ");
			unit->assertEquals(target->getWidth(),w);
			unit->addAnnotation("height - ");
			unit->assertEquals(target->getHeight(),h);
			target->setWidth(500);
			target->setHeight(100);
			target->load();
			unit->addAnnotation("load 3 - success - ");
			unit->assertTrue(target->isLoaded());
			unit->addAnnotation("width - ");
			unit->assertEquals(target->getWidth(),500);
			unit->addAnnotation("height - ");
			unit->assertEquals(target->getHeight(),100);
			w = target->getWidth();
			h = target->getHeight();
			target->load();
			unit->addAnnotation("load 4 - don't change - ");
			unit->assertTrue(target->isLoaded());
			unit->addAnnotation("width - ");
			unit->assertEquals(target->getWidth(),w);
			unit->addAnnotation("height - ");
			unit->assertEquals(target->getHeight(),h);
			delete target;

			program1 = new Shader("prog1");
			program1->addShaderFile("data/test/sxengine/files/rendertarget1.vp",VERTEX);
			program1->addShaderFile("data/test/sxengine/files/rendertarget1.fp",FRAGMENT);
			list->getMarkups().clear();
			program1->load();
			unit->addAnnotation("load program 1 - ");
			try {
				for(unsigned int i=0 ; true ; i++) {
					unit->addAnnotation(list->getStringEntry("Shader::load",i));
				}
			} catch(Exception &) {
			}
			unit->assertTrue(program1->isLoaded());

			program2 = new Shader("prog2");
			program2->addShaderFile("data/test/sxengine/files/rendertarget2.vp",VERTEX);
			program2->addShaderFile("data/test/sxengine/files/rendertarget2.fp",FRAGMENT);
			list->getMarkups().clear();
			program2->load();
			try {
				for(unsigned int i=0 ; true ; i++) {
					unit->addAnnotation(list->getStringEntry("Shader::load",i));
				}
			} catch(Exception &) {
			}
			unit->addAnnotation("load program 2 - ");
			unit->assertTrue(program2->isLoaded());

			float meshVArray[] = {
			-1,-1,0,1,
			1,-1,0,1,
			1,1,0,1,
			-1,-1,0,1,
			1,1,0,1,
			-1,1,0,1
			};
			float meshTArray[] = {
			0,0,
			1,0,
			1,1,
			0,0,
			1,1,
			0,1
			};
			vector<float> meshVList(meshVArray,meshVArray + 24);
			vector<float> meshTList(meshTArray,meshTArray + 12);
			mesh = new BufferedMesh("mesh");
			mesh->addBuffer("vertices", meshVList, 4);
			mesh->addBuffer("texcoords",meshTList, 2);
			mesh->setFaceSize(3);
			mesh->load();
			unit->addAnnotation("load vertices - ");
			unit->assertTrue(mesh->isLoaded());

			logo = new Texture("logo");
			logo->setPath("data/test/sxengine/files/ShadeXEngine.jpg");
			logo->load();
			unit->addAnnotation("load texture - ");
			unit->assertTrue(logo->isLoaded());


			target = new RenderTarget("target");
			target->setWidth(width);
			target->setHeight(height);
			target->load();
			unit->addAnnotation("load rendertarget - ");
			unit->assertTrue(target->isLoaded());

			target1 = new Texture("frag1");
			target1->setWidth(width);
			target1->setHeight(height);
			target1->setPixelFormat(FLOAT_RGBA);
			target1->load();
			unit->addAnnotation("load targettexture 1 - ");
			unit->assertTrue(target1->isLoaded());

			target2 = new Texture("somefrag"); 
			target2->setUniformName("frag2","resource");
			target2->setWidth(width);
			target2->setHeight(height);
			target2->setPixelFormat(BYTE_RGBA);
			target2->load();
			unit->addAnnotation("load targettexture 2 - ");
			unit->assertTrue(target2->isLoaded());

			useFrag1 = new UniformFloat("useFrag1");

			//start measuring time
			timeMeasurement.restart();
		}

		void reshape(SXRenderArea &area) {
			this->width = area.getWidth();
			this->height = area.getHeight();
		}
		
		void render(SXRenderArea &area) {
			double millies = timeMeasurement.elapsed();

			vector<Texture *> textures;
			textures.push_back(target1);
			textures.push_back(target2);

			target->beginRender(*program1,textures,"resource");
			target->setViewport();
			RenderTarget::clearTarget();
			program1->use();
			logo->use(*program1,"resource");
			mesh->render(*program1);
			target->endRender();

			RenderTarget::clearTarget();

			*useFrag1 = 1;
			RenderTarget::setViewport(0,0,width/2,height/2);
			program2->use();
			useFrag1->use(*program2,"");
			target1->use(*program2,"");
			target2->use(*program2,"");
			mesh->render(*program2);

			*useFrag1 = 0;
			RenderTarget::setViewport(width/2,0,width/2,height/2);
			program2->use();
			useFrag1->use(*program2,"");
			target1->use(*program2,"");
			target2->use(*program2,"");
			mesh->render(*program2);

			if(millies > 6) {
				//leave window after five seconds
				area.stopRendering();
			}
		}

		void stop(SXRenderArea &area) {
			unit->addMarkup("destruction phase");
			delete program1;
			delete program2;
			delete mesh;
			delete logo;
			delete target;
			delete target1;
			delete target2;
			delete useFrag1;
		}

	};

	void testRenderTarget(const string &logname) {
		XUnit unit(new FileLogger(logname));
		try {
			Logger::addLogger("testrendertarget",new ListLogger());
			Logger::setDefaultLogger("testrendertarget");
			ListLogger &logger = dynamic_cast<ListLogger &>(Logger::get());

			QDialog dialog;
			QGridLayout *dialog_l = new QGridLayout();
			dialog.setLayout(dialog_l);
			SXWidget *widget = new SXWidget();
			dialog_l->addWidget(widget);
			widget->renderPeriodically(0.01f);
			widget->setMinimumSize(600,400);
			QObject::connect(widget, SIGNAL(finishedRendering()), &dialog, SLOT(close()));
			widget->addRenderListener(*new TestRenderTarget1(unit,logger));
			dialog.show();

			unit.addAnnotation("execute dialog - ");
			unit.assertEquals(dialog.exec(),0);

			unit.addMarkup("image validation");
			unit.assertInputYes("Did you see two pictures, the left one blurred, the right one sharp?");
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}
	}

}

#endif
