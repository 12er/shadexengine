#ifndef _TEST_SX_TESTPASS_CPP_
#define _TEST_SX_TESTPASS_CPP_

/**
 * effect test cases
 * (c) 2012 by Tristan Bauer
 */
#include <test/SXEngine.h>
#include <sx/XUnit.h>
#include <sx/Log4SX.h>
#include <sx/SXCore.h>
#include <sx/SX.h>
#include <sx/SXWidget.h>
#include <QApplication>
#include <QGridLayout>
#include <QDialog>
#include <vector>
#include <cmath>
using namespace sx;
using namespace std;

namespace sxengine {

	class TestEffect1: public SXRenderListener {
	private:
		int width;
		int height;

		RenderTarget *target1;
		RenderTarget *target2;
		Shader *shader1;
		Shader *shader2;
		Shader *shader3;
		Texture *tex1;
		Texture *tex2_1;
		Texture *tex2_2;
		Texture *tex3_1;
		Texture *tex3_2;
		Texture *frag1;
		Texture *frag2;
		UniformVector *translate1;
		UniformVector *translate2;
		UniformVector *translate3;
		UniformVector *translate4;
		UniformVector *translate5;
		UniformVector *translate6;
		BufferedMesh *mesh;
		BufferedMesh *quad;
		RenderObject *obj1;
		RenderObject *obj2;
		RenderObject *obj3;
		RenderObject *obj4;
		RenderObject *obj5;
		RenderObject *obj6;
		RenderObject *objquad;
		Pass *pass1;
		Pass *pass2;
		Effect *effect;

		XUnit *unit;
		ListLogger *list;
		TimeMeasurement timeMeasurement;

		TestEffect1(const TestEffect1 &);
		TestEffect1 &operator = (const TestEffect1 &);
	public:

		TestEffect1(XUnit &unit, ListLogger &list) {
			this->unit = &unit;
			this->list = &list;
		}

		void create(SXRenderArea &area) {
			this->width = area.getWidth();
			this->height = area.getHeight();


			unit->addMarkup("loading phase");

			target1 = new RenderTarget("target1");
			target1->setWidth(600);
			target1->setHeight(600);
			target1->load();
			unit->addAnnotation("load target1 - ");
			unit->assertTrue(target1->isLoaded());

			frag1 = new Texture("f.frag1");
			frag1->setUniformName("tex1","pass2");
			frag1->setWidth(600);
			frag1->setHeight(600);
			frag1->setPixelFormat(FLOAT_RGBA);
			frag1->load();
			unit->addAnnotation("load frag1 - ");
			unit->assertTrue(frag1->isLoaded());

			frag2 = new Texture("f.frag2");
			frag2->setUniformName("tex2","pass2");
			frag2->setWidth(600);
			frag2->setHeight(600);
			frag2->setPixelFormat(BYTE_RGBA);
			frag2->load();
			unit->addAnnotation("load frag2 - ");
			unit->assertTrue(frag2->isLoaded());
			
			target2 = new RenderTarget("target2");
			target2->setWidth(width);
			target2->setHeight(height);
			target2->setRenderToDisplay(true);
			target2->load();
			unit->addAnnotation("load target2 - ");
			unit->assertTrue(target2->isLoaded());

			shader1 = new Shader("shader1");
			shader1->addShaderFile("data/test/sxengine/files/pass1.vp",VERTEX);
			shader1->addShaderFile("data/test/sxengine/files/pass1.fp",FRAGMENT);
			list->getMarkups().clear();
			shader1->load();
			unit->addAnnotation("load shader1 - ");
			try {
				for(unsigned int i=0 ; true ; i++) {
					unit->addAnnotation(list->getStringEntry("Shader::load",i));
				}
			} catch(Exception &) {
			}
			unit->assertTrue(shader1->isLoaded());

			shader2 = new Shader("shader2");
			shader2->addShaderFile("data/test/sxengine/files/pass1extra.vp",VERTEX);
			shader2->addShaderFile("data/test/sxengine/files/pass1extra.fp",FRAGMENT);
			list->getMarkups().clear();
			shader2->load();
			unit->addAnnotation("load shader2 - ");
			try {
				for(unsigned int i=0 ; true ; i++) {
					unit->addAnnotation(list->getStringEntry("Shader::load",i));
				}
			} catch(Exception &) {
			}
			unit->assertTrue(shader2->isLoaded());

			shader3 = new Shader("shader3");
			shader3->addShaderFile("data/test/sxengine/files/pass2.vp",VERTEX);
			shader3->addShaderFile("data/test/sxengine/files/pass2.fp",FRAGMENT);
			list->getMarkups().clear();
			shader3->load();
			unit->addAnnotation("load shader3 - ");
			try {
				for(unsigned int i=0 ; true ; i++) {
					unit->addAnnotation(list->getStringEntry("Shader::load",i));
				}
			} catch(Exception &) {
			}
			unit->assertTrue(shader3->isLoaded());

			tex1 = new Texture("tex1.tex1");
			tex1->setPath("data/test/sxengine/files/pass1.jpg");
			tex1->load();
			unit->addAnnotation("load tex1 - ");
			unit->assertTrue(tex1->isLoaded());

			tex2_1 = new Texture("tex2_1.tex2");
			tex2_1->setPath("data/test/sxengine/files/pass2_1.jpg");
			tex2_1->load();
			unit->addAnnotation("load tex2_1 - ");
			unit->assertTrue(tex2_1->isLoaded());

			tex2_2 = new Texture("tex2_2.tex2");
			tex2_2->setPath("data/test/sxengine/files/pass2_2.jpg");
			tex2_2->load();
			unit->addAnnotation("load tex2_2 - ");
			unit->assertTrue(tex2_2->isLoaded());

			tex3_1 = new Texture("tex3_1.tex3");
			tex3_1->setPath("data/test/sxengine/files/pass3_1.jpg");
			tex3_1->load();
			unit->addAnnotation("load tex3_1 - ");
			unit->assertTrue(tex3_1->isLoaded());

			tex3_2 = new Texture("tex3_2.tex3");
			tex3_2->setPath("data/test/sxengine/files/pass3_2.jpg");
			tex3_2->load();
			unit->addAnnotation("load tex3_2 - ");
			unit->assertTrue(tex3_2->isLoaded());

			translate1 = new UniformVector("t1.translate");
			*translate1 = Vector( 0.5f , 1.5f , 0 );

			translate2 = new UniformVector("t2.translate");
			*translate2 = Vector( 1.5f , 1.5f , 0 );

			translate3 = new UniformVector("t3.translate");
			*translate3 = Vector( 0.5f , 0.5f , 0 );

			translate4 = new UniformVector("t4.translate");
			*translate4 = Vector( 1.5f , 0.5f , 0);

			translate5 = new UniformVector("t5.translate");
			*translate5 = Vector( 0.9f , 1.1f , 0 );

			translate6 = new UniformVector("t6.translate");
			*translate6 = Vector( 1.1f , 0.9f , 0);

			float meshVArray1[] = {
			-1,-1,0,1,
			-0.875f,-1,0,1,
			-0.875f,-0.875f,0,1,
			-1,-1,0,1,
			-0.875f,-0.875f,0,1,
			-1,-0.875f,0,1
			};
			float meshVArray2[] = {
			-1,-1,0,1,
			1,-1,0,1,
			1,1,0,1,
			-1,-1,0,1,
			1,1,0,1,
			-1,1,0,1
			};
			float meshTArray[] = {
			0,0,
			1,0,
			1,1,
			0,0,
			1,1,
			0,1
			};
			vector<float> meshVList1(meshVArray1,meshVArray1 + 24);
			vector<float> meshVList2(meshVArray2,meshVArray2 + 24);
			vector<float> meshTList(meshTArray,meshTArray + 12);
			
			mesh = new BufferedMesh("mesh");
			mesh->addBuffer("vertices",meshVList1,4);
			mesh->addBuffer("texcoords",meshTList,2);
			mesh->setFaceSize(3);
			mesh->load();
			unit->addAnnotation("load mesh - ");
			unit->assertTrue(mesh->isLoaded());

			quad = new BufferedMesh("quad");
			quad->addBuffer("vertices",meshVList2,4);
			quad->addBuffer("texcoords",meshTList,2);
			quad->setFaceSize(3);
			quad->load();
			unit->addAnnotation("load quad - ");
			unit->assertTrue(quad->isLoaded());

			obj1 = new RenderObject("obj1");
			obj1->setMesh(*mesh);
			obj1->setShader(shader2);
			obj1->addUniform(*translate1);
			obj1->addUniform(*tex2_1);
			obj1->addUniform(*tex3_1);
			obj1->addUniform(*translate1);

			obj2 = new RenderObject("obj2");
			obj2->setMesh(*mesh);
			obj2->addUniform(*translate2);
			obj2->addUniform(*tex2_2);
			obj2->addUniform(*translate2);

			obj3 = new RenderObject("obj3");
			obj3->setMesh(*mesh);
			obj3->addUniform(*translate3);
			obj3->addUniform(*tex2_1);
			obj3->addUniform(*translate3);

			obj4 = new RenderObject("obj4");
			obj4->setMesh(*mesh);
			obj4->setShader(shader2);
			obj4->addUniform(*translate4);
			obj4->addUniform(*tex2_2);
			obj4->addUniform(*tex3_2);
			obj4->addUniform(*translate4);

			obj5 = new RenderObject("obj5");
			obj5->setMesh(*mesh);
			obj5->addUniform(*translate5);
			obj5->addUniform(*tex2_1);
			obj5->addUniform(*translate5);

			obj6 = new RenderObject("obj6");
			obj6->setMesh(*mesh);
			obj6->setShader(shader2);
			obj6->addUniform(*translate6);
			obj6->addUniform(*tex2_2);
			obj6->addUniform(*tex3_2);
			obj6->addUniform(*translate6);

			objquad = new RenderObject("quad");
			objquad->setMesh(*quad);

			pass1 = new Pass("pass1");
			vector<Texture *> output1;
			output1.push_back(frag1);
			output1.push_back(frag2);
			pass1->setOutput(*target1,output1);
			pass1->setShader(shader1);
			pass1->addUniform(*tex1);
			pass1->addRenderObject(*obj1);
			pass1->addRenderObject(*obj2);
			pass1->addRenderObject(*obj3);
			pass1->addRenderObject(*obj4);

			pass2 = new Pass("pass2");
			pass2->setOutput(*target2,vector<Texture *>());
			pass2->setShader(shader3);
			pass2->addUniform(*frag1);
			pass2->addUniform(*frag2);
			pass2->addRenderObject(*objquad);

			effect = new Effect("effect");
			vector<EffectCommand> &passes = effect->getObjects();
			EffectCommand ec1;
			ec1.object = pass1;
			EffectCommand ec2;
			ec2.object = pass2;
			passes.push_back(ec1);
			passes.push_back(ec2);
	
			//start measuring time
			timeMeasurement.restart();
		}

		void reshape(SXRenderArea &area) {
			this->width = area.getWidth();
			this->height = area.getHeight();

			target2->setWidth(width);
			target2->setHeight(height);
			target2->load();
		}
		
		void render(SXRenderArea &area) {
			double millies = timeMeasurement.elapsed();

			//plink with temp renderobjects
			double plink = sin(millies * Tau);
			if(plink > 0) {
				vector<RenderObject *> &temp = pass1->getTempRenderObjects();
				temp.push_back(obj5);
				temp.push_back(obj6);
			}
			effect->render();

			if(millies > 6) {
				//leave window after five seconds
				area.stopRendering();
			}
		}

		void stop(SXRenderArea &area) {
			unit->addMarkup("destruction phase");
			delete shader1;
			delete shader2;
			delete shader3;
			delete tex1;
			delete tex2_1;
			delete tex2_2;
			delete tex3_1;
			delete tex3_2;
			delete frag1;
			delete frag2;
			delete translate1;
			delete translate2;
			delete translate3;
			delete translate4;
			delete translate5;
			delete translate6;
			delete target1;
			delete target2;
			delete mesh;
			delete quad;
			delete obj1;
			delete obj2;
			delete obj3;
			delete obj4;
			delete obj5;
			delete obj6;
			delete objquad;
			delete pass1;
			delete pass2;
			delete effect;
		}

	};

	void testEffect(const string &logname) {
		XUnit unit(new FileLogger(logname));
		try {
			Logger::addLogger("testeffect",new ListLogger());
			Logger::setDefaultLogger("testeffect");
			ListLogger &logger = dynamic_cast<ListLogger &>(Logger::get());

			QDialog dialog;
			QGridLayout *dialog_l = new QGridLayout();
			dialog.setLayout(dialog_l);
			SXWidget *widget = new SXWidget();
			dialog_l->addWidget(widget);
			widget->renderPeriodically(0.01f);
			widget->setMinimumSize(600,400);
			QObject::connect(widget, SIGNAL(finishedRendering()), &dialog, SLOT(close()));
			widget->addRenderListener(*new TestEffect1(unit,logger));
			dialog.show();

			unit.addAnnotation("execute dialog - ");
			unit.assertEquals(dialog.exec(),0);

			unit.addMarkup("image validation");
			unit.assertInputYes("pink unicorn with wings?");
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}
	}

}

#endif
