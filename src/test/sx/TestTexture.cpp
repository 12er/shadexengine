#ifndef _TEST_SX_TESTTEXTURE_CPP_
#define _TEST_SX_TESTTEXTURE_CPP_

/**
 * texture test cases
 * (c) 2013 by Tristan Bauer
 */
#include <test/SXEngine.h>
#include <sx/XUnit.h>
#include <sx/Log4SX.h>
#include <sx/SXCore.h>
#include <sx/SX.h>
#include <sx/SXWidget.h>
#include <QApplication>
#include <QDialog>
#include <QGridLayout>
#include <vector>
#include <sstream>
#include <cmath>
using namespace sx;
using namespace std;

namespace sxengine {

	class TestTexture1: public SXRenderListener {
	private:

		XUnit *unit;
		ListLogger *logger;
	public:

		TestTexture1(XUnit &unit, ListLogger &logger) {
			this->unit = &unit;
			this->logger = &logger;
		}

		void create(SXRenderArea &area) {
			unit->addMarkup("save some textures");
			
			Texture *tex1 = new Texture("tex");
			tex1->setPath("data/test/sxparser/files/img1.jpg");
			tex1->load();
			unit->addAnnotation("tex1 loaded - ");
			unit->assertTrue(tex1->isLoaded());
			unit->addAnnotation("tex1 pixelformat - ");
			unit->assertEquals((int)tex1->getPixelFormat(),(int)BYTE_RGBA);

			RenderTarget *target1 = new RenderTarget("t");
			target1->setWidth(tex1->getWidth());
			target1->setHeight(tex1->getHeight());
			target1->load();
			unit->addAnnotation("target1 loaded - ");
			unit->assertTrue(target1->isLoaded());

			Shader *shader1 = new Shader("shader");
			shader1->addShader(
				"#version 400 core\n"
				"in vec4 vertices;\n"
				"in vec2 texcoords;\n"
				"out vec2 vtexcoords;\n"
				"void main() {\n"
				"	gl_Position = vertices;\n"
				"	vtexcoords = texcoords;\n"
				"}\n"
				,VERTEX);
			shader1->addShader(
				"#version 400 core\n"
				"in vec2 vtexcoords;\n"
				"out vec4 targ;\n"
				"uniform sampler2D tex;\n"
				"void main() {\n"
				"	targ = texture(tex,vtexcoords).arbg*0.314159;\n"
				"}\n"
				,FRAGMENT);
			shader1->load();
			unit->addAnnotation("load shader1 - ");
			unit->assertTrue(shader1->isLoaded());

			Texture *tex2 = new Texture("targ");
			tex2->setWidth(tex1->getWidth());
			tex2->setHeight(tex1->getHeight());
			tex2->setPixelFormat(FLOAT16_RGBA);
			tex2->load();
			unit->addAnnotation("load tex2 - ");
			unit->assertTrue(tex2->isLoaded());

			vector<Texture *> tVec;
			tVec.push_back(tex2);
			Pass *pass1 = new Pass("pass");
			pass1->setShader(shader1);
			pass1->addUniform(*tex1);
			pass1->setOutput(*target1,tVec);
			pass1->useBackgroundQuad(true);

			pass1->render();

			Texture *tex3 = new Texture("tex3");

			vector<float> texBuffer;
			float pi4 = ((float)Pi)/4.0f;
			float e4 = 2.71828182845f/4.0f;
			float g4 = 1.6180339887f/4.0f;
			//pixel 0
			texBuffer.push_back(pi4);
			texBuffer.push_back(e4);
			texBuffer.push_back(e4);
			texBuffer.push_back(pi4);
			//pixel 1
			texBuffer.push_back(g4);
			texBuffer.push_back(e4);
			texBuffer.push_back(g4);
			texBuffer.push_back(pi4);
			//pixel 2
			texBuffer.push_back(e4);
			texBuffer.push_back(g4);
			texBuffer.push_back(pi4);
			texBuffer.push_back(pi4);
			//pixel 3
			texBuffer.push_back(e4);
			texBuffer.push_back(e4);
			texBuffer.push_back(pi4);
			texBuffer.push_back(g4);

			tex3->setWidth(2);
			tex3->setHeight(2);
			tex3->setPixelFormat(FLOAT_RGBA);
			tex3->setFloatBuffer(texBuffer);
			tex3->load();
			unit->addAnnotation("load tex3 - ");
			unit->assertTrue(tex3->isLoaded());

			unit->addAnnotation("save tex1 - ");
			try {
				tex1->save("data/test/sxengine/files/output/write1.bmp");
				unit->assertTrue(true);
			} catch(Exception &) {
				unit->assertTrue(false);
			}

			unit->addAnnotation("save tex2 - ");
			try {
				tex2->save("data/test/sxengine/files/output/write2.png");
				unit->assertTrue(true);
				tex2->save("data/test/sxengine/files/output/write2.bmp");
				unit->assertTrue(true);
			} catch(Exception &) {
				unit->assertTrue(false);
			}

			unit->addAnnotation("save tex3 - ");
			try {
				tex3->save("data/test/sxengine/files/output/write3.png");
				unit->assertTrue(true);
			} catch(Exception &) {
				unit->assertTrue(false);
			}

			delete tex1;
			delete target1;
			delete shader1;
			delete tex2;
			delete pass1;
			delete tex3;
		}

		void reshape(SXRenderArea &area) {
		}

		void render(SXRenderArea &area) {
			area.stopRendering();
		}

		void stop(SXRenderArea &area) {
		}

	};

	class TestTexture2: public SXRenderListener {
	private:

		XUnit *unit;
		ListLogger *logger;
	public:

		TestTexture2(XUnit &unit, ListLogger &logger) {
			this->unit = &unit;
			this->logger = &logger;
		}

		void create(SXRenderArea &area) {
			unit->addMarkup("load saved textures");

			unit->addAnnotation("load bitmaps - ");
			try {
				Bitmap m1("data/test/sxengine/files/output/write1.bmp");
				Bitmap m2("data/test/sxengine/files/output/write2.bmp");
				Bitmap m3("data/test/sxengine/files/output/write2.png");
				Bitmap m4("data/test/sxengine/files/output/write3.png");
				unit->assertTrue(true);

				unit->addAnnotation("bytes per pixel bitmap 1 - ");
				unit->assertEquals((unsigned int)m1.getPixelSize(), (unsigned int)4);
				unit->addAnnotation("bytes per pixel bitmap 1 - ");
				unit->assertEquals((unsigned int)m2.getPixelSize(), (unsigned int)4);
				unit->addAnnotation("bytes per pixel bitmap 1 - ");
				unit->assertEquals((unsigned int)m3.getPixelSize(), (unsigned int)16);
				unit->addAnnotation("bytes per pixel bitmap 1 - ");
				unit->assertEquals((unsigned int)m4.getPixelSize(), (unsigned int)16);
				unit->addAnnotation("width and height bitmap1 - ");
				unit->assertEquals((unsigned int)m1.getWidth(), (unsigned int)3);
				unit->assertEquals((unsigned int)m1.getHeight(), (unsigned int)3);
				unit->addAnnotation("width and height bitmap2 - ");
				unit->assertEquals((unsigned int)m2.getWidth(), (unsigned int)3);
				unit->assertEquals((unsigned int)m2.getHeight(), (unsigned int)3);
				unit->addAnnotation("width and height bitmap3 - ");
				unit->assertEquals((unsigned int)m3.getWidth(), (unsigned int)3);
				unit->assertEquals((unsigned int)m3.getHeight(), (unsigned int)3);
				unit->addAnnotation("width and height bitmap4 - ");
				unit->assertEquals((unsigned int)m4.getWidth(), (unsigned int)2);
				unit->assertEquals((unsigned int)m4.getHeight(), (unsigned int)2);

				unsigned char *data1 = m1.getData();
				unsigned char *data2 = m2.getData();
				float *data3 = (float *)m3.getData();
				float *data4 = (float *)m4.getData();

				unsigned char d1[] = {
					0, 0, 254, 255,		255, 255, 0, 255,		254, 0, 0, 255,
					255, 0, 254, 255,		255, 255, 0, 255,		0, 255, 255, 255,
					254, 0, 0, 255,		0, 255, 1, 255,		0, 0, 254, 255
				};

				unsigned char d2[] = {
					80, 0, 80, 255,		80, 80, 0, 255,		80, 80, 0, 255,
					80, 80, 80, 255,		80, 80, 0, 255,		80, 0, 80, 255,
					80, 80, 0, 255,		80, 0, 0, 255,		80, 0, 80, 255
				};
				
				float d3[] = {
					0.31415f, 0, 0.3129f, 0,  		0.31415f, 0.31415f, 0, 0.31415f, 	0.3415f, 0.3129f, 0, 0, 
					0.31415f, 0.31415f, 0.3129f, 0,  	0.31415f, 0.31415f, 0, 0.31415f, 	0.31415f, 0, 0.31415f, 0.31415f,
					0.31415f, 0.3129f, 0, 0, 		0.31415f, 0, 0.001231f, 0.31415f,  	0.31415f, 0, 0.3129f, 0
				};

				float pi4 = ((float)Pi)/4.0f;
				float e4 = 2.71828182845f/4.0f;
				float g4 = 1.6180339887f/4.0f;
				float d4[] = {
					pi4,e4,e4,pi4,
					g4,e4,g4,pi4,
					e4,g4,pi4,pi4,
					e4,e4,pi4,g4
				};

				for(unsigned int i=0 ; i<3*3*4 ; i++) {
					stringstream astr;
					astr << "compare bitmap 1 - color " << i << " - ";
					unit->addAnnotation(astr.str());
					unit->assertEquals((float)data1[i],(float)d1[i],20.0f);
				}

				for(unsigned int i=0 ; i<3*3*4 ; i++) {
					stringstream astr;
					astr << "compare bitmap 2 - color " << i << " - ";
					unit->addAnnotation(astr.str());
					unit->assertEquals((float)data2[i],(float)d2[i],5.0f);
				}

				for(unsigned int i=0 ; i<3*3*4 ; i++) {
					stringstream astr;
					astr << "compare bitmap 3 - color " << i << " - ";
					unit->addAnnotation(astr.str());
					unit->assertEquals(data3[i],d3[i],0.03f);
				}

				for(unsigned int i=0 ; i<2*2*4 ; i++) {
					stringstream astr;
					astr << "compare bitmap 4 - color " << i << " - ";
					unit->addAnnotation(astr.str());
					unit->assertEquals(data4[i],d4[i],0.00001f);
				}
			} catch(Exception &) {
				unit->assertTrue(false);
			}

			Texture *tex1 = new Texture("tex");
			tex1->setPath("data/test/sxengine/files/output/write1.bmp");
			tex1->load();
			unit->addAnnotation("load tex1 - ");
			unit->assertTrue(tex1->isLoaded());
			unit->addAnnotation("tex1 pixelformat - ");
			unit->assertEquals((int)tex1->getPixelFormat(),(int)BYTE_RGBA);
			unit->addAnnotation("tex1 width - ");
			unit->assertEquals(tex1->getWidth(),(int)3);
			unit->addAnnotation("tex1 height - ");
			unit->assertEquals(tex1->getHeight(),(int)3);

			Texture *tex2 = new Texture("tex");
			tex2->setPath("data/test/sxengine/files/output/write2.bmp");
			tex2->load();
			unit->addAnnotation("load tex2 - ");
			unit->assertTrue(tex2->isLoaded());
			unit->addAnnotation("tex2 pixelformat - ");
			unit->assertEquals((int)tex2->getPixelFormat(),(int)BYTE_RGBA);
			unit->addAnnotation("tex2 width - ");
			unit->assertEquals(tex2->getWidth(),(int)3);
			unit->addAnnotation("tex2 height - ");
			unit->assertEquals(tex2->getHeight(),(int)3);

			Texture *tex3 = new Texture("tex");
			tex3->setPath("data/test/sxengine/files/output/write2.png");
			tex3->load();
			unit->addAnnotation("load tex3 - ");
			unit->assertTrue(tex3->isLoaded());
			unit->addAnnotation("tex3 pixelformat - ");
			unit->assertEquals((int)tex3->getPixelFormat(),(int)FLOAT16_RGBA);
			unit->addAnnotation("tex3 width - ");
			unit->assertEquals(tex3->getWidth(),(int)3);
			unit->addAnnotation("tex3 height - ");
			unit->assertEquals(tex3->getHeight(),(int)3);

			Texture *tex4 = new Texture("tex");
			tex4->setPath("data/test/sxengine/files/output/write3.png");
			tex4->load();
			unit->addAnnotation("load tex4 - ");
			unit->assertTrue(tex4->isLoaded());
			unit->addAnnotation("tex4 pixelformat - ");
			unit->assertEquals((int)tex4->getPixelFormat(),(int)FLOAT16_RGBA);
			unit->addAnnotation("tex4 width - ");
			unit->assertEquals(tex4->getWidth(),(int)2);
			unit->addAnnotation("tex4 height - ");
			unit->assertEquals(tex4->getHeight(),(int)2);

			unit->addAnnotation("save tex4 - ");
			try {
				tex4->save("data/test/sxengine/files/output/write4.png");
				unit->assertTrue(true);
			} catch(Exception &) {
				unit->assertTrue(false);
			}

			delete tex1;
			delete tex2;
			delete tex3;
			delete tex4;

			unit->addAnnotation("load bitmap5 - ");
			try {
				float pi4 = ((float)Pi)/4.0f;
				float e4 = 2.71828182845f/4.0f;
				float g4 = 1.6180339887f/4.0f;
				float d5[] = {
					pi4,e4,e4,pi4,
					g4,e4,g4,pi4,
					e4,g4,pi4,pi4,
					e4,e4,pi4,g4
				};

				Bitmap m5("data/test/sxengine/files/output/write4.png");
				unit->assertTrue(true);

				float *data5 = (float *)m5.getData();
				for(unsigned int i=0 ; i<2*2*4 ; i++) {
					stringstream astr;
					astr << "compare bitmap 5 - color " << i << " - ";
					unit->addAnnotation(astr.str());
					unit->assertEquals(data5[i],d5[i],0.0003f);
				}
			} catch(Exception &) {
				unit->assertTrue(false);
			}
		}

		void reshape(SXRenderArea &area) {
		}

		void render(SXRenderArea &area) {
			area.stopRendering();
		}

		void stop(SXRenderArea &area) {
		}

	};

	void testTexture(const string &logname) {
		XUnit unit(new FileLogger(logname));
		try {
			Logger::addLogger("testtexture",new ListLogger());
			Logger::setDefaultLogger("testtexture");
			ListLogger &logger = dynamic_cast<ListLogger &>(Logger::get());

			QDialog dialog1;
			QGridLayout *dialog1_l = new QGridLayout();
			dialog1.setLayout(dialog1_l);
			SXWidget *widget1 = new SXWidget();
			dialog1_l->addWidget(widget1);
			widget1->renderPeriodically(0.01f);
			widget1->setMinimumSize(600,400);
			QObject::connect(widget1, SIGNAL(finishedRendering()), &dialog1, SLOT(close()));
			widget1->addRenderListener(*new TestTexture1(unit,logger));
			dialog1.show();

			unit.addAnnotation("execute dialog 1 - ");
			unit.assertEquals(dialog1.exec(),0);

			QDialog dialog2;
			QGridLayout *dialog2_l = new QGridLayout();
			dialog2.setLayout(dialog2_l);
			SXWidget *widget2 = new SXWidget();
			dialog2_l->addWidget(widget2);
			widget2->renderPeriodically(0.01f);
			widget2->setMinimumSize(600,400);
			QObject::connect(widget2, SIGNAL(finishedRendering()), &dialog2, SLOT(close()));
			widget2->addRenderListener(*new TestTexture2(unit,logger));
			dialog2.show();

			unit.addAnnotation("execute dialog 2 - ");
			unit.assertEquals(dialog2.exec(),0);

		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}
	}

}

#endif
