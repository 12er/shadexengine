#ifndef _TEST_SX_TESTSHADER_CPP_
#define _TEST_SX_TESTSHADER_CPP_

/**
 * Shader test cases
 * (c) 2012 by Tristan Bauer
 */
#include <test/SXEngine.h>
#include <sx/Log4SX.h>
#include <sx/XUnit.h>
#include <sx/SXCore.h>
#include <sx/SXCore.h>
#include <sx/SX.h>
#include <sx/SXWidget.h>
#include <QApplication>
#include <QDialog>
#include <QGridLayout>
#include <sstream>
using namespace std;
using namespace sx;

namespace sxengine {

	/**
	 * concatenates all strings in the log of the markup
	 */
	string testShaderAConcat(ListLogger &logger, const string &markup) {
		stringstream msg;
		map<string,ListMarkup>::iterator iter = logger.getMarkups().find(markup);
		if(iter != logger.getMarkups().end()) {
			for(vector<Annotation>::iterator iter2 = (*iter).second.getAnnotations().begin() ; iter2 != (*iter).second.getAnnotations().end() ; iter2++) {
				msg << (*iter2).getAnnotation();
			}
		}
		return msg.str();
	}

	/**
	 * concatenates all strings in the log of the markup
	 */
	string testShaderStrConcat(ListLogger &logger, const string &markup) {
		stringstream msg;
		map<string,ListMarkup>::iterator iter = logger.getMarkups().find(markup);
		if(iter != logger.getMarkups().end()) {
			for(vector<string>::iterator iter2 = (*iter).second.getStrings().begin() ; iter2 != (*iter).second.getStrings().end() ; iter2++) {
				msg << (*iter2);
			}
		}
		return msg.str();
	}

	class TestShader1: public SXRenderListener {
	private:
		int width;
		int height;
		TestShader1(const TestShader1 &);
		TestShader1 &operator = (const TestShader1 &);
		XUnit *unit;
		ListLogger *list;
	public:
		TestShader1(XUnit &unit, ListLogger &list) {
			this->unit = &unit;
			this->list = &list;
		}

		void create(SXRenderArea &area) {
			unit->addMarkup("test loading shader programs");
			
			list->getMarkups().clear();
			Shader *shader = new Shader("s1");
			shader->addShaderFile("not",VERTEX);
			shader->addShaderFile("existing",FRAGMENT);
			shader->load();
			unit->addAnnotation("compile nonexistent programfiles - ");
			unit->addAnnotation(testShaderAConcat(*list,"Shader::load"));
			unit->addAnnotation(testShaderStrConcat(*list,"Shader::load"));
			unit->assertTrue(!shader->isLoaded());
			delete shader;

			list->getMarkups().clear();
			shader = new Shader("s1");
			shader->addShaderFile("data/test/sxengine/files/error1.vp",VERTEX);
			shader->addShaderFile("data/test/sxengine/files/error1.fp",FRAGMENT);
			shader->load();
			unit->addAnnotation("compile a program with syntax errors - ");
			unit->addAnnotation(testShaderAConcat(*list,"Shader::load"));
			unit->addAnnotation(testShaderStrConcat(*list,"Shader::load"));
			unit->assertTrue(!shader->isLoaded());
			delete shader;

			list->getMarkups().clear();
			shader = new Shader("s1");
			shader->addShaderFile("data/test/sxengine/files/shader1.vp",VERTEX);
			shader->addShaderFile("data/test/sxengine/files/shader1.fp",FRAGMENT);
			shader->load();
			unit->addAnnotation("compile a minimal program - ");
			unit->addAnnotation(testShaderAConcat(*list,"Shader::load"));
			unit->addAnnotation(testShaderStrConcat(*list,"Shader::load"));
			unit->assertTrue(shader->isLoaded());
			delete shader;

			list->getMarkups().clear();
			shader = new Shader("s1");
			shader->addShaderFile("data/test/sxengine/files/shader2.vp",VERTEX);
			shader->addShaderFile("data/test/sxengine/files/shader2.tcp",TESSELLATION_CONTROL);
			shader->addShaderFile("data/test/sxengine/files/shader2.tep",TESSELLATION_EVALUATION);
			shader->addShaderFile("data/test/sxengine/files/shader2.gp",GEOMETRY);
			shader->addShaderFile("data/test/sxengine/files/shader2.fp",FRAGMENT);
			shader->load();
			unit->addAnnotation("compile a correct program with all variants of shaders - ");
			unit->addAnnotation(testShaderAConcat(*list,"Shader::load"));
			unit->addAnnotation(testShaderStrConcat(*list,"Shader::load"));
			unit->assertTrue(shader->isLoaded());
			delete shader;

			list->getMarkups().clear();
			shader = new Shader("s1");
			shader->addShaderFile("data/test/sxengine/files/shader3.vp",VERTEX);
			shader->addShaderFile("data/test/sxengine/files/shader3.tcp",TESSELLATION_CONTROL);
			shader->addShaderFile("data/test/sxengine/files/shader3.tep",TESSELLATION_EVALUATION);
			shader->addShaderFile("data/test/sxengine/files/shader3.gp",GEOMETRY);
			shader->addShaderFile("data/test/sxengine/files/shader3.fp",FRAGMENT);
			shader->load();
			unit->addAnnotation("compile a correct program with warnings - ");
			unit->addAnnotation(testShaderAConcat(*list,"Shader::load"));
			unit->addAnnotation(testShaderStrConcat(*list,"Shader::load"));
			unit->assertTrue(shader->isLoaded());
			delete shader;

			list->getMarkups().clear();
			shader = new Shader("s1");
			shader->addShaderFile("data/test/sxengine/files/shader1.vp",VERTEX);
			shader->addShader(
				"#version 400 core\n"
				"out vec4 frag;\n"
				"void main() {\n"
				"	frag = vec4(1);\n"
				"}\n"
				,FRAGMENT);
			shader->load();
			unit->addAnnotation("load correct program from string - ");
			unit->addAnnotation(testShaderAConcat(*list,"Shader::load"));
			unit->addAnnotation(testShaderStrConcat(*list,"Shader::load"));
			unit->assertTrue(shader->isLoaded());
			delete shader;

			list->getMarkups().clear();
			shader = new Shader("s1");
			shader->addShaderFile("data/test/sxengine/files/shader1.vp",VERTEX);
			shader->addShader(
				"#version 400 core\n"
				"out vec4 frag;\n"
				"void main() {\n"
				"	frag = vec4(1);\n"
				"\n"
				,FRAGMENT);
			shader->load();
			unit->addAnnotation("load incorrect program from string - ");
			unit->addAnnotation(testShaderAConcat(*list,"Shader::load"));
			unit->addAnnotation(testShaderStrConcat(*list,"Shader::load"));
			unit->assertTrue(!shader->isLoaded());
			delete shader;


			unit->addMarkup("test reloading shaders");

			shader = new Shader("s1");
			shader->addShaderFile("data/test/sxengine/files/shader2.vp",VERTEX);
			shader->addShaderFile("data/test/sxengine/files/shader2.tcp",TESSELLATION_CONTROL);
			shader->addShaderFile("data/test/sxengine/files/shader2.tep",TESSELLATION_EVALUATION);
			shader->addShaderFile("data/test/sxengine/files/shader2.gp",GEOMETRY);
			shader->addShaderFile("data/test/sxengine/files/shader2.fp",FRAGMENT);
			shader->load();
			unit->addAnnotation("successfully loaded the first time - ");
			unit->assertTrue(shader->isLoaded());
			shader->addShaderFile("data/test/sxengine/files/shader1.vp",VERTEX);
			shader->addShaderFile("data/test/sxengine/files/shader1.fp",FRAGMENT);
			list->getMarkups().clear();
			shader->load();
			map<string,ListMarkup>::iterator unloadShader = list->getMarkups().find("Shader::load/unload shader");
			map<string,ListMarkup>::iterator unloadProgram = list->getMarkups().find("Shader::load/unload program");
			if(unloadShader == list->getMarkups().end() || unloadProgram == list->getMarkups().end()) {
				unit->addAnnotation("shaders not deleted - ");
				unit->assertTrue(false);
			} else {
				unit->addAnnotation("test, if all shaders were deleted - ");
				unit->assertEquals((unsigned int)(*unloadShader).second.getStrings().size(), (unsigned int)5);
				unit->addAnnotation("test, if program was deleted - ");
				unit->assertEquals((unsigned int)(*unloadProgram).second.getStrings().size(), (unsigned int)1);
			}
			unit->addAnnotation("successfully loaded the second time - ");
			unit->assertTrue(shader->isLoaded());
			shader->addShaderFile("data/test/sxengine/files/error1.vp",VERTEX);
			shader->addShaderFile("data/test/sxengine/files/error1.fp",FRAGMENT);
			list->getMarkups().clear();
			shader->load();
			unloadShader = list->getMarkups().find("Shader::load/unload shader");
			unloadProgram = list->getMarkups().find("Shader::load/unload program");
			if(unloadShader == list->getMarkups().end() || unloadProgram == list->getMarkups().end()) {
				unit->addAnnotation("shaders not deleted - ");
				unit->assertTrue(false);
			} else {
				unit->addAnnotation("test, if all shaders were deleted - ");
				unit->assertEquals((unsigned int)(*unloadShader).second.getStrings().size(), (unsigned int)2);
				unit->addAnnotation("test, if program was deleted - ");
				unit->assertEquals((unsigned int)(*unloadProgram).second.getStrings().size(), (unsigned int)1);
			}
			unit->addAnnotation("failed loading the third time - ");
			unit->assertTrue(!shader->isLoaded());
			shader->addShaderFile("data/test/sxengine/files/shader2.vp",VERTEX);
			shader->addShaderFile("data/test/sxengine/files/shader2.tcp",TESSELLATION_CONTROL);
			shader->addShaderFile("data/test/sxengine/files/shader2.tep",TESSELLATION_EVALUATION);
			shader->addShaderFile("data/test/sxengine/files/shader2.gp",GEOMETRY);
			shader->addShaderFile("data/test/sxengine/files/shader2.fp",FRAGMENT);
			list->getMarkups().clear();
			shader->load();
			unloadShader = list->getMarkups().find("Shader::load/unload shader");
			unloadProgram = list->getMarkups().find("Shader::load/unload program");
			if(unloadShader == list->getMarkups().end() && unloadProgram == list->getMarkups().end()) {
				unit->addAnnotation("shaders not deleted - ");
				unit->assertTrue(true);
			} else {
				unit->addAnnotation("some shaders were deleted - ");
				unit->assertTrue(false);
			}
			unit->addAnnotation("successfully loaded the fourth time - ");
			unit->assertTrue(shader->isLoaded());
			delete shader;

			
			unit->addMarkup("test distributing texture slots");

			shader = new Shader("some");
			shader->createTextureSlot();
			shader->createTextureSlot();
			shader->createTextureSlot();
			unit->addAnnotation("four texture slots generated - ");
			unit->assertEquals((unsigned int)shader->createTextureSlot(), (unsigned int)3);
			shader->freeTextureSlots();
			shader->createTextureSlot();
			shader->createTextureSlot();
			unit->addAnnotation("three texture slots generated - ");
			unit->assertEquals((unsigned int)shader->createTextureSlot(), (unsigned int)2);
			shader->lockTextureSlots();
			shader->createTextureSlot();
			shader->createTextureSlot();
			unit->addAnnotation("locked one time - ");
			unit->assertEquals((unsigned int)shader->createTextureSlot(), (unsigned int)5);
			shader->freeTextureSlots();
			unit->addAnnotation("free unlocked texture slots - ");
			unit->assertEquals((unsigned int)shader->createTextureSlot(), (unsigned int)3);
			shader->unlockTextureSlots();
			shader->freeTextureSlots();
			unit->addAnnotation("unlock all texture slots - ");
			unit->assertEquals((unsigned int)shader->createTextureSlot(), (unsigned int)0);
			shader->freeTextureSlots();
			unit->addAnnotation("unlock all texture slots - ");
			unit->assertEquals((unsigned int)shader->createTextureSlot(), (unsigned int)0);
			shader->createTextureSlot();
			shader->createTextureSlot();
			shader->lockTextureSlots();//3
			shader->createTextureSlot();
			shader->createTextureSlot();
			shader->createTextureSlot();
			shader->createTextureSlot();
			shader->lockTextureSlots();//7
			shader->createTextureSlot();
			shader->createTextureSlot();
			shader->createTextureSlot();
			shader->createTextureSlot();
			shader->createTextureSlot();
			shader->lockTextureSlots();//12
			shader->createTextureSlot();
			shader->createTextureSlot();
			shader->createTextureSlot();
			shader->lockTextureSlots();//15
			unit->addAnnotation("locked four times - ");
			unit->assertEquals((unsigned int)shader->createTextureSlot(), (unsigned int)15);
			shader->unlockTextureSlots();
			unit->addAnnotation("unlocked slots - locked three times - freed no slots - ");
			unit->assertEquals((unsigned int)shader->createTextureSlot(), (unsigned int)16);
			shader->freeTextureSlots();
			unit->addAnnotation("unlocked slots - locked three times - ");
			unit->assertEquals((unsigned int)shader->createTextureSlot(), (unsigned int)12);
			shader->unlockTextureSlots();
			shader->freeTextureSlots();
			unit->addAnnotation("unlocked slots - locked two times - ");
			unit->assertEquals((unsigned int)shader->createTextureSlot(), (unsigned int)7);
			shader->unlockTextureSlots();
			shader->freeTextureSlots();
			unit->addAnnotation("unlocked slots - locked one time - ");
			unit->assertEquals((unsigned int)shader->createTextureSlot(), (unsigned int)3);
			shader->unlockTextureSlots();
			shader->freeTextureSlots();
			unit->addAnnotation("unlocked all slots - ");
			unit->assertEquals((unsigned int)shader->createTextureSlot(), (unsigned int)0);
			shader->unlockTextureSlots();
			shader->freeTextureSlots();
			unit->addAnnotation("unlocked all slots again - ");
			unit->assertEquals((unsigned int)shader->createTextureSlot(), (unsigned int)0);

			delete shader;

			area.stopRendering();
		}

		void reshape(SXRenderArea &area) {
		}

		void render(SXRenderArea &area) {
		}

		void stop(SXRenderArea &area) {
		}

	};

	void testShader(const string &log) {
		XUnit unit(new FileLogger(log));
		try {
			Logger::addLogger("testshader",new ListLogger());
			Logger::setDefaultLogger("testshader");
			ListLogger &logger = dynamic_cast<ListLogger &>(Logger::get());

			QDialog dialog;
			QGridLayout *dialog_l = new QGridLayout();
			dialog.setLayout(dialog_l);
			SXWidget *widget = new SXWidget();
			dialog_l->addWidget(widget);
			QObject::connect(widget,SIGNAL(finishedRendering()),&dialog,SLOT(close()));
			widget->setMinimumSize(1,1);
			widget->addRenderListener(*new TestShader1(unit,logger));
			dialog.show();

			unit.addAnnotation("execute dialog - ");
			unit.assertEquals(dialog.exec(),0);
		} catch(Exception &e) {
			unit.addAnnotation(e.getMessage());
			unit.assertTrue(false);
		}
	}

}

#endif
