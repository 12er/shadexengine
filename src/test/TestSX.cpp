#ifndef _TEST_TESTSX_CPP_
#define _TEST_TESTSX_CPP_

/**
 * entry point for test cases
 * (c) 2012 by Tristan Bauer
 */

#include <test/SXParser.h>
#include <test/SXEngine.h>
#include <test/SXWidget.h>
#include <QApplication>

#define SX_testReadFile() sxparser::testReadFile("data/test/sxparser/ReadFile.html");
#define SX_testParseNumbers() sxparser::testParseNumbers("data/test/sxparser/ParseNumbers.html");
#define SX_testParseStrings() sxparser::testParseStrings("data/test/sxparser/ParseStrings.html");
#define SX_testNodes() sxparser::testNodes("data/test/sxparser/Nodes.html");
#define SX_testParseSXdata() sxparser::testParseSXdata("data/test/sxparser/ParseSXdata.html");
#define SX_testParseXMLdata() sxparser::testParseXMLdata("data/test/sxparser/ParseXMLdata.html");
#define SX_testParsePLYdata() sxparser::testParsePLYdata("data/test/sxparser/ParsePLYdata.html");
#define SX_testParseColladaData() sxparser::testParseColladaData("data/test/sxparser/ParseColladaData.html")
#define SX_testParseColladaSkeleton() sxparser::testParseColladaSkeleton("data/test/sxparser/ParseColladaSkeleton.html");
#define SX_testBitmap() sxparser::testBitmap("data/test/sxparser/Bitmap.html");
#define SX_testMath() sxengine::testMath("data/test/sxengine/Math.html");
#define SX_testShader() sxengine::testShader("data/test/sxengine/Shader.html");
#define SX_testBufferedMesh() sxengine::testBufferedMesh("data/test/sxengine/BufferedMesh.html");
#define SX_testTexture() sxengine::testTexture("data/test/sxengine/Texture.html");
#define SX_testUniforms() sxengine::testUniforms("data/test/sxengine/Uniforms.html");
#define SX_testTransformFeedback() sxengine::testTransformFeedback("data/test/sxengine/TransformFeedback.html");
#define SX_testInstancing() sxengine::testInstancing("data/test/sxengine/Instancing.html");
#define SX_testRenderTarget() sxengine::testRenderTarget("data/test/sxengine/RenderTarget.html");
#define SX_testVolume() sxengine::testVolume("data/test/sxengine/Volume.html");
#define SX_testRenderObject() sxengine::testRenderObject("data/test/sxengine/RenderObject.html");
#define SX_testPass() sxengine::testPass("data/test/sxengine/Pass.html");
#define SX_testEffect() sxengine::testEffect("data/test/sxengine/Effect.html");
#define SX_testShadeX() sxengine::testShadeX("data/test/sxengine/ShadeX.html");
#define SX_testSXWidget(app) sxwidget::testSXWidget("data/test/sxwidget/SXWidget.html", app);

/**
 * entry point of SXParser testcases
 */
int main(int argc, char **argv) {
	QApplication app(argc, argv);

	SX_testReadFile();
	SX_testParseNumbers();
	SX_testParseStrings()
	SX_testNodes();
	SX_testParseSXdata();
	SX_testParseXMLdata();
	SX_testParsePLYdata();
	SX_testParseColladaData();
	SX_testParseColladaSkeleton();
	SX_testBitmap();
	SX_testMath();
	SX_testShader();
	SX_testBufferedMesh();
	SX_testTexture();
	SX_testUniforms();
	SX_testTransformFeedback();
	SX_testInstancing();
	SX_testRenderTarget();
	SX_testVolume();
	SX_testRenderObject();
	SX_testPass();
	SX_testEffect();
	SX_testShadeX();
	SX_testSXWidget(app);

	return 0;
}

#endif
