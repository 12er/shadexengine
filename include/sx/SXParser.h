#ifndef _SX_SXPARSER_H_
#define _SX_SXPARSER_H_

/**
 * SX parser framework
 * (c) 2012 by Tristan Bauer
 */

#include <export/Export.h>
#include <string>
#include <map>
#include <vector>
#include <sx/SXMath.h>
using namespace std;

namespace sx {

	namespace parserInternal {

		void parseStrings(const string &stringList, vector<string> &strings);

	}

	/**
	 * Parses a list of double values separated by space, tab and newline character sequences.
	 * If the string doesn't have the format (<double>)*, an exception is thrown.
	 *
	 * @param numberList string containing a sequence of numbers separated by empty space, tabs and newlines
	 * @return vector of equivalent numbers listed in the same order as in parameter numberList
	 * @exception in the case of an error, an exception Exception with type EX_SYNTAX is thrown
	 */
	EXPA vector<double> parseNumbers(const string &numberList);

	/**
	 * Parses a list of strings separated by space, tab and newline character sequences.
	 * 
	 * @param stringList string containing a sequence of strings separated by empty space, tabs and newlines
	 * @return vector of equivalent strings listed in the same order as in parameter stringList
	 * @exception in the case of an error, an exception Exception with typ EX_SYNTAX is thrown
	 */
	EXPA vector<string> parseStrings(const string &stringList);

	/**
	 * Reads file at location filename,
	 * and returns the content as a string.
	 * The file must be a text file. If the
	 * file can't be read, an exception is thrown.
	 *
	 * @param filename a valid path to a file
	 * @return content of the textfile
	 * @exception in the case of an error, an exception Exception with type EX_IO is thrown
	 */
	EXPA string readFile(string filename);

	/**
	 * Tokenizes the string str delimited by delimiter.
	 * If no delimiter is specified, '.' is used as a delimiter.
	 *
	 * @param str string to be tokenized
	 * @param delimiter delimiter, having default value '.'
	 * @return tokenized string str tokenized by delimiter delimiter
	 */
	EXPA vector<string> tokenizeString(const string &str, const char *delimiter = ".");

	/**
	 * XNode is the basic type of
	 * the SX tag language
	 */
	struct XNode {
	private:

		/**
		 * copy constructor, not used
		 */
		XNode(const XNode &);

		/**
		 * assignment operator, not used
		 */
		XNode &operator = (const XNode &);

	protected:

		/**
		 * default constructor, only used
		 * subtypes of XNode
		 */
		EXPA XNode();

	public:

		/**
		 * deconstructor
		 */
		EXPA virtual ~XNode();

	};

	/**
	 * A node of the SX tag language
	 * holding some text
	 */
	struct XText: public XNode {
	private:

		/**
		 * copy constructor, not used
		 */
		XText(const XText &);

		/**
		 * assignment operator, not used
		 */
		XText &operator = (const XText &);

	public:

		/**
		 * text of the XText node
		 */
		string text;

		/**
		 * default constructor
		 */
		EXPA XText();

		/**
		 * deconstructor
		 */
		EXPA ~XText();

	};

	/**
	 * tag node of the SX tag language, having a name assigned
	 * , and holding texts, attributes, and trees
	 * of tags
	 */
	struct XTag: public XNode {
	private:

		/**
		 * copy constructor, not used
		 */
		XTag(const XTag &);

		/**
		 * assignment operator, not used
		 */
		XTag &operator = (const XTag &);

	public:

		/**
		 * name of the node
		 */
		string name;

		/**
		 * String attributes associated with
		 * string. The attributes
		 * only need to be unique over the set
		 * of all string attributes in the same
		 * tag.
		 */
		map<string,string> stringAttribs;

		/**
		 * Number attributes associated with
		 * numbers. Number attributes
		 * only need to be unique over the set
		 * of all number attributes.
		 */
		map<string,double> realAttribs;

		/**
		 * List of child nodes
		 */
		vector<XNode *> nodes;

		/**
		 * default constructor, initializing
		 * an empty node
		 */
		EXPA XTag();

		/**
		 * deconstructor, deconstructing every
		 * element attached to the tag (for instance
		 * as a (sub)child node, or a text node)
		 */
		EXPA ~XTag();

		/**
		 * Searches all nodes given by path location,
		 * and stores the outcome in the list nodes. 
		 *
		 * @param location Specification of a nodes location. Let be <n1>
		 * a child node of this, <n2> a childnode of <n1>, and so
		 * on, up to a node <nm>, such that the sequence forms a branch
		 * of a childtree of this, then the location of <nm> can be
		 * specified by a the sequence of names concatenated by points, like:
		 * <n1>.<n2>.<n3>. ... .<nm-2>.<nm-1>.<nm>
		 * If one of the names in the part equals the string '*', all nodes of that
		 * path are taken into consideration. Nodes always require to have a nonempty
		 * name, hence an empty location will lead to an empty outcome.
		 * 
		 * @param texts nodes specified by the string location are stored in vector nodes
		 */
		EXPA void getTags(string location, vector<XTag *> &nodes);

		/**
		 * Searches the first node from left given by path location,
		 * and returns it. 
		 *
		 * @param location Specification of a nodes location. Let be <n1>
		 * a child node of this, <n2> a childnode of <n1>, and so
		 * on, up to a node <nm>, such that the sequence forms a branch
		 * of a childtree of this, then the location of <nm> can be
		 * specified by a the sequence of names concatenated by points, like:
		 * <n1>.<n2>.<n3>. ... .<nm-2>.<nm-1>.<nm>
		 * If one of the names in the part equals the string '*', all nodes of that
		 * path are taken into consideration. Nodes always require to have a nonempty
		 * name, hence an empty location will lead to an empty outcome.
		 * 
		 * @return first node from left with path location
		 * @exception if no such first node exists, an exception Exception of type EX_NODATA is thrown
		 */
		EXPA XTag *getFirst(string location);

		/**
		 * Returns the first childnode.
		 * @return first childnode from left
		 * @exception if no such first node exists, an exception Exception of type EX_NODATA is thrown
		 */
		EXPA XTag *getFirst();

		/**
		 * Returns the value of the string attribute with identifyer ID.
		 *
		 * @param ID identifyer of the attribute
		 * @return value of the attribute
		 * @exception if no attribute with ID exists, an exception Exception of type EX_NODATA is thrown
		 */
		EXPA string getStrAttribute(string ID);

		/**
		 * Returns the value of the real attribute with identifyer ID.
		 *
		 * @param ID identifyer of the attribute
		 * @return value of the attribute
		 * @exception if no attribute with ID exists, an exception Exception of type EX_NODATA is thrown
		 */
		EXPA double getRealAttribute(string ID);

		/**
		 * Stores all texts of the direct textnode children
		 * the given order in texts.
		 *
		 * @param texts stores texts in this parameter
		 */
		EXPA void getDirectTexts(vector<string> &texts);

		/**
		 * Stores all texts of the text nodes in
		 * the given order in texts.
		 *
		 * @param texts stores texts in this parameter
		 */
		EXPA void getTexts(vector<string> &texts);

		/**
		 * Concatenates the text of all direct child nodes
		 * of type XText, and returns it.
		 *
		 * @return concatenation of all direct textnode children
		 */
		EXPA string getDirectTexts();

		/**
		 * Concatenates the text of all child nodes
		 * of type XText, and returns it.
		 *
		 * @return concatenation of all textnode children
		 */
		EXPA string getTexts();

		/**
		 * Collects all the text, textless leaf tags and attributes of the
		 * tree, and stores it in the node tag. Textless leaf tags with
		 * attributes are replaced by empty tags with the same names.
		 * The order of the texts and nodes is preserved.
		 * If attributes with the same identifyer appear multiple times
		 * in different nodes, their values are stored in the maps
		 * ambiguousReals and ambiguousStrings.
		 * Nodes of the tree are traversed from left to right, from the root
		 * to the leafs, and ambiguous attributes are owerwritten in the same
		 * order.
		 *
		 * @param tag A tag node, in which the outcome is stored
		 * 
		 * @param ambiguousReals If in different nodes real attributes with the same identifyer appear,
		 * the values are stored in a vector associated with the attribute name in ambiguousReals.
		 *
		 * @param ambiguousStrings If in different nodes string attributes with the same identifyer appear,
		 * the values are stored in a vector associated with the attribute name in ambiguousStrings.
		 */
		EXPA void flattenTag(XTag &tag, map<string,vector<double>> &ambiguousReals, map<string,vector<string>> &ambiguousStrings);

	};

	/**
	 * Parses a string in SX markup language, and returns
	 * the equivalent node. If the string is no instance of
	 * the SX markup language, or the function fails to parse
	 * for other reasons, an exception is thrown.
	 *
	 * @param data string in SX markup language
	 * @return root of the tree representing the data of the parameter data
	 * @exception an instance of Exception of type EX_SYNTAX is raised, if data can't be parsed
	 */
	EXPA XTag *parseSXdata(const string &data);

	/**
	 * Reads the file in SX markup language at location path, parses the content
	 * , and returns the equivalent node. If the content is no instance of
	 * the SX markup language, or the file can't be read, an exception is thrown.
	 *
	 * @param path path of the file, which should be parsed
	 *
	 * @return root of the tree representing the data of the file at location path
	 *
	 * @exception an instance of Exception is raised. It's of type EX_SYNTAX, if the file's content
	 * cannot be parsed, and it's of type EX_IO, if the file can't be read.
	 */
	EXPA XTag *parseSXFile(const string path);

	/**
	 * Parses a string in XML, and returns
	 * the equivalent node. If the string is no instance of
	 * XML, or the function fails to parse
	 * for other reasons, an exception is thrown.
	 *
	 * @param data string in XML
	 * @return root of the tree representing the data of the parameter data
	 * @exception an instance of Exception of type EX_SYNTAX is raised, if data can't be parsed
	 */
	EXPA XTag *parseXMLdata(const string &data);

	/**
	 * Reads the file in XML at location path, parses the content
	 * , and returns the equivalent node. If the content is no instance of
	 * XML, or the file can't be read, an exception is thrown.
	 *
	 * @param path path of the file, which should be parsed
	 *
	 * @return root of the tree representing the data of the file at location path
	 *
	 * @exception an instance of Exception is raised. It' of type EX_SYNTAX, if the file's content
	 * cannot be parsed, and it's of type EX_IO, if the file can't be read.
	 */
	EXPA XTag *parseXMLFile(const string path);

	/**
	 * a vertex attribute buffer
	 */
	struct XBuffer {
	private:

		/**
		 * copy constructor, not used
		 */
		XBuffer(const XBuffer &);

		/**
		 * assignment operator, not used
		 */
		XBuffer &operator = (const XBuffer &);

	public:

		/**
		 * identifyer of the buffer
		 */
		string name;

		/**
		 * specifies how many elements in
		 * sequence form a vertex attribute
		 */
		unsigned int attributeSize;

		/**
		 * elements of the buffer, attributeSize
		 * bufferelements in sequence form a vertex attribute
		 */
		vector<double> vertexAttributes;

		/**
		 * default constructor
		 */
		EXPA XBuffer();

		/**
		 * deconstructor
		 */
		EXPA ~XBuffer();
	};

	/**
	 * An XMesh is made of several vertex attribute buffers.
	 * Additionally the XMesh specifies, how many vertex attributes
	 * in sequence form a face.
	 */
	struct XMesh {
	private:

		/**
		 * copy constructor, not used
		 */
		XMesh(const XMesh &);

		/**
		 * assignment operator, not used
		 */
		XMesh &operator = (const XMesh &);

	public:

		/**
		 * Specifies the number of vertex attributes,
		 * which in sequence in a buffer form a face.
		 * Allowed values are 1 (points or line/polygon-strip),
		 * 2 (lines) or 3 (triangles).
		 */
		unsigned int faceSize;

		/**
		 * vertex attribute buffers
		 */
		map<string,XBuffer *> buffers;

		/**
		 * default constructor
		 */
		EXPA XMesh();

		/**
		 * deconstructor, deletes all buffers
		 */
		EXPA ~XMesh();
	};

	/**
	 * Parses a string in PLY format, and returns
	 * an equivalent mesh, if it contains properties like x,y,z,nx,ny,nz,s,t,u,r,g,b, and a property
	 * vertex_indices for the specification of the faces. All faces must be of the same type:
	 * one vertex per face, two vertex per face, or at least three vertices per face. If the faces are
	 * of the last type, every single one is transformed into triangles. If the string does not represent
	 * a mesh in PLY format with one of the above face types, or the function fails to parse
	 * for other reasons, an exception is thrown.
	 *
	 * @param data string in PLY, a mesh
	 * @return mesh representing the data of the parameter data
	 * @exception an instance of Exception of type EX_SYNTAX is raised, if data can't be parsed
	 */
	EXPA XMesh *parsePLYdata(const string &data);

	/**
	 * Parses a file in PLY format at location path, and returns
	 * an equivalent mesh, if it contains properties like x,y,z,nx,ny,nz,s,t,u,r,g,b, and a property
	 * vertex_indices for the specification of the faces. All faces must be of the same type:
	 * one vertex per face, two vertex per face, or at least three vertices per face. If the faces are
	 * of the last type, every single one is transformed into triangles. If the file does not represent
	 * a mesh in PLY format with one of the above face types, or the file can't be read, an
	 * exception is thrown.
	 *
	 * @param path path of the file, which should be parsed
	 *
	 * @return mesh representing the data of the file at location path
	 *
	 * @exception an instance of Exception is raised. It' of type EX_SYNTAX, if the file's content
	 * cannot be parsed, and it's of type EX_IO, if the file can't be read.
	 */
	EXPA XMesh *parsePLYFile(const string &path);

	/**
	 * A bone in bind pose. The bind pose
	 * of a bone is the state of the bone
	 * in its resting position.
	 */
	struct Bone {

		/**
		 * identifier of the bone, must be
		 * unique in a bone hierachy
		 */
		string ID;

		/**
		 * Transformation from the space of
		 * this bone in bind pose to the space
		 * of the parentbone in bind pose.
		 * The origin of this in bonespace
		 * is called joint of the bone.
		 */
		Matrix parentTransform;

		/**
		 * transformation from modelspace to
		 * the bonespace of this bone in bind pose
		 */
		Matrix inverseBindPoseMatrix;

		/**
		 * childbones
		 */
		vector<Bone> bones;

		/**
		 * copy constructor
		 */
		EXPA Bone(const Bone &);

		/**
		 * default constructor
		 */
		EXPA Bone();

	};

	/**
	 * Parses the skeleton of XTag in Collada format,
	 * and returns an equivalent bone structure. If the XTag
	 * is not in Collada format, or does not specify bones,
	 * an exception is thrown.
	 *
	 * @param data XTag in Collada format with a bone structure in it
	 * @return collection of root bones, and a transformation from the space
	 *  the root bones into model space.
	 * @exception an instance of Exception of type EX_SYNTAX is raised, if data
	 *  doesn't specify a skeleton
	 */
	EXPA pair<vector<Bone>,Matrix> parseColladaSkeleton(XTag &collada);

	/**
	 * Parses the skeleton of a string in Collada format,
	 * and returns an equivalent bone structure. If the string
	 * is not in Collada format, or does not specify bones,
	 * an exception is thrown.
	 *
	 * @param data string in Collada format with a bone structure in it
	 * @return collection of root bones, and a transformation from the space
	 *  the root bones into model space.
	 * @exception an instance of Exception of type EX_SYNTAX is raised, if data
	 *  can't be parsed, or no skeleton is specified
	 */
	EXPA pair<vector<Bone>,Matrix> parseColladaSkeleton(const string &data);

	/**
	 * Parses the skeleton of a file in Collada format at location path,
	 * and returns an equivalent bone structure. If the file
	 * does not exist, or it's not in Collada format, or does not specify bones,
	 * an exception is thrown.
	 *
	 * @param path location of a file in Collada format with a bone structure in it
	 * @return collection of root bones, and a transformation from the space
	 *  the root bones into model space.
	 * @exception an instance of Exception is raised. The Exception's type type EX_SYNTAX,
	 *  if data can't be parsed, or no skeleton is specified. The Exception's type is
	 *  EX_IO, if the file can't be read.
	 */
	EXPA pair<vector<Bone>,Matrix> parseColladaSkeletonFile(const string &path);

	/**
	 * Parses a XTag in Collada format, and returns an equivalent mesh. If the mesh has a bone structure
	 * attached to it, the wheights are stored in buffers with the names of the bones.
	 * All faces must be of the same type: one vertex per face, two vertex per face, or at least
	 * three vertices per face. If the faces are of the last type, every single one is transformed
	 * into triangles. If the XTag does not represent a mesh in Collada format with one of the
	 * above face types, or the function fails to parse for other reasons, an exception is thrown.
	 *
	 * @param data XTag in Collada, a mesh
	 * @return mesh representing the data of the parameter data
	 * @exception an instance of Exception of type EX_SYNTAX is raised, if data can't be parsed
	 */
	EXPA XMesh *parseColladaData(XTag &data);

	/**
	 * Parses a string in Collada format, and returns an equivalent mesh. If the mesh has a bone structure
	 * attached to it, the wheights are stored in buffers with the names of the bones.
	 * All faces must be of the same type: one vertex per face, two vertex per face, or at least
	 * three vertices per face. If the faces are of the last type, every single one is transformed
	 * into triangles. If the string does not represent a mesh in Collada format with one of the
	 * above face types, or the function fails to parse for other reasons, an exception is thrown.
	 *
	 * @param data string in Collada, a mesh
	 * @return mesh representing the data of the parameter data
	 * @exception an instance of Exception of type EX_SYNTAX is raised, if data can't be parsed
	 */
	EXPA XMesh *parseColladaData(const string &data);

	/**
	 * Parses a file in Collada format at location path, and returns an equivalent mesh.
	 * If the mesh has a bone structure attached to it, the wheights are stored in buffers with
	 * the names of the bones. All faces must be of the same type: one vertex per face, two vertex per
	 * face, or at least three vertices per face. If the faces are of the last type, every single one
	 * is transformed into triangles. If the string does not represent a mesh in Collada format with one
	 * of the above face types, or the function fails to parse for other reasons, an exception is thrown.
	 *
	 * @param path path of the file, which should be parsed
	 * @return mesh representing the data of the parameter data
	 * @exception an instance of Exception is raised. It' of type EX_SYNTAX, if the file's content
	 * cannot be parsed, and it's of type EX_IO, if the file can't be read.
	 */
	EXPA XMesh *parseColladaFile(const string &path);

	/**
	 * A bitmap representing an rgba image.
	 */
	class Bitmap {
	private:

		/**
		 * Array representing an image. The first pixel is located
		 * on the bottom left corner of the image, and the successor
		 * pixel is always neightbouring on the right of it's predecessor,
		 * if there is a right pixel of the predecessor. If a pixel has
		 * no right neightbour, because it's on the right border of the image,
		 * it's successor is located one line above on the left border of the image.
		 * If getPixelSize() returns 16, data should be cast to (float *) in
		 * order to manipulate pixels.
		 */
		unsigned char *data;

		/**
		 * width of the image in pixels
		 */
		unsigned int width;

		/**
		 * height of the image in pixels
		 */
		unsigned int height;

		/**
		 * number of bytes of each pixel in the image
		 */
		unsigned int bytesPerPixel;

		/**
		 * copy constructor, not used
		 */
		Bitmap(const Bitmap &);

		/**
		 * assigment operator, not used
		 */
		Bitmap &operator = (const Bitmap &);

	public:

		/**
		 * Constructs an empty image with the given width and
		 * height with 4 bytes per pixel.
		 */
		EXPA Bitmap(unsigned int width, unsigned int height);

		/**
		 * Constructs an empty image with the given width and
		 * height and bytesPerPixel bytes per pixel. The value
		 * of bytesPerPixel can be 4 or 16.
		 */
		EXPA Bitmap(unsigned int width, unsigned int height, unsigned int bytesPerPixel);

		/**
		 * Constructs an image from a file. Currently bmp, jpg and png files are supported.
		 * 8, 16, 24 bit-per-pixel images are supported for bmp, jpg and png files.
		 * 32 and 64 bit-per-pixel images are supported for png files.
		 * Image files with 8, 16, 24 or 32 bits per pixel are represented with 4 bytes per pixel.
		 * After loading such an image, getPixelSize() will return 4.
		 * Image files with 64 bis per pixel are represented with 16 bytes per pixel.
		 * After loading such an image, getPixelSize() will return 16.
		 *
		 * @param filename path to the image loaded by the constructor
		 * @exception if the image can't be loaded, an exception Exception of type EX_IO is thrown
		 */
		EXPA Bitmap(const string filename);

		/**
		 * deconstructor
		 */
		EXPA ~Bitmap();

		/**
		 * Saves the bitmap in its current state to a file.
		 * Currently bmp, jpg and png files are supported.
		 * If getPixelSize() returns 4, 24 bits per pixels will be used for bmp and jpg files,
		 * and 32 bits per pixel will be used for png files.
		 * If getPixelSize() returns 16, 24 bits per pixel will be used for bmp and jpg files,
		 * and 64 bits per pixel will be used for png files.
		 *
		 * @param filename destination path of the image
		 * @exception if the image can't be saved, an exception Exception of type EX_IO is thrown
		 */
		EXPA void save(const string filename);

		/**
		 * returns the width of the image in pixels
		 */
		EXPA unsigned int getWidth() const;

		/**
		 * returns the height of the image in pixels
		 */
		EXPA unsigned int getHeight() const;

		/**
		 * Number of bytes per pixels. Currently only four color channels
		 * with one or four bytes are supported.
		 */
		EXPA unsigned int getPixelSize() const;

		/**
		 * Returns the data of the image as a char array.
		 * If getPixelSize()
		 * returns 4, each four arrayelements can interpreted as a pixel's
		 * red, green, blue and alpha channel such that the lowest value
		 * is 0 and the highest value is 255.
		 * If getPixelSize()
		 * returns 16, the returned array should be cast to float*
		 * Each four arrayelements of the cast float* array can be interpreted as a pixel's
		 * red, green, blue and alpha channel such that the lowest value
		 * is 0, and the highest value is 1.
		 */
		EXPA unsigned char *getData();

	};

}

#endif
