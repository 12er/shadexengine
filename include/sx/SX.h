#ifndef _SX_SX_H_
#define _SX_SX_H_

/**
 * ShadeX Engine
 * (c) 2012 by Tristan Bauer
 */
#include <export/Export.h>
#include <sx/SXCore.h>
#include <sx/SXParser.h>
#include <string>
#include <vector>
#include <map>
#include <unordered_map>
using namespace std;

namespace sx {

	/**
	 * mousebuttons
	 */
	enum MouseButton {
		MOUSE_LEFT,
		MOUSE_MIDDLE,
		MOUSE_RIGHT
	};

	/**
	 * keyboard special keys
	 */
	enum KeyboardSpecial {
		SX_F1 = 0x1000000,
		SX_F2 = 0x1000001,
		SX_F3 = 0x1000002,
		SX_F4 = 0x1000003,
		SX_F5 = 0x1000004,
		SX_F6 = 0x1000005,
		SX_F7 = 0x1000006,
		SX_F8 = 0x1000007,
		SX_F9 = 0x1000008,
		SX_F10 = 0x1000009,
		SX_F11 = 0x100000a,
		SX_F12 = 0x100000b,
		SX_ESC = 0x100000c,
		SX_INSERT = 0x100000d,
		SX_PAUSE = 0x100000e,
		SX_HOME = 0x100000f,
		SX_END = 0x1000010,
		SX_DELETE = 0x1000011,
		SX_PAGEUP = 0x1000012,
		SX_PAGEDOWN = 0x1000013,
		SX_LEFT = 0x1000014,
		SX_RIGHT = 0x1000015,
		SX_DOWN = 0x1000016,
		SX_UP = 0x1000017,
		SX_NUMLOCK = 0x1000018,
		SX_CLEAR = 0x1000019,
		SX_ENTER = 0x100001a,
		SX_CTRL = 0x100001b,
		SX_SHIFT = 0x100001c,
		SX_CAPSLOCK = 0x100001d,
		SX_START = 0x100001e,
		SX_ALT = 0x100001f,
		SX_RETURN = 0x1000020,
		SX_BACKSPACE = 0x1000021
	};

	/**
	 * provides access for SXRenderListeners to its
	 * render area
	 */
	class SXRenderArea {
	public:

		/**
		 * returns the time elapsed from the
		 * creation of the SXRenderArea to
		 * the last frame, which started processing
		 */
		virtual double getTime() const = 0;

		/**
		 * returns the time leapsed since the
		 * last frame, which finished processing
		 */
		virtual double getDeltaTime() const = 0;

		/**
		 * returns the x coordinate of the mouse
		 */
		virtual int getMouseX() const = 0;

		/**
		 * returns the y coordinate of the mouse
		 */
		virtual int getMouseY() const = 0;

		/**
		 * returns the x coordinate of the difference of the
		 * mousepointer shortly before set with setMousePosition
		 * and the position set with setMousePosition
		 */
		virtual double getMouseDeltaX() const = 0;

		/**
		 * returns the y coordinate of the difference of the
		 * mousepointer shortly before set with setMousePosition
		 * and the position set with setMousePosition
		 */
		virtual double getMouseDeltaY() const = 0;

		/**
		 * returns true iff key is pressed
		 */
		virtual bool hasKey(int key) const = 0;

		/**
		 * returns true iff mousebutton key is pressed
		 */
		virtual bool hasMouseKey(MouseButton key) const = 0;

		/**
		 * returns the width of the render area
		 */
		virtual int getWidth() const = 0;

		/**
		 * returns the height of the render area
		 */
		virtual int getHeight() const = 0;

		/**
		 * makes the renderarea quit rendering
		 */
		virtual void stopRendering() = 0;

		/**
		 * sets the mousepointer to a position
		 * relative to the left bottom windowcorner
		 */
		virtual void setMousePointer(int x, int y) = 0;

		/**
		 * sets if the cursor is rendered or not,
		 * if its located at the render area
		 */
		virtual void setShowCursor(bool showCursor) = 0;

	};

	/**
	 * listener of a render area
	 */
	class SXRenderListener {
	public:

		/**
		 * Called when the render area is initialized.
		 * Initializes resources of the listener.
		 * All resources must be allocated in this method,
		 * and not in the constructor!
		 * Parameter area provides access for the listener
		 * to its render area.
		 */
		virtual void create(SXRenderArea &area) = 0;

		/**
		 * Called when the render area's dimensions are changed.
		 * Parameter area provides access for the listener
		 * to its render area.
		 */
		virtual void reshape(SXRenderArea &area) = 0;

		/**
		 * Called when the render area needs to
		 * be rendered by this listener.
		 * Parameter area provides access for the listener
		 * to its render area.
		 */
		virtual void render(SXRenderArea &area) = 0;

		/**
		 * Executed before the render area stops rendering.
		 * All resources must be deallocated in this method,
		 * and not by the deconstructor.
		 * Parameter area provides access for the listener
		 * to its render area.
		 */
		virtual void stop(SXRenderArea &area) = 0;

	};

	/**
	 * A window class temporarily used during building the ShadeX Engine.
	 * It's sole purpose serves for implementing and testing the engine's
	 * functionality. Future versions will be able to set ab an SX widget
	 * offering the functionality for gui frameworks such as Qt.
	 */
/*	class SXWindow: public SXRenderArea {
	private:

		static map<int,int> keys_glut2sx;

		static map<int,int> initKeys_glut2sx();

		static map<int,int> keys_glutnotspecial2sx;

		static map<int,int> initKeys_glutnotspecial2sx();
*/
		/**
		 * the last time a frame started rendering
		 */
//		double currentTimeRendering;

		/**
		 * the last time a frame finished rendering
		 */
//		double lastTimeRendered;

		/**
		 * width of the window
		 */
//		int width;

		/**
		 * height of the window
		 */
//		int height;

		/**
		 * x coordinate of mouseposition
		 */
//		int mouseX;

		/**
		 * y coordinate of mouseposition
		 */
//		int mouseY;

		/**
		 * x coordinate of the difference of the
		 * mousepointer shortly before set with setMousePosition
		 * and the position set with setMousePosition
		 */
//		static double mouseDeltaX;

		/**
		 * y coordinate of the difference of the
		 * mousepointer shortly before set with setMousePosition
		 * and the position set with setMousePosition
		 */
//		static double mouseDeltaY;

		/**
		 * map of currently pressed keys
		 */
//		unordered_map<int,int> keys;

		/**
		 * map of currently pressed mouse buttons
		 */
//		unordered_map<int,int> mouseKeys;

		/**
		 * measurement of the time since windowcreation in seconds
		 */
//		TimeMeasurement timeMeasurement;

		/**
		 * window listeners
		 */
//		vector<SXRenderListener *> listeners;

		/**
		 * window listeners, which have not been initialized yet
		 */
//		vector<SXRenderListener *> initListeners;

		/**
		 * ID of the window
		 */
//		int windowID;

		/**
		 * true if the window's resources are deleted
		 */
//		bool removedWindow;

		/**
		 * Calls reshape in all listeners, and create,
		 * if a listener is in the initListeners list.
		 * The listener is removed from the initListeners list.
		 */
//		void listenersReshape();

		/**
		 * calls render in all listeners
		 */
//		void listenersRender();

		/**
		 * calls stop in all listeners, which are not
		 * in the initListeners list
		 */
/*		void listenersStop();

		void updateTime();

		void setWidth(int width);

		void setHeight(int height);

		void setMouseX(int x);

		void setMouseY(int y);

		void addKey(int key);

		void removeKey(int key);

		void addMouseKey(MouseButton key);

		void removeMouseKey(MouseButton key);
*/
		/**
		 * copy constructor, not used
		 */
//		SXWindow(const SXWindow &);

		/**
		 * assignment operator, not used
		 */
//		SXWindow &operator = (const SXWindow &);

		/**
		 * Map of all windows identified by windowID.
		 * Contains only windows with removedWindow == false.
		 */
//		static map<int, SXWindow *> windows;

		/**
		 * intern argument, zero, if the windows system
		 * is not initialized, one otherwise
		 */
//		static int argc;

		/**
		 * intern argument, array of size one of strings,
		 * initialized, if argc is not zero
		 */
//		static char **argv;

		/**
		 * intern argument, element of argv
		 */
/*		static char *argv0;

		friend void windowKeyUp(unsigned char key, int x, int y);

		friend void windowKeyDown(unsigned char key, int x, int y);

		friend void windowSpecialKeyUp(int key, int x, int y);

		friend void windowSpecialKeyDown(int key, int x, int y);

		friend void windowMouseKey(int button, int state, int x, int y);

		friend void windowMouseMotion(int x, int y);

		friend void windowPassiveMotion(int x, int y);
*/
		/**
		 * calls listenersReshape in all windows
		 */
//		friend void reshapeAll(int width, int height);

		/**
		 * calls listenersRender in all windows
		 */
//		friend void renderAll();

		/**
		 * calls listenersStop in all windows
		 */
//		friend void stopAll();

//	public:

		/**
		 * Initializes the window with title name and a width and height. Throws an
		 * exception Exception, if the window can't be initialized.
		 */
//		EX SXWindow(const string &name, unsigned int width, unsigned int height);

		/**
		 * Deconstructor of window. Removes all listeners, and the window, if
		 * it removeWindow has not been called yet, and removedWindow is false.
		 */
//		EX ~SXWindow();

		/**
		 * removes the window, if it has not been removed yet
		 */
//		EX void removeWindow();

		/**
		 * Adds a listener. If the window has been removed, the listener
		 * is not added, but deleted. The listener is removed, when the window
		 * is removed.
		 */
//		EX void addRenderListener(SXRenderListener &l);

		/**
		 * Removes a listener, if it's part of the window. Otherwise the listener is not removed.
		 */
//		EX void deleteRenderListener(SXRenderListener &l);

		/**
		 * removes all listeners, if the window has not been removed yet
		 */
//		EX void deleteRenderListeners();

		/**
		 * @see sx::SXRenderArea::getTime()
		 */
//		EX double getTime() const;

		/**
		 * @see sx::SXRenderArea::getDeltaTime()
		 */
//		EX double getDeltaTime() const;

		/**
		 * @see sx::SXRenderArea::getMouseX()
		 */
//		EX int getMouseX() const;

		/**
		 * @see sx::SXRenderArea::getMouseY()
		 */
//		EX int getMouseY() const;

		/**
		 * @see sx::SXRenderArea::getMouseDeltaX()
		 */
//		EX double getMouseDeltaX() const;
		
		/**
		 * @see sx::SXRenderArea::getMouseDeltaY()
		 */
//		EX double getMouseDeltaY() const;

		/**
		 * @see sx::SXRenderArea::hasKey(int)
		 */
//		EX bool hasKey(int key) const;

		/**
		 * @see sx::SXRenderArea::hasMouseKey(MouseButton)
		 */
//		EX bool hasMouseKey(MouseButton key) const;

		/**
		 * @see sx::SXRenderArea::getWidth()
		 */
//		EX int getWidth() const;

		/**
		 * @see sx::SXRenderArea::getHeight()
		 */
//		EX int getHeight() const;
		
		/**
		 * Keeps all open windows in idle mode. Only during the execution of run,
		 * the windows can feed their listeners with messages. After the execution
		 * of run, all windows are removed.
		 */
//		EX static void run();

		/**
		 * @see sx::SXRenderArea::stopRendering()
		 */
//		EX void stopRendering();

		/**
		 * @see sx::SXRenderArea::setMousePointer(int,int)
		 */
//		EX void setMousePointer(int x, int y);

		/**
		 * @see sx::SXRenderArea::setShowCursor(bool)
		 */
//		EX void setShowCursor(bool showCursor);

//	};

	/**
	 * internal datastructure of ShadeX for
	 * storing the uniforms added to
	 * BufferedMeshes with setUniforms
	 */
	struct BufferUniforms {
		
		/**
		 * maps a location to its uniforms' bufferIDs
		 */
		map<unsigned int,vector<string> > attributeNames;

		/**
		 * maps a location to its uniforms
		 */
		map<unsigned int,vector<Uniform *> > uniforms;

	};

	class ShadeX {
	private:

		unordered_map<string,SXResource *> resources;
		unordered_map<string,Shader *> shaders;
		unordered_map<string,BufferedMesh *> bufferedMeshes;
		unordered_map<string,Skeleton *> skeletons;
		unordered_map<string,UniformFloat *> uniformFloats;
		unordered_map<string,UniformDouble *> uniformDoubles;
		unordered_map<string,UniformVector *> uniformVectors;
		unordered_map<string,UniformDVector *> uniformDVectors;
		unordered_map<string,UniformMatrix *> uniformMatrices;
		unordered_map<string,UniformDMatrix *> uniformDMatrices;
		unordered_map<string,Texture *> textures;
		unordered_map<string,Volume *> volumes;
		unordered_map<string,RenderTarget *> renderTargets;
		unordered_map<string,RenderObject *> renderObjects;
		unordered_map<string,Pass *> passes;
		unordered_map<string,Effect *> effects;
		unordered_map<string,AudioBuffer *> audioBuffers;
		unordered_map<string,AudioObject *> audioObjects;
		unordered_map<string,AudioListener *> audioListeners;
		unordered_map<string,AudioPass *> audioPasses;

		unordered_map<string,BufferUniforms> bufferUniforms;

		/**
		 * copy constructor, not used
		 */
		ShadeX(const ShadeX &);

		/**
		 * assignment operator, not used
		 */
		ShadeX &operator = (const ShadeX &);

		void INTERNAL_LOAD_SHADER(XTag *rNode, XTag *data);

		void INTERNAL_LOAD_MATRIXCONTENT(Matrix &matrix, const string &id, XTag *rNode, XTag *data);

		void INTERNAL_LOAD_DMATRIXCONTENT(DMatrix &matrix, const string &id, XTag *rNode, XTag *data);

		void INTERNAL_LOAD_VECTORCONTENT(Vector &vec, const string &id, XTag *rNode, XTag *data);

		void INTERNAL_LOAD_DVECTORCONTENT(DVector &vec, const string &id, XTag *rNode, XTag *data);

		void INTERNAL_LOAD_BUFFEREDMESH(XTag *rNode, XTag *data);

		void INTERNAL_LOAD_SKELETON(XTag *rNode, XTag *data);

		void INTERNAL_LOAD_UNIFORMFLOAT(XTag *rNode, XTag *data);

		void INTERNAL_LOAD_UNIFORMDOUBLE(XTag *rNode, XTag *data);

		void INTERNAL_LOAD_UNIFORMVECTOR(XTag *rNode, XTag *data);

		void INTERNAL_LOAD_UNIFORMDVECTOR(XTag *rNode, XTag *data);

		void INTERNAL_LOAD_UNIFORMMATRIX(XTag *rNode, XTag *data);

		void INTERNAL_LOAD_UNIFORMDMATRIX(XTag *rNode, XTag *data);

		void INTERNAL_LOAD_TEXTURE(XTag *rNode, XTag *data);

		void INTERNAL_LOAD_VOLUME(XTag *rNode, XTag *data);

		void INTERNAL_LOAD_RENDERTARGET(XTag *rNode, XTag *data);

		void INTERNAL_LOAD_RENDEROBJECT(XTag *rNode, XTag *data);

		void INTERNAL_RENDEROBJECT_COLLECT_UNIFORMS(RenderObject &obj, const string &id, const string &uType, bool isUniformSet, int uniformSetID, XTag *rNode, XTag *data);

		void INTERNAL_LOAD_PASS(XTag *rNode, XTag *data);

		void INTERNAL_PASS_COLLECT_UNIFORMS(Pass &pass, const string &id, const string &uType, XTag *rNode, XTag *data);

		void INTERNAL_LOAD_EFFECT(XTag *rNode, XTag *data);

		void INTERNAL_LOAD_AUDIOBUFFER(XTag *rNode, XTag *data);

		void INTERNAL_LOAD_AUDIOOBJECT(XTag *rNode, XTag *data);

		void INTERNAL_LOAD_AUDIOLISTENER(XTag *rNode, XTag *data);

		void INTERNAL_LOAD_AUDIOPASS(XTag *rNode, XTag *data);

	public:

		/**
		 * default constructor
		 */
		EX ShadeX();
		
		/**
		 * deconstructor
		 */
		EX ~ShadeX();

		/**
		 * If a resource with identifyer id exists,
		 * the resource with the identifyer is deconstructed and removed.
		 * If no such resource exists, an exception is thrown.
		 *
		 * @param id identifyer of the resource
		 * @exception an Exception object of type EX_NODATA is thrown, if no resource with identifyer id exists
		 */
		EX void clear(const string &id);

		/**
		 * Deconstructs and removes all resources.
		 */
		EX void clear();

		/**
		 * Parses a configuration string in SX language to load several
		 * resources.
		 * If the resources can't be loaded, an exception is thrown.
		 * -- TODO: SPECIFY FORMAT OF CONFIGURATION STRING! --
		 * look at "doc\SX reference cards\SX language"
		 */
		EX void addResourcesFromString(const string &resources);

		/**
		 * Parses a configuration file in SX language from path
		 * to load several resources. 
		 * If the resources can't be loaded, an exception is thrown.
		 * To optain a specification of the format of the fileformat
		 * look at
		 * @see ShadeX::addResources(const string &resources)
		 */
		EX void addResources(const string &path);

		/**
		 * Tries to initialize as much resources as possible.
		 * If some resources are still not loaded after trying to
		 * load every single one, an exception is thrown.
		 *
		 * @Exception an exception Exception of type EX_INIT
		 */
		EX void load();

		/**
		 * Searches for the shader with identifyer id,
		 * and returns it. If no such shader exists, a
		 * new shader with this id is created before it
		 * is returned. If another resource than
		 * a shader has the id, an exception is thrown.
		 *
		 * @param id identifyer of the shader
		 * @return a reference to the shader
		 * @exception an Exception object of type EX_AMBIGUOUS is thrown, if a non-shader resource with identifyer id exists
		 */
		EX Shader &getShader(const string &id);

		/**
		 * adds a new shader with identifyer id
		 */
		EX void addResource(const string &id, Shader &shader);

		/**
		 * Searches for a BufferedMesh with identifyer id,
		 * and returns it. If no such BufferedMesh exists, a
		 * new BufferedMesh with this id is created before it
		 * is returned. If another resource than a BufferedMesh
		 * has the id, an exception is thrown.
		 *
		 * @param id identifyer of the BufferedMesh
		 * @return a reference to the BufferedMesh
		 * @exception an Exception object of type EX_AMBIGUOUS is thrown, if a non-BufferedMesh resource with identifyer id exists
		 */
		EX BufferedMesh &getBufferedMesh(const string &id);

		/**
		 * adds a new mesh with identifyer id
		 */
		EX void addResource(const string &id, BufferedMesh &mesh);

		/**
		 * Searches for a Skeleton with identifyer id,
		 * and returns it. If no such Skeleton exists, a
		 * new Skeleton with this id is created before it
		 * is returned. If another resource than a Skeleton
		 * has the id, an exception is thrown.
		 *
		 * @param id identifyer of the Skeleton
		 * @return a reference to the Skeleton
		 * @exception an Exception object of type EX_AMBIGUOUS is thrown, if a non-Skeleton resource with identifyer id exists
		 */
		EX Skeleton &getSkeleton(const string &id);

		/**
		 * adds a new skeleton with identifyer id
		 */
		EX void addResource(const string &id, Skeleton &skeleton);

		/**
		 * Searches for a uniform float with identifyer id,
		 * and returns it. If no such uniform float exists, a
		 * new uniform float with this id is created before it
		 * is returned. If another resource than a uniform float
		 * has the id, an exception is thrown.
		 *
		 * @param id identifyer of the uniform float
		 * @return a reference to the uniform float
		 * @exception an Exception object of type EX_AMBIGUOUS is thrown, if a non-uniform float resource with identifyer id exists
		 */
		EX UniformFloat &getUniformFloat(const string &id);

		/**
		 * adds a new uniform float with identifyer id
		 */
		EX void addResource(const string &id, UniformFloat &uFloat);

		/**
		 * Searches for a uniform double with identifyer id,
		 * and returns it. If no such uniform double exists, a
		 * new uniform double with this id is created before it
		 * is returned. If another resource than a uniform double
		 * has the id, an exception is thrown.
		 *
		 * @param id identifyer of the uniform double
		 * @return a reference to the uniform double
		 * @exception an Exception object of type EX_AMBIGUOUS is thrown, if a non-uniform double resource with identifyer id exists
		 */
		EX UniformDouble &getUniformDouble(const string &id);

		/**
		 * adds a new uniform double with identifyer id
		 */
		EX void addResource(const string &id, UniformDouble &uDouble);

		/**
		 * Searches for a uniform vector with identifyer id,
		 * and returns it. If no such uniform vector exists, a
		 * new uniform vector with this id is created before it
		 * is returned. If another resource than a uniform vector
		 * has the id, an exception is thrown.
		 *
		 * @param id identifyer of the uniform vector
		 * @return a reference to the uniform vector
		 * @exception an Exception object of type EX_AMBIGUOUS is thrown, if a non-uniform vector resource with identifyer id exists
		 */
		EX UniformVector &getUniformVector(const string &id);

		/**
		 * adds a new uniform vector with identifyer id
		 */
		EX void addResource(const string &id, UniformVector &uVector);

		/**
		 * Searches for a uniform double vector with identifyer id,
		 * and returns it. If no such uniform double vector exists, a
		 * new uniform double vector with this id is created before it
		 * is returned. If another resource than a uniform double vector
		 * has the id, an exception is thrown.
		 *
		 * @param id identifyer of the uniform double vector
		 * @return a reference to the uniform double vector
		 * @exception an Exception object of type EX_AMBIGUOUS is thrown, if a non-uniform double vector resource with identifyer id exists
		 */
		EX UniformDVector &getUniformDVector(const string &id);

		/**
		 * adds a new uniform double vector with identifyer id
		 */
		EX void addResource(const string &id, UniformDVector &uDVector);

		/**
		 * Searches for a uniform matrix with identifyer id,
		 * and returns it. If no such uniform matrix exists, a
		 * new uniform matrix with this id is created before it
		 * is returned. If another resource than a uniform matrix
		 * has the id, an exception is thrown.
		 *
		 * @param id identifyer of the uniform matrix
		 * @return a reference to the uniform matrix
		 * @exception an Exception object of type EX_AMBIGUOUS is thrown, if a non-uniform matrix resource with identifyer id exists
		 */
		EX UniformMatrix &getUniformMatrix(const string &id);

		/**
		 * adds a new uniform matrix with identifyer id
		 */
		EX void addResource(const string &id, UniformMatrix &uMatrix);

		/**
		 * Searches for a uniform double matrix with identifyer id,
		 * and returns it. If no such uniform double matrix exists, a
		 * new uniform double matrix with this id is created before it
		 * is returned. If another resource than a uniform double matrix
		 * has the id, an exception is thrown.
		 *
		 * @param id identifyer of the uniform double matrix
		 * @return a reference to the uniform double matrix
		 * @exception an Exception object of type EX_AMBIGUOUS is thrown, if a non-uniform double matrix resource with identifyer id exists
		 */
		EX UniformDMatrix &getUniformDMatrix(const string &id);

		/**
		 * adds a new uniform double matrix with identifyer id
		 */
		EX void addResource(const string &id, UniformDMatrix &uDMatrix);

		/**
		 * Searches for a Texture with identifyer id,
		 * and returns it. If no such Texture exists, a
		 * new Texture with this id is created before it
		 * is returned. If another resource than a Texture
		 * has the id, an exception is thrown.
		 *
		 * @param id identifyer of the Texture
		 * @return a reference to the Texture
		 * @exception an Exception object of type EX_AMBIGUOUS is thrown, if a non-texture resource with identifyer id exists
		 */
		EX Texture &getTexture(const string &id);

		/**
		 * adds a new texture with identifyer id
		 */
		EX void addResource(const string &id, Texture &texture);

		/**
		 * Searches for a Volume with identifyer id,
		 * and returns it. If no such Volume exists, a
		 * new Volume with this id is created before it
		 * is returned. If another resource than a Volume
		 * has the id, an exception is thrown.
		 *
		 * @param id identifyer of the Volume
		 * @return a reference to the Volume
		 * @exception an Exception object of type EX_AMBIGUOUS is thrown, if a non-texture resource with identifyer id exists
		 */
		EX Volume &getVolume(const string &id);

		/**
		 * adds a new volume with identifyer id
		 */
		EX void addResource(const string &id, Volume &volume);

		/**
		 * Searches for a RenderTarget with identifyer id,
		 * and returns it. If no such RenderTarget exists, a
		 * new RenderTarget with this id is created before it
		 * is returned. If another resource than a RenderTarget
		 * has the id, an exception is thrown.
		 *
		 * @param id identifyer of the RenderTarget
		 * @return a reference to the RenderTarget
		 * @exception an Exception object of type EX_AMBIGUOUS is thrown, if a non-rendertarget resource with identifyer id exists
		 */
		EX RenderTarget &getRenderTarget(const string &id);

		/**
		 * adds a new rendertarget with identifyer id
		 */
		EX void addResource(const string &id, RenderTarget &target);

		/**
		 * Searches for a RenderObject with identifyer id,
		 * and returns it. If no such RenderObject exists, a
		 * new RenderObject with this id is created before it
		 * is returned. If another resource than a RenderObject
		 * has the id, an exception is thrown.
		 *
		 * @param id identifyer of the RenderObject
		 * @return a reference to the RenderObject
		 * @exception an Exception object of type EX_AMBIGUOUS is thrown, if a non-renderobject resource with identifyer id exists
		 */
		EX RenderObject &getRenderObject(const string &id);

		/**
		 * adds a new renderobject with identifyer id
		 */
		EX void addResource(const string &id, RenderObject &object);

		/**
		 * Searches for a Pass with identifyer id,
		 * and returns it. If no such Pass exists, a
		 * new Pass with this id is created before it
		 * is returned. If another resource than a Pass
		 * has the id, an exception is thrown.
		 *
		 * @param id identifyer of the Pass
		 * @return a reference to the Pass
		 * @exception an Exception object of type EX_AMBIGUOUS is thrown, if a non-pass resource with identifyer id exists
		 */
		EX Pass &getPass(const string &id);

		/**
		 * adds a new Pass with identifyer id
		 */
		EX void addResource(const string &id, Pass &pass);

		/**
		 * Searches for an Effect with identifyer id,
		 * and returns it. If no such Effect exists, a
		 * new Effect with this id is created before it
		 * is returned. If another resource than a Effect
		 * has the id, an exception is thrown.
		 *
		 * @param id identifyer of the Effect
		 * @return a reference to the Effect
		 * @exception an Exception object of type EX_AMBIGUOUS is thrown, if a non-effect resource with identifyer id exists
		 */
		EX Effect &getEffect(const string &id);

		/**
		 * adds a new Effect with identifyer id
		 */
		EX void addResource(const string &id, Effect &effect);

		/**
		 * Searches for an AudioBuffer with identifyer id,
		 * and returns it. If no such AudioBuffer exists, a
		 * new AudioBuffer with this id is created before it
		 * is returned. If another resource than an AudioBuffer
		 * has the id, an exception is thrown.
		 *
		 * @param id identifyer of the AudioBuffer
		 * @return a reference to the AudioBuffer
		 * @exception an Exception object of type EX_AMBIGUOUS is thrown, if a non-effect resource with identifyer id exists
		 */
		EX AudioBuffer &getAudioBuffer(const string &id);

		/**
		 * adds a new AudioBuffer with identifyer id
		 */
		EX void addResource(const string &id, AudioBuffer &buffer);

		/**
		 * Searches for an AudioObject with identifyer id,
		 * and returns it. If no such AudioObject exists, a
		 * new AudioObject with this id is created before it
		 * is returned. If another resource than an AudioObject
		 * has the id, an exception is thrown.
		 *
		 * @param id identifyer of the AudioObject
		 * @return a reference to the AudioObject
		 * @exception an Exception object of type EX_AMBIGUOUS is thrown, if a non-effect resource with identifyer id exists
		 */
		EX AudioObject &getAudioObject(const string &id);

		/**
		 * adds a new AudioObject with identifyer id
		 */
		EX void addResource(const string &id, AudioObject &object);

		/**
		 * Searches for an AudioListener with identifyer id,
		 * and returns it. If no such AudioListener exists, a
		 * new AudioListener with this id is created before it
		 * is returned. If another resource than an AudioListener
		 * has the id, an exception is thrown.
		 *
		 * @param id identifyer of the AudioListener
		 * @return a reference to the AudioListener
		 * @exception an Exception object of type EX_AMBIGUOUS is thrown, if a non-effect resource with identifyer id exists
		 */
		EX AudioListener &getAudioListener(const string &id);

		/**
		 * adds a new AudioListener with identifyer id
		 */
		EX void addResource(const string &id, AudioListener &object);

		/**
		 * Searches for an AudioPass with identifyer id,
		 * and returns it. If no such AudioPass exists, a
		 * new AudioPass with this id is created before it
		 * is returned. If another resource than an AudioPass
		 * has the id, an exception is thrown.
		 *
		 * @param id identifyer of the AudioPass
		 * @return a reference to the AudioPass
		 * @exception an Exception object of type EX_AMBIGUOUS is thrown, if a non-effect resource with identifyer id exists
		 */
		EX AudioPass &getAudioPass(const string &id);

		/**
		 * adds a new AudioPass with identifyer id
		 */
		EX void addResource(const string &id, AudioPass &object);

	};

}

#endif