#ifndef _LOG4SX_LOG4SX_H_
#define _LOG4SX_LOG4SX_H_

/**
 * Logger Utilities
 * (c) 2012 by Tristan Bauer
 */

#include <export/Export.h>
#include <sx/Exception.h>
#include <string>
#include <fstream>
#include <vector>
#include <map>
#include <chrono>
using namespace std;

namespace sx {

	/**
	 * Only iff the minLevel of
	 * the logger is not L_NONE, and
	 * the level is greater or equal than
	 * the minLevel of the logger,
	 * messages are recognized by the
	 * logger. The LogLevels from the lowest
	 * to the highest level are
	 * L_HARMLESS, L_WARNING, L_ERROR, L_FATAL_ERROR, L_NONE
	 */
	enum LogLevel {
		L_HARMLESS = 0,
		L_WARNING = 1,
		L_ERROR = 2,
		L_FATAL_ERROR = 3,
		L_NONE = 4
	};

	/**
	 * Encapsulates LogLevel. This class is
	 * used with a logger to change the level
	 * of the logger by overloading the << operator like:
	 * <logger> << Level(<entry of type LogLevel>) << <Log message>
	 */
	class Level {
	private:

		/**
		 * A LogLevel
		 */
		LogLevel level;

	public:

		/**
		 * Constructs a Level object with LogLevel level.
		 * This class is meant to overload the << operator
		 * of the logger to change the level of the logger like this:
		 * <logger> << Level(<entry of type LogLevel>) << <log message>
		 *
		 * @param level LogLevel
		 */
		EXL Level(LogLevel level);

		/**
		 * copy constructor
		 */
		EXL Level(const Level &l);

		/**
		 * assignment operator
		 */
		EXL Level &operator = (const Level &l);

		/**
		 * getter
		 */
		EXL LogLevel getLevel() const;
	};

	/**
	 * A LogMarkup helps structuring the log into parts.
	 * Each time a LogMarkup is passed to the Logger, all
	 * the messages are part of this markup, until another
	 * LogMarkup is posted. This class is meant to be used
	 * by overloading the << operator like this:
	 * <logger> << LogMarkup(<markup message>)
	 */
	class LogMarkup {
	protected:

		/**
		 * markup message
		 */
		string message;

	public:

		/**
		 * Constructs a new LogMarkup with markup message message.
		 * This class is meant to overload the << operator
		 * of the logger to append a new markup like this:
		 * <logger> << LogMarkup(<markup message>)
		 * 
		 * @param message markup message
		 */
		EXL LogMarkup(string message);

		/**
		 * copy constructor
		 */
		EXL LogMarkup(const LogMarkup &lm);

		/**
		 * assignment operator
		 */
		EXL LogMarkup &operator = (const LogMarkup &lm);

		/**
		 * deconstructor
		 */
		EXL virtual ~LogMarkup();

		/**
		 * getter
		 */
		EXL string getMessage() const;

	};

	/**
	 * A log annotation. This class is meant to overload the << operator
	 * of the logger to append a new annotation like this:
	 * <logger> << Annotation(<annotation text>)
	 */
	class Annotation {
	private:

		/**
		 * annotation message
		 */
		string annotation;

	public:

		/**
		 * Constructs an annotation with an annotation text.
		 * This class is meant to overload the << operator
		 * of the logger to append a new markup like this:
		 * <logger> << LogMarkup(<annotation text>)
		 *
		 * @param annotation annotation text
		 */
		EXL Annotation(string annotation);
		
		/**
		 * copy constructor
		 */
		EXL Annotation(const Annotation &a);

		/**
		 * assignment operator
		 */
		EXL Annotation &operator = (const Annotation &a);

		/**
		 * getter
		 */
		EXL string getAnnotation() const;

	};

	class Logger;

	/**
	 * Class holding the loggers. The static methods
	 * of Logger serve for accessing the loggers of LoggerManager.
	 */
	class LoggerManager {
	private:

		/**
		 * Constructor
		 */
		LoggerManager();

		/**
		 * Copy constructor is not used.
		 */
		LoggerManager(const LoggerManager &);

		/**
		 * Assignment operator is not used.
		 */
		LoggerManager &operator = (const LoggerManager &);

		/**
		 * Map of loggers accessible by string IDs.
		 * one of the loggers can be used as a
		 * default logger. The map is initialized
		 * with a logger with ID SX.
		 */
		map<string,Logger *> loggers;

		/**
		 * A default logger, contained
		 * in the map loggers. This pointer
		 * must always point to a valid Logger.
		 * Initialized with a logger with ID SX.
		 */
		Logger *defaultLogger;

	public:

		/**
		 * Deconstructor
		 */
		~LoggerManager();

		friend class Logger;

	};

	/**
	 * A Logger base class. The base types, and
	 * string objects can be passed to
	 * the Logger as messages. Messages are
	 * only passed to the logger, iff the
	 * minLevel is not higher than level,
	 * and minLevel is different from L_NONE.
	 * Otherwise the messages are simply ignored.
	 * Passing a message <message> to the Logger
	 * <logger> at LogLevel <loglevel> would look like
	 * <logger> << Level(<loglevel>) << <message>;
	 */
	class Logger {
	private:

		/**
		 * Holder object of the loggers
		 */
		static LoggerManager loggers;

		/**
		 * copy constructor, not used
		 */
		Logger(const Logger &);

		/**
		 * assignment operator, not used
		 */
		Logger &operator = (const Logger &);

	protected:

		/**
		 * minimum level the property
		 * level is required to be, if messages
		 * should not be ignored. If
		 * minLevel equals L_NONE, all messages
		 * are ignored.
		 */
		LogLevel minLevel;

		/**
		 * level required to be equal
		 * or greater than minLevel, if messages
		 * should not be ignored
		 */
		LogLevel level;

		/**
		 * default constructor, not used,
		 * because Logger is abstract
		 */
		EXL Logger();

		/**
		 * returns true, if messages are
		 * not be ignored by the logger
		 */
		bool takeMessage() const;

	public:

		/**
		 * returns newline
		 */
		EXL static string newLine();

		/**
		 * Adds a logger with identifyer ID to the loggers map.
		 * If another logger with the same ID exists already,
		 * or the pointer logger has value 0,
		 * an exception is thrown.
		 *
		 * @param ID unique identifyer of the logger
		 * @param logger a new logger, must not be 0
		 * @exception an exception of type Exception is thrown, if another logger has identifyer ID
		 */
		EXL static void addLogger(string ID, Logger *logger);

		/**
		 * Makes the logger with identifyer ID to the default logger.
		 * An exception is thrown, if a logger with identifyer ID
		 * does not exist.
		 *
		 * @param ID identifyer of the default logger
		 * @exception exception of type Exception is thrown, if an error occurs
		 */
		EXL static void setDefaultLogger(string ID);

		/**
		 * Returns the default logger. A valid default logger must always exist.
		 *
		 * @return reference of the default logger
		 */
		EXL static Logger & get();

		/**
		 * Deletes the logger with identifyer ID. If no logger with identifyer ID
		 * exists, an exception is thrown.
		 *
		 * @param ID identifyer of logger, which should be deleted
		 * @exception exception of type Exception is thrown, if an error occurs
		 */
		EXL static void deleteLogger(string ID);

		/**
		 * deconstructor
		 */
		EXL virtual ~Logger();

		/**
		 * setter
		 */
		EXL void setMinLevel(LogLevel level);

		/**
		 * getter
		 */
		EXL LogLevel getMinLevel() const;

		/**
		 * getter
		 */
		EXL LogLevel getLevel() const;

		/**
		 * Changes the minLevel of the logger to l.
		 * The following messages are ignored, if
		 * l is L_NONE or greater than level.
		 *
		 * @param l new level of the logger
		 * @return a reference to this object
		 */
		EXL virtual Logger & operator << (const Level l);

		/**
		 * Appends a new LogMarkup to the Logger.
		 * Each new Message passed to the logger
		 * is part of the new markup. Markups are
		 * passed to the log independently from the
		 * values of level and minLevel.
		 *
		 * @param m new LogMarkup
		 * @return a reference to this object
		 */
		EXL virtual Logger & operator << (const LogMarkup m) = 0;

		/**
		 * Appends a new Annotation to the Logger.
		 * Annotation should be used to document
		 * the messages passed to the logger.
		 * Annotations are passed to the log
		 * independently from the values of level
		 * and minLevel.
		 *
		 * @param a Annotation
		 * @return a reference to this object
		 */
		EXL virtual Logger & operator << (const Annotation a) = 0;

		/**
		 * Passes a message to the logger,
		 * and returns a reference to the logger.
		 */
		EXL virtual Logger & operator << (char value) = 0;

		/**
		 * Passes a message to the logger,
		 * and returns a reference to the logger.
		 */
		EXL virtual Logger & operator << (unsigned char value) = 0;

		/**
		 * Passes a message to the logger,
		 * and returns a reference to the logger.
		 */
		EXL virtual Logger & operator << (short value) = 0;

		/**
		 * Passes a message to the logger,
		 * and returns a reference to the logger.
		 */
		EXL virtual Logger & operator << (unsigned short value) = 0;

		/**
		 * Passes a message to the logger,
		 * and returns a reference to the logger.
		 */
		EXL virtual Logger & operator << (int value) = 0;

		/**
		 * Passes a message to the logger,
		 * and returns a reference to the logger.
		 */
		EXL virtual Logger & operator << (unsigned int value) = 0;

		/**
		 * Passes a message to the logger,
		 * and returns a reference to the logger.
		 */
		EXL virtual Logger & operator << (long value) = 0;

		/**
		 * Passes a message to the logger,
		 * and returns a reference to the logger.
		 */
		EXL virtual Logger & operator << (unsigned long value) = 0;

		/**
		 * Passes a message to the logger,
		 * and returns a reference to the logger.
		 */
		EXL virtual Logger & operator << (float value) = 0;

		/**
		 * Passes a message to the logger,
		 * and returns a reference to the logger.
		 */
		EXL virtual Logger & operator << (double value) = 0;

		/**
		 * Passes a message to the logger,
		 * and returns a reference to the logger.
		 */
		EXL virtual Logger & operator << (string value) = 0;

	};

	/**
	 * A Logger, which passes messages to
	 * the standard output.
	 * 
	 * @see sx::Logger
	 */
	class ConsoleLogger: public Logger {
	private:

		/**
		 * copy constructor, not used
		 */
		ConsoleLogger(const ConsoleLogger &);

		/**
		 * assignment operator, not used
		 */
		ConsoleLogger &operator = (const ConsoleLogger &);


	public:

		/**
		 * default constructor
		 */
		EXL ConsoleLogger();

		/**
		 * destructor
		 */
		EXL ~ConsoleLogger();

		/**
		 * @see sx::Logger::operator <<(const Level)
		 */
		EXL Logger &operator << (const Level level);

		/**
		 * The LogMarkup message is shown on the standard output
		 * followed by newline, and enclosed by the charactersequence ---.
		 * 
		 * @see sx::Logger::operator <<(const LogMarkup m)
		 */
		EXL Logger & operator << (const LogMarkup m);

		/**
		 * The Annotation message is shown on the standard output
		 * started with newline.
		 *
		 * @see sx::Logger::operator << (const Annotation)
		 */
		EXL Logger & operator << (const Annotation a);

		/**
		 * Passes the message to standard output.
		 * 
		 * @see sx::Logger::operator << (char value)
		 */
		EXL Logger & operator << (char value);

		/**
		 * Passes the message to standard output.
		 * 
		 * @see sx::Logger::operator << (unsigned char value)
		 */
		EXL Logger & operator << (unsigned char value);

		/**
		 * Passes the message to standard output.
		 * 
		 * @see sx::Logger::operator << (short value)
		 */
		EXL Logger & operator << (short value);

		/**
		 * Passes the message to standard output.
		 * 
		 * @see sx::Logger::operator << (unsigned short value)
		 */
		EXL Logger & operator << (unsigned short value);

		/**
		 * Passes the message to standard output.
		 * 
		 * @see sx::Logger::operator << (int value)
		 */
		EXL Logger & operator << (int value);

		/**
		 * Passes the message to standard output.
		 * 
		 * @see sx::Logger::operator << (unsigned int value)
		 */
		EXL Logger & operator << (unsigned int value);

		/**
		 * Passes the message to standard output.
		 * 
		 * @see sx::Logger::operator << (long value)
		 */
		EXL Logger & operator << (long value);

		/**
		 * Passes the message to standard output.
		 * 
		 * @see sx::Logger::operator << (unsigned long value)
		 */
		EXL Logger & operator << (unsigned long value);

		/**
		 * Passes the message to standard output.
		 * 
		 * @see sx::Logger::operator << (float value)
		 */
		EXL Logger & operator << (float value);

		/**
		 * Passes the message to standard output.
		 * 
		 * @see sx::Logger::operator << (double value)
		 */
		EXL Logger & operator << (double value);

		/**
		 * Passes the message to standard output.
		 * 
		 * @see sx::Logger::operator << (string value)
		 */
		EXL Logger & operator << (string value);
	};

	/**
	 * a Markup for the class ListLogger, which holds
	 * all the following messages posted to the log
	 *
	 * @see sx::LogMarkup
	 */
	class ListMarkup: public LogMarkup {
	private:

		/**
		 * list of char messages
		 */
		vector<char> chars;

		/**
		 * list of unsigned char messages
		 */
		vector<unsigned char> uchars;

		/**
		 * list of short messages
		 */
		vector<short> shorts;

		/**
		 * list of unsigned short messages
		 */
		vector<unsigned short> ushorts;

		/**
		 * list of int messages
		 */
		vector<int> ints;

		/**
		 * list of unsigned int messages
		 */
		vector<unsigned int> uints;

		/**
		 * list of long messages
		 */
		vector<long> longs;

		/**
		 * list of unsigned long messages
		 */
		vector<unsigned long> ulongs;

		/**
		 * list of float messages
		 */
		vector<float> floats;

		/**
		 * list of double messages
		 */
		vector<double> doubles;

		/**
		 * list of strings messages
		 */
		vector<string> strings;

		/**
		 * list of annotations
		 */
		vector<Annotation> annotations;

	public:

		/**
		 * constructor
		 *
		 * @see sx::LogMarkup::LogMarkup(string message)
		 */
		EXL ListMarkup(string message);

		/**
		 * copy constructor
		 */
		EXL ListMarkup(const ListMarkup &m);

		/**
		 * assignment operator
		 */
		EXL ListMarkup &operator = (const ListMarkup &m);

		/**
		 * destructor
		 */
		EXL ~ListMarkup();

		/**
		 * getter
		 */
		EXL vector<char> & getChars();

		/**
		 * getter
		 */
		EXL vector<unsigned char> & getUchars();

		/**
		 * getter
		 */
		EXL vector<short> & getShorts();

		/**
		 * getter
		 */
		EXL vector<unsigned short> & getUshorts();

		/**
		 * getter
		 */
		EXL vector<int> & getInts();

		/**
		 * getter
		 */
		EXL vector<unsigned int> & getUints();

		/**
		 * getter
		 */
		EXL vector<long> & getLongs();

		/**
		 * getter
		 */
		EXL vector<unsigned long> & getUlongs();

		/**
		 * getter
		 */
		EXL vector<float> & getFloats();

		/**
		 * getter
		 */
		EXL vector<double> & getDoubles();

		/**
		 * getter
		 */
		EXL vector<string> & getStrings();

		/** 
		 * getter
		 */
		EXL vector<Annotation> & getAnnotations();

	};

	/**
	 * A Logger, which passes messages to
	 * the list compatible with the type of
	 * the message.
	 * 
	 * @see sx::Logger
	 */
	class ListLogger: public Logger {
	private:

		/**
		 * The markup messages are posted to
		 * currently. Must always point to
		 * a valid markup in the map markups.
		 */
		ListMarkup *defaultMarkup;

		/**
		 * LogMarkups holding the log entries.
		 * Each markup is associated with its
		 * markup message. The map is associated
		 * with a markup with an empty message.
		 */
		map<string,ListMarkup> markups;

		/**
		 * copy constructor, not used
		 */
		ListLogger(const ListLogger &);

		/**
		 * assignment operator, not used
		 */
		ListLogger &operator = (const ListLogger &);

	public:

		/**
		 * default constructor
		 */
		EXL ListLogger();

		/**
		 * deconstructor
		 */
		EXL ~ListLogger();

		/**
		 * @see sx::Logger::operator <<(const Level)
		 */
		EXL Logger &operator << (const Level level);

		/**
		 * Appends a ListMarkup equivalent to m to the log,
		 * and uses it as the default markup, until a new
		 * default markup is set. Messages are always posted
		 * to the current default markup.
		 *
		 * @see sx::Logger::operator << (const LogMarkup)
		 */
		EXL Logger & operator << (const LogMarkup m);

		/**
		 * @see sx::Logger::operator << (const Annotation)
		 */
		EXL Logger & operator << (const Annotation a);

		/**
		 * Pushes value onto the back end of
		 * chars.
		 * 
		 * @see sx::Logger::operator << (char value)
		 */
		EXL Logger & operator << (char value);

		/**
		 * Pushes value onto the back end of
		 * uchars.
		 * 
		 * @see sx::Logger::operator << (unsigned char value)
		 */
		EXL Logger & operator << (unsigned char value);

		/**
		 * Pushes value onto the back end of
		 * shorts.
		 * 
		 * @see sx::Logger::operator << (short value)
		 */
		EXL Logger & operator << (short value);

		/**
		 * Pushes value onto the back end of
		 * ushorts.
		 * 
		 * @see sx::Logger::operator << (unsigned short value)
		 */
		EXL Logger & operator << (unsigned short value);

		/**
		 * Pushes value onto the back end of
		 * ints.
		 * 
		 * @see sx::Logger::operator << (int value)
		 */
		EXL Logger & operator << (int value);

		/**
		 * Pushes value onto the back end of
		 * uints.
		 * 
		 * @see sx::Logger::operator << (unsigned int value)
		 */
		EXL Logger & operator << (unsigned int value);

		/**
		 * Pushes value onto the back end of
		 * longs.
		 * 
		 * @see sx::Logger::operator << (long value)
		 */
		EXL Logger & operator << (long value);

		/**
		 * Pushes value onto the back end of
		 * ulongs.
		 * 
		 * @see sx::Logger::operator << (unsigned long value)
		 */
		EXL Logger & operator << (unsigned long value);

		/**
		 * Pushes value onto the back end of
		 * floats.
		 * 
		 * @see sx::Logger::operator << (float value)
		 */
		EXL Logger & operator << (float value);

		/**
		 * Pushes value onto the back end of
		 * doubles.
		 * 
		 * @see sx::Logger::operator << (double value)
		 */
		EXL Logger & operator << (double value);

		/**
		 * Pushes value onto the back end of
		 * strings.
		 * 
		 * @see sx::Logger::operator << (const string value)
		 */
		EXL Logger & operator << (string value);

		/**
		 * Returns a map of all markups, each
		 * associated with its markup message.
		 * Each markup is holding the messages
		 * passed to the markup.
		 *
		 * @return map of all markups
		 */
		EXL map<string,ListMarkup> & getMarkups();

		/**
		 * Searches the markup associated with ID, and returns the list entry
		 * on position index of the considered list in the markup. If the entry can't
		 * be extracted, an exception is thrown.
		 *
		 * @param ID identifyer of the markup
		 * @param index index of the entry in the coresponding list
		 * @return entry at position index the list of markup associated to ID
		 * @exception in the case of an error, an exception of type Exception is thrown
		 */
		EXL Annotation getAnnotationEntry(string ID, unsigned int index);

		/**
		 * Searches the markup associated with ID, and returns the list entry
		 * on position index of the considered list in the markup. If the entry can't
		 * be extracted, an exception is thrown.
		 *
		 * @param ID identifyer of the markup
		 * @param index index of the entry in the coresponding list
		 * @return entry at position index the list of markup associated to ID
		 * @exception in the case of an error, an exception of type Exception is thrown
		 */
		EXL char getCharEntry(string ID, unsigned int index);

		/**
		 * Searches the markup associated with ID, and returns the list entry
		 * on position index of the considered list in the markup. If the entry can't
		 * be extracted, an exception is thrown.
		 *
		 * @param ID identifyer of the markup
		 * @param index index of the entry in the coresponding list
		 * @return entry at position index the list of markup associated to ID
		 * @exception in the case of an error, an exception of type Exception is thrown
		 */
		EXL unsigned char getUcharEntry(string ID, unsigned int index);

		/**
		 * Searches the markup associated with ID, and returns the list entry
		 * on position index of the considered list in the markup. If the entry can't
		 * be extracted, an exception is thrown.
		 *
		 * @param ID identifyer of the markup
		 * @param index index of the entry in the coresponding list
		 * @return entry at position index the list of markup associated to ID
		 * @exception in the case of an error, an exception of type Exception is thrown
		 */
		EXL short getShortEntry(string ID, unsigned int index);

		/**
		 * Searches the markup associated with ID, and returns the list entry
		 * on position index of the considered list in the markup. If the entry can't
		 * be extracted, an exception is thrown.
		 *
		 * @param ID identifyer of the markup
		 * @param index index of the entry in the coresponding list
		 * @return entry at position index the list of markup associated to ID
		 * @exception in the case of an error, an exception of type Exception is thrown
		 */
		EXL unsigned short getUshortEntry(string ID, unsigned int index);

		/**
		 * Searches the markup associated with ID, and returns the list entry
		 * on position index of the considered list in the markup. If the entry can't
		 * be extracted, an exception is thrown.
		 *
		 * @param ID identifyer of the markup
		 * @param index index of the entry in the coresponding list
		 * @return entry at position index the list of markup associated to ID
		 * @exception in the case of an error, an exception of type Exception is thrown
		 */
		EXL int getIntEntry(string ID, unsigned int index);

		/**
		 * Searches the markup associated with ID, and returns the list entry
		 * on position index of the considered list in the markup. If the entry can't
		 * be extracted, an exception is thrown.
		 *
		 * @param ID identifyer of the markup
		 * @param index index of the entry in the coresponding list
		 * @return entry at position index the list of markup associated to ID
		 * @exception in the case of an error, an exception of type Exception is thrown
		 */
		EXL unsigned int getUintEntry(string ID, unsigned int index);

		/**
		 * Searches the markup associated with ID, and returns the list entry
		 * on position index of the considered list in the markup. If the entry can't
		 * be extracted, an exception is thrown.
		 *
		 * @param ID identifyer of the markup
		 * @param index index of the entry in the coresponding list
		 * @return entry at position index the list of markup associated to ID
		 * @exception in the case of an error, an exception of type Exception is thrown
		 */
		EXL long getLongEntry(string ID, unsigned int index);

		/**
		 * Searches the markup associated with ID, and returns the list entry
		 * on position index of the considered list in the markup. If the entry can't
		 * be extracted, an exception is thrown.
		 *
		 * @param ID identifyer of the markup
		 * @param index index of the entry in the coresponding list
		 * @return entry at position index the list of markup associated to ID
		 * @exception in the case of an error, an exception of type Exception is thrown
		 */
		EXL unsigned long getUlongEntry(string ID, unsigned int index);

		/**
		 * Searches the markup associated with ID, and returns the list entry
		 * on position index of the considered list in the markup. If the entry can't
		 * be extracted, an exception is thrown.
		 *
		 * @param ID identifyer of the markup
		 * @param index index of the entry in the coresponding list
		 * @return entry at position index the list of markup associated to ID
		 * @exception in the case of an error, an exception of type Exception is thrown
		 */
		EXL float getFloatEntry(string ID, unsigned int index);

		/**
		 * Searches the markup associated with ID, and returns the list entry
		 * on position index of the considered list in the markup. If the entry can't
		 * be extracted, an exception is thrown.
		 *
		 * @param ID identifyer of the markup
		 * @param index index of the entry in the coresponding list
		 * @return entry at position index the list of markup associated to ID
		 * @exception in the case of an error, an exception of type Exception is thrown
		 */
		EXL double getDoubleEntry(string ID, unsigned int index);

		/**
		 * Searches the markup associated with ID, and returns the list entry
		 * on position index of the considered list in the markup. If the entry can't
		 * be extracted, an exception is thrown.
		 *
		 * @param ID identifyer of the markup
		 * @param index index of the entry in the coresponding list
		 * @return entry at position index the list of markup associated to ID
		 * @exception in the case of an error, an exception of type Exception is thrown
		 */
		EXL string getStringEntry(string ID, unsigned int index);

	};

	/**
	 * Timemeasurement class
	 */
	class TimeMeasurement {
	private:

		/**
		 * Timepoint of last call of restart(). If restart()
		 * hasn't been called yet, startTime is the time of the
		 * creation of this class.
		 */
		std::chrono::time_point <std::chrono::system_clock, std::chrono::system_clock::duration> startTime;

	public:

		/**
		 * Constructor
		 */
		EXL TimeMeasurement();

		/**
		 * Copy constructor
		 */
		EXL TimeMeasurement(const TimeMeasurement &tm);

		/**
		 * Assignment operator
		 */
		EXL TimeMeasurement &operator = (const TimeMeasurement &tm);

		/**
		 * Deconstructor
		 */
		EXL ~TimeMeasurement();

		/**
		 * Overwrites startTime with the current point of time.
		 */
		EXL void restart();

		/**
		 * Returns the time elapsed since startTime.
		 *
		 * @return Time elapsed since startTime
		 */
		EXL double elapsed() const;

		/**
		 * Returns the startTime.
		 *
		 * @return startTime
		 */
		EXL std::chrono::time_point <std::chrono::system_clock, std::chrono::system_clock::duration> getStartTime() const;

		/**
		 * Converts timepoint t to string.
		 *
		 * @param t timepoint
		 * @return string representation of timepoint t
		 */
		EXL static string timepointToString(const std::chrono::time_point <std::chrono::system_clock, std::chrono::system_clock::duration> &t);
	
	};

	/**
	 * state of the file logger
	 */
	enum FileLoggerState {

		/**
		 * First table has not been started.
		 * If a message or annotation appears
		 * without a markup before, print
		 * a table, and immediately change
		 * to state COLLECT_ANNOTATIONS, if
		 * an annotation was posted, or to state
		 * COLLECT_MESSAGES, if a common message
		 * was posted.
		 */
		FIRST_TABLE,

		/**
		 * accumulate incoming annotations to
		 * a single annotation, until a common
		 * message appears, then change to state
		 * COLLECT_MESSAGES
		 */
		COLLECT_ANNOTATIONS,

		/**
		 * accumulate incoming common messages,
		 * until an annotation appears, then
		 * change to state COLLECT_ANNOTATIONS
		 */
		COLLECT_MESSAGES,

	};

	/**
	 * A Logger, which collects the messages,
	 * and stores the log in a file. In the
	 * case of an IO error
	 * of the logger, messages are simply
	 * ignored.
	 * 
	 * @see sx::Logger
	 */
	class FileLogger: public Logger {
	private:

		/**
		 * Filestream, to which messages are
		 * written to. If file does not work,
		 * the messages passed to the logger
		 * are ignored.
		 */
		ofstream file;

		/**
		 * true, if file did not work properly
		 */
		bool errorbit;

		/** 
		 * state of the logger, influencing
		 * the structure of the output
		 */
		FileLoggerState state;

		/**
		 * a timer
		 */
		TimeMeasurement timeMeasurement;

		/**
		 * internal table print functions
		 */
		string INTERNAL_TABLE_PRINT_FUNCTION();
		string INTERNAL_SEPARATE_ANNOTATION();
		string INTERNAL_FINISH_TABLE();

		/**
		 * copy constructor, not used
		 */
		FileLogger(const FileLogger &);

		/**
		 * assignment operator, not used
		 */
		FileLogger &operator = (const FileLogger &);

	public:

		/**
		 * Default constructor. Opens
		 * file at location filename for
		 * writing to it.
		 */
		EXL FileLogger(string filename);

		/**
		 * deconstructor
		 */
		EXL ~FileLogger();

		/**
		 * If file caused errors, true
		 * is returned.
		 */
		EXL bool hasErrors() const;

		/**
		 * Closes file. Messages passed to
		 * the logger after execution of close
		 * are ignored.
		 */
		EXL void close();

		/**
		 * @see sx::Logger::operator <<(const Level)
		 */
		EXL Logger &operator << (const Level level);

		/**
		 * The LogMarkup message is posted in the file
		 * followed by newline, and enclosed by the charactersequence ---.
		 *
		 * @see sx::Logger::operator << (const LogMarkup m)
		 */
		EXL Logger & operator << (const LogMarkup m);

		/**
		 * The LogMarkup message is posted in the file
		 * starting with newline.
		 *
		 * @see sx::Logger::operator << (const Annotation)
		 */
		EXL Logger & operator << (const Annotation a);

		/**
		 * Writes value to file.
		 * 
		 * @see sx::Logger::operator << (char value)
		 */
		EXL Logger & operator << (char value);

		/**
		 * Writes value to file.
		 * 
		 * @see sx::Logger::operator << (unsigned char value)
		 */
		EXL Logger & operator << (unsigned char value);

		/**
		 * Writes value to file.
		 * 
		 * @see sx::Logger::operator << (short value)
		 */
		EXL Logger & operator << (short value);

		/**
		 * Writes value to file.
		 * 
		 * @see sx::Logger::operator << (unsigned short value)
		 */
		EXL Logger & operator << (unsigned short value);

		/**
		 * Writes value to file.
		 * 
		 * @see sx::Logger::operator << (int value)
		 */
		EXL Logger & operator << (int value);

		/**
		 * Writes value to file.
		 * 
		 * @see sx::Logger::operator << (unsigned int value)
		 */
		EXL Logger & operator << (unsigned int value);

		/**
		 * Writes value to file.
		 * 
		 * @see sx::Logger::operator << (long value)
		 */
		EXL Logger & operator << (long value);

		/**
		 * Writes value to file.
		 * 
		 * @see sx::Logger::operator << (unsigned long value)
		 */
		EXL Logger & operator << (unsigned long value);

		/**
		 * Writes value to file.
		 * 
		 * @see sx::Logger::operator << (float value)
		 */
		EXL Logger & operator << (float value);

		/**
		 * Writes value to file.
		 * 
		 * @see sx::Logger::operator << (double value)
		 */
		EXL Logger & operator << (double value);

		/**
		 * Writes value to file.
		 * 
		 * @see sx::Logger::operator << (string value)
		 */
		EXL Logger & operator << (string value);
	};

}

/**
 * If _IS_LOGGING_ is defined, the macro SXout passes
 * messages to the default logger. Otherwise, SXout is
 * replaced by white space by the preprocessor to safe
 * ressources.
 */
#ifdef _IS_LOGGING_
	#define SXout(MSG) sx::Logger::get() << MSG;
#else
	#define SXout(MSG)
#endif

#endif
