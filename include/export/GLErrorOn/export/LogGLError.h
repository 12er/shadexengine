#ifndef _EXPORT_LOGGLERROR_H_
#define _EXPORT_LOGGLERROR_H_

/**
 * activate logging of gl error
 * (c) 2015 by Tristan Bauer
 */

/**
 * This define entails that
 * the whole framework outputs an extensive
 * log about the OpenGL state.
 * _LOG_GL_ERROR_ is required for the testcases
 * of the framework.
 * Add include/export/GLErrorOff to
 * the include path instead of include/export/LoggingOn for
 * not having _LOG_GL_ERROR_ defined in export/Export.h.
 */
#define _LOG_GL_ERROR_

#endif


