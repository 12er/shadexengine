#ifndef _EXPORT_EXPORT_H_
#define _EXPORT_EXPORT_H_

/**
 * Export Header
 * (c) 2012 by Tristan Bauer
 */
#include <platform/PlatformSpecific.h>
#include <export/ProjectSpecific.h>
#include <export/Logging.h>
#include <export/LogGLError.h>

	/**
	 * Default:
	 * No Log4SX export
	 */
	#define EXL
	/**
	 * Default:
	 * No SXParse export
	 */
	#define EXPA
	/**
	 * Default:
	 * No ShadeX Engine export
	 */
	#define EX
	/**
	 * Default:
	 * No ShadeX Widget export
	 */
	#define EXW

	#ifdef _PROJECT_LOG4SX_
		/**
		* Log4SX is a framework, and
		* its interfaces are exported with the
		* aid of EXL
		*/
		#ifdef _PLATFORM_WINDOWS_
			/**
			 * futurework: uncomment, and let
			 * the user decide via cmake, if the
			 * libraries should be static or dlls
			 */
			//#undef EXL
			//#define EXL __declspec(dllexport)
		#endif
	#endif

	#ifdef _PROJECT_SXPARSER_
		/**
		* SXParse is a framework, and
		* its interfaces are exported with the
		* aid of EXL
		*/
		#ifdef _PLATFORM_WINDOWS_
			/**
			* futurework: uncomment, and let
			* the user decide via cmake, if the
			* libraries should be static or dlls
			*/
			//#undef EXPA
			//#define EXPA __declspec(dllexport)
		#endif
	#endif

	#ifdef _PROJECT_SHADEXENGINE_
		/**
		* The ShadeX Engine is a framework, and
		* its interfaces are exported with the
		* aid of EX
		*/
		#ifdef _PLATFORM_WINDOWS_
			/**
			* futurework: uncomment, and let
			* the user decide via cmake, if the
			* libraries should be static or dlls
			*/
			//#undef EX
			//#define EX __declspec(dllexport)
		#endif
	#endif

	#ifdef _PROJECT_SHADEXENGINEWIDGET_
		/**
		* The ShadeXEngineWidget is a QWidget
		* making the use of the ShadeXEngine in
		* a Qt Application possible. Its interfaces
		* are exported with the aid of EXW
		*/
		#ifdef _PLATFORM_WINDOWS_
			/**
			* futurework: uncomment, and let
			* the user decide via cmake, if the
			* libraries should be static or dlls
			*/
			//#undef EXW
			//#define EXW __declspec(dllexport)
		#endif
	#endif

	#ifdef _PROJECT_SHADEXENGINEDEMO_
		/**
		 * The ShadeX Engine Demo does not
		 * require any exported interfaces
		 */
	#endif

	#ifdef _PROJECT_TESTSX_
		/**
		 * TestSX does not
		 * require any exported interfaces
		 */
	#endif

#endif
