#ifndef _EXPORT_PROJECTSPECIFIC_H_
#define _EXPORT_PROJECTSPECIFIC_H_

/**
 * project specific export settings
 * (c) 2012 by Tristan Bauer
 */

/**
 * This define entails that the current
 * project is shadeXEngineEditor
 */
#define _PROJECT_SHADEXENGINEEDITOR_

#endif