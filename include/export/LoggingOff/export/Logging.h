#ifndef _EXPORT_LOGGING_H_
#define _EXPORT_LOGGING_H_

/**
 * activate logging for 
 * (c) 2015 by Tristan Bauer
 */

/**
 * _IS_LOGGING_ is not defined, add include/export/LoggingOn to
 * the include path instead of include/export/LoggingOff for
 * having _IS_LOGGING_ defined in export/Export.h. If _IS_LOGGING_
 * is defined, SXout from sx/Log4SX.h outputs
 * the argument on the log.
 * _IS_LOGGING_ is required for the testcases
 * of the framework.
 */

#endif

