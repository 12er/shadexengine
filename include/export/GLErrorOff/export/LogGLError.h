#ifndef _EXPORT_LOGGLERROR_H_
#define _EXPORT_LOGGLERROR_H_

/**
 * deactivate logging of gl error
 * (c) 2015 by Tristan Bauer
 */

/**
 * _LOG_GL_ERROR_ is not defined, add include/export/GLErrorOn to
 * the include path instead of include/export/LoggingOff for
 * having _LOG_GL_ERROR_ defined in export/Export.h. If _LOG_GL_ERROR_
 * is defined, internal details in the engine about the OpenGL state
 * are logged.
 * _LOG_GL_ERROR_ is required for the testcases
 * of the framework.
 */

#endif


