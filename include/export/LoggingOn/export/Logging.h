#ifndef _EXPORT_LOGGING_H_
#define _EXPORT_LOGGING_H_

/**
 * activate logging for 
 * (c) 2015 by Tristan Bauer
 */

/**
 * This define entails that
 * the whole framework outputs an extensive
 * log about internal stuff. If _IS_LOGGING_
 * is defined, SXout from sx/Log4SX.h outputs
 * the argument on the log.
 * _IS_LOGGING_ is required for the testcases
 * of the framework.
 * Add include/export/LoggingOff to
 * the include path instead of include/export/LoggingOn for
 * having _IS_LOGGING_ not defined in export/Export.h
 */
#define _IS_LOGGING_

#endif

