#ifndef _INTERNAL_PARSERS_H_
#define _INTERNAL_PARSERS_H_

/**
 * Parser definitions
 * (c) 2015 by Tristan Bauer
 */

#include <vector>
#include <sstream>
using namespace std;

#define YY_NO_UNISTD_H

typedef struct VectorDouble {
	vector<double> v;
} VectorDouble;

typedef struct VectorInt {
	vector<int> v;
} VectorInt;

/**
 * String to double conversion for flex.
 * Strangely strtod and std::stod do not work
 * on Linux gcc/c++ after calling QApplication::QApplication(int, char**).
 * Hence this function is needed, which is returning the same results on
 * all systems.
 */
double stringToDouble(char *c);

/**
 * datastructures of pNumbers parser
 */

void resetPNumbersNo();
unsigned int getPNumbersLineno();
unsigned int getPNumbersColumnno();

/**
 * datastructures of parseSXdata
 */

typedef struct _SStream_ {
	stringstream str;
public:
	void appendChar(char c);
} _SStream_;

void resetSXdataNo();
unsigned int getSXdataLineno();
unsigned int getSXdataColumnno();

/**
 * types of _XNODE_
 */
enum _XTYPE_ {
	_XTEXT_,
	_XTAG_,
	_S_ATTRIB_,
	_R_ATTRIB_
};

/**
 * datastructures for parseSXdata
 */
struct _XNODE_ {

	/**
	 * type of node
	 */
	int type;

	/**
	 * line and column of the node
	 */
	unsigned int lineno;
	unsigned int columnno;

	/**
	 * data of XText
	 */
	string text;

	/**
	 * data of XTag
	 */
	string name;
	vector<_XNODE_ *> nodes;

	/**
	 * data of string attribute
	 */
	string strID;
	string strAttrib;

	/**
	 * data of real attribute
	 */
	string rID;
	double rAttrib;

public:
	_XNODE_();
	void newText(const string &text, unsigned int lineno, unsigned int columnno);
	void newTag(const string &name, unsigned int lineno, unsigned int columnno);
	void newStrAttrib(const string &ID, const string &value, unsigned int lineno, unsigned int columnno);
	void newRealAttrib(const string &ID, double value, unsigned int lineno, unsigned int columnno);
	~_XNODE_();

};

/**
* datastructures for parseXMLdata
*/

void resetXMLdataNo();
unsigned int getXMLdataLineno();
unsigned int getXMLdataColumnno();

/**
* structure for boost spirit
* parsetree datastructure
*/
struct _XMLNODE_ {

	/**
	* type of node
	*/
	int type;

	/**
	* line and column of the node
	*/
	unsigned int lineno;
	unsigned int columnno;

	/**
	* data of XText
	*/
	stringstream text;

	/**
	* data of XTag
	*/
	string name;
	string endname;
	vector<_XMLNODE_ *> nodes;
	vector<_XMLNODE_ *> ats;

	/**
	* data of string attribute
	*/
	string strID;
	stringstream strAttrib;

	bool deconstructChildren;
public:
	_XMLNODE_();
	void newText(unsigned int lineno, unsigned int columnno);
	void newTag(const string &lname, const string &rname, unsigned int lineno, unsigned int columnno);
	void newStrAttrib(const string &ID, unsigned int lineno, unsigned int columnno);
	void addText(char text);
	void addAttrib(char text);
	~_XMLNODE_();
};

/**
 * datastructures for parsePLYdata
 */

void resetPLYdataNo();
unsigned int getPLYdataLineno();
unsigned int getPLYdataColumnno();

struct _PROPERTY_;

typedef struct VectorProperty{
	vector<_PROPERTY_> v;
} VectorProperty;

/**
 * data types of the properties
 */
enum _TYPE_ {
	T_CHAR,
	T_UCHAR,
	T_SHORT,
	T_USHORT,
	T_INT,
	T_UINT,
	T_LONG,
	T_ULONG,
	T_FLOAT,
	T_DOUBLE
};

/**
 * determines, if only a number or
 * a list of number is assigned to a property
 */
enum _MULTIPLICITY_ {
	M_SINGLE,
	M_LIST
};

/**
 * property
 */
struct _PROPERTY_ {

	/**
	 * property identifyer
	 */
	string name;

	/**
	 * M_SINGLE, if each entry
	 * in data should just have one
	 * value of the property, M_LIST,
	 * if each entry in data can have
	 * a list of the property
	 */
	int multiplicity;

	/**
	 * data type of the property
	 */
	vector<int> types;

};

/**
 * Specifies the number of data entries
 * assigned to this _ELEMENT_, and the
 * property identifyers of the numbers
 * in the data entry.
 */
struct _ELEMENT_ {

	/**
	 * element name
	 */
	string name;

	/**
	 * number of entries in data
	 * assigned to this _ELEMENT_
	 */
	unsigned int count;

	/**
	 * properties
	 */
	vector<_PROPERTY_> properties;

};

/**
 * structure of PLY mesh
 */
struct _XMESH_ {

	/**
	 * each element holds properties
	 * and number of lines of data
	 * assigned to the element
	 */
	vector<_ELEMENT_> elements;

	/**
	 * Each entry of data represents
	 * an instance of an element, and all the entries
	 * are sorted by the element they are assigned to.
	 * Each element's count equals the number of entries
	 * in data assigned to the element. Each entry
	 * is a tuple of values of the properties of the element.
	 */
	vector<vector<double> > data;

};

#endif
