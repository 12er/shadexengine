#ifndef _TEST_SXENGINE_H_
#define _TEST_SXENGINE_H_

/**
 * ShadeXEngine test cases
 * (c) 2012 by Tristan Bauer
 */

#include <export/Export.h>
#include <string>
#include <QApplication>
using namespace std;

namespace sxengine {

	void testMath(const string &logname);
	void testShader(const string &logname);
	void testBufferedMesh(const string &logname);
	void testTexture(const string &logname);
	void testUniforms(const string &logname);
	void testTransformFeedback(const string &logname);
	void testInstancing(const string &logname);
	void testRenderTarget(const string &logname);
	void testVolume(const string &logname);
	void testRenderObject(const string &logname);
	void testPass(const string &logname);
	void testEffect(const string &logname);
	void testShadeX(const string &logname);

}

#endif
