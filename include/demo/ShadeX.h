#ifndef _DEMO_SHADEX_H_
#define _DEMO_SHADEX_H_

/**
 * ShadeX Engine Demo
 * (c) 2012 by Tristan Bauer
 */
#include <export/Export.h>
#include <sx/SX.h>

namespace sx {

	class SX {
	public:
		static ShadeX shadeX;
	};

}

#endif